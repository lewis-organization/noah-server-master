package org.noah.function;

@FunctionalInterface
public interface BranchHandle {

    /**
     * 分支操作
     * @author Liuxu
     * @date 2024/1/23 11:30
     * @param trueHandle true执行
     * @param falseHandle false执行
     */
    void trueOrFalseHandle(Runnable trueHandle, Runnable falseHandle);
}
