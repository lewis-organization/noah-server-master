package org.noah.function;

/**
 * 异常接口
 */
@FunctionalInterface
public interface ThrowExceptionFunction {

    /**
     * 抛出异常信息
     * @author Liuxu
     * @date 2024/1/23 11:31
     * @param message 提示信息
     */

    void throwException(String message);
}
