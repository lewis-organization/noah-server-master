package org.noah.utils;

import org.noah.config.exception.BusinessException;
import org.noah.function.BranchHandle;
import org.noah.function.ThrowExceptionFunction;

public class VUtil {

    /**
     *  如果参数为true抛出异常
     *
     * @param b
     * @return ThrowExceptionFunction
     **/
    public static ThrowExceptionFunction isTure(boolean b) {
        return (errorMessage) -> {
            if (b){
                throw new BusinessException(errorMessage);
            }
        };
    }

    /**
     * 参数为true或false时，分别进行不同的操作
     *
     * @param b boolean条件
     * @return BranchHandle
     **/
    public static BranchHandle isTureOrFalse(boolean b) {
        return (trueHandle, falseHandle) -> {
            if (b) {
                trueHandle.run();
            } else {
                falseHandle.run();
            }
        };
    }
}
