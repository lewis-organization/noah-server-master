package org.noah.utils;

import cn.hutool.core.io.FileUtil;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Position;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

@Slf4j
public class ImageUtil {

    private static final Integer BYTE = 1024;

    /**
     * 指定大小进行缩放 若图片横比width小，高比height小，不变 若图片横比width小，高比height大，高缩小到height，图片比例不变
     * 若图片横比width大，高比height小，横缩小到width，图片比例不变
     * 若图片横比width大，高比height大，图片按比例缩小，横为width或高为height
     * @author Liuxu
     * @date 2024/1/23 11:32
     * @param source 输入源
     * @param output 输出源
     * @param width 宽
     * @param height 高
     * @param quality 比例
     */
    public static void imgThumb(String source, String output, int width,
                                int height, float quality) throws IOException {
        Thumbnails.of(source).size(width, height).outputQuality(quality).toFile(output);
    }

    public static OutputStream imgThumbOutputStream(String source, int width,
                                                    int height, float quality) throws IOException {

        OutputStream os = new ByteArrayOutputStream();
        Thumbnails.of(source).size(width, height).outputQuality(quality).toOutputStream(os);
        return os;
    }

    /**
     * 指定大小进行缩放
     *
     * @param source 输入源
     * @param output 输出源
     * @param width  宽
     * @param height 高
     */
    public static void imgThumb(File source, String output, int width,
                                int height, float quality) throws IOException {
        Thumbnails.of(source).size(width, height).outputQuality(quality).toFile(output);
    }

    /**
     * 按照比例进行缩放
     *
     * @param source 输入源
     * @param output 输出源
     */
    public static void imgScale(String source, String output, double scale)
            throws IOException {
        Thumbnails.of(source).scale(scale).toFile(output);
    }

    public static void imgScale(File source, String output, double scale)
            throws IOException {
        Thumbnails.of(source).scale(scale).toFile(output);
    }

    public static OutputStream imgScaleOutputStream(File source, double scale)
            throws IOException {
        OutputStream os = new ByteArrayOutputStream();
        Thumbnails.of(source).scale(scale)
                .toOutputStream(os);
        return os;
    }

    public static byte[] imgScaleByte(byte[] source, double scale)
            throws IOException {

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        InputStream input = new ByteArrayInputStream(source);
        Thumbnails.of(input).scale(scale)
                .toOutputStream(os);
        return os.toByteArray();
    }

    /**
     * 不按照比例，指定大小进行缩放
     *
     * @param source          输入源
     * @param output          输出源
     * @param width           宽
     * @param height          高
     * @param keepAspectRatio 默认是按照比例缩放的,值为false 时不按比例缩放
     */
    public static void imgNoScale(String source, String output, int width,
                                  int height, boolean keepAspectRatio) throws IOException {
        Thumbnails.of(source).size(width, height)
                .keepAspectRatio(keepAspectRatio)
                .toFile(output);
    }

    public static void imgNoScale(File source, String output, int width,
                                  int height, boolean keepAspectRatio) throws IOException {
        Thumbnails.of(source).size(width, height)
                .keepAspectRatio(keepAspectRatio)
                .toFile(output);
    }

    /**
     * 旋转 ,正数：顺时针 负数：逆时针
     *
     * @param source 输入源
     * @param output 输出源
     * @param width  宽
     * @param height 高
     * @param rotate 角度
     */
    public static void imgRotate(String source, String output, int width,
                                 int height, double rotate) throws IOException {
        Thumbnails.of(source).size(width, height).rotate(rotate).toFile(output);
    }

    public static void imgRotate(File source, String output, int width,
                                 int height, double rotate) throws IOException {
        Thumbnails.of(source).size(width, height).rotate(rotate).toFile(output);
    }

    /**
     * 水印
     *
     * @param source       输入源
     * @param output       输入源
     * @param width        宽
     * @param height       高
     * @param position     水印位置 Positions.BOTTOM_RIGHT o.5f
     * @param watermark    水印图片地址
     * @param transparency 透明度 0.5f
     * @param quality      图片质量 0.8f
     */
    public static void imgWatermark(String source, String output, int width,
                                    int height, Position position, String watermark,
                                    float transparency, float quality) throws IOException {
        Thumbnails
                .of(source)
                .size(width, height)
                .watermark(position, ImageIO.read(new File(watermark)),
                        transparency).outputQuality(0.8f)
                .toFile(output);
    }

    public static void imgWatermark(File source, String output, int width,
                                    int height, Position position, String watermark,
                                    float transparency, float quality) throws IOException {
        Thumbnails
                .of(source)
                .size(width, height)
                .watermark(position, ImageIO.read(new File(watermark)),
                        transparency).outputQuality(0.8f)
                .toFile(output);
    }

    /**
     * 裁剪图片
     *
     * @param source          输入源
     * @param output          输出源
     * @param position        裁剪位置
     * @param x               裁剪区域x
     * @param y               裁剪区域y
     * @param width           宽
     * @param height          高
     * @param keepAspectRatio 默认是按照比例缩放的,值为false 时不按比例缩放
     */
    public static void imgSourceRegion(String source, String output,
                                       Position position, int x, int y, int width, int height,
                                       boolean keepAspectRatio) throws IOException {
        Thumbnails.of(source).sourceRegion(position, x, y).size(width, height)
                .keepAspectRatio(keepAspectRatio)
                .toFile(output);
    }

    public static void imgSourceRegion(File source, String output,
                                       Position position, int x, int y, int width, int height,
                                       boolean keepAspectRatio) throws IOException {
        Thumbnails.of(source).sourceRegion(position, x, y).size(width, height)
                .keepAspectRatio(keepAspectRatio)
                .toFile(output);
    }

    /**
     * 按坐标裁剪
     *
     * @param source          输入源
     * @param output          输出源
     * @param x               起始x坐标
     * @param y               起始y坐标
     * @param x1              结束x坐标
     * @param y1              结束y坐标
     * @param width           宽
     * @param height          高
     * @param keepAspectRatio 默认是按照比例缩放的,值为false 时不按比例缩放
     */
    public static void imgSourceRegion(String source, String output, int x,
                                       int y, int x1, int y1, int width, int height,
                                       boolean keepAspectRatio) throws IOException {
        Thumbnails.of(source).sourceRegion(x, y, x1, y1).size(width, height)
                .keepAspectRatio(keepAspectRatio).toFile(output);
    }

    public static void imgSourceRegion(File source, String output, int x,
                                       int y, int x1, int y1, int width, int height,
                                       boolean keepAspectRatio) throws IOException {
        Thumbnails.of(source).sourceRegion(x, y, x1, y1).size(width, height)
                .keepAspectRatio(keepAspectRatio)
                .toFile(output);
    }

    /**
     * 转化图像格式
     *
     * @param source 输入源
     * @param output 输出源
     * @param width  宽
     * @param height 高
     * @param format 图片类型，gif、png、jpg
     */
    public static void imgFormat(String source, String output, int width,
                                 int height, String format) throws IOException {
        Thumbnails.of(source).size(width, height).outputFormat(format)
                .toFile(output);
    }

    public static void imgFormat(File source, String output, int width,
                                 int height, String format) throws IOException {
        Thumbnails.of(source).size(width, height).outputFormat(format).toFile(output);
    }

    /**
     * 输出到OutputStream
     *
     * @param source 输入源
     * @param output 输出源
     * @param width  宽
     * @param height 高
     * @return toOutputStream(流对象)
     */
    public static OutputStream imgOutputStream(String source, String output,
                                               int width, int height) throws IOException {
        OutputStream os = Files.newOutputStream(Paths.get(output));
        Thumbnails.of(source).size(width, height)
                .toOutputStream(os);
        return os;
    }

    public static OutputStream imgOutputStream(File source, String output,
                                               int width, int height) throws IOException {
        OutputStream os = Files.newOutputStream(Paths.get(output));
        Thumbnails.of(source).size(width, height)
                .toOutputStream(os);
        return os;
    }

    /**
     * 输出到BufferedImage
     *
     * @param source 输入源
     * @param output 输出源
     * @param width  宽
     * @param height 高
     * @param format 图片类型，gif、png、jpg
     * @return BufferedImage
     */
    public static BufferedImage imgBufferedImage(String source, String output,
                                                 int width, int height, String format) throws IOException {
        BufferedImage buf = Thumbnails.of(source).size(width, height).asBufferedImage();
        ImageIO.write(buf, format, new File(output));
        return buf;
    }

    public static BufferedImage imgBufferedImage(File source, String output,
                                                 int width, int height, String format) throws IOException {
        BufferedImage buf = Thumbnails.of(source).size(width, height).asBufferedImage();
        ImageIO.write(buf, format, new File(output));
        return buf;
    }

    /**
     * 图片压缩处理
     *
     * @param inputStream 文件流
     * @param fileSize    压缩到几兆以内(单位kb)
     * @param fileName    文件名称
     * @param output      指定输出目录（压缩处理需要的目录，默认临时文件目录）
     * @return
     */
    public static InputStream imgProcess(InputStream inputStream, Integer fileSize,
                                         String fileName, String output) throws IOException {
        String fullFilePath = output + "/" + fileName;
        File file = FileUtil.writeFromStream(inputStream, fullFilePath);
        log.info("上传图片大小: {}", file.length() / BYTE);
        if (file.length() / BYTE < fileSize) {
            log.info("不进行压缩!");
        } else {
            //压缩图片 循环压缩，质量递减一半
            float size = 0.5f;
            do {
                imgScale(file, fullFilePath, size);
                file = new File(fullFilePath);
                log.info("压缩图片大小: {}", file.length() / BYTE);
                size = size / 2;
            } while (file.length() / BYTE > fileSize);
        }
        return FileUtil.getInputStream(file);
    }

    /**
     * 图片压缩处理(默认临时文件夹输出)
     * @author Liuxu
     * @date 2024/1/23 11:35
     * @param inputStream 文件流
     * @param fileSize    压缩到几兆以内(单位kb)
     * @param fileName    文件名称
     * @return java.io.InputStream
     */
    public static InputStream imgProcess(InputStream inputStream, Integer fileSize,
                                         String fileName) throws IOException {
        String output = FileUtil.getTmpDirPath();
        String fullFilePath = output + "/" + fileName;
        File file = FileUtil.writeFromStream(inputStream, fullFilePath);
        log.info("上传图片大小: {}", file.length() / BYTE);
        if (file.length() / BYTE < fileSize) {
            log.info("不进行压缩!");
            return inputStream;
        }
        //压缩图片 循环压缩，质量递减一半
        float size = 0.5f;
        do {
            imgScale(file, fullFilePath, size);
            file = new File(fullFilePath);
            log.info("压缩图片大小: {}", file.length() / BYTE);
            size = size / 2;
        } while (file.length() / BYTE > fileSize);
        return FileUtil.getInputStream(file);
    }

    public static InputStream imgProcess(InputStream inputStream,
                                         String fileName) throws IOException {
        String output = FileUtil.getTmpDirPath();
        String fullFilePath = output + "/" + fileName;
        File file = FileUtil.writeFromStream(inputStream, fullFilePath);
        log.info("上传图片大小: {}", file.length() / BYTE);
        //压缩图片 循环压缩，质量递减一半
        float size = 0.5f;
        imgScale(file, fullFilePath, size);
        file = new File(fullFilePath);
        log.info("压缩图片大小: {}", file.length() / BYTE);
        return FileUtil.getInputStream(file);
    }

    public static void main(String[] args) {
        File file = new File("d:/attach/11.7.jpg");
        //图片压缩
        try {
            File newFile = FileUtil.writeFromStream(ImageUtil.imgProcess(FileUtil.getInputStream(file), 1024, "aaa.jpg"), "D:/dev/bbb.jpg");
        } catch (IOException e) {
            log.error("", e);
        }
    }
}
