package org.noah.utils;


import cn.hutool.core.codec.Base64;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Base64Utils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@Slf4j
public class AesUtil {

    private static final String KEY_ALGORITHM = "AES";
    //默认的加密算法
    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";
    //自定义密码
    private static final String ASSETS_DEV_PWD_FIELD = "12345678abcdefgh";

    public static String getAssetsDevPwdField() {
        return ASSETS_DEV_PWD_FIELD;
    }

    /**
     * AES 加密操作
     *
     * @param content  待加密内容
     * @param password 加密密码
     * @return 返回Base64转码后的加密数据
     */
    public static String encrypt(String content, String password) {
        try {
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);// 创建密码器

            byte[] byteContent = content.getBytes(StandardCharsets.UTF_8);

            cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(password));// 初始化为加密模式的密码器

            byte[] result = cipher.doFinal(byteContent);// 加密

            return Base64.encode(result);//通过Base64转码返回
        } catch (Exception ex) {
            log.error("AES加密错误", ex);
        }
        return null;
    }

    /**
     * AES 解密操作
     * @author Liuxu
     * @date 2024/1/23 11:31
     * @param content 内容
     * @param password 密码
     * @return java.lang.String
     */
    public static String decrypt(String content, String password) {

        try {
            //实例化
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);

            //使用密钥初始化，设置为解密模式
            cipher.init(Cipher.DECRYPT_MODE, getSecretKey(password));

            //执行操作
            byte[] result = cipher.doFinal(Base64Utils.decodeFromString(content));
            return new String(result, StandardCharsets.UTF_8);
        } catch (Exception ex) {
            log.error("AES解密错误", ex);
        }
        return null;
    }

    /**
     * 生成加密秘钥
     * @author Liuxu
     * @date 2024/1/23 11:31
     * @param password 密码
     * @return javax.crypto.spec.SecretKeySpec
     */
    private static SecretKeySpec getSecretKey(String password) {
        //返回生成指定算法密钥生成器的 KeyGenerator 对象
        KeyGenerator kg = null;
        try {
            kg = KeyGenerator.getInstance(KEY_ALGORITHM);
            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
            random.setSeed(password.getBytes());
            //AES 要求密钥长度为 128
            kg.init(128, random);
            //生成一个密钥
            SecretKey secretKey = kg.generateKey();
            return new SecretKeySpec(secretKey.getEncoded(), KEY_ALGORITHM);// 转换为AES专用密钥
        } catch (NoSuchAlgorithmException ex) {
            log.error("生成AES加密秘钥错误", ex);
        }
        return null;
    }

    public static void main(String[] args) {
        String origin = "my test string";
        String encrypt = AesUtil.encrypt(origin, AesUtil.ASSETS_DEV_PWD_FIELD);
        String decrypt = AesUtil.decrypt(encrypt, AesUtil.ASSETS_DEV_PWD_FIELD);
        System.out.println(origin);
        System.out.println(encrypt);
        System.out.println(decrypt);
    }
}
