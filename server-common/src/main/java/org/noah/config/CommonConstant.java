package org.noah.config;

/**
 * @author liuxu
 * @date 2021/10/19-14:44
 */
public class CommonConstant {

    public static final String DATABASE_MYSQL = "mysql";

    public static final String PERMS = "perms[{0}]";

    public static final String PASSWORD = "fe40a4414023e51aa50b8218bd70f39f";
    public static final String SALT = "QgTIF";

    public static final String AUTHORIZATION = "token";
    public static final String REFERENCED_SESSION_ID_SOURCE = "Stateless request";
    public static final long ONE_HOUR = 3600000L;
    public static final long ONE_MINUTE = 60000L;
    public static final String JIN = "#";

    public static final String ICON_PREFIX = "layui-icon ";

    public static final String ZERO = "0";

    public static final Long LONG_ZERO = 0L;

    public static final String ONE = "1";

    public static final String EMPTY = "";
}
