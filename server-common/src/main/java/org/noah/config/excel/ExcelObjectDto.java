package org.noah.config.excel;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class ExcelObjectDto implements Serializable {
}
