package org.noah.config.excel;

import cn.hutool.core.date.DatePattern;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 测试导出模板
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Accessors(chain = true)
@ContentRowHeight(45)
@HeadRowHeight(25)
public class TestExcelDto extends ExcelObjectDto {

    @ExcelProperty(value = "ID", index = 0)
    private Long id;

    @ExcelProperty(value = "设备名称", index = 1)
    private String name;

    @ExcelProperty(value = "使用状况", index = 2)
    private String useType;

    @ExcelProperty(value = "设备数量", index = 3)
    private Integer amount;

    @ExcelProperty(value = "使用部门", index = 4)
    private String useDepartment;

    @ExcelProperty(value = "使用人", index = 5)
    private String usePeople;

    @ExcelProperty(value = "资产来源", index = 6)
    private String resource;

    @ExcelProperty(value = "取得日期", index = 7)
    @DateTimeFormat(value = DatePattern.NORM_DATE_PATTERN)
    private Date getDate;

    @ExcelProperty(value = "经手人", index = 8)
    private String postUser;

    @ExcelProperty(value = "经费来源", index = 9)
    private String foundSource;

    @ExcelProperty(value = "使用方向", index = 10)
    private String direction;

    @ExcelProperty(value = "发票号", index = 11)
    private String bill;

    @ExcelProperty(value = "价值类型", index = 12)
    private String valueType;

    @ExcelProperty(value = "价值", index = 13)
    private BigDecimal valueCost;

    @ExcelProperty(value = "账面原值", index = 14)
    private BigDecimal originalBookValue;

    @ExcelProperty(value = "出厂日期", index = 15)
    @DateTimeFormat(value = DatePattern.NORM_DATE_PATTERN)
    private Date factoryDate;

    @ExcelProperty(value = "预期使用年限", index = 16)
    private Float expectedLife;

    @ExcelProperty(value = "累计折旧", index = 17)
    private BigDecimal sumDeprec;

    @ExcelProperty(value = "折旧总次数", index = 18)
    private String depreciationTotalTimes;

    @ExcelProperty(value = "账面净值", index = 19)
    private BigDecimal netBookValue;

    @ExcelProperty(value = "凭证号", index = 20)
    private String voucherNumber;

    @ExcelProperty(value = "入账日期", index = 21)
    @DateTimeFormat(value = DatePattern.NORM_DATE_PATTERN)
    private Date dateDue;

    @ExcelProperty(value = "采购组织形式", index = 22)
    private String purchaseForm;

    @ExcelProperty(value = "规格型号", index = 23)
    private String modles;

    @ExcelProperty(value = "设备号", index = 24)
    private String equipmentNumber;

    @ExcelProperty(value = "存放点", index = 25)
    private String storage;

    @ExcelProperty(value = "上次保养日期", index = 26)
    @DateTimeFormat(value = DatePattern.NORM_DATE_PATTERN)
    private Date lastMaintainDate;

    @ExcelProperty(value = "保养期限(年)", index = 27)
    private Integer timeLag;

    @ExcelProperty(value = "保养内容", index = 28)
    private String maintainMessage;

    @ExcelProperty(value = "备注", index = 29)
    private String remark;

    @ExcelProperty(value = {"品牌"}, index = 30)
    private String blending;

    @ExcelProperty(value = {"设备产地"}, index = 31)
    private String regions;

    @ExcelProperty(value = {"进口价值(元)"}, index = 32)
    private String importValue;

    @ExcelProperty(value = {"采购人"}, index = 33)
    private String buyer;

    @ExcelProperty(value = {"生产厂家"}, index = 34)
    private String factory;

    @ExcelProperty(value = {"设备识别号"}, index = 35)
    private String factoryNumber;

    @ExcelProperty(value = {"销售商"}, index = 36)
    private String retailer;

    @ExcelProperty(value = {"合同编号"}, index = 37)
    private String contract;

    @ExcelProperty(value = {"保修截止日期"}, index = 38)
    @DateTimeFormat(value = DatePattern.NORM_DATE_PATTERN)
    private String warrantyExpriationDate;

    @ExcelProperty(value = {"单据号"}, index = 39)
    private String receipts;

    @ExcelProperty(value = {"资料"}, index = 40)
    private String datum;

    @ExcelProperty(value = {"档案号"}, index = 41)
    private String fn;

    @ExcelProperty(value = {"功率"}, index = 42)
    private String power;

    @ExcelProperty(value = {"出租/出借对方单位"}, index = 43)
    private String rentout;
}
