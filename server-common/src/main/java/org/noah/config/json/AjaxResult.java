package org.noah.config.json;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.map.MapUtil;
import enumc.AjaxReturnCodeEnum;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @author liuxu
 * @date 2021/10/20-9:39
 */
@Data
public class AjaxResult implements Serializable {

    private static final long serialVersionUID = -757662388799378920L;

    private Boolean success;

    private Integer code;

    private Object data;

    private String msg;

    public AjaxResult() {
    }

    public AjaxResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static AjaxResult success(Object data) {
        AjaxResult ajaxResult = success();
        ajaxResult.setData(data);
        return ajaxResult;
    }

    public static AjaxResult success(String msg) {
        AjaxResult ajaxResult = success();
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }

    public static AjaxResult success() {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setSuccess(true);
        ajaxResult.setCode(AjaxReturnCodeEnum.AJAX_SUCCESS.getCode());
        return ajaxResult;
    }

    public static AjaxResult fail(String msg) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setSuccess(false);
        ajaxResult.setCode(AjaxReturnCodeEnum.AJAX_FAIL.getCode());
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }

    public static AjaxResult fail() {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setSuccess(false);
        ajaxResult.setCode(AjaxReturnCodeEnum.AJAX_FAIL.getCode());
        ajaxResult.setMsg(AjaxReturnCodeEnum.AJAX_FAIL.getMsg());
        return ajaxResult;
    }

    public static AjaxResult fail(Integer code, String msg) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setSuccess(false);
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }

    public static AjaxResult success(Object data, String msg) {
        AjaxResult ajaxResult = success(data);
        ajaxResult.setMsg(msg);
        return ajaxResult;
    }

    public AjaxResult put(String key, Object value) {
        if (null == data) {
            this.data = MapUtil.newHashMap(true);
        }
        Map<String, Object> data = Convert.toMap(String.class, Object.class, this.data) ;
        data.put(key, value);
        this.data = data;
        return this;
    }
}
