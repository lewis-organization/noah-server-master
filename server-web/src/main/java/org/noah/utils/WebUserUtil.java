package org.noah.utils;

import cn.hutool.core.collection.CollectionUtil;
import enumc.RedisCacheKeyEnum;
import enumc.UserRoleEnum;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.noah.modules.sys.user.entity.WebUser;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author liuxu
 * @date 2021/11/1-15:52
 */
public class WebUserUtil {

    public static WebUser getWebUser() {
        Subject subject = SecurityUtils.getSubject();
        return (WebUser) subject.getPrincipal();
    }

    public static boolean isAdmin() {
        List<String> roleCodeList = SpringContextUtil.getBean(RedisUtil.class)
                .lGet(RedisCacheKeyEnum.USER_ROLES_CACHE.getKey() +
                        getWebUser().getId(), 0, -1)
                .stream().map(String::valueOf).collect(Collectors.toList());
        List<String> adminRoleCodeList = new ArrayList<>();
        adminRoleCodeList.add(UserRoleEnum.ADMIN.getType());
        return CollectionUtil.containsAny(roleCodeList, adminRoleCodeList);
    }
}
