package org.noah.config.systemInfo;

import org.hyperic.sigar.Sigar;

/**
 * @author liuxu
 * @date 2022/4/8 14:03
 **/
public class SigarInstance {

    private static class SigarHolder {
        private static final Sigar INSTANCE = new Sigar();
    }

    private SigarInstance() {
    }

    public static Sigar getInstance() {
        return SigarHolder.INSTANCE;
    }
}
