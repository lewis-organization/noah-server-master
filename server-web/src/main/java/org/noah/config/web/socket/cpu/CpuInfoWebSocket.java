package org.noah.config.web.socket.cpu;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.hyperic.sigar.CpuInfo;
import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.Sigar;
import org.noah.config.systemInfo.SigarInstance;
import org.springframework.stereotype.Component;

import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author liuxu
 * @date 2022/4/8 13:49
 **/
@Slf4j
@ServerEndpoint(value = "/cpu/getInfo")
@Component
public class CpuInfoWebSocket {

    private static final List<Queue<String>> queueList = new ArrayList<>();
    //时间队列
    private static final Queue<String> dateQueue = new LinkedList<>();

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        Map<String, Object> resultMap = new HashMap<>();
        dateQueue.offer(DateUtil.format(DateUtil.date(), "HH:mm:ss"));
        int queueSize = 21;
        if (dateQueue.size() == queueSize) {
            dateQueue.poll();
        }
        //名称
        List<String> nameList = new ArrayList<>();
        //数据
        List<Map<String, Object>> resultList = new ArrayList<>();
        Sigar sigar = SigarInstance.getInstance();
        try {
            CpuInfo[] infos = sigar.getCpuInfoList();
            CpuPerc[] cpuList = sigar.getCpuPercList();
            // 不管是单块CPU还是多CPU都适用
            for (int i = 0; i < infos.length; i++) {
                Map<String, Object> map = new HashMap<>();
                if (queueList.size() < infos.length) {
                    Queue<String> queue = new ArrayBlockingQueue<>(queueSize);
                    queueList.add(queue);
                }
                String name = "第" + (i + 1) + "块CPU占用";
                nameList.add(name);
                map.put("name", name);
                map.put("type", "line");
                CpuPerc cpuPerc = cpuList[i];
                if (queueList.get(i).size() == queueSize) {
                    //当队列满了，移除第一个
                    queueList.get(i).poll();
                }
                queueList.get(i).offer(format(cpuPerc.getCombined()));
                map.put("data", queueList.get(i).toArray());
                resultList.add(map);
            }
            resultMap.put("nameList", nameList);
            resultMap.put("time", dateQueue.toArray());
            resultMap.put("data", resultList);
            this.sendMessage(JSON.toJSONString(resultMap), session);
        } catch (Exception e) {
            log.error("", e);
        }
    }

    private String format(double val) {
        String p = String.valueOf(val * 100.0D);
        int ix = p.indexOf(".") + 1;
        return p.substring(0, ix) + p.charAt(ix);
    }

    /**
     * 服务端发送消息给客户端
     */
    private void sendMessage(String message, Session toSession) {
        try {
            toSession.getBasicRemote().sendText(message);
        } catch (Exception e) {
            log.error("服务端发送CPU信息给客户端失败：{0}", e);
        }
    }
}
