package org.noah.config.web.socket.ram;

import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;
import org.hyperic.sigar.Swap;
import org.noah.config.systemInfo.SigarInstance;
import org.springframework.stereotype.Component;

import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liuxu
 * @date 2022/4/8 13:49
 **/
@Slf4j
@ServerEndpoint(value = "/ram/getInfo")
@Component
public class RamInfoWebSocket {

    private static final Map<String, Object> resultMap = new HashMap<>();

    static {
        resultMap.put("total", 0);
        resultMap.put("used", 0);
        resultMap.put("free", 0);
        resultMap.put("exchange", 0);
        resultMap.put("exchangeUsed", 0);
        resultMap.put("exchangeFree", 0);
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        Sigar sigar = SigarInstance.getInstance();
        try {
            Mem mem = sigar.getMem();
            resultMap.put("total", mem.getTotal() / 1024L);
            resultMap.put("used", mem.getUsed() / 1024L);
            resultMap.put("free", mem.getFree() / 1024L);
            Swap swap = sigar.getSwap();
            resultMap.put("exchange", swap.getTotal() / 1024L);
            resultMap.put("exchangeUsed", swap.getUsed() / 1024L);
            resultMap.put("exchangeFree", swap.getFree() / 1024L);
        } catch (SigarException e) {
            log.error("", e);
        }
        this.sendMessage(JSON.toJSONString(resultMap), session);
    }

    /**
     * 服务端发送消息给客户端
     */
    private void sendMessage(String message, Session toSession) {
        try {
            toSession.getBasicRemote().sendText(message);
        } catch (Exception e) {
            log.error("服务端发送内存信息给客户端失败：{0}", e);
        }
    }
}
