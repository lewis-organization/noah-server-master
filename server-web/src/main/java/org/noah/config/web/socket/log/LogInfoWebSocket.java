package org.noah.config.web.socket.log;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListener;
import org.apache.commons.io.input.TailerListenerAdapter;
import org.noah.config.CommonConstant;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

/**
 * @author liuxu
 * @date 2022/4/8 13:49
 **/
@Slf4j
@ServerEndpoint(value = "/log/getInfo/{logPath}")
@Component
public class LogInfoWebSocket {

    //文件内容监听器
    private static TailerListener listener;
    private static Tailer tailer;

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(@PathParam("logPath") String logPath, Session session) throws IOException {
        listener = new TailerListenerAdapter() {
            @Override
            public void handle(String line) {
                try {
                    Map<String, Object> dataMap = MapUtil.newHashMap();
                    dataMap.put(CommonConstant.ZERO, line);
                    sendMessage(JSON.toJSONString(dataMap), session);
                } catch (Exception ex) {
                    log.error("存储队列日志发生异常：" + ex.getMessage());
                }
            }
        };
        long sleepInterval = 1000L;
        tailer = new Tailer(new File(Base64.decodeStr(logPath)), listener, sleepInterval, true);
        tailer.run();
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() throws IOException {
        if (Objects.nonNull(tailer)) {
            tailer.stop();
        }
        if (Objects.nonNull(listener)) {
            listener = null;
        }
    }

    /**
     * 收到消息调用的方法
     * @author Liuxu
     * @date 2024/1/23 11:14
     * @param message 消息
     * @param session 会话
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        Map<String, Object> dataMap = MapUtil.newHashMap();
        dataMap.put(CommonConstant.ONE, message);
        sendMessage(message, session);
    }

    /**
     * 服务端发送消息给客户端
     */
    private void sendMessage(String message, Session toSession) {
        try {
            synchronized (toSession) {
                toSession.getBasicRemote().sendText(message);
            }
        } catch (Exception e) {
            log.error("服务端发送日志信息给客户端失败：", e);
        }
    }
}
