package org.noah.config.web.filter;

import enumc.RedisCacheKeyEnum;
import enumc.SystemErrorCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;
import org.noah.config.json.AjaxResult;
import org.noah.config.shiro.cache.ShiroRedisCacheManager;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.utils.RequestUtil;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Deque;
import java.util.LinkedList;


@Data
@EqualsAndHashCode(callSuper = false)
public class SessionControlFilter extends AccessControlFilter {

    //踢出之前登录的/之后登录的用户 默认踢出之前登录的用户
    private boolean kickoutAfter = false;
    //同一个帐号最大会话数 默认1
    private int maxSession = 1;

    private SessionManager sessionManager;

    private ShiroRedisCacheManager redisCacheManager;

    private Cache<String, Deque<Serializable>> cache;

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
        Subject subject = this.getSubject(servletRequest, servletResponse);
        if (!subject.isAuthenticated() && !subject.isRemembered()) {
            //如果没有登录，直接进行之后的流程
            return true;
        }
        Session session = subject.getSession();
        WebUser webUser = (WebUser) subject.getPrincipal();
        Serializable sessionId = session.getId();

        //读取缓存   没有就存入
        cache = redisCacheManager.getCache(RedisCacheKeyEnum.WEB_KICKOUT_CACHE.getKey());
        Deque<Serializable> deque = cache.get(RedisCacheKeyEnum.WEB_KICKOUT_CACHE.getKey() + webUser.getUsername());

        //如果此用户没有session队列，也就是还没有登录过，缓存中没有
        //就new一个空队列，不然deque对象为空，会报空指针
        if (deque == null) {
            deque = new LinkedList<>();
        }

        //如果队列里没有此sessionId，且用户没有被踢出；放入队列
        if (!deque.contains(sessionId) && session.getAttribute("kickout") == null) {
            //将sessionId存入队列
            deque.push(sessionId);
            //将用户的sessionId队列缓存
            cache.put(RedisCacheKeyEnum.WEB_KICKOUT_CACHE.getKey() + webUser.getUsername(), deque);
        }

        //如果队列里的sessionId数超出最大会话数，开始踢人
        while (deque.size() > maxSession) {
            Serializable kickoutSessionId;
            if (kickoutAfter) { //如果踢出后者
                kickoutSessionId = deque.removeFirst();
                //踢出后再更新下缓存队列
            } else { //否则踢出前者
                kickoutSessionId = deque.removeLast();
                //踢出后再更新下缓存队列
            }
            cache.put(RedisCacheKeyEnum.WEB_KICKOUT_CACHE.getKey() + webUser.getUsername(), deque);

            try {
                //获取被踢出的sessionId的session对象
                Session kickoutSession = sessionManager.getSession(new DefaultSessionKey(kickoutSessionId));
                if (kickoutSession != null) {
                    //设置会话的kickout属性表示踢出了
                    kickoutSession.setAttribute("kickout", true);
                }
            } catch (Exception e) {//ignore exception

            }
        }

        //如果被踢出了，直接退出，重定向到踢出后的地址
        if (session.getAttribute("kickout") != null) {
            //会话被踢出了
            try {
                //退出登录
                subject.logout();
            } catch (Exception e) { //ignore
            }
            //如果是ajax请求
            if (RequestUtil.isAjaxRequest((HttpServletRequest) servletRequest)) {
                //输出json串
                this.out(servletResponse, AjaxResult.fail(SystemErrorCodeEnum.KICKOUT_CODE.getCode(), SystemErrorCodeEnum.KICKOUT_CODE.getMsg()));
            } else {
                WebUtils.issueRedirect(servletRequest, servletResponse, "/kickout");
            }
            return false;
        }
        return true;
    }

    private void out(ServletResponse response, Object info) {
        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json;charset=utf-8");
            PrintWriter out = response.getWriter();
            out.println(info);
            out.flush();
            out.close();
        } catch (Exception e) {
            System.err.println("SessionControlFilter.class 输出JSON异常，可以忽略。");
        }
    }
}
