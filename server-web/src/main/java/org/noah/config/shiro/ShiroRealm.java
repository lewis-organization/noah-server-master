package org.noah.config.shiro;

import enumc.RedisCacheKeyEnum;
import enumc.TrueOrFalseEnum;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.noah.modules.sys.resources.entity.Resources;
import org.noah.modules.sys.role.service.RoleService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.modules.sys.user.service.WebUserService;
import org.noah.utils.RedisUtil;
import org.noah.utils.SpringContextUtil;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 自定义授权认证
 * @author liuxu
 * @date 2024-01-23 11:07
 */
public class ShiroRealm extends AuthorizingRealm {

    @DubboReference(version = "1.0.0")
    private WebUserService webUserService;
    @DubboReference
    private RoleService roleService;

    /**
     * 用户授权
     * @author Liuxu
     * @date 2024/1/23 11:08
     * @param principals principals
     * @return org.apache.shiro.authz.AuthorizationInfo
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        // 添加角色和权限
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        // 获取登录用户
        WebUser webUser = (WebUser) principals.getPrimaryPrincipal();
        // 添加角色
        List<String> roleCodeList = roleService.getRolesCodeByUserId(webUser.getId());
        simpleAuthorizationInfo.addRoles(roleCodeList);
        //缓存角色
        RedisUtil redisUtil = SpringContextUtil.getBean(RedisUtil.class);
        redisUtil.delete(RedisCacheKeyEnum.USER_ROLES_CACHE.getKey() + webUser.getId());
        for (String roleCode : roleCodeList) {
            redisUtil.lSet(RedisCacheKeyEnum.USER_ROLES_CACHE.getKey() + webUser.getId(), roleCode);
        }
        // 添加权限
        simpleAuthorizationInfo.addStringPermissions(redisUtil.lGet(RedisCacheKeyEnum.USER_RESOURCES_CACHE.getKey()
                .concat(String.valueOf(webUser.getId())), 0, -1)
            .stream().map(r -> {
                    Resources resources = (Resources) r;
                    return resources.getCode();
                }).collect(Collectors.toList()));
        return simpleAuthorizationInfo;
    }

    /**
     * 用户认证
     * @author Liuxu
     * @date 2024/1/23 11:09
     * @param authenticationToken token
     * @return org.apache.shiro.authc.AuthenticationInfo
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 这里验证authenticationToken和simpleAuthenticationInfo的信息
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        WebUser webUser = webUserService.getWebUserByUsername(token.getUsername());
        //账户不存在
        if (null == webUser) {
            throw new UnknownAccountException();
        }
        //账户不可用
        if (TrueOrFalseEnum.FALSE.getValue().equals(webUser.getEnable())) {
            throw new DisabledAccountException();
        }
        return new SimpleAuthenticationInfo(
                webUser, webUser.getPassword(), ByteSource.Util.bytes(webUser.getSalt()), this.getName());
    }

    @Override
    protected Object getAuthorizationCacheKey(PrincipalCollection principals) {
        WebUser webUser = (WebUser) principals.getPrimaryPrincipal();
        return RedisCacheKeyEnum.WEB_PERM_CACHE.getKey() + webUser.getUsername();
    }
}
