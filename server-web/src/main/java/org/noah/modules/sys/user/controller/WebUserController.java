package org.noah.modules.sys.user.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import enumc.TrueOrFalseEnum;
import enumc.UserTypeEnum;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.session.Session;
import org.apache.shiro.util.ByteSource;
import org.noah.config.CommonConstant;
import org.noah.config.json.AjaxResult;
import org.noah.config.shiro.session.RedisSessionDao;
import org.noah.dto.WebUserChangePasswordDto;
import org.noah.dto.WebUserDto;
import org.noah.dto.WebUserQueryDto;
import org.noah.mapstruct.webUser.WebUserStructMapper;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.org.entity.Org;
import org.noah.modules.sys.org.service.OrgService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.modules.sys.user.service.WebUserService;
import org.noah.utils.WebUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2021-11-01
 */
@Controller
@RequestMapping("/webUser")
@RequiredArgsConstructor
public class WebUserController extends BaseController {

    @DubboReference(version = "1.0.0")
    private WebUserService webUserService;
    private final RedisSessionDao redisSessionDao;
    @DubboReference
    private OrgService orgService;

    @GetMapping(value = "/list")
    public String list() {
        return "sys/user/list";
    }

    @PostMapping(value = "/getPage")
    @ResponseBody
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, WebUserQueryDto webUserQueryDto) {
        Page<Map<String, Object>> mapPage = webUserService.getPage(page, webUserQueryDto);
        //获取所有在线用户
        Collection<Session> sessions = redisSessionDao.getActiveSessions();
        Org org;
        for (Map<String, Object> map : mapPage.getRecords()) {
            map.put("typeDes", UserTypeEnum.getValueByKey(String.valueOf(map.get("type"))));
            map.put("enableDes", TrueOrFalseEnum.getValueByKey(String.valueOf(map.get("enable"))));
            //判断在线状态
            map.put("login", sessions.stream().anyMatch(s -> {
                if (null != s.getAttribute("user")) {
                    WebUser webUser = (WebUser) s.getAttribute("user");
                    return String.valueOf(map.get("id")).equals(webUser.getId().toString());
                }
                return false;
            }) ? "<span class=\"layui-green\">在线</span>" : "<span class=\"layui-deeppink\">离线</span>");
            //归属组织名称
            map.put("orgName", CommonConstant.EMPTY);
            org = orgService.getById(String.valueOf(map.get("orgId")));
            if (Objects.nonNull(org)) {
                map.put("orgName", org.getTitle());
            }
        }
        return mapPage;
    }

    @GetMapping(value = "/add/{orgId}")
    public String add(@PathVariable Integer orgId) {
        Org org = orgService.getById(orgId);
        request.setAttribute("org", org);
        return "sys/user/add";
    }

    @PostMapping(value = "/save")
    @ResponseBody
    public AjaxResult save(@RequestBody WebUserDto webUserDto) {
        //设置密码
        String salt = webUserService.getRandomStr();
        ByteSource bytes = ByteSource.Util.bytes(salt);
        String password = new SimpleHash("MD5", webUserDto.getPassword(), bytes, 2).toString();
        webUserDto.setPassword(password);
        webUserDto.setSalt(salt);
        webUserService.saveEntity(webUserDto);
        return AjaxResult.success();
    }

    @GetMapping(value = "/edit/{id}/{orgId}")
    public String edit(@PathVariable String id, @PathVariable Integer orgId) {
        WebUser webUser = webUserService.getById(id);
        request.setAttribute("model", webUser);
        Org org = new Org();
        if (!CommonConstant.ZERO.equals(orgId.toString())) {
            org = orgService.getById(orgId);
        }
        request.setAttribute("org", org);
        return "sys/user/edit";
    }

    @PostMapping(value = "/update")
    @ResponseBody
    public AjaxResult update(@RequestBody WebUserDto webUserDto) {
        webUserService.updateEntity(webUserDto);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable Integer id) {
        webUserService.removeById(id);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/batchRemove/{ids}")
    @ResponseBody
    public AjaxResult batchRemove(@PathVariable String ids) {
        webUserService.removeByIds(StrUtil.split(ids, StrUtil.COMMA)
                .stream().map(Integer::new).collect(Collectors.toList()));
        return AjaxResult.success();
    }

    /**
     * 更改状态
     * @author Liuxu
     * @date 2024/1/23 10:58
     * @param id id
     * @param state 状态
     * @return org.noah.config.json.AjaxResult
     */
    @PostMapping(value = "/changeState/{id}/{state}")
    @ResponseBody
    @RequiresPermissions("webUser:changeState")
    public AjaxResult changeState(@PathVariable String id, @PathVariable String state) {
        webUserService.changeState(id, state);
        return AjaxResult.success();
    }

    /**
     * 更改密码
     * @author Liuxu
     * @date 2024/1/23 10:59
     * @param webUserChangePasswordDto dto
     * @return org.noah.config.json.AjaxResult
     */
    @PostMapping(value = "/changePassword")
    @ResponseBody
    public AjaxResult changePassword(@RequestBody WebUserChangePasswordDto webUserChangePasswordDto) {
        //校验原始密码
        WebUser webUser = webUserService.getById(webUserChangePasswordDto.getId());
        ByteSource bytes = ByteSource.Util.bytes(webUser.getSalt());
        String password = new SimpleHash("MD5", webUserChangePasswordDto.getOldPassword(), bytes, 2).toString();
        if (!webUser.getPassword().equalsIgnoreCase(password)) {
            return AjaxResult.fail("原始密码不正确");
        }
        password = new SimpleHash("MD5", webUserChangePasswordDto.getPassword(), bytes, 2).toString();
        webUserChangePasswordDto.setPassword(password);
        webUserService.changePassword(webUserChangePasswordDto);
        return AjaxResult.success();
    }

    /**
     * 根据组织湖区用户列表
     * @author Liuxu
     * @date 2024/1/23 10:59
     * @param orgId 组织id
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/getByOrgId/{orgId}")
    @ResponseBody
    public AjaxResult getByOrgId(@PathVariable String orgId) {
        return AjaxResult.success(webUserService.getByOrgId(orgId));
    }

    /**
     * 获取用户组织树
     * @author Liuxu
     * @date 2024/1/23 10:59
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/getOrgAndUserTree")
    @ResponseBody
    public AjaxResult getOrgAndUserTree() {
        WebUser webUser = WebUserUtil.getWebUser();
        WebUserDto webUserDto = WebUserStructMapper.INSTANCE.toWebUserDto(webUser);
        webUserDto.setAdmin(WebUserUtil.isAdmin());
        return AjaxResult.success(webUserService.getOrgAndUserTree(webUserDto).get("list"));
    }
}

