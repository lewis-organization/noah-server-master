package org.noah.modules.sys.approve.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.dto.ApproveRecordsQueryDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.approve.entity.ApproveRecords;
import org.noah.modules.sys.approve.service.ApproveRecordsService;
import org.noah.utils.WebUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Liuxu
 * @since 2024-05-31
 */
@Controller
@RequestMapping("/approveRecords")
public class ApproveRecordsController extends BaseController {

    @DubboReference
    private ApproveRecordsService approveRecordsService;

    /**
     * 处理请求以显示审批记录列表页面。
     * @author liuxu
     * @date 2024/6/3 10:21
     * @return String 返回审批记录列表页面的视图名称。
     */
    @GetMapping(value = "/list")
    public String list() {
        return "sys/approveRecords/list";
    }

    /**
     * 根据查询条件获取审批记录的分页结果。
     * 本方法通过调用approveRecordsService的getCustomPage方法，实现根据特定查询条件对审批记录进行分页查询。
     * @author liuxu
     * @date 2024/6/3 09:26
     * @param page 分页对象，包含当前页码、每页大小等信息，同时用于存放查询结果。
     * @param approveRecordsQueryDto 查询条件对象，封装了所有可用于筛选审批记录的条件。
     * @return Page<Map < String, Object>> 返回查询后的分页结果，包含符合条件的审批记录集合及分页信息。
     */
    @PostMapping(value = "/getPage")
    @ResponseBody
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, ApproveRecordsQueryDto approveRecordsQueryDto) {
        approveRecordsQueryDto.setUserId(WebUserUtil.getWebUser().getId());
        return approveRecordsService.getCustomPage(page, approveRecordsQueryDto);
    }

    /**
     * 处理审批记录的批准操作。
     * 该方法接收多个请求参数，用于指定待批准的审批记录ID、业务类型、业务ID、批准的业务ID和批准人员ID。
     * 它将这些参数存储在请求对象中，以便后续处理。
     * 返回值指定了视图的路径，用于展示批准操作的结果或引导用户到下一个操作步骤。
     *
     * @param approveRecordsIds 待批准的审批记录ID字符串，多个ID使用逗号分隔。
     * @return 指定的视图路径，用于展示批准结果或引导下一步操作。
     */
    @GetMapping(value = "/approve")
    public String approve(@RequestParam(value = "approveRecordsIds") String approveRecordsIds) {
        // 将请求参数存储在请求对象中，以供后续处理使用
        request.setAttribute("approveRecordsIds", approveRecordsIds);

        // 返回指定的视图路径
        return "sys/approveRecords/approve";
    }

    /**
     * 处理审批操作。
     * 通过接收来自前端的审批记录对象，调用服务层方法执行审批逻辑。
     *
     * @param approveRecords 包含审批相关信息的实体类，由前端通过POST请求体发送。
     * @return 返回一个表示操作结果的AjaxResult对象，成功时包含成功标识。
     */
    @PostMapping(value = "/doApprove")
    @ResponseBody
    public AjaxResult doApprove(@RequestBody ApproveRecords approveRecords) {
        // 调用审批服务，执行具体的审批操作
        approveRecords.setWebUserId(WebUserUtil.getWebUser().getId());
        approveRecordsService.doApprove(approveRecords);
        // 返回操作成功的结果给前端
        return AjaxResult.success();
    }

    /**
     * 审核流程查看
     * @author liuxu
     * @date 2024/6/6 14:53
     * @param id 审核记录ID
     * @return String
     */
    @GetMapping(value = "/viewFlow/{id}")
    public String viewFlow(@PathVariable Integer id) {
        request.setAttribute("id", id);
        return "sys/approveRecords/viewFlow";
    }

    /**
     * 获取流程图
     * @author liuxu
     * @date 2024/6/7 09:36
     * @param id 审核记录ID
     * @return AjaxResult
     */
    @GetMapping(value = "/getApproveFlowInfo/{id}")
    @ResponseBody
    public AjaxResult getApproveFlowInfo(@PathVariable Integer id) {
        return AjaxResult.success(approveRecordsService.getApproveFlowInfo(id));
    }
}
