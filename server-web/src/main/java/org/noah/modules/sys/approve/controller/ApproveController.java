package org.noah.modules.sys.approve.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.dto.ApproveDto;
import org.noah.dto.ApproveQueryDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.approve.service.ApproveService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
@Controller
@RequestMapping("/approve")
public class ApproveController extends BaseController {

    @DubboReference
    private ApproveService approveService;

    /**
     * 首页
     * @author Liuxu
     * @date 2024/1/23 10:44
     * @return java.lang.String
     */
    @GetMapping(value = "/list")
    public String list() {
        return "sys/approve/list";
    }

    /**
     * 分页查询
     * @author liuxu
     * @date 2024/5/22 17:23
     * @param page 分页参数
     * @param approveQueryDto 查询参数
     * @return Page<Map < String, Object>>
     */
    @PostMapping(value = "/getPage")
    @ResponseBody
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, ApproveQueryDto approveQueryDto) {
        return approveService.getPage(page, approveQueryDto);
    }

    /**
     * 新增页面
     * @author Liuxu
     * @date 2024/1/23 10:45
     * @return java.lang.String
     */
    @GetMapping(value = "/add")
    public String add() {
        return "sys/approve/add";
    }

    /**
     * 保存
     * @author Liuxu
     * @date 2024/1/23 14:57
     * @param approveDto 保存dto
     * @return org.noah.config.json.AjaxResult
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public AjaxResult save(@RequestBody ApproveDto approveDto) {
        approveService.saveObj(approveDto);
        return AjaxResult.success();
    }

    /**
     * 编辑页面
     * @author Liuxu
     * @date 2024/1/24 14:24
     * @param id id
     * @return java.lang.String
     */
    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable Integer id) {
        request.setAttribute("model", approveService.getById(id));
        return "sys/approve/edit";
    }

    /**
     * 修改
     * @author Liuxu
     * @date 2024/1/24 17:11
     * @param approveDto 保存dto
     * @return org.noah.config.json.AjaxResult
     */
    @PostMapping(value = "/update")
    @ResponseBody
    public AjaxResult update(@RequestBody ApproveDto approveDto) {
        approveService.updateObj(approveDto);
        return AjaxResult.success();
    }

    /**
     * 删除
     * @author Liuxu
     * @date 2024/1/25 15:53
     * @param id 审核id
     * @return org.noah.config.json.AjaxResult
     */
    @DeleteMapping(value = "/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable Integer id) {
        approveService.removeObj(id);
        return AjaxResult.success();
    }

    /**
     * 批量删除
     * @author Liuxu
     * @date 2024/1/25 15:55
     * @param ids 审核id
     * @return org.noah.config.json.AjaxResult
     */
    @DeleteMapping(value = "/batchRemove/{ids}")
    @ResponseBody
    public AjaxResult batchRemove(@PathVariable String ids) {
        approveService.removeByIds(StrUtil.split(ids, StrUtil.COMMA)
                .stream().map(Integer::new).collect(Collectors.toList()));
        return AjaxResult.success();
    }
}
