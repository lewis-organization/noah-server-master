package org.noah.modules.sys.approve.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.dto.ApproveBusinessQueryDto;
import org.noah.modules.sys.approve.entity.ApproveBusiness;
import org.noah.modules.sys.approve.service.ApproveBusinessService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
@Controller
@RequestMapping("/approveBusiness")
public class ApproveBusinessController {

    @DubboReference
    private ApproveBusinessService approveBusinessService;

    /**
     * 审核业务列表
     * @author liuxu
     * @date 2024/5/28 15:01
     * @return String
     */
    @GetMapping(value = "/list")
    public String list() {
        return "sys/approveBusiness/list";
    }

    /**
     * 列表分页查询
     * @author liuxu
     * @date 2024/5/28 15:04
     * @param page 分页参数
     * @param approveBusinessQueryDto 查询参数
     * @return Page<Map < String, Object>>
     */
    @PostMapping(value = "/getPage")
    @ResponseBody
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, ApproveBusinessQueryDto approveBusinessQueryDto) {
        return approveBusinessService.getPage(page, approveBusinessQueryDto);
    }

    /**
     * 完成审批业务的处理方法。
     * 通过接收来自前端的审批业务对象，调用服务层方法完成审批流程。
     *
     * @param approveBusiness 包含审批业务信息的对象，通过@RequestBody注解绑定请求体中的数据。
     * @return 返回一个表示操作结果的AjaxResult对象，成功时包含成功标识。
     */
    @PostMapping(value = "/finish")
    @ResponseBody
    public AjaxResult finish(@RequestBody ApproveBusiness approveBusiness) {
        // 调用服务层方法，完成审批业务的处理
        approveBusinessService.finish(approveBusiness);
        // 返回操作成功的AjaxResult对象
        return AjaxResult.success();
    }
}
