package org.noah.modules.sys.message.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.noah.config.json.AjaxResult;
import org.noah.dto.WebMessageQueryDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.message.entity.WebMessage;
import org.noah.modules.sys.message.service.WebMessageService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.modules.sys.user.service.WebUserService;
import org.noah.utils.WebUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2022-01-19
 */
@Controller
@RequestMapping("/webMessage")
public class WebMessageController extends BaseController {

    @DubboReference
    private WebMessageService webMessageService;
    @DubboReference(version = "1.0.0")
    private WebUserService webUserService;

    @GetMapping(value = "/list")
    public String list() {
        return "sys/message/list";
    }

    @PostMapping(value = "/getPage")
    @ResponseBody
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, WebMessageQueryDto webMessageQueryDto) {
        webMessageQueryDto.setAdmin(WebUserUtil.isAdmin());
        webMessageQueryDto.setUserId(WebUserUtil.getWebUser().getId());
        return webMessageService.getPage(page, webMessageQueryDto);
    }

    @GetMapping(value = "/add")
    public String add() {
        return "sys/message/add";
    }

    @PostMapping(value = "/save")
    @ResponseBody
    public AjaxResult save(@RequestBody WebMessage webMessage) {
        WebUser webUser = webUserService.getById(WebUserUtil.getWebUser().getId());
        webMessage.setFromUser(webUser.getNickName());
        webMessageService.saveEntity(webMessage);
        return AjaxResult.success();
    }

    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable Integer id) {
        WebMessage webMessage = webMessageService.getById(id);
        request.setAttribute("model", webMessage);
        return "sys/message/edit";
    }

    @PutMapping(value = "/update")
    @ResponseBody
    public AjaxResult update(@RequestBody WebMessage webMessage) {
        webMessageService.updateEntity(webMessage);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable Integer id) {
        webMessageService.removeById(id);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/batchRemove/{ids}")
    @ResponseBody
    public AjaxResult batchRemove(@PathVariable String ids) {
        webMessageService.removeByIds(StrUtil.split(ids, StrUtil.COMMA)
                .stream().map(Integer::new).collect(Collectors.toList()));
        return AjaxResult.success();
    }

    /**
     * 更改状态
     *
     * @param id 消息id
     * @param state 消息状态
     * @return 变更结果
     */
    @PostMapping(value = "/changeState/{id}/{state}")
    @ResponseBody
    @RequiresPermissions("webMessage:changeState")
    public AjaxResult changeState(@PathVariable String id, @PathVariable String state) {
        webMessageService.changeState(id, state);
        return AjaxResult.success();
    }

    /**
     * 全标已读
     * @return 操作结果
     */
    @PostMapping(value = "/allRead")
    @ResponseBody
    public AjaxResult allRead() {
        webMessageService.allRead(WebUserUtil.getWebUser().getId());
        return AjaxResult.success();
    }
}

