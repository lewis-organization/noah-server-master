package org.noah.modules.sys.attach.controller;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import enumc.LogTypeEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.annotation.SysLog;
import org.noah.config.json.AjaxResult;
import org.noah.config.upload.MultipartConfig;
import org.noah.dto.SaveAttachDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.attach.entity.Attach;
import org.noah.modules.sys.attach.entity.AttachConfig;
import org.noah.modules.sys.attach.service.AttachConfigService;
import org.noah.modules.sys.attach.service.AttachService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.modules.sys.user.service.WebUserService;
import org.noah.utils.ImageUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Controller
@RequestMapping(value = "/attach")
@RequiredArgsConstructor
public class AttachController extends BaseController {

    @DubboReference
    private AttachService attachService;
    @DubboReference(version = "1.0.0")
    private WebUserService webUserService;
    @DubboReference
    private AttachConfigService attachConfigService;
    private final MultipartConfig multipartConfig;

    @GetMapping(value = "/list")
    public String list() {
        WebUser webUser = webUserService.getById(1);
        request.setAttribute("imgIds", webUser.getImgIds());
        return "sys/attach/list";
    }

    /**
     * 文件上传
     *
     * @param file 文件
     * @return 上传结果
     */
    @PostMapping(value = "/upload/{route}")
    @ResponseBody
    public AjaxResult upload(@RequestPart(value = "file") MultipartFile file, @PathVariable String route) {
        AttachConfig attachConfig = attachConfigService.getOne(null);
        if (Objects.isNull(attachConfig)) {
            return AjaxResult.fail("上传配置不明确");
        }
        multipartConfig.setSize(attachConfig.getFileSize());
        multipartConfig.setPath(attachConfig.getSavePath());
        multipartConfig.setType(".*(" + attachConfig.getSupportType().replaceAll(StrUtil.COMMA, "|") + ").*");
        multipartConfig.setCompressTypes(".*(" + attachConfig.getCompressType().replaceAll(StrUtil.COMMA, "|") + ").*");
        if (!multipartConfig.validateSuffix(file)) {
            return AjaxResult.fail("上传文件格式不支持");
        }
        if (multipartConfig.validateSize(file)) {
            return AjaxResult.fail("上传文件大小超出系统限制[" + multipartConfig.getSize() / 1024 + "]M");
        }
        Map<String, Object> resultMap;
        try {
            InputStream inputStream = file.getInputStream();
            //指定类型的文件才进行压缩
            if (multipartConfig.compressSuffix(file)) {
                inputStream = ImageUtil.imgProcess(file.getInputStream(), file.getOriginalFilename());
            }
            resultMap = attachService.upload(IoUtil.readBytes(inputStream), file.getOriginalFilename(), file.getSize(),
                    multipartConfig.getPath() + "/" + route);
            resultMap.put("file_url", request.getContextPath() + "/attach/" + route + "/" + resultMap.get("file_id"));
        } catch (Exception ex) {
            return AjaxResult.fail("上传错误");
        }
        return AjaxResult.success(resultMap);
    }

    /**
     * 图片显示
     *
     * @param id 图片id
     * @param response response
     */
    @GetMapping(value = "/{route}/{id}")
    public void show(@PathVariable String id, @PathVariable String route, HttpServletResponse response) {
        if (StringUtils.isBlank(id)) {
            return;
        }
        Attach attach = attachService.getAttachById(id);
        if (null == attach) {
            return;
        }
        AttachConfig attachConfig = attachConfigService.getOne(null);
        multipartConfig.setPath(attachConfig.getSavePath());
        multipartConfig.setCompressTypes(".*(" + attachConfig.getPreviewType().replaceAll(StrUtil.COMMA, "|") + ").*");
        renderImage(response, multipartConfig, attach, route);
    }

    @PostMapping(value = "/save")
    @ResponseBody
    @SysLog(type = LogTypeEnum.COMMON)
    public AjaxResult save(@RequestBody SaveAttachDto saveAttachDto) {
        attachService.saveAttach(saveAttachDto);
        return AjaxResult.success();
    }

    @GetMapping(value = "/getInfo/{route}/{ids}")
    @ResponseBody
    public AjaxResult getInfo(@PathVariable String ids, @PathVariable String route) {
        List<Map<String, Object>> resultList = attachService.getInfo(ids);
        for (Map<String, Object> map : resultList) {
            map.put("file_url", request.getContextPath() + "/attach/" + route + "/" + map.get("file_id"));
        }
        return AjaxResult.success(resultList);
    }

    /**
     * 删除
     *
     * @param id 图片id
     * @return 删除结果
     */
    @DeleteMapping(value = "/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable Integer id) {
        attachService.removeById(id);
        return AjaxResult.success();
    }
}
