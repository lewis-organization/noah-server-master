package org.noah.modules.sys.roleResources.controller;


import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.dto.SaveRoleResourcesDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.resources.service.ResourcesService;
import org.noah.modules.sys.role.entity.Role;
import org.noah.modules.sys.role.service.RoleService;
import org.noah.modules.sys.roleResources.service.RoleResourcesService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2022-01-07
 */
@Controller
@RequestMapping("/roleResources")
public class RoleResourcesController extends BaseController {

    @DubboReference
    private RoleResourcesService roleResourcesService;
    @DubboReference
    private RoleService roleService;
    @DubboReference(version = "1.0.0")
    private ResourcesService resourcesService;

    /**
     * 角色权限分配
     * @author Liuxu
     * @date 2024/1/23 10:55
     * @param roleId 角色id
     * @return java.lang.String
     */
    @GetMapping(value = "/setting/{roleId}")
    public String setting(@PathVariable String roleId) {
        Role role = roleService.getById(roleId);
        request.setAttribute("role", role);
        return "sys/roleResources/setting";
    }

    /**
     * 加载角色权限树
     * @author Liuxu
     * @date 2024/1/23 10:55
     * @param roleId 角色id
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/getRoleResourcesByRoleId/{roleId}")
    @ResponseBody
    public AjaxResult getRoleResourcesByRoleId(@PathVariable String roleId) {
        return AjaxResult.success(roleResourcesService.getRoleResourcesByRoleId(roleId));
    }

    /**
     * 保存设置
     * @author Liuxu
     * @date 2024/1/23 10:55
     * @param saveRoleResourcesDto 保存dto
     * @return org.noah.config.json.AjaxResult
     */
    @PostMapping(value = "/saveSetting")
    @ResponseBody
    public AjaxResult saveSetting(@RequestBody SaveRoleResourcesDto saveRoleResourcesDto) {
        roleResourcesService.saveSetting(saveRoleResourcesDto);
        return AjaxResult.success();
    }
}

