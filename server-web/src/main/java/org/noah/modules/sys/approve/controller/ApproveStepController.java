package org.noah.modules.sys.approve.controller;

import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.modules.sys.approve.service.ApproveStepService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Liuxu
 * @since 2024-01-24
 */
@Controller
@RequestMapping("/approveStep")
public class ApproveStepController {

    @DubboReference
    private ApproveStepService approveStepService;

    /**
     * 根据id获取人员
     * @author Liuxu
     * @date 2024/1/24 14:33
     * @param id id
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/getByApproveId/{id}")
    @ResponseBody
    public AjaxResult getByApproveId(@PathVariable Integer id) {
        return AjaxResult.success(approveStepService.getByApproveId(id));
    }
}
