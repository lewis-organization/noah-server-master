package org.noah.modules.sys.task.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.noah.config.json.AjaxResult;
import org.noah.dto.TaskQueryDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.task.entity.Task;
import org.noah.modules.sys.task.service.TaskService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 系统任务 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2022-01-13
 */
@Controller
@RequestMapping("/task")
public class TaskController extends BaseController {

    @DubboReference
    private TaskService taskService;

    /**
     * 任务列表
     * @author Liuxu
     * @date 2024/1/23 10:55
     * @return java.lang.String
     */
    @GetMapping(value = "/list")
    public String list() {
        return "sys/task/list";
    }

    /**
     * 任务分页数据
     * @author Liuxu
     * @date 2024/1/23 10:56
     * @param page 分页参数
     * @param taskQueryDto 查询dto
     * @return com.baomidou.mybatisplus.extension.plugins.pagination.Page<java.util.Map<java.lang.String,java.lang.Object>>
     */
    @PostMapping(value = "/getPage")
    @ResponseBody
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, TaskQueryDto taskQueryDto) {
        return taskService.getPage(page, taskQueryDto);
    }

    /**
     * 新增
     * @author Liuxu
     * @date 2024/1/23 10:56
     * @return java.lang.String
     */
    @GetMapping(value = "/add")
    public String add() {
        return "sys/task/add";
    }

    /**
     * 保存
     * @author Liuxu
     * @date 2024/1/23 10:56
     * @param task 保存dto
     * @return org.noah.config.json.AjaxResult
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public AjaxResult save(@RequestBody Task task) {
        taskService.saveTask(task);
        return AjaxResult.success();
    }

    /**
     * 编辑
     * @author Liuxu
     * @date 2024/1/23 10:56
     * @param id id
     * @return java.lang.String
     */
    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable Integer id) {
        Task task = taskService.getById(id);
        request.setAttribute("model", task);
        return "sys/task/edit";
    }

    /**
     * 修改
     * @author Liuxu
     * @date 2024/1/23 10:56
     * @param task 修改dto
     * @return org.noah.config.json.AjaxResult
     */
    @PutMapping(value = "/update")
    @ResponseBody
    public AjaxResult update(@RequestBody Task task) {
        taskService.updateTask(task);
        return AjaxResult.success();
    }

    /**
     * 删除任务
     * @author Liuxu
     * @date 2024/1/23 10:56
     * @param id id
     * @return org.noah.config.json.AjaxResult
     */
    @DeleteMapping(value = "/removeById/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable Integer id) {
        taskService.removeTask(id);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/batchRemove/{ids}")
    @ResponseBody
    public AjaxResult batchRemove(@PathVariable String ids) {
        taskService.removeByIds(StrUtil.split(ids, StrUtil.COMMA)
                .stream().map(Integer::new).collect(Collectors.toList()));
        return AjaxResult.success();
    }

    /**
     * 开启所有任务
     * @author Liuxu
     * @date 2024/1/23 10:57
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/start")
    @ResponseBody
    public AjaxResult start() {
        taskService.start();
        return AjaxResult.success();
    }

    /**
     * 停止所有任务
     * @author Liuxu
     * @date 2024/1/23 10:57
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/shutdown")
    @ResponseBody
    public AjaxResult shutdown() {
        taskService.shutdown();
        return AjaxResult.success();
    }

    /**
     * 获取所有任务
     * @author Liuxu
     * @date 2024/1/23 10:57
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/getAllJob")
    @ResponseBody
    public AjaxResult getAllJob() {
        taskService.allJob();
        return AjaxResult.success();
    }

    /**
     * 更改状态
     * @author Liuxu
     * @date 2024/1/23 10:57
     * @param id 任务id
     * @param state 任务状态
     * @return org.noah.config.json.AjaxResult
     */
    @PostMapping(value = "/changeState/{id}/{state}")
    @ResponseBody
    @RequiresPermissions("task:changeState")
    public AjaxResult changeState(@PathVariable String id, @PathVariable String state) {
        taskService.changeState(id, state);
        return AjaxResult.success();
    }
}

