package org.noah.modules.sys.userRole.controller;


import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.dto.SaveUserRoleDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.user.service.WebUserService;
import org.noah.modules.sys.userRole.service.UserRoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2021-12-21
 */
@Controller
@RequestMapping("/userRole")
public class UserRoleController extends BaseController {

    @DubboReference
    private UserRoleService userRoleService;
    @DubboReference(version = "1.0.0")
    private WebUserService webUserService;

    @GetMapping(value = "/setting/{userId}")
    public String setting(@PathVariable String userId) {
        request.setAttribute("user", webUserService.getUserUseByUserRole(userId));
        return "sys/userRole/setting";
    }

    @GetMapping(value = "/getUserRoleByUserId/{userId}")
    @ResponseBody
    public AjaxResult getUserRoleByUserId(@PathVariable Integer userId) {
        return AjaxResult.success(userRoleService.getUserRoleByUserId(userId));
    }

    @PostMapping(value = "/saveSetting")
    @ResponseBody
    public AjaxResult saveSetting(@RequestBody SaveUserRoleDto saveUserRoleDto) {
        userRoleService.saveSetting(saveUserRoleDto);
        return AjaxResult.success();
    }
}

