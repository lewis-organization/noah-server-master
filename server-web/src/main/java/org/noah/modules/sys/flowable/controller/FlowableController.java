package org.noah.modules.sys.flowable.controller;

import cn.hutool.core.io.IoUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.dto.DoApproveDto;
import org.noah.dto.SaveFlowableDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.fl.notes.service.NotesService;
import org.noah.modules.sys.flowable.service.FlowableService;
import org.noah.utils.WebUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

@Controller
@RequestMapping("/flowable")
@RequiredArgsConstructor
public class FlowableController extends BaseController {

//    @DubboReference
    private FlowableService flowableService;
    @DubboReference
    private NotesService notesService;

    @GetMapping(value = "/list")
    public String list() {
        return "sys/flowableDemo/list";
    }

    @GetMapping(value = "/waitProcess")
    public String waitProcess() {
        return "sys/flowableDemo/wait-process";
    }

    @PostMapping(value = "/getWaitProcessPage")
    @ResponseBody
    public Page<Map<String, Object>> getWaitProcessPage(Page<Map<String, Object>> page) {
        return flowableService.getWaitProcessPage(page, WebUserUtil.getWebUser().getNickName());
    }

    @GetMapping(value = "/historyProcess")
    public String historyProcess() {
        return "sys/flowableDemo/history-process";
    }

    @PostMapping(value = "/getHistoryProcessPage")
    @ResponseBody
    public Page<Map<String, Object>> getHistoryProcessPage(Page<Map<String, Object>> page) {
        return flowableService.getHistoryProcessPage(page, WebUserUtil.getWebUser().getNickName());
    }

    @GetMapping(value = "/historyTask")
    public String historyTask() {
        return "sys/flowableDemo/history-task";
    }

    @PostMapping(value = "/getHistoryTaskPage")
    @ResponseBody
    public Page<Map<String, Object>> getHistoryTaskPage(Page<Map<String, Object>> page) {
        return flowableService.getHistoryTaskPage(page, WebUserUtil.getWebUser().getNickName());
    }

    @GetMapping(value = "/add")
    public String add() {
        return "sys/flowableDemo/add";
    }

    @PostMapping(value = "/save")
    @ResponseBody
    public AjaxResult save(@RequestBody SaveFlowableDto saveFlowableDto) {
        saveFlowableDto.setUserName(WebUserUtil.getWebUser().getNickName());
        saveFlowableDto.setUserId(WebUserUtil.getWebUser().getId());
        flowableService.saveFlowable(saveFlowableDto);
        return AjaxResult.success();
    }

    @PostMapping(value = "/submitAsk/{taskId}/{processInstanceId}")
    @ResponseBody
    public AjaxResult submitAsk(@PathVariable String taskId, @PathVariable String processInstanceId) {
        flowableService.submitAsk(taskId, processInstanceId, WebUserUtil.getWebUser().getNickName());
        return AjaxResult.success();
    }

    @PostMapping(value = "/doApprove")
    @ResponseBody
    public AjaxResult doApprove(@RequestBody DoApproveDto doApproveDto) {
        doApproveDto.setFinished(WebUserUtil.isAdmin());
        doApproveDto.setNickName(WebUserUtil.getWebUser().getNickName());
        flowableService.doApprove(doApproveDto);
        return AjaxResult.success();
    }

    @GetMapping(value = "/refused/{taskId}")
    public String refused(@PathVariable String taskId) {
        request.setAttribute("taskId", taskId);
        return "sys/flowableDemo/refused";
    }

    /**
     * 提交请假的流程审批
     *
     * @param userId 用户id
     * @return 结果
     */
    @PostMapping("/start/{userId}")
    @ResponseBody
    public AjaxResult startFlow(@PathVariable String userId) {
        return AjaxResult.success(flowableService.startFlow(userId));
    }

    /**
     * 查询指定流程所有启动的实例列表
     * @author Liuxu
     * @date 2024/1/23 10:46
     * @param page 页码
     * @param size 个数
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/processDefinitionList")
    @ResponseBody
    public AjaxResult processDefinitionList(@RequestParam(required = false) Integer page,
                                            @RequestParam(required = false) Integer size) {
        return AjaxResult.success(flowableService.processDefinitionList(page, size));
    }

    /**
     * 获取用户的任务
     *
     * @param userId 用户id
     * @return 任务结果
     */
    @GetMapping("/getTasks/{userId}")
    @ResponseBody
    public AjaxResult getTasks(@PathVariable String userId) {
        return AjaxResult.success(flowableService.getTasks(userId), null);
    }

    /**
     * 用户申领任务并且完成
     *
     * @param userId 用户id
     * @return 申领结果
     */
    @GetMapping(value = "/taskAssigneeComplete/{userId}/{idea}")
    @ResponseBody
    public AjaxResult taskAssigneeComplete(@PathVariable String userId, @PathVariable String idea) {
        flowableService.taskAssigneeComplete(userId, idea);
        return AjaxResult.success();
    }

    /**
     * 删除创建的流程实例
     *
     * @param processInstanceId 流程id
     * @param deleteReason 删除原因
     * @return 删除结果
     */
    @DeleteMapping(value = "/deleteProcessInstance/{processInstanceId}")
    @ResponseBody
    public AjaxResult deleteProcessInstance(@PathVariable String processInstanceId, @RequestParam String deleteReason) {
        flowableService.deleteProcessInstance(processInstanceId, deleteReason, WebUserUtil.getWebUser().getNickName());
        return AjaxResult.success();
    }

    @GetMapping(value = "/processImage/{processId}")
    public String processImage(@PathVariable String processId) {
        request.setAttribute("processId", processId);
        return "sys/flowableDemo/process-image";
    }

    /**
     * 生成流程图
     *
     * @param httpServletResponse 响应
     * @param processId 流程id
     * @throws Exception 异常
     */
    @GetMapping(value = "/processDiagram/{processId}")
    public void genProcessDiagram(HttpServletResponse httpServletResponse, @PathVariable String processId) throws Exception {
        byte[] buf = new byte[1024];
        int legth;
        try (InputStream in = IoUtil.toStream(flowableService.genProcessDiagram(processId)); OutputStream out = httpServletResponse.getOutputStream()) {
            while ((legth = in.read(buf)) != -1) {
                out.write(buf, 0, legth);
            }
        }
    }

    /**
     * 历史流程实例查询
     * 历史查询，因为一旦流程执行完毕，活动的数据都会被清空，上面查询的接口都查不到数据，但是提供历史查询接口
     *
     * @return 历史结果
     */
    @GetMapping(value = "/createHistoricProcessInstanceQuery")
    @ResponseBody
    public AjaxResult createHistoricProcessInstanceQuery() {
        return AjaxResult.success(flowableService.createHistoricProcessInstanceQuery());
    }

    /**
     * 历史任务查询
     *
     * @return 历史人物结果
     */
    @GetMapping(value = "/createHistoricTaskInstanceQuery")
    @ResponseBody
    public AjaxResult createHistoricTaskInstanceQuery() {
        return AjaxResult.success(flowableService.createHistoricTaskInstanceQuery());
    }

    @GetMapping(value = "/showNotes/{processInstanceId}")
    public String showNotes(@PathVariable String processInstanceId) {
        request.setAttribute("notes", notesService.getByProcessInstanceId(processInstanceId));
        return "sys/flowableDemo/show-notes";
    }
}
