package org.noah.modules.sys.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import enumc.LogTypeEnum;
import enumc.RedisCacheKeyEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.noah.config.annotation.SysLog;
import org.noah.config.exception.BusinessException;
import org.noah.config.json.AjaxResult;
import org.noah.modules.controller.BaseController;
import org.noah.utils.RedisUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Controller
@RequestMapping(value = "/referer")
@RequiredArgsConstructor
public class RefererController extends BaseController {

    private final RedisUtil redisUtil;

    /**
     * 来源白名单列表
     *
     * @return 页面地址
     */
    @GetMapping(value = "/list")
    public String list() {
        return "sys/referer/list";
    }

    @GetMapping(value = "/getList")
    @ResponseBody
    public Page<Map<String, Object>> getList() {
        Page<Map<String, Object>> objectPage = new Page<>();
        objectPage.setRecords(redisUtil.lGet(RedisCacheKeyEnum.REFERER_WHITE_LIST.getKey(), 0, -1)
                .stream().map(o -> {
                    Map<String, Object> map = MapUtil.newHashMap();
                    map.put("host", o);
                    return map;
                }).collect(Collectors.toList()));
        return objectPage;
    }

    @GetMapping(value = "/add")
    public String add() {
        return "sys/referer/add";
    }

    @PostMapping(value = "/save")
    @ResponseBody
    @SysLog(type = LogTypeEnum.COMMON)
    public AjaxResult save(@RequestBody JSONObject jsonObject) {
        if (!redisUtil.lSet(RedisCacheKeyEnum.REFERER_WHITE_LIST.getKey(), jsonObject.getString("host"), -1L)) {
            log.warn("缓存引用配置地址失败...");
        }
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/remove/{host}")
    @ResponseBody
    public AjaxResult remove(@PathVariable String host) {
        if (0L == redisUtil.lRemove(RedisCacheKeyEnum.REFERER_WHITE_LIST.getKey(), 1, host)) {
            return AjaxResult.fail("删除失败");
        }
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/batchRemove/{hosts}")
    @ResponseBody
    public AjaxResult batchRemove(@PathVariable String hosts) {
        String[] array = hosts.split(StrUtil.COMMA);
        Arrays.stream(array).forEach(s -> {
            if (0L == redisUtil.lRemove(RedisCacheKeyEnum.REFERER_WHITE_LIST.getKey(), 1, s)) {
                throw new BusinessException("删除失败");
            }
        });
        return AjaxResult.success();
    }
}
