package org.noah.modules.sys.controller;

import cn.hutool.core.lang.UUID;
import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.SpecCaptcha;
import lombok.RequiredArgsConstructor;
import org.noah.config.json.AjaxResult;
import org.noah.utils.RedisUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;

@RestController
@RequestMapping(value = "/captcha")
@RequiredArgsConstructor
public class EasyCaptchaController {

    private final RedisUtil redisUtils;

    /**
     * 获取验证码
     */
    @GetMapping("/getCaptcha")
    public AjaxResult getCaptcha() {
        // 三个参数分别为宽、高、位数
        //SpecCaptcha specCaptcha = new SpecCaptcha(130, 48, 5);
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(130, 48);
        // 设置字体
        captcha.setFont(new Font("Verdana", Font.PLAIN, 32));  // 有默认字体，可以不用设置
        // 设置类型，纯数字、纯字母、字母数字混合
        //captcha.setCharType(Captcha.TYPE_ONLY_NUMBER);
        String verCode = captcha.text();
        String key = UUID.randomUUID().toString(true);
        // 存入redis并设置过期时间为30分钟
        redisUtils.set(key, verCode, 1800);
        return AjaxResult.success()
                .put("key", key).put("image", captcha.toBase64());
    }

    public static void main(String[] args) {
        // png类型
        SpecCaptcha captcha = new SpecCaptcha(130, 48);
        captcha.text();  // 获取验证码的字符
        captcha.textChar();  // 获取验证码的字符数组

        // gif类型
        //GifCaptcha captcha = new GifCaptcha(130, 48);

        // 中文类型
        //ChineseCaptcha captcha = new ChineseCaptcha(130, 48);

        // 中文gif类型
        //ChineseGifCaptcha captcha = new ChineseGifCaptcha(130, 48);

        // 算术类型
        //ArithmeticCaptcha captcha = new ArithmeticCaptcha(130, 48);
        captcha.setLen(3);  // 几位数运算，默认是两位
        //captcha.getArithmeticString();  // 获取运算的公式：3+2=?
        captcha.text();  // 获取运算的结果：5

        //captcha.out(outputStream);  // 输出验证码
    }
}
