package org.noah.modules.sys.controller;

import cn.hutool.core.map.MapUtil;
import enumc.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.noah.config.CommonConstant;
import org.noah.config.annotation.SysLog;
import org.noah.config.json.AjaxResult;
import org.noah.dto.LoginDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.bs.service.AreaService;
import org.noah.modules.sys.bs.service.CityService;
import org.noah.modules.sys.bs.service.ProvinceService;
import org.noah.modules.sys.bs.service.StreetService;
import org.noah.modules.sys.config.entity.SystemConfig;
import org.noah.modules.sys.config.service.SystemConfigService;
import org.noah.modules.sys.message.service.WebMessageService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.modules.sys.user.service.WebUserService;
import org.noah.utils.RedisUtil;
import org.noah.utils.RsaUtil;
import org.noah.utils.WebUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Slf4j
@Controller
@RequestMapping(value = "")
@RequiredArgsConstructor
public class SystemController extends BaseController {

    private final RedisUtil redisUtil;
    @DubboReference(version = "1.0.0")
    private WebUserService webUserService;
    @DubboReference
    private WebMessageService webMessageService;
    @DubboReference
    private ProvinceService provinceService;
    @DubboReference
    private CityService cityService;
    @DubboReference
    private AreaService areaService;
    @DubboReference
    private StreetService streetService;
    @DubboReference
    private SystemConfigService systemConfigService;

    /**
     * 系统默认提示
     *
     * @return 页面地址
     */
    @GetMapping(value = "/login")
    public String login() {
        SystemConfig systemConfig = (SystemConfig) redisUtil.get(RedisCacheKeyEnum.SYSTEM_CONFIG.getKey());
        if (Objects.isNull(systemConfig)) {
            systemConfig = systemConfigService.getOne(null);
            redisUtil.set(RedisCacheKeyEnum.SYSTEM_CONFIG.getKey(), systemConfig);
        }
        request.setAttribute("pubKey", systemConfig.getPubKey());
        request.setAttribute("applicationName", systemConfig.getApplicationName());
        request.setAttribute("showCaptcha", systemConfig.getShowCaptcha());
        request.setAttribute("sysDes", systemConfig.getSysDes());
        return "login";
    }

    /**
     * 退出登录
     *
     * @return 退出结果
     */
    @GetMapping(value = "/logout")
    @ResponseBody
    public AjaxResult logout() {
        Subject subject = SecurityUtils.getSubject();
        WebUser webUser = (WebUser) subject.getPrincipal();
        subject.logout();
        //删除菜单缓存
        String webUserResourcesCacheKey = RedisCacheKeyEnum.USER_RESOURCES_CACHE.getKey() + webUser.getId(),
            webUserMenuCacheKey = RedisCacheKeyEnum.USER_MENU_CACHE.getKey() + webUser.getId();
        redisUtil.delete(webUserResourcesCacheKey, webUserMenuCacheKey);
        return new AjaxResult(AjaxReturnCodeEnum.AJAX_SUCCESS.getCode(), null);
    }

    /**
     * 无权限返回
     *
     * @return 页面地址
     */
    @GetMapping(value = "/unauthorized")
    public String unauthorized() {
        return "error/403";
    }


    /**
     * 页面超时跳转
     *
     * @return 页面地址
     */
    @GetMapping(value = "/timeout")
    public String timeout() {
        return "redirect:/login";
    }

    /**
     * 账号被顶提示
     *
     * @return 页面地址
     */
    @GetMapping(value = "/kickout")
    public String kickout() {
        return "kickout";
    }

    /**
     * 登陆方法
     *
     * @param loginDto 登录dto
     * @return 登录结果
     */
    @PostMapping(value = "/userLogin")
    @ResponseBody
    @SysLog(type = LogTypeEnum.LOGIN)
    public AjaxResult userLogin(@Valid LoginDto loginDto) {
        Subject subject = SecurityUtils.getSubject();
        SystemConfig systemConfig = (SystemConfig) redisUtil.get(RedisCacheKeyEnum.SYSTEM_CONFIG.getKey());
        if (TrueOrFalseEnum.TRUE.getValue().equals(systemConfig.getShowCaptcha())) {
            // 获取redis中的验证码
            String redisCode = (String) redisUtil.get(loginDto.getVerKey());
            if (StringUtils.isBlank(redisCode) || !redisCode.equals(loginDto.getCode())) {
                return AjaxResult.fail("验证码错误");
            }
        }
        String decryptPassword = RsaUtil.decryptDataByPrivate(loginDto.getPassword(), systemConfig.getPriKey());
        log.info("解密后的密码为: {}", decryptPassword);
        UsernamePasswordToken token = new UsernamePasswordToken(loginDto.getUsername(),
                decryptPassword, loginDto.isRememberMe());
        try {
            subject.login(token);
            subject.getSession().setAttribute("user", subject.getPrincipal());
            subject.getSession().setTimeout(CommonConstant.ONE_HOUR);
        } catch (IncorrectCredentialsException e) {
            //密码错误次数累计
            String key = RedisCacheKeyEnum.LOGIN_FAIL_LOCKED.getKey().concat(loginDto.getUsername()),
                msg;
            if (redisUtil.hasKey(key)) {
                long times = redisUtil.incr(key, 1);
                msg = "密码错误, 已经失败"+times+"次, 失败3次账号将被锁定";
                if (times == 3) {
                    webUserService.lockedAccount(loginDto.getUsername());
                    redisUtil.delete(key);
                    msg = "密码错误, 账号已被锁定";
                }
            } else {
                redisUtil.incr(key, 1);
                msg = "密码错误";
            }
            return AjaxResult.fail(msg);
        } catch (UnknownAccountException e) {
            return AjaxResult.fail("账号不存在");
        } catch (DisabledAccountException e) {
            return AjaxResult.fail("账号不可用(已被锁定)");
        }
        if (TrueOrFalseEnum.TRUE.getValue().equals(systemConfig.getShowCaptcha())) {
            redisUtil.delete(loginDto.getVerKey());
        }
        //登录成功删除缓存的失败次数
        redisUtil.delete(RedisCacheKeyEnum.LOGIN_FAIL_LOCKED.getKey().concat(loginDto.getUsername()));
        return AjaxResult.success();
    }


    @GetMapping(value = "/index")
    public String index() {
        SystemConfig systemConfig = (SystemConfig) redisUtil.get(RedisCacheKeyEnum.SYSTEM_CONFIG.getKey());
        request.setAttribute("applicationName", systemConfig.getApplicationName());
        return "index";
    }

    @GetMapping(value = "/console1")
    public String console1() {
        WebUser webUser = WebUserUtil.getWebUser();
        if (UserTypeEnum.SYSTEM.getType().equals(webUser.getType())) {
            request.setAttribute("host", request.getServerName() +
                    ":" + request.getServerPort() + request.getContextPath());
            return "console1";
        }
        return "default";
    }

    @GetMapping(value = "/code")
    public String code() {
        return "sys/tool/code";
    }

    @GetMapping(value = "/personal")
    public String personal() {
        WebUser webUser = webUserService.getById(WebUserUtil.getWebUser().getId());
        request.setAttribute("user", webUser);
        return "sys/user/personal";
    }

    /**
     * 查询系统消息
     *
     * @return 消息结果
     */
    @GetMapping(value = "/getMessageInfo")
    @ResponseBody
    public List<Map<String, Object>> getMessageInfo() {
        Map<String, Object> paramsMap = MapUtil.newHashMap();
        paramsMap.put("path", request.getContextPath());
        paramsMap.put("userId", WebUserUtil.getWebUser().getId());
        paramsMap.put("orgId", WebUserUtil.getWebUser().getOrgId());
        return webMessageService.getMessageInfo(paramsMap);
    }

    /**
     * 省市区demo页面
     *
     * @return 页面地址
     */
    @RequestMapping(value = "/areaList")
    public String list() {
        return "sys/tool/area-list";
    }

    /**
     * 获取省信息
     *
     * @return 省结果
     */
    @GetMapping(value = "/getProvinceList")
    @ResponseBody
    public AjaxResult getProvinceList() {
        return AjaxResult.success(provinceService.getList());
    }

    /**
     * 获取市列表
     *
     * @return 市结果
     */
    @GetMapping(value = "/getCityList/{code}")
    @ResponseBody
    public AjaxResult getCityList(@PathVariable String code) {
        return AjaxResult.success(cityService.getListByProvinceCode(code));
    }

    /**
     * 获取区列表
     *
     * @param code 城市编码
     * @return 区结果
     */
    @GetMapping(value = "/getAreaList/{code}")
    @ResponseBody
    public AjaxResult getAreaList(@PathVariable String code) {
        return AjaxResult.success(areaService.getListByCityCode(code));
    }

    /**
     * 获取街道列表
     *
     * @param code 区域编码
     * @return 街道结果
     */
    @GetMapping(value = "/getStreetList/{code}")
    @ResponseBody
    public AjaxResult getStreetList(@PathVariable String code) {
        return AjaxResult.success(streetService.getListByAreaCode(code));
    }

    /**
     * webSocket测试页
     * @author Liuxu
     * @date 2024/1/23 10:40
     * @return java.lang.String
     */
    @GetMapping(value = "/webSocket")
    public String webSocket() {
        return "sys/socket/index";
    }

}
