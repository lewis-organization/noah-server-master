package org.noah.modules.sys.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.modules.sys.user.service.WebUserService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping(value = "/test")
public class TestController {

    @DubboReference(version = "1.0.0")
    private WebUserService webUserService;

    @GetMapping("/{id}")
    public AjaxResult test(@PathVariable String id) {
        webUserService.test(id);
        return AjaxResult.success().put("number", id);
    }

    @PostMapping(value = "/testEasyExcel")
    @ResponseBody
    public AjaxResult testEasyExcel(@RequestPart(value = "file") MultipartFile file) {
        try {
            EasyExcel.read(file.getInputStream(), new AnalysisEventListener() {
                @Override
                public void invoke(Object o, AnalysisContext analysisContext) {
                    System.out.println(o);
                }

                @Override
                public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                    System.out.println("读取完成");
                }
            }).sheet(0).doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return AjaxResult.success();
    }
}
