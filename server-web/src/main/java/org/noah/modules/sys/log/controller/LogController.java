package org.noah.modules.sys.log.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.log.service.LogService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2022-04-15
 */
@Controller
@RequestMapping("/log")
public class LogController extends BaseController {

    @DubboReference
    private LogService logService;

    @GetMapping(value = "/list")
    public String list() {
        return "sys/log/list";
    }

    @GetMapping(value = "/getPage/{type}")
    @ResponseBody
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, @PathVariable String type) {
        return logService.getPage(page, type);
    }

    @GetMapping(value = "/logInfo")
    public String logInfo() {
        request.setAttribute("host", request.getServerName() +
                ":" + request.getServerPort() + request.getContextPath());
        return "sys/log/logInfo";
    }
}

