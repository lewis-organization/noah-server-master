package org.noah.modules.sys.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.dto.TestFunctionQueryDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.test.entity.TestFunction;
import org.noah.modules.sys.test.service.TestFunctionService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Liuxu
 * @since 2024-05-28
 */
@Controller
@RequestMapping("/testFunction")
public class TestFunctionController extends BaseController {

    @DubboReference
    private TestFunctionService testFunctionService;

    /**
     * 测试业务首页
     * @author liuxu
     * @date 2024/5/28 16:06
     * @return String
     */
    @GetMapping(value = "/list")
    public String list() {
        return "sys/test/function/list";
    }

    /**
     * 分页查询
     * @author liuxu
     * @date 2024/5/28 16:08
     * @param page 分页参数
     * @param testFunctionQueryDto 查询参数
     * @return Page<Map < String, Object>>
     */
    @PostMapping(value = "/getPage")
    @ResponseBody
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, TestFunctionQueryDto testFunctionQueryDto) {
        return testFunctionService.getPage(page, testFunctionQueryDto);
    }

    @GetMapping(value = "/add")
    public String add() {
        return "sys/test/function/add";
    }

    /**
     * 新增保存
     * @author liuxu
     * @date 2024/5/28 16:16
     * @param testFunction 保存参数
     * @return AjaxResult
     */
    @PostMapping(value = "/saveObj")
    @ResponseBody
    public AjaxResult saveObj(@RequestBody TestFunction testFunction) {
        testFunctionService.saveObj(testFunction);
        return AjaxResult.success();
    }

    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable Integer id) {
        request.setAttribute("model", testFunctionService.getById(id));
        return "sys/test/function/edit";
    }

    /**
     * 修改保存
     * @author liuxu
     * @date 2024/5/28 16:16
     * @param testFunction 保存参数
     * @return AjaxResult
     */
    @PostMapping(value = "/updateObj")
    @ResponseBody
    public AjaxResult updateObj(@RequestBody TestFunction testFunction) {
        testFunctionService.updateObj(testFunction);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable Integer id) {
        testFunctionService.removeById(id);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/batchRemove/{ids}")
    @ResponseBody
    public AjaxResult batchRemove(@PathVariable String ids) {
        testFunctionService.removeByIds(StrUtil.split(ids, StrUtil.COMMA)
                .stream().map(Integer::new).collect(Collectors.toList()));
        return AjaxResult.success();
    }

    /**
     * 提交审核
     * @author liuxu
     * @date 2024/5/30 15:06
     * @param ids id参数
     * @return AjaxResult
     */
    @PostMapping(value = "/submit/{ids}")
    @ResponseBody
    public AjaxResult submitApprove(@PathVariable String ids) {
        testFunctionService.submitApprove(StrUtil.split(ids, StrUtil.COMMA)
                .stream().map(Integer::new).collect(Collectors.toList()));
        return AjaxResult.success();
    }

    /**
     * 通过GET请求查看审批记录
     *
     * @param id 审批记录的ID，由URL路径参数提供
     * @return 包含审批记录详细信息的AjaxResult对象
     */
    @GetMapping(value = "/viewApprove/{id}")
    @ResponseBody
    public AjaxResult viewApprove(@PathVariable Integer id) {
        // 调用测试函数服务获取指定ID的审批记录，并将结果封装为AjaxResult返回
        return AjaxResult.success(testFunctionService.getApproveRecordsId(id));
    }
}
