package org.noah.modules.sys.resources.controller;


import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.dto.ResourcesQueryDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.resources.entity.Resources;
import org.noah.modules.sys.resources.service.ResourcesService;
import org.noah.utils.WebUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2021-11-04
 */
@Controller
@RequestMapping("/resources")
public class ResourcesController extends BaseController {

    @DubboReference(version = "1.0.0")
    private ResourcesService resourcesService;

    @GetMapping(value = "/list")
    public String list() {
        return "sys/resources/list";
    }

    /**
     * 获取用户菜单
     * @author Liuxu
     * @date 2024/1/23 10:52
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/getUserResources")
    @ResponseBody
    public AjaxResult getUserResources() {
        return AjaxResult.success(resourcesService.getUserResources(request.getContextPath(),
                WebUserUtil.getWebUser().getId()));
    }

    /**
     * 获取菜单数据
     * @author Liuxu
     * @date 2024/1/23 10:53
     * @param resourcesQueryDto 查询dto
     * @return org.noah.config.json.AjaxResult
     */
    @PostMapping(value = "/getPage")
    @ResponseBody
    public AjaxResult getPage(ResourcesQueryDto resourcesQueryDto) {
        return AjaxResult.success(resourcesService.getList(resourcesQueryDto));
    }

    @DeleteMapping(value = "/deleteById/{id}")
    @ResponseBody
    public AjaxResult deleteById(@PathVariable Integer id) {
        resourcesService.deleteById(id);
        return AjaxResult.success();
    }

    @GetMapping(value = "/add")
    public String add() {
        return "sys/resources/add";
    }

    /**
     * 获取全部根据类型查询
     * @author Liuxu
     * @date 2024/1/23 10:53
     * @param type 类型
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/getResourcesByType/{type}")
    @ResponseBody
    public AjaxResult getResourcesByType(@PathVariable String type) {
        return AjaxResult.success(resourcesService.getResourcesByType(type));
    }

    /**
     * 保存方法
     * @author Liuxu
     * @date 2024/1/23 10:54
     * @param resources 保存dto
     * @return org.noah.config.json.AjaxResult
     */
    @PostMapping(value = "/save")
    @ResponseBody
    public AjaxResult save(@RequestBody Resources resources) {
        resourcesService.saveEntity(resources);
        return AjaxResult.success();
    }

    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable Integer id) {
        Resources resources = resourcesService.getById(id);
        request.setAttribute("model", resources);
        return "sys/resources/edit";
    }

    @PutMapping(value = "/update")
    @ResponseBody
    public AjaxResult update(@RequestBody Resources resources) {
        resourcesService.updateEntity(resources);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/batchRemove/{ids}")
    @ResponseBody
    public AjaxResult batchRemove(@PathVariable String ids) {
        resourcesService.batchRemove(ids);
        return AjaxResult.success();
    }

    @GetMapping(value = "/refresh")
    @ResponseBody
    public AjaxResult refresh() {
        resourcesService.setResourcesListCache(null);
        return AjaxResult.success();
    }
}

