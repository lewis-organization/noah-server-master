package org.noah.modules.sys.config.controller;


import enumc.LogTypeEnum;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.annotation.SysLog;
import org.noah.config.json.AjaxResult;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.config.entity.SystemConfig;
import org.noah.modules.sys.config.service.SystemConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2022-09-30
 */
@Controller
@RequestMapping("/systemConfig")
public class SystemConfigController extends BaseController {

    @DubboReference
    private SystemConfigService systemConfigService;

    @GetMapping(value = "/setting")
    public String setting() {
        SystemConfig systemConfig = systemConfigService.getOne(null);
        if (Objects.isNull(systemConfig)) {
            systemConfig = new SystemConfig();
        }
        request.setAttribute("model", systemConfig);
        return "sys/config/setting";
    }

    @PostMapping(value = "/saveSetting")
    @ResponseBody
    @SysLog(type = LogTypeEnum.COMMON)
    public AjaxResult saveSetting(@RequestBody SystemConfig systemConfig) {
        systemConfigService.saveSetting(systemConfig);
        return AjaxResult.success();
    }
}

