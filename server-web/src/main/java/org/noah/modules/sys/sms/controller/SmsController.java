package org.noah.modules.sys.sms.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.sms.entity.Sms;
import org.noah.modules.sys.sms.service.SmsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2023-02-27
 */
@Controller
@RequestMapping("/sms")
public class SmsController extends BaseController {

    @DubboReference
    private SmsService smsService;

    @GetMapping(value = "/list")
    public String list() {
        return "sys/sms/list";
    }

    @PostMapping(value = "/getPage")
    @ResponseBody
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, Sms sms) {
        return smsService.getPage(page, sms);
    }

    @GetMapping(value = "/send")
    public String send() {
        return "sys/sms/send";
    }

    @PostMapping(value = "/doSend")
    @ResponseBody
    public AjaxResult doSend(@RequestBody Sms sms) {
        smsService.doSend(sms);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable Integer id) {
        smsService.removeById(id);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/batchRemove/{ids}")
    @ResponseBody
    public AjaxResult batchRemove(@PathVariable String ids) {
        smsService.removeByIds(StrUtil.split(ids, StrUtil.COMMA)
                .stream().map(Integer::new).collect(Collectors.toList()));
        return AjaxResult.success();
    }
}

