package org.noah.modules.sys.org.controller;


import enumc.RedisCacheKeyEnum;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.dto.WebUserDto;
import org.noah.mapstruct.webUser.WebUserStructMapper;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.org.entity.Org;
import org.noah.modules.sys.org.service.OrgService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.utils.RedisUtil;
import org.noah.utils.WebUserUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2022-11-07
 */
@Controller
@RequestMapping("/org")
@RequiredArgsConstructor
public class OrgController extends BaseController {

    @DubboReference
    private OrgService orgService;
    private final RedisUtil redisUtil;

    @GetMapping(value = "/list")
    public String list() {
        return "sys/org/list";
    }

    @GetMapping(value = "/getOrgTree")
    @ResponseBody
    public Map<String, Object> getOrgTree() {
        WebUser webUser = WebUserUtil.getWebUser();
        WebUserDto webUserDto = WebUserStructMapper.INSTANCE.toWebUserDto(webUser);
        webUserDto.setAdmin(WebUserUtil.isAdmin());
        return orgService.getOrgTree(webUserDto);
    }

    @GetMapping(value = "/add/{id}")
    public String add(@PathVariable Integer id) {
        Org org = orgService.getById(id);
        request.setAttribute("model", org);
        return "sys/org/add";
    }

    @PostMapping(value = "/save")
    @ResponseBody
    public AjaxResult save(@RequestBody Org org) {
        orgService.saveObj(org);
        return AjaxResult.success();
    }

    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable Integer id) {
        Org org = orgService.getById(id);
        request.setAttribute("model", org);
        Org parent = orgService.getById(org.getParentId());
        request.setAttribute("parentName", Objects.isNull(parent) ? "顶级" : parent.getTitle());
        return "sys/org/edit";
    }

    @PutMapping(value = "/update")
    @ResponseBody
    public AjaxResult update(@RequestBody Org org) {
        orgService.updateObj(org);
        return AjaxResult.success();
    }

    @GetMapping(value = "/refresh")
    @ResponseBody
    public AjaxResult refresh() {
        orgService.refresh();
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable Integer id) {
        List<Org> resultList;
        List<Integer> orgIds = new ArrayList<>();
        if (redisUtil.hasKey(RedisCacheKeyEnum.ORG_CACHE.getKey())) {
            resultList = redisUtil.lGet(RedisCacheKeyEnum.ORG_CACHE.getKey(), 0, -1)
                    .stream().map(o -> (Org) o).collect(Collectors.toList());
        } else {
            resultList = orgService.list();
        }
        orgIds.add(id);
        for (Org org : resultList) {
            if (id.equals(org.getParentId())) {
                orgIds.add(org.getId());
                orgService.recursionOrgIds(org, resultList, orgIds);
            }
        }
        orgService.removeById(id);
        return AjaxResult.success();
    }
}

