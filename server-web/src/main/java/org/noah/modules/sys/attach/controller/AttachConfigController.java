package org.noah.modules.sys.attach.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.attach.entity.AttachConfig;
import org.noah.modules.sys.attach.service.AttachConfigService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2023-03-06
 */
@Controller
@RequestMapping("/attachConfig")
public class AttachConfigController extends BaseController {

    @DubboReference
    private AttachConfigService attachConfigService;

    @GetMapping(value = "/setting")
    public String setting() {
        AttachConfig attachConfig = attachConfigService.getOne(null);
        if (Objects.isNull(attachConfig)) {
            attachConfig = new AttachConfig();
        }
        request.setAttribute("model", attachConfig);
        return "sys/attach/setting";
    }

    @GetMapping(value = "/getById/{id}")
    @ResponseBody
    public AjaxResult getById(@PathVariable Integer id) {
        Map<String, Object> modelMap = attachConfigService.getModelMapById(id);
        String compressType = MapUtil.getStr(modelMap, "compressType"),
                supportType = MapUtil.getStr(modelMap, "supportType"),
                previewType = MapUtil.getStr(modelMap, "previewType");
        return AjaxResult.success()
                .put("compressTypeArray", StrUtil.splitToArray(compressType, StrUtil.C_COMMA))
                .put("supportTypeArray", StrUtil.splitToArray(supportType, StrUtil.C_COMMA))
                .put("previewTypeArray", StrUtil.splitToArray(previewType, StrUtil.C_COMMA));
    }

    @PostMapping(value = "/saveSetting")
    @ResponseBody
    public AjaxResult saveSetting(@RequestBody AttachConfig attachConfig) {
        attachConfigService.saveSetting(attachConfig);
        return AjaxResult.success();
    }
}
