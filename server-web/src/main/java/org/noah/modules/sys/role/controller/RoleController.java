package org.noah.modules.sys.role.controller;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.dto.RoleQueryDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.role.entity.Role;
import org.noah.modules.sys.role.service.RoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author liuxu
 * @since 2021-12-21
 */
@Controller
@RequestMapping("/role")
public class RoleController extends BaseController {

    @DubboReference
    private RoleService roleService;

    @GetMapping(value = "/list")
    public String list() {
        return "sys/role/list";
    }

    @PostMapping(value = "/getPage")
    @ResponseBody
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, RoleQueryDto roleQueryDto) {
        return roleService.getPage(page, roleQueryDto);
    }

    @GetMapping(value = "/add")
    public String add() {
        return "sys/role/add";
    }

    @PostMapping(value = "/save")
    @ResponseBody
    public AjaxResult save(@RequestBody Role role) {
        roleService.saveRole(role);
        return AjaxResult.success();
    }

    @GetMapping(value = "/edit/{id}")
    public String edit(@PathVariable String id) {
        Role role = roleService.getById(id);
        request.setAttribute("model", role);
        return "sys/role/edit";
    }

    @PutMapping(value = "/update")
    @ResponseBody
    public AjaxResult update(@RequestBody Role role) {
        roleService.updateRole(role);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable String id) {
        roleService.removeById(id);
        return AjaxResult.success();
    }

    @DeleteMapping(value = "/batchRemove/{ids}")
    @ResponseBody
    public AjaxResult batchRemove(@PathVariable String ids) {
        roleService.removeByIds(StrUtil.split(ids, StrUtil.COMMA)
                .stream().map(Integer::new).collect(Collectors.toList()));
        return AjaxResult.success();
    }
}

