layui.define(['table', 'jquery', 'element'], function(exports) {
	"use strict";

	var MOD_NAME = 'message',
		$ = layui.jquery,
		element = layui.element,
		admin = layui.admin;

	var message = function(opt) {
		this.option = opt;
	};

	message.prototype.render = function(opt) {
		//默认配置值
		var option = {
			elem: opt.elem,
			url: opt.url ? opt.url : false,
			height: opt.height,
			data: opt.data
		}
		if (option.url != false) {
			option.data = getData(option.url);
			var notice = createHtml(option);
			$(option.elem).html(notice);
		}
		setTimeout(function() {
			element.init();
			$(opt.elem + " li").click(function(e) {
				$(this).siblings().removeClass('pear-this');
				$(this).addClass('pear-this');
			})
		}, 300);
		return new message(option);
	}

	message.prototype.click = function(callback) {
		$("*[notice-id]").click(function(event) {
			event.preventDefault();
			var id = $(this).attr("notice-id");
			var title = $(this).attr("notice-title");
			var context = $(this).attr("notice-context");
			var form = $(this).attr("notice-form");
			callback(id, title, context, form);
		})
	}

	/** 同 步 请 求 获 取 数 据 */
	function getData(url) {
		$.ajaxSettings.async = false;
		var data = null;
		$.get(url, function(result) {
			data = result;
		});
		$.ajaxSettings.async = true;
		return data;
	}

	function createHtml(option) {

		var count = 0;
		var noticeTitle = '<ul class="layui-tab-title">';
		var noticeContent = '<div class="layui-tab-content" style="height:' + option.height + ';overflow-x: hidden;">';

		let size = 0;
		// 根据 data 便利数据
		$.each(option.data, function(i, item) {
			let numHtml = '';
			if (item.children.length > 0) {
				numHtml = '<span class="titleNum">('+item.children.length+')</span>';
			}
			if (i === 0) {
				noticeTitle += '<li class="pear-this">' + item.title + ''+numHtml+'</li>';
				noticeContent += '<div class="layui-tab-item layui-show">';
			} else {
				noticeTitle += '<li>' + item.title + ''+numHtml+'</li>';
				noticeContent += '<div class="layui-tab-item">';
			}
			$.each(item.children, function(i, note) {
				noticeContent += '<div class="pear-notice-item" notice-form="' + note.form + '" notice-context="' + note.content +
					'" notice-title="' + note.title + '" notice-id="' + note.id + '">' +
					'<img src="' + note.avatar + '" style="float: left;margin-top: 3px;"/>' +
					'<div style="display:inline-block;">' +
					'<a href="javascript:;" class="messageInfo" style="overflow: hidden;text-overflow: ellipsis;width: 150px;">' + note.content + '</a></div>'+
					'<div class="pear-notice-end">' + note.time + '</div>' +
					'</div>';
				size++;
			})
			noticeContent += '</div>';
		})
		if (size === 0) {
			size = '';
		} else if (size > 99) {
			size = '<span class="layui-badge">99+</span>';
		} else {
			size = '<span class="layui-badge">' + size + '</span>';
		}
		var notice = '<li class="layui-nav-item" lay-unselect="">' +
			'<a href="#" class="notice layui-icon layui-icon-notice">'+size+'</div></a>' +
			'<div class="layui-nav-child layui-tab pear-notice" style="margin-top: 0;left: -200px;">';

		noticeTitle += '</ul>';
		noticeContent += '</div>';
		notice += noticeTitle;
		notice += noticeContent;
		notice += '<div class="layui-footer" style="left: 0;text-align: center;">' +
			'<button type="button" class="layui-btn layui-btn-primary clearMessage">清空 通知</button></div></div></li>';
		return notice;
	}

	exports(MOD_NAME, new message());
})
