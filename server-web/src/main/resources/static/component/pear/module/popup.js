layui.define(['layer', 'jquery', 'element'], function(exports) {
	"use strict";

	var MOD_NAME = 'popup',
		$ = layui.jquery,
		layer = layui.layer,
		element = layui.element;

	var popup = new function() {

			this.success = function(msg) {
				layer.msg(msg, {
					icon: 1,
					time: 1500
				})
			},
			this.failure = function(msg) {
				layer.msg(msg, {
					icon: 2,
					time: 1500
				})
			},
			this.warning = function(msg) {
				layer.msg(msg, {
					icon: 3,
					time: 1500
				})
			},
			this.success = function(msg, callback) {
				layer.msg(msg, {
					icon: 1,
					time: 1500
				}, callback);
			},
			this.failure = function(msg, callback) {
				layer.msg(msg, {
					icon: 2,
					time: 1500
				}, callback);
			},
			this.warning = function(msg, callback) {
				layer.msg(msg, {
					icon: 3,
					time: 1500
				}, callback);
			},
			this.alert = function (msg, callback) {
				parent.layer.alert(msg, {
					icon:2,
					cancel: function(index, layero){
						return false;
					}
				}, callback);
			}
	};
	exports(MOD_NAME, popup);
})
