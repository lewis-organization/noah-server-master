package org.noah.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author liuxu
 * @date 2021/11/25-11:42
 */
@Data
public class ResourcesQueryDto implements Serializable {
    private static final long serialVersionUID = -2773509849954532087L;

    private String title;
}
