package org.noah.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class WebMessageQueryDto implements Serializable {

    private static final long serialVersionUID = -6728203867706303116L;
    private String title;

    private String type;

    private String display;

    private boolean admin;

    private Integer userId;
}
