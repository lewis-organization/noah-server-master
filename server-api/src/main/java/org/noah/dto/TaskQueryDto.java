package org.noah.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class TaskQueryDto implements Serializable {

    private static final long serialVersionUID = -7186336090632739005L;
    private String name;

    private String group;

    private String state;
}
