package org.noah.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SaveRoleResourcesDto implements Serializable {

    private static final long serialVersionUID = 2261428536758230032L;

    private Integer roleId;

    private List<Integer> resourcesIds;

    private List<Integer> cancelIds;
}
