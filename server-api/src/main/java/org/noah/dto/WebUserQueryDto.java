package org.noah.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.noah.modules.sys.user.entity.WebUser;

import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
public class WebUserQueryDto extends WebUser implements Serializable {

    private static final long serialVersionUID = -8030148877921108933L;
}
