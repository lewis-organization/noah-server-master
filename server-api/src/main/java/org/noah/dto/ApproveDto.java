package org.noah.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.noah.modules.sys.approve.entity.Approve;

import java.util.List;

/**
 * @author Liuxu
 * @date 2024年01月23日 14:56
 */
@Getter
@Setter
@NoArgsConstructor
public class ApproveDto extends Approve {

    private List<List<Integer>> approveIds;
}
