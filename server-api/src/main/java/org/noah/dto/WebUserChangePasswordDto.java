package org.noah.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class WebUserChangePasswordDto implements Serializable {

    private static final long serialVersionUID = -2768416713437972796L;
    private Integer id;

    private String oldPassword;

    private String password;
}
