package org.noah.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SaveUserRoleDto implements Serializable {

    private static final long serialVersionUID = -8929033496577215469L;
    private Integer userId;

    private List<Integer> roleIds;

    private List<Integer> cancelIds;
}
