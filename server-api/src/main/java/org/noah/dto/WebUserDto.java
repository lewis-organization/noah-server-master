package org.noah.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.noah.modules.sys.user.entity.WebUser;

@Data
@EqualsAndHashCode(callSuper = false)
public class WebUserDto extends WebUser {

    private static final long serialVersionUID = -3491200572305704845L;
    private String rePassword;

    private boolean admin;
}
