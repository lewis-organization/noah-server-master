package org.noah.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SaveFlowableDto implements Serializable {

    private static final long serialVersionUID = 1321028361373139834L;
    private String leaveDays;

    private String leaveReason;

    private String userName;

    private Integer userId;
}
