package org.noah.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.noah.modules.sys.approve.entity.Approve;

@EqualsAndHashCode(callSuper = true)
@Data
public class ApproveQueryDto extends Approve {
}
