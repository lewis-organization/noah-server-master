package org.noah.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class DoApproveDto implements Serializable {

    private static final long serialVersionUID = 7363938995294150691L;

    private String nickName;

    private String taskId;

    private String idea;

    private String refusedReason;

    private boolean finished;

    private String processInstanceId;
}
