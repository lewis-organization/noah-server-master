package org.noah.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginDto implements Serializable {

    private static final long serialVersionUID = -1404574095735322859L;
    private String username;

    private String password;

    private String code;

    private String verKey;

    private boolean rememberMe;

    private String phone;
}
