package org.noah.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SaveAttachDto implements Serializable {

    private static final long serialVersionUID = -3512476065631213673L;
    private String images;
}
