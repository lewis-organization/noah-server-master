package org.noah.dto;

import lombok.Getter;
import lombok.Setter;
import org.noah.modules.sys.approve.entity.ApproveRecords;

@Getter
@Setter
public class ApproveRecordsQueryDto extends ApproveRecords {

    private String businessType;

    private String complete;

    private String startTime;

    private String endTime;
}
