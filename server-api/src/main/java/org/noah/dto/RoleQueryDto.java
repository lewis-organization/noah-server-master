package org.noah.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RoleQueryDto implements Serializable {

    private static final long serialVersionUID = -6464938272247970639L;

    private String roleName;

    private String roleCode;

    private String roleType;
}
