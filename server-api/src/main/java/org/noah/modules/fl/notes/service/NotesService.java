package org.noah.modules.fl.notes.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.fl.notes.entity.Notes;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 审核历史记录 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-11-18
 */
public interface NotesService extends IService<Notes> {

    void saveObj(String processInstanceId, String idea, String des);

    List<Map<String, Object>> getByProcessInstanceId(String processInstanceId);
}
