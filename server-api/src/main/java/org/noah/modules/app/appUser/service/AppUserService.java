package org.noah.modules.app.appUser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.app.appUser.entity.AppUser;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2023-02-28
 */
public interface AppUserService extends IService<AppUser> {

    AppUser getAppUserByUserName(String username);

    AppUser getAppUserByPhone(String phone);

    Map<Object, Object> getUserInfo(Integer id);
}
