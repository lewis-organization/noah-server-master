package org.noah.modules.sys.message.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.WebMessageQueryDto;
import org.noah.modules.sys.message.entity.WebMessage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-01-19
 */
public interface WebMessageService extends IService<WebMessage> {

    List<Map<String, Object>> getMessageInfo(Map<String, Object> paramsMap) throws BusinessException;

    Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, WebMessageQueryDto webMessageQueryDto) throws BusinessException;

    void saveEntity(WebMessage webMessage) throws BusinessException;

    void updateEntity(WebMessage webMessage) throws BusinessException;

    void changeState(String id, String state);

    void allRead(Integer id);

    void sendApproveWebMessage(List<Integer> webUserIds);
}
