package org.noah.modules.sys.org.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.dto.WebUserDto;
import org.noah.modules.sys.org.entity.Org;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-11-07
 */
public interface OrgService extends IService<Org> {

    Map<String, Object> getOrgTree(WebUserDto webUserDto);

    void saveObj(Org org);

    void updateObj(Org org);

    void refresh();

    void recursionOrgIds(Org org, List<Org> resultList, List<Integer> orgIds);
}
