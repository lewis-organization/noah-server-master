package org.noah.modules.sys.bs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.sys.bs.entity.Area;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 地区设置 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
public interface AreaService extends IService<Area> {

    List<Map<String, Object>> getListByCityCode(String code);
}
