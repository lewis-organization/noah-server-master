package org.noah.modules.sys.log.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.sys.log.entity.Log;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-04-15
 */
public interface LogService extends IService<Log> {

    Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, String type);
}
