package org.noah.modules.sys.resources.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.ResourcesQueryDto;
import org.noah.modules.sys.resources.entity.Resources;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2021-11-04
 */
public interface ResourcesService extends IService<Resources> {

    Map<String, Object> getUserResources(String contextPath, Integer userId) throws BusinessException;

    List<Map<String, Object>> getList(ResourcesQueryDto resourcesQueryDto) throws BusinessException;

    void deleteById(Integer id) throws BusinessException;

    List<Map<String, Object>> getResourcesByType(String type) throws BusinessException;

    void saveEntity(Resources resources) throws BusinessException;

    void updateEntity(Resources resources) throws BusinessException;

    void batchRemove(String ids) throws BusinessException;

    void setCache() throws BusinessException;

    List<Map<String, Object>> getListUseByRole() throws BusinessException;

    List<Resources> getAll() throws BusinessException;

    void setResourcesListCache(List<Map<String, Object>> map);

    List<Resources> getResourcesByUserId(Integer id);
}
