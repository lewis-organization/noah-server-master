package org.noah.modules.sys.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.SaveAttachDto;
import org.noah.modules.sys.test.entity.Test;

import java.util.Map;

/**
 * @author liuxu
 * @date 2021/10/20-10:17
 */
public interface TestService extends IService<Test> {
    String test() throws BusinessException;

    void addQuartzTask() throws BusinessException;

    void testSync() throws BusinessException;

    Map<String, Object> testUpload(byte[] byteFiles, String fileName, long size) throws BusinessException;

    void saveAttach(SaveAttachDto saveAttachDto) throws BusinessException;

    String getNewMessage();

    //void esAdd();

    //Object esQuery();
}
