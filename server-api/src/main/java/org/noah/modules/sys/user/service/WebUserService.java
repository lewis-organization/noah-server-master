package org.noah.modules.sys.user.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.WebUserChangePasswordDto;
import org.noah.dto.WebUserDto;
import org.noah.dto.WebUserQueryDto;
import org.noah.modules.sys.user.entity.WebUser;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2021-11-01
 */
public interface WebUserService extends IService<WebUser> {

    WebUser getWebUserByUsername(String username) throws BusinessException;

    /**
     * 测试方法忽略
     * @author Liuxu
     * @date 2024/1/23 11:22
     * @param id id
     */
    void test(String id) throws BusinessException;

    Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, WebUserQueryDto webUserQueryDto) throws BusinessException;

    void saveEntity(WebUserDto webUserDto) throws BusinessException;

    void updateEntity(WebUserDto webUserDto) throws BusinessException;

    String getRandomStr() throws BusinessException;

    WebUser getUserUseByUserRole(String userId) throws BusinessException;

    void changeState(String id, String state) throws BusinessException;

    void changePassword(WebUserChangePasswordDto webUserChangePasswordDto) throws BusinessException;

    void lockedAccount(String username);

    List<Map<String, Object>> getByOrgId(String orgId);

    Map<String, Object> getOrgAndUserTree(WebUserDto webUserDto);
}
