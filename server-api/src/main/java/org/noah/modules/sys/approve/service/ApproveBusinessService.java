package org.noah.modules.sys.approve.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.dto.ApproveBusinessQueryDto;
import org.noah.modules.sys.approve.entity.ApproveBusiness;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
public interface ApproveBusinessService extends IService<ApproveBusiness> {

    /**
     * 列表分页查询
     * @author liuxu
     * @date 2024/5/28 15:04
     * @param page 分页参数
     * @param approveBusinessQueryDto 查询参数
     * @return Page<Map < String, Object>>
     */
    Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, ApproveBusinessQueryDto approveBusinessQueryDto);

    /**
     * 开始审核流程
     * @author liuxu
     * @date 2024/5/28 16:22
     * @param approveCode 审核流程编码
     * @param businessId 业务id
     * @param businessType 业务类型
     */
    void startFlow(String approveCode, Integer businessId, String businessType);

    /**
     * 提交审核流程
     * @author liuxu
     * @date 2024/5/30 15:16
     * @param businessType 业务类型
     * @param collect 业务参数集合
     */
    void submitApprove(String businessType, List<Integer> collect);

    /**
     * 完成审批业务的处理方法。
     * 通过接收来自前端的审批业务对象，调用服务层方法完成审批流程。
     *
     * @param approveBusiness 包含审批业务信息的对象，通过@RequestBody注解绑定请求体中的数据。
     */
    void finish(ApproveBusiness approveBusiness);

    /**
     * 根据业务id和业务类型获取审核业务id
     * @author liuxu
     * @date 2024/6/17 14:55
     * @param businessId 业务id
     * @param businessType 业务类型
     * @return Integer
     */
    Integer getIdByBusinessIdAndBusinessType(Integer businessId, String businessType);

    /**
     * 根据审核业务id获取审核记录id
     * @author liuxu
     * @date 2024/6/17 15:00
     * @param approveBusinessId
     * @return Integer
     */
    Integer getApproveRecordsIdByApproveBusinessId(Integer approveBusinessId);
}
