package org.noah.modules.sys.attach.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.sys.attach.entity.AttachConfig;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2023-03-06
 */
public interface AttachConfigService extends IService<AttachConfig> {

    Map<String, Object> getModelMapById(Integer id);

    void saveSetting(AttachConfig attachConfig);
}
