package org.noah.modules.sys.approve.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.noah.dto.ApproveRecordsQueryDto;
import org.noah.modules.sys.approve.entity.ApproveBusiness;
import org.noah.modules.sys.approve.entity.ApprovePersonnel;
import org.noah.modules.sys.approve.entity.ApproveRecords;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Liuxu
 * @since 2024-05-31
 */
public interface ApproveRecordsService extends IService<ApproveRecords> {

    /**
     * 根据查询条件获取审批记录的分页结果。
     * 本方法通过调用approveRecordsService的getCustomPage方法，实现根据特定查询条件对审批记录进行分页查询。
     * @author liuxu
     * @date 2024/6/3 09:26
     * @param page 分页对象，包含当前页码、每页大小等信息，同时用于存放查询结果。
     * @param approveRecordsQueryDto 查询条件对象，封装了所有可用于筛选审批记录的条件。
     * @return Page<Map < String, Object>> 返回查询后的分页结果，包含符合条件的审批记录集合及分页信息。
     */
    Page<Map<String, Object>> getCustomPage(Page<Map<String, Object>> page, ApproveRecordsQueryDto approveRecordsQueryDto);

    /**
     * 处理审批操作
     * @author liuxu
     * @date 2024/6/3 14:59
     * @param approveRecords 包含审批相关信息的实体类
     */
    void doApprove(ApproveRecords approveRecords);

    void queryAndCreateApproveRecords(List<ApproveRecords> approveRecordsList, ApproveBusiness approveBusiness,
                                      List<ApprovePersonnel> approvePersonnelList, List<Integer> webUserIds);

    List<Map<String, Object>> getApproveFlowInfo(Integer id);
}
