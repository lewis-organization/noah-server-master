package org.noah.modules.sys.config.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.sys.config.entity.SystemConfig;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-09-30
 */
public interface SystemConfigService extends IService<SystemConfig> {

    void saveSetting(SystemConfig systemConfig);
}
