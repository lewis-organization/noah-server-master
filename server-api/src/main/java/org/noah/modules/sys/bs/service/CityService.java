package org.noah.modules.sys.bs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.sys.bs.entity.City;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 城市设置 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
public interface CityService extends IService<City> {

    List<Map<String, Object>> getListByProvinceCode(String code);
}
