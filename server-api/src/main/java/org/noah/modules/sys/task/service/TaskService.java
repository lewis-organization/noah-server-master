package org.noah.modules.sys.task.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.TaskQueryDto;
import org.noah.modules.sys.task.entity.Task;

import java.util.Map;

/**
 * <p>
 * 系统任务 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-01-13
 */
public interface TaskService extends IService<Task> {

    Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, TaskQueryDto taskQueryDto) throws BusinessException;

    void saveTask(Task task) throws BusinessException;

    void updateTask(Task task) throws BusinessException;

    void removeTask(Integer id) throws BusinessException;

    void start() throws BusinessException;

    void shutdown() throws BusinessException;

    void allJob() throws BusinessException;

    void changeState(String id, String state) throws BusinessException;

}
