package org.noah.modules.sys.userRole.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.SaveUserRoleDto;
import org.noah.modules.sys.userRole.entity.UserRole;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2021-12-21
 */
public interface UserRoleService extends IService<UserRole> {

    Map<String, Object> getUserRoleByUserId(Integer userId) throws BusinessException;

    void saveSetting(SaveUserRoleDto saveUserRoleDto) throws BusinessException;
}
