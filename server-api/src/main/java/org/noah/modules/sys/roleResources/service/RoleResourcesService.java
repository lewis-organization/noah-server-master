package org.noah.modules.sys.roleResources.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.SaveRoleResourcesDto;
import org.noah.modules.sys.roleResources.entity.RoleResources;

import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-01-07
 */
public interface RoleResourcesService extends IService<RoleResources> {

    Map<String, Object> getRoleResourcesByRoleId(String roleId) throws BusinessException;

    void saveSetting(SaveRoleResourcesDto saveRoleResourcesDto) throws BusinessException;
}
