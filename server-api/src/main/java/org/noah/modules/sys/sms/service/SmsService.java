package org.noah.modules.sys.sms.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.noah.modules.sys.sms.entity.Sms;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuxu
 * @since 2023-02-27
 */
public interface SmsService extends IService<Sms> {

    boolean send(String phone);

    boolean send(String phone, String signName);

    boolean send(String phone, String signName, String templateCode);

    Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, Sms sms);

    void doSend(Sms sms);

    boolean isExpired(String phone, String code);

    Sms getByPhoneAndCode(String phone, String code);
}
