package org.noah.modules.sys.role.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.RoleQueryDto;
import org.noah.modules.sys.role.entity.Role;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2021-12-21
 */
public interface RoleService extends IService<Role> {

    void testAddRole() throws BusinessException;

    List<Role> queryRoleList(String name) throws BusinessException;

    /**
     * 角色分页数据
     * @author Liuxu
     * @date 2024/1/23 11:21
     * @param page 分页参数
     * @param roleQueryDto 查询dto
     * @return com.baomidou.mybatisplus.extension.plugins.pagination.Page<java.util.Map<java.lang.String,java.lang.Object>>
     */
    Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, RoleQueryDto roleQueryDto) throws BusinessException;

    void saveRole(Role role) throws BusinessException;

    void updateRole(Role role) throws BusinessException;

    List<Map<String, Object>> getListUseByUser() throws BusinessException;

    List<String> getRolesCodeByUserId(Integer id);
}
