package org.noah.modules.sys.bs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.sys.bs.entity.Street;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 街道设置 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
public interface StreetService extends IService<Street> {

    List<Map<String, Object>> getListByAreaCode(String code);
}
