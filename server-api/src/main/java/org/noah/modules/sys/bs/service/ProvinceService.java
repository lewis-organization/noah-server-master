package org.noah.modules.sys.bs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.sys.bs.entity.Province;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 省份设置 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
public interface ProvinceService extends IService<Province> {

    List<Map<String, Object>> getList();
}
