package org.noah.modules.sys.test.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.noah.dto.TestFunctionQueryDto;
import org.noah.modules.sys.test.entity.TestFunction;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Liuxu
 * @since 2024-05-28
 */
public interface TestFunctionService extends IService<TestFunction> {

    /**
     * 分页查询
     * @author liuxu
     * @date 2024/5/28 16:08
     * @param page 分页参数
     * @param testFunctionQueryDto 查询参数
     * @return Page<Map < String, Object>>
     */
    Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, TestFunctionQueryDto testFunctionQueryDto);

    /**
     * 新增保存
     * @author liuxu
     * @date 2024/5/28 16:16
     * @param testFunction 保存参数
     */
    void saveObj(TestFunction testFunction);

    /**
     * 修改保存
     * @author liuxu
     * @date 2024/5/28 16:16
     * @param testFunction 保存参数
     */
    void updateObj(TestFunction testFunction);

    /**
     * 提交审核
     * @author liuxu
     * @date 2024/5/30 15:07
     * @param collect id集合
     */
    void submitApprove(List<Integer> collect);

    /**
     * 获取审核记录ID
     * @author liuxu
     * @date 2024/6/17 14:53
     * @param id 业务id参数
     * @return Map<String, Object>
     */
    Map<String, Object> getApproveRecordsId(Integer id);
}
