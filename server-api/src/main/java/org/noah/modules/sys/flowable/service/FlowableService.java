package org.noah.modules.sys.flowable.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.noah.dto.DoApproveDto;
import org.noah.dto.SaveFlowableDto;

import java.util.List;
import java.util.Map;

public interface FlowableService {

    Map<Object, Object> startFlow(String userId);

    List<String> processDefinitionList(Integer page, Integer size);

    List<Map<String, Object>> getTasks(String userId);

    void taskAssigneeComplete(String userId, String idea);

    void deleteProcessInstance(String instanceId, String deleteReason, String nickName);

    byte[] genProcessDiagram(String processId);

    List<String> createHistoricProcessInstanceQuery();

    List<String> createHistoricTaskInstanceQuery();

    Page<Map<String, Object>> getWaitProcessPage(Page<Map<String, Object>> page, String username);

    Page<Map<String, Object>> getHistoryProcessPage(Page<Map<String, Object>> page, String username);

    Page<Map<String, Object>> getHistoryTaskPage(Page<Map<String, Object>> page, String username);

    void saveFlowable(SaveFlowableDto saveFlowableDto);

    void submitAsk(String taskId, String nickName, String name);

    void doApprove(DoApproveDto doApproveDto);
}
