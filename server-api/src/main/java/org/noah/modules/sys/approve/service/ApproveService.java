package org.noah.modules.sys.approve.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.dto.ApproveDto;
import org.noah.dto.ApproveQueryDto;
import org.noah.modules.sys.approve.entity.Approve;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
public interface ApproveService extends IService<Approve> {

    Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, ApproveQueryDto approveQueryDto);

    void saveObj(ApproveDto approveDto);

    void updateObj(ApproveDto approveDto);

    void removeObj(Integer id);

    Approve getByCode(String approveCode);
}
