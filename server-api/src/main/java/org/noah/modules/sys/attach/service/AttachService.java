package org.noah.modules.sys.attach.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.SaveAttachDto;
import org.noah.modules.sys.attach.entity.Attach;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author liuxu
 * @since 2022-01-18
 */
public interface AttachService extends IService<Attach> {

    Map<String, Object> upload(byte[] bytes, String originalFilename, long size, String path) throws BusinessException;

    void saveAttach(SaveAttachDto saveAttachDto) throws BusinessException;

    List<Map<String, Object>> getInfo(String ids) throws BusinessException;

    Attach getAttachById(String id);
}
