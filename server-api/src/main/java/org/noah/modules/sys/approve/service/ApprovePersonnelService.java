package org.noah.modules.sys.approve.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.sys.approve.entity.ApprovePersonnel;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
public interface ApprovePersonnelService extends IService<ApprovePersonnel> {

}
