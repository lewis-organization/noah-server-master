package org.noah.modules.sys.approve.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.noah.modules.sys.approve.entity.ApproveStep;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Liuxu
 * @since 2024-01-24
 */
public interface ApproveStepService extends IService<ApproveStep> {

    List<List<Integer>> getByApproveId(Integer id);
}
