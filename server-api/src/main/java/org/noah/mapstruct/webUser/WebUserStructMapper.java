package org.noah.mapstruct.webUser;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.noah.dto.WebUserDto;
import org.noah.modules.sys.user.entity.WebUser;

@Mapper
public interface WebUserStructMapper {

    WebUserStructMapper INSTANCE = Mappers.getMapper(WebUserStructMapper.class);

    WebUser toWebUser(WebUserDto webUserDto);

    WebUserDto toWebUserDto(WebUser webUser);
}
