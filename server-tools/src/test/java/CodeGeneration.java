import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.Collections;

public class CodeGeneration {

    static final String packageName = "approve";
    static final String tableName = "sys_approve_records";
    static final String tablePrefix = "sys_";
    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://192.168.200.3:3306/noah-server-master?&useSSL=true&useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai","root","123456")
                .globalConfig(builder -> builder.author("Liuxu")
                        //启用swagger
                        //.enableSwagger()
                        //指定输出目录
                        .outputDir("D:\\code1\\src\\main\\java"))
                .packageConfig(builder -> {
                    builder.entity("sys."+packageName+".entity")//实体类包名
                            .parent("org.noah.modules")//父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
                            .controller("sys."+packageName+".controller")//控制层包名
                            .mapper("sys."+packageName+".mapper")//mapper层包名
                            //.other("dto")//生成dto目录 可不用
                            .service("sys."+packageName+".service")//service层包名
                            .serviceImpl("sys."+packageName+".service.impl")
                            .xml("D:\\code1\\src\\main\\resources\\mapper")
                            .pathInfo(Collections.singletonMap(OutputFile.xml,"D:\\code1\\src\\main\\resources\\mapper"));//service实现类包名
                })
                .strategyConfig(builder -> {
                    //设置要生成的表名
                    builder.addInclude(tableName)
                            .addTablePrefix(tablePrefix)//设置表前缀过滤
                            .entityBuilder()
                            .enableLombok()
                            .enableChainModel()
                            .enableActiveRecord()
                            .naming(NamingStrategy.underline_to_camel)//数据表映射实体命名策略：默认下划线转驼峰underline_to_camel
                            .columnNaming(NamingStrategy.underline_to_camel)//表字段映射实体属性命名规则：默认null，不指定按照naming执行
                            .idType(IdType.AUTO)//添加全局主键类型
                            .formatFileName("%s")//格式化实体名称，%s取消首字母I,
                            .enableTableFieldAnnotation()
                            .addIgnoreColumns("id", "create_time", "update_time", "delete_flag", "version")
                            .disableSerialVersionUID()
                            .superClass("org.noah.config.entity.BaseEntity")
                            .mapperBuilder()
                            .formatMapperFileName("%sMapper")//格式化Dao类名称
                            .formatXmlFileName("%sMapper")//格式化xml文件名称
                            .serviceBuilder()
                            .formatServiceFileName("%sService")//格式化 service 接口文件名称
                            .formatServiceImplFileName("%sServiceImpl")//格式化 service 接口文件名称
                            .controllerBuilder()
                            .superClass("org.noah.modules.controller.BaseController");
                }).execute();
    }
}
