# noah-server

 :fa-diamond: 一套通用的快速开发后台管理系统

#### 软件架构
 :fa-diamond: 软件架构说明
- 1. 核心依赖

| 依赖   | 版本 |
|--------------|--------|
| Spring Boot | 2.7.18  |
| Mybatis Plus | 3.5.6  |
| Dubbo | 2.7.8  |
| Shiro| 1.10.1  |
| Druid | 1.2.16  |
- 2. 模块说明

noah-server

├── server-api --- dubbo接口

├── server-app --- 外部接口服务-dubbo消费者

├── server-common --- 公共功能

├── server-dict --- 字典项维护

├── server-domain --- 实体

├── server-entrance --- web前置公共功能

├── server-exception --- 异常封装

├── server-provider --- 接口实现-dubbo提供者

├── server-tools --- 工具包-代码生成

└── server-web --- 后台管理-dubbo消费者





#### 安装教程

1.  项目运行需要jdk1.8、mysql5.7、redis、zookeeper3.4.13环境支持。
2.  将sql文件夹下的`noah-server-master.sql`，`noah-server-quartz.sql`，依次导入数据库中，一个是系统功能表，另一个是定时任务功能管理表。
3.  将项目导入idea中更改server-provider，server-web 目录下application-dev.yml的数据库地址、redis地址、zookeeper连接地址
4. server-web 目录下的application-dev.yml还需要更改静态资源绝对路径![改成你本地的绝对路径](sinfo/static.png)
5. 服务器部署启动以及docker部署方式参考[启动方式.txt](sinfo/启动方式.txt)