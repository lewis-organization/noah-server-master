package org.noah.config.exception;

import config.BaseEnum;
import enumc.AjaxReturnCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 3673328229169147579L;

    private Integer code;  //异常状态码

    private String message;  //异常信息

    private String method;   //发生的方法，位置等

    private String des;   //描述

    public BusinessException(Integer code, String message, String method, String des) {
        this.code = code;
        this.message = message;
        this.method = method;
        this.des = des;
    }

    public BusinessException(BaseEnum baseEnum) {
        this.code = baseEnum.getCode();
        this.message = baseEnum.getMsg();
    }

    public BusinessException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public BusinessException(String message) {
        this.code = AjaxReturnCodeEnum.AJAX_FAIL.getCode();
        this.message = message;
    }

    public BusinessException() {
        this.code = AjaxReturnCodeEnum.AJAX_FAIL.getCode();
        this.message = AjaxReturnCodeEnum.AJAX_FAIL.getMsg();
    }

}
