package org.noah.runner;

import com.baomidou.dynamic.datasource.toolkit.DynamicDataSourceContextHolder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.noah.modules.sys.resources.service.ResourcesService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 项目启动后执行的逻辑
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ApplicationRunnerImpl implements ApplicationRunner {

    private final ResourcesService resourcesService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //缓存菜单信息
        resourcesService.setCache();
        //强制清空本地线程
        DynamicDataSourceContextHolder.clear();
    }
}
