package org.noah.modules.controller;

import org.noah.config.json.AjaxResult;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BaseController implements ErrorController {

    @RequestMapping("/error")
    public AjaxResult error(){
        return AjaxResult.fail("禁止访问");
    }
}
