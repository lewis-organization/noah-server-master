package org.noah.modules.sys.sms.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson2.JSONObject;
import com.aliyun.auth.credentials.Credential;
import com.aliyun.auth.credentials.provider.StaticCredentialProvider;
import com.aliyun.sdk.service.dysmsapi20170525.AsyncClient;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.sdk.service.dysmsapi20170525.models.SendSmsResponse;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import darabonba.core.client.ClientOverrideConfiguration;
import enumc.TrueOrFalseEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.config.sms.SmsConfig;
import org.noah.modules.sys.sms.entity.Sms;
import org.noah.modules.sys.sms.mapper.SmsMapper;
import org.noah.modules.sys.sms.service.SmsService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2023-02-27
 */
@Slf4j
@Component
@DubboService
@RequiredArgsConstructor
public class SmsServiceImpl extends ServiceImpl<SmsMapper, Sms> implements SmsService {

    private final SmsConfig smsConfig;

    @Override
    public boolean send(String phone) {
        return send(phone, smsConfig.getSignName(), smsConfig.getTemplateCode());
    }

    @Override
    public boolean send(String phone, String signName) {
        return send(phone, signName, smsConfig.getTemplateCode());
    }

    @Override
    public boolean send(String phone, String signName, String templateCode) {
        StaticCredentialProvider provider = StaticCredentialProvider.create(Credential.builder()
                .accessKeyId(smsConfig.getAccessKeyId())
                .accessKeySecret(smsConfig.getAccessKeySecret())
                .build());
        AsyncClient client = AsyncClient.builder()
                .region(smsConfig.getRegion()) // Region ID
                .credentialsProvider(provider)
                .overrideConfiguration(
                        ClientOverrideConfiguration.create()
                                .setEndpointOverride("dysmsapi.aliyuncs.com")
                ).build();
        String code = RandomUtil.randomNumbers(6);
        SendSmsRequest sendSmsRequest = SendSmsRequest.builder()
                .signName(signName)
                .templateCode(templateCode)
                .phoneNumbers(phone)
                .templateParam(JSONObject.of("code", code).toString())
                .build();
        CompletableFuture<SendSmsResponse> response = client.sendSms(sendSmsRequest);
        try {
            SendSmsResponse resp = response.get();
            Sms sms = new Sms();
            sms.setPhone(phone);
            sms.setCode(code);
            sms.setExpireTime(LocalDateTime.now().plusMinutes(5));
            sms.setState(TrueOrFalseEnum.TRUE.getValue());
            if (!"OK".equals(resp.getBody().getCode())) {
                sms.setState(TrueOrFalseEnum.FALSE.getValue());
            }
            sms.setReason(resp.getBody().getMessage());
            save(sms);
        } catch (InterruptedException | ExecutionException e) {
            log.error("验证码发送失败", e);
            throw new BusinessException("验证码发送失败");
        }
        return false;
    }

    @Override
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, Sms sms) {
        Page<Map<String, Object>> mapPage = new Page<>();
        IPage<Map<String, Object>> iPage = pageMaps(page, new QueryWrapper<Sms>()
                .lambda()
                .like(StringUtils.isNotBlank(sms.getPhone()), Sms::getPhone, sms.getPhone())
                .like(StringUtils.isNotBlank(sms.getCode()), Sms::getCode, sms.getCode())
                .orderByDesc(Sms::getCreateTime));
        mapPage.setRecords(iPage.getRecords());
        mapPage.setTotal(iPage.getTotal());
        return mapPage;
    }

    @Override
    public void doSend(Sms sms) {
        send(sms.getPhone());
    }

    @Override
    public boolean isExpired(String phone, String code) {
        Sms sms = this.getByPhoneAndCode(phone, code);
        return LocalDateTime.now().isAfter(sms.getExpireTime());
    }

    @Override
    public Sms getByPhoneAndCode(String phone, String code) {
        return getOne(new QueryWrapper<Sms>()
                .lambda()
                .eq(Sms::getPhone, phone)
                .eq(Sms::getCode, code)
                .orderByDesc(Sms::getCreateTime)
                .last("limit 1"));
    }
}
