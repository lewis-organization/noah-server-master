package org.noah.modules.sys.userRole.service.impl;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.SaveUserRoleDto;
import org.noah.modules.sys.role.service.RoleService;
import org.noah.modules.sys.userRole.entity.UserRole;
import org.noah.modules.sys.userRole.mapper.UserRoleMapper;
import org.noah.modules.sys.userRole.service.UserRoleService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2021-12-21
 */
@Slf4j
@DubboService
@Component
@RequiredArgsConstructor
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    private final RoleService roleService;

    @Override
    public Map<String, Object> getUserRoleByUserId(Integer userId) {
        Map<String, Object> resultMap = MapUtil.newHashMap();
        List<Map<String, Object>> roleList = roleService.getListUseByUser();
        List<Integer> checkedIds = this.listMaps(new QueryWrapper<UserRole>()
                .lambda().select(UserRole::getRoleId).eq(UserRole::getWebUserId, userId))
                .stream().map(r -> Integer.valueOf(String.valueOf(r.get("roleId")))).collect(Collectors.toList());
        resultMap.put("list", roleList);
        resultMap.put("checkedIds", checkedIds);
        return resultMap;
    }

    @Override
    public void saveSetting(SaveUserRoleDto saveUserRoleDto) throws BusinessException {
        //保存选中的数据
        UserRole userRole;
        for (Integer id : saveUserRoleDto.getRoleIds()) {
            userRole = new UserRole();
            userRole.setWebUserId(saveUserRoleDto.getUserId());
            userRole.setRoleId(id);
            this.save(userRole);
        }
        //删除取消选中的数据
        for (Integer id : saveUserRoleDto.getCancelIds()) {
            if (!this.remove(new QueryWrapper<UserRole>().lambda()
                    .eq(UserRole::getWebUserId, saveUserRoleDto.getUserId())
                    .eq(UserRole::getRoleId, id))) {
                throw new BusinessException();
            }
        }
    }
}
