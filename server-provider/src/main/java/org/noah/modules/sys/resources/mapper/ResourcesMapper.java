package org.noah.modules.sys.resources.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.noah.modules.sys.resources.entity.Resources;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2021-11-04
 */
public interface ResourcesMapper extends BaseMapper<Resources> {

    /**
     * 获取用户菜单
     *
     * @param userId
     * @return
     */
    List<Resources> getResourcesByUserId(@Param("userId") Integer userId);
}
