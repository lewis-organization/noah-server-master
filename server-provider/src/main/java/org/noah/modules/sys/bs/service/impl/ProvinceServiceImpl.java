package org.noah.modules.sys.bs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.modules.sys.bs.entity.Province;
import org.noah.modules.sys.bs.mapper.ProvinceMapper;
import org.noah.modules.sys.bs.service.ProvinceService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 省份设置 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
@Slf4j
@DubboService
@Component
public class ProvinceServiceImpl extends ServiceImpl<ProvinceMapper, Province> implements ProvinceService {

    @Override
    public List<Map<String, Object>> getList() {
        return listMaps(new QueryWrapper<Province>()
                .lambda().select(Province::getProvinceCode, Province::getProvinceName)
                .orderByAsc(Province::getSort));
    }
}
