package org.noah.modules.sys.org.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.RedisCacheKeyEnum;
import enumc.TrueOrFalseEnum;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.CommonConstant;
import org.noah.dto.WebUserDto;
import org.noah.modules.sys.org.entity.Org;
import org.noah.modules.sys.org.mapper.OrgMapper;
import org.noah.modules.sys.org.service.OrgService;
import org.noah.utils.RedisUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-11-07
 */
@DubboService
@Component
@RequiredArgsConstructor
public class OrgServiceImpl extends ServiceImpl<OrgMapper, Org> implements OrgService {

    private final RedisUtil redisUtil;

    @Override
    public Map<String, Object> getOrgTree(WebUserDto webUserDto) {
        Map<String, Object> resultMap = MapUtil.newHashMap();
        List<Org> resultList;
        if (redisUtil.hasKey(RedisCacheKeyEnum.ORG_CACHE.getKey())) {
            resultList = redisUtil.lGet(RedisCacheKeyEnum.ORG_CACHE.getKey(), 0, -1)
                    .stream().map(o -> (Org) o).collect(Collectors.toList());
        } else {
            resultList = list(new QueryWrapper<Org>()
                    .lambda()
                    .eq(Org::getEnable, TrueOrFalseEnum.TRUE.getValue()));
        }
        String parentId = CommonConstant.ZERO;
        Org org = null;
        if (!webUserDto.isAdmin()) {
            org = resultList.stream().filter(o -> o.getId().equals(webUserDto.getOrgId())).findAny().get();
            parentId = String.valueOf(org.getId());
        }
        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        List<Tree<String>> list = TreeUtil.build(resultList, parentId, treeNodeConfig, (o, tree) -> {
            tree.setId(String.valueOf(o.getId()));
            tree.setParentId(String.valueOf(o.getParentId()));
            tree.putExtra("title", o.getTitle());
            tree.putExtra("spread", true);
            tree.putExtra("checkArr", getCheckType());
        });
        resultMap.put("status", MapUtil.builder().put("code", 200)
                .put("message", "操作成功").map());
        if (!CommonConstant.ZERO.equals(parentId)) {
            List<Map<Object, Object>> finalList = new ArrayList<>();
            finalList.add(MapUtil.builder().put("id", org.getId())
                    .put("parentId", org.getParentId())
                    .put("title", org.getTitle())
                    .put("children", list)
                    .map());
            resultMap.put("data", finalList);
            return resultMap;
        }
        resultMap.put("data", list);
        return resultMap;
    }

    private Map<Object, Object> getCheckType() {
        return MapUtil.builder()
                .put("type", "0")
                .put("checked", "0")
                .map();
    }

    @Override
    public void saveObj(Org org) {
        org.setEnable(TrueOrFalseEnum.TRUE.getValue());
        save(org);
        refresh();
    }

    @Override
    public void updateObj(Org org) {
        updateById(org);
        refresh();
    }

    @Override
    public void refresh() {
        redisUtil.delete(RedisCacheKeyEnum.ORG_CACHE.getKey());
        List<Org> objects = list();
        for (Org o : objects) {
            redisUtil.lSet(RedisCacheKeyEnum.ORG_CACHE.getKey(), o);
        }
    }

    @Override
    public void recursionOrgIds(Org org, List<Org> resultList, List<Integer> orgIds) {
        for (Org o : resultList) {
            if (org.getId().equals(o.getParentId())) {
                orgIds.add(o.getId());
                recursionOrgIds(o, resultList, orgIds);
            }
        }
    }
}
