package org.noah.modules.sys.sms.mapper;

import org.noah.modules.sys.sms.entity.Sms;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2023-02-27
 */
public interface SmsMapper extends BaseMapper<Sms> {

}
