package org.noah.modules.sys.config.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.RedisCacheKeyEnum;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.modules.sys.config.entity.SystemConfig;
import org.noah.modules.sys.config.mapper.SystemConfigMapper;
import org.noah.modules.sys.config.service.SystemConfigService;
import org.noah.utils.RedisUtil;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-09-30
 */
@DubboService
@Component
@RequiredArgsConstructor
public class SystemConfigServiceImpl extends ServiceImpl<SystemConfigMapper, SystemConfig> implements SystemConfigService {

    private final RedisUtil redisUtil;

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void saveSetting(SystemConfig systemConfig) {
        this.saveOrUpdate(systemConfig);
        redisUtil.set(RedisCacheKeyEnum.SYSTEM_CONFIG.getKey(), systemConfig);
    }
}
