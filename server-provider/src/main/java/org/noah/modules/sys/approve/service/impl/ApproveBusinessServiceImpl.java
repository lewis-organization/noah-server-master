package org.noah.modules.sys.approve.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.ApproveBusinessTypeEnum;
import enumc.ApproveStatusEnum;
import enumc.TrueOrFalseEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.config.id.IdGenerator;
import org.noah.dto.ApproveBusinessQueryDto;
import org.noah.modules.sys.approve.entity.*;
import org.noah.modules.sys.approve.mapper.ApproveBusinessMapper;
import org.noah.modules.sys.approve.service.*;
import org.noah.modules.sys.message.service.WebMessageService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
@Slf4j
@Component
@DubboService
@RequiredArgsConstructor
public class ApproveBusinessServiceImpl extends ServiceImpl<ApproveBusinessMapper, ApproveBusiness> implements ApproveBusinessService {

    private final ApproveService approveService;
    private final ApproveStepService approveStepService;
    private final ApprovePersonnelService approvePersonnelService;
    private final ApproveRecordsService approveRecordsService;
    private final ThreadPoolTaskExecutor executor;
    private final IdGenerator idGenerator;
    private final WebMessageService webMessageService;

    /**
     * 获取审批业务分页数据，包含状态和完成情况的描述信息。
     * @author liuxu
     * @date 2024/5/28 15:04
     * @param page 分页参数，包含当前页数和每页数量。
     * @param approveBusinessQueryDto 查询条件对象，包含审批业务的状态等信息。
     * @return Page<Map < String, Object>> 返回封装了查询结果的分页对象，其中包含了状态和完成情况的描述信息。
     */
    @Override
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, ApproveBusinessQueryDto approveBusinessQueryDto) {
        Page<Map<String, Object>> resultPage = new Page<>();
        IPage<Map<String, Object>> iPage = this.pageMaps(page, new QueryWrapper<ApproveBusiness>()
                // 使用Lambda表达式构建查询条件，根据状态查询，如果状态不为空
                .lambda()
                .eq(StringUtils.isNotBlank(approveBusinessQueryDto.getStatus()), ApproveBusiness::getStatus, approveBusinessQueryDto.getStatus())
                .orderByDesc(ApproveBusiness::getCreateTime));

        // 遍历查询结果，添加状态和完成情况的描述信息
        for (Map<String, Object> map : iPage.getRecords()) {
            map.put("statusDes", ApproveStatusEnum.getValueByKey(MapUtil.getStr(map, "status"))); // 添加状态描述
            map.put("completeDes", TrueOrFalseEnum.getValueByKey(MapUtil.getStr(map, "complete"))); // 添加完成情况描述
            map.put("businessTypeDes", ApproveBusinessTypeEnum.getValueByKey(MapUtil.getStr(map, "businessType"))); // 添加业务类型描述
        }

        // 将处理后的查询结果和总条数设置到新的分页对象中
        resultPage.setRecords(iPage.getRecords());
        resultPage.setTotal(iPage.getTotal());

        // 返回封装后的分页对象
        return resultPage;
    }


    /**
     * 根据审核流程编码启动审核流程。
     * 此方法首先通过审核流程编码查询对应的审核流程信息。如果查询结果为空，则不启动任何流程；
     * 如果查询结果存在，则创建一个新的审核业务关联记录，并将该记录与对应的业务和审核流程关联起来，
     * 同时设置初始状态为未提交状态，且标记为未完成，最后保存这个新的审核业务关联记录。
     *
     * @param approveCode 审核流程的唯一编码。
     * @param businessId 与审核流程关联的业务实体的ID。
     * @param businessType 与审核流程关联的业务类型。
     */
    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void startFlow(String approveCode, Integer businessId, String businessType) {
        // 根据审核流程编码查询审核信息
        Approve approve = approveService.getByCode(approveCode);
        // 如果查询结果为空，则不启动审核流程
        if (Objects.isNull(approve)) {
            return;
        }
        // 创建新的审核业务关联实体
        ApproveBusiness approveBusiness = new ApproveBusiness();
        // 设置审核业务关联实体的属性，包括审核ID、业务ID、业务类型、状态和完成情况
        approveBusiness.setApproveId(approve.getId());
        approveBusiness.setBusinessId(businessId);
        approveBusiness.setBusinessType(businessType);
        approveBusiness.setStatus(ApproveStatusEnum.NO_SUBMIT.getState());
        approveBusiness.setComplete(TrueOrFalseEnum.FALSE.getValue());
        // 保存新的审核业务关联实体
        this.save(approveBusiness);
    }


    /**
     * 提交审批业务，为每个审批业务分配审批人员。
     * 此方法负责将特定类型的审批业务设置为等待状态，并根据审批流程为每个业务分配审批人员。
     * 使用事务确保操作的原子性。
     *
     * @param businessType 审批业务的类型。
     * @param collect 审批业务的ID列表。
     * @author liuxu
     * @date 2024/5/30 15:16
     */
    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void submitApprove(String businessType, List<Integer> collect) {
        List<ApproveBusiness> approveBusinessList = new ArrayList<>();

        // 遍历收集的审批业务ID列表，为每个审批业务分配审批人员。
        for (Integer approveBusinessId : collect) {
            // 根据ID获取审批业务详情。
            ApproveBusiness approveBusiness = this.getById(approveBusinessId);
            approveBusiness.setStatus(ApproveStatusEnum.WAIT.getState());
            approveBusiness.setComplete(TrueOrFalseEnum.FALSE.getValue());
            approveBusiness.setBatchCode(idGenerator.getSequence());

            // 查询当前审批业务的第一个审批步骤，用于确定审批人员。
            List<ApproveStep> approveStepList = approveStepService.list(new QueryWrapper<ApproveStep>()
                    .lambda()
                    .eq(ApproveStep::getApproveId, approveBusiness.getApproveId())
                    .orderByAsc(ApproveStep::getSeq)
                    .last("limit 1"));

            // 如果存在审批步骤，则进行审批人员的分配。
            if (CollectionUtil.isNotEmpty(approveStepList)) {
                ApproveStep approveStep = approveStepList.get(0);

                // 根据审批步骤ID查询对应的审批人员列表。
                List<ApprovePersonnel> approvePersonnelList = approvePersonnelService.list(new QueryWrapper<ApprovePersonnel>()
                        .lambda()
                        .eq(ApprovePersonnel::getApproveId, approveBusiness.getApproveId())
                        .eq(ApprovePersonnel::getApproveStepId, approveStep.getId()));

                // 准备审批记录列表和待通知的Web用户ID列表。
                List<ApproveRecords> approveRecordsList = new ArrayList<>();
                List<Integer> webUserIds = new ArrayList<>();
                // 查询并创建审批记录。
                approveRecordsService.queryAndCreateApproveRecords(approveRecordsList, approveBusiness, approvePersonnelList, webUserIds);
                // 保存审批记录。
                approveRecordsService.saveBatch(approveRecordsList);
                // 异步发送审批通知给相关的Web用户。
                executor.execute(() -> webMessageService.sendApproveWebMessage(webUserIds));
            }
            approveBusinessList.add(approveBusiness);
        }
        this.updateBatchById(approveBusinessList);
    }


    @Override
    public void finish(ApproveBusiness approveBusiness) {

    }

    /**
     * 根据业务ID和业务类型获取审批业务的ID。
     * 此方法旨在通过业务ID和业务类型从数据库中检索对应的审批业务实体，并返回该实体的ID。
     * 如果找到匹配的审批业务，则返回其ID；如果没有找到匹配项，则返回null。
     *
     * @param businessId 业务ID，用于唯一标识一个业务实体。
     * @param businessType 业务类型，用于区分不同的业务实体类型。
     * @return 审批业务的ID，如果找不到匹配的业务则返回null。
     */
    @Override
    public Integer getIdByBusinessIdAndBusinessType(Integer businessId, String businessType) {
        // 使用Lambda表达式和QueryWrapper构建查询条件，根据业务ID和业务类型查询数据库
        ApproveBusiness approveBusiness = this.getOne(new QueryWrapper<ApproveBusiness>()
                .lambda()
                .eq(ApproveBusiness::getBusinessId, businessId)
                .eq(ApproveBusiness::getBusinessType, businessType));
        // 返回查询结果中审批业务的ID
        return approveBusiness.getId();
    }


    /**
     * 根据审批业务ID获取审批记录ID。
     *
     * @param approveBusinessId 审批业务的ID，用于查询审批记录。
     * @return 如果找到相关审批记录，则返回第一条记录的ID；如果未找到相关记录或记录列表为空，则返回null。
     */
    @Override
    public Integer getApproveRecordsIdByApproveBusinessId(Integer approveBusinessId) {
        // 使用Lambda表达式和QueryWrapper查询审批记录，条件为审批业务ID等于传入的approveBusinessId
        List<ApproveRecords> approveRecordsList = approveRecordsService.list(new QueryWrapper<ApproveRecords>()
                .lambda()
                .eq(ApproveRecords::getApproveBusinessId, approveBusinessId));

        // 检查查询结果是否为空，如果不为空则返回第一条记录的ID，否则返回null
        if (CollectionUtil.isNotEmpty(approveRecordsList)) {
            return approveRecordsList.get(0).getId();
        }
        return null;
    }


}
