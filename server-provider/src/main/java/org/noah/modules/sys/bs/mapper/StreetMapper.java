package org.noah.modules.sys.bs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.bs.entity.Street;

/**
 * <p>
 * 街道设置 Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
public interface StreetMapper extends BaseMapper<Street> {

}
