package org.noah.modules.sys.org.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.org.entity.Org;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2022-11-07
 */
public interface OrgMapper extends BaseMapper<Org> {

}
