package org.noah.modules.sys.message.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.TrueOrFalseEnum;
import enumc.WebMessageTypeEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.WebMessageQueryDto;
import org.noah.modules.sys.message.entity.WebMessage;
import org.noah.modules.sys.message.mapper.WebMessageMapper;
import org.noah.modules.sys.message.service.WebMessageService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.modules.sys.user.service.WebUserService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-01-19
 */
@Slf4j
@DubboService
@Component
@RequiredArgsConstructor
public class WebMessageServiceImpl extends ServiceImpl<WebMessageMapper, WebMessage> implements WebMessageService {

    private static final List<String> avatarList = new ArrayList<>();
    private final WebUserService webUserService;

    @Override
    public List<Map<String, Object>> getMessageInfo(Map<String, Object> paramsMap) throws BusinessException {
        List<Map<String, Object>> resultList = new ArrayList<>();
        if (avatarList.isEmpty()) {
            String catalogue = "/static/admin/images/";
            avatarList.add(paramsMap.get("path") + catalogue + "m1.png");
            avatarList.add(paramsMap.get("path") + catalogue + "m2.png");
            avatarList.add(paramsMap.get("path") + catalogue + "m2.png");
        }
        for (WebMessageTypeEnum webMessageTypeEnum : WebMessageTypeEnum.values()) {
            Map<String, Object> map = MapUtil.newHashMap();
            map.put("id", webMessageTypeEnum.getType());
            map.put("title", webMessageTypeEnum.getDes());
            map.put("children", this.listMaps(new QueryWrapper<WebMessage>()
                    .lambda().select(WebMessage::getId, WebMessage::getTitle, WebMessage::getContent, WebMessage::getFromUser,
                            WebMessage::getCreateTime)
                    .eq(WebMessage::getDisplay, TrueOrFalseEnum.FALSE.getValue())
                    .eq(WebMessage::getReadFlag, TrueOrFalseEnum.FALSE.getValue())
                    .eq(WebMessage::getType, webMessageTypeEnum.getType())
                    .and(w -> w.eq(WebMessage::getReceiver, MapUtil.getStr(paramsMap, "userId"))
                            .or().eq(WebMessage::getOrgId, MapUtil.getStr(paramsMap, "orgId")))
                    .orderByDesc(WebMessage::getCreateTime).last("limit 10")).stream().peek(m -> {
                m.put("avatar", RandomUtil.randomEle(avatarList));
                m.put("time", this.dateDifferenceDesc(LocalDateTime.now(), LocalDateTimeUtil.parse(String.valueOf(m.get("createTime")))));
                m.put("form", m.get("fromUser"));
                m.put("context", m.get("content"));
            }).collect(Collectors.toList()));
            resultList.add(map);
        }
        return resultList;
    }

    @Override
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, WebMessageQueryDto webMessageQueryDto) throws BusinessException {
        Page<Map<String, Object>> mapPage = new Page<>();
        IPage<Map<String, Object>> iPage = this.pageMaps(page, new QueryWrapper<WebMessage>()
                .lambda().like(StringUtils.isNotBlank(webMessageQueryDto.getTitle()), WebMessage::getTitle, webMessageQueryDto.getTitle())
                .eq(StringUtils.isNotBlank(webMessageQueryDto.getType()), WebMessage::getType, webMessageQueryDto.getType())
                .eq(StringUtils.isNotBlank(webMessageQueryDto.getDisplay()), WebMessage::getDisplay, webMessageQueryDto.getDisplay())
                .eq(!webMessageQueryDto.isAdmin(), WebMessage::getReceiver, webMessageQueryDto.getUserId())
                .orderByDesc(WebMessage::getCreateTime));
        WebUser webUser;
        for (Map<String, Object> map : iPage.getRecords()) {
            map.put("typeDes", WebMessageTypeEnum.getValueByKey(String.valueOf(map.get("type"))));
            webUser = webUserService.getById(MapUtil.getInt(map, "receiver"));
            if (Objects.nonNull(webUser)) {
                map.put("receiver", webUser.getNickName());
            }
        }
        mapPage.setRecords(iPage.getRecords());
        mapPage.setTotal(iPage.getTotal());
        return mapPage;
    }

    @Override
    public void saveEntity(WebMessage webMessage) throws BusinessException {
        webMessage.setDisplay(TrueOrFalseEnum.FALSE.getValue());
        webMessage.setReadFlag(TrueOrFalseEnum.FALSE.getValue());
        webMessage.getUserIds().forEach(i -> {
            if (i < 0) {
                webMessage.setId(null);
                webMessage.setReceiver(-i);
                this.save(webMessage);
            }
        });
    }

    @Override
    public void updateEntity(WebMessage webMessage) throws BusinessException {
        this.updateById(webMessage);
    }

    @Override
    public void changeState(String id, String state) {
        WebMessage webMessage = new WebMessage();
        if (TrueOrFalseEnum.TRUE.getValue().equals(state)) {
            webMessage.setDisplay(TrueOrFalseEnum.FALSE.getValue());
        } else if (TrueOrFalseEnum.FALSE.getValue().equals(state)) {
            webMessage.setDisplay(TrueOrFalseEnum.TRUE.getValue());
        }
        this.update(webMessage,
                new QueryWrapper<WebMessage>().lambda().eq(WebMessage::getId, id));
    }

    /**
     * 全标已读
     * @author Liuxu
     * @date 2024/1/23 12:08
     * @param id id
     */
    @Override
    public void allRead(Integer id) {
        this.update(new WebMessage().setReadFlag(TrueOrFalseEnum.TRUE.getValue()), new QueryWrapper<WebMessage>()
                .lambda().eq(WebMessage::getReceiver, id).eq(WebMessage::getReadFlag, TrueOrFalseEnum.FALSE.getValue()));
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void sendApproveWebMessage(List<Integer> webUserIds) {
        List<WebMessage> webMessageList = new ArrayList<>();
        for (Integer webUserId : webUserIds) {
            WebMessage webMessage = new WebMessage();
            webMessage.setReceiver(webUserId);
            webMessage.setDisplay(TrueOrFalseEnum.FALSE.getValue());
            webMessage.setTitle("审核通知");
            webMessage.setReadFlag(TrueOrFalseEnum.FALSE.getValue());
            webMessage.setContent("您有新的审核任务，请及时处理。");
            webMessage.setType(WebMessageTypeEnum.WAIT.getType());
            webMessageList.add(webMessage);
        }
        this.saveBatch(webMessageList);
    }

    private String dateDifferenceDesc(LocalDateTime now, LocalDateTime early) {
        String res = "";
        if (null != now && null != early) {
            long preTime = LocalDateTimeUtil.between(early, now).toMillis();
            if (preTime < 60000L) {
                res = "刚刚";
            } else if (preTime < 3600000L) {
                res = (preTime / 60000L) + "分钟前";
            } else if (preTime < 86400000L) {
                res = (preTime / 3600000L) + "小时前";
            } else if (preTime < 172800000L) {
                res = "昨天";
            } else if (preTime < 259200000L) {
                res = "前天";
            } else if (preTime < 31536000000L) {
                res = (preTime / 86400000L) + "天前";
            } else {
                res = (preTime / 31536000000L) + "年前";
            }
        }
        return res;
    }
}
