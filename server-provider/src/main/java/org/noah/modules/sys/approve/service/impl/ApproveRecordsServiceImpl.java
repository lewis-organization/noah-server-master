package org.noah.modules.sys.approve.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.ApproveBusinessTypeEnum;
import enumc.ApproveStatusEnum;
import enumc.TrueOrFalseEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.config.id.IdGenerator;
import org.noah.dto.ApproveRecordsQueryDto;
import org.noah.modules.sys.approve.entity.ApproveBusiness;
import org.noah.modules.sys.approve.entity.ApprovePersonnel;
import org.noah.modules.sys.approve.entity.ApproveRecords;
import org.noah.modules.sys.approve.entity.ApproveStep;
import org.noah.modules.sys.approve.mapper.ApproveRecordsMapper;
import org.noah.modules.sys.approve.service.ApproveBusinessService;
import org.noah.modules.sys.approve.service.ApprovePersonnelService;
import org.noah.modules.sys.approve.service.ApproveRecordsService;
import org.noah.modules.sys.approve.service.ApproveStepService;
import org.noah.modules.sys.message.service.WebMessageService;
import org.noah.modules.sys.org.entity.Org;
import org.noah.modules.sys.org.service.OrgService;
import org.noah.modules.sys.test.entity.TestFunction;
import org.noah.modules.sys.test.service.TestFunctionService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.modules.sys.user.service.WebUserService;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author Liuxu
 * @since 2024-05-31
 */
@Slf4j
@Component
@DubboService
@RequiredArgsConstructor(onConstructor_ = {@Lazy})
public class ApproveRecordsServiceImpl extends ServiceImpl<ApproveRecordsMapper, ApproveRecords> implements ApproveRecordsService {

    private final ApproveBusinessService approveBusinessService;
    private final ApprovePersonnelService approvePersonnelService;
    private final ApproveStepService approveStepService;
    private final WebUserService webUserService;
    private final TestFunctionService testFunctionService;
    private final IdGenerator idGenerator;
    private final WebMessageService webMessageService;
    private final OrgService orgService;
    private final ThreadPoolTaskExecutor executor;

    /**
     * 根据查询条件获取审批记录的分页结果。
     * 本方法通过调用approveRecordsService的getCustomPage方法，实现根据特定查询条件对审批记录进行分页查询。
     *
     * @param page                   分页对象，包含当前页码、每页大小等信息，同时用于存放查询结果。
     * @param approveRecordsQueryDto 查询条件对象，封装了所有可用于筛选审批记录的条件。
     * @return Page<Map < String, Object>> 返回查询后的分页结果，包含符合条件的审批记录集合及分页信息。
     * @author liuxu
     * @date 2024/6/3 09:26
     */
    @Override
    public Page<Map<String, Object>> getCustomPage(Page<Map<String, Object>> page, ApproveRecordsQueryDto approveRecordsQueryDto) {
        Page<Map<String, Object>> resultPage = new Page<>();
        IPage<Map<String, Object>> iPage = this.baseMapper.getCustomPage(page, approveRecordsQueryDto);
        for (Map<String, Object> map : iPage.getRecords()) {
            map.put("businessTypeDes", ApproveBusinessTypeEnum.getValueByKey(MapUtil.getStr(map, "businessType")));
            map.put("ideaDes", ApproveStatusEnum.getValueByKey(MapUtil.getStr(map, "idea"))); // 添加状态描述
            map.put("completeDes", TrueOrFalseEnum.getValueByKey(MapUtil.getStr(map, "complete"))); // 添加完成情况描述
        }
        resultPage.setRecords(iPage.getRecords());
        resultPage.setTotal(iPage.getTotal());
        return resultPage;
    }

    /**
     * 执行审批操作。
     * 根据审批记录ID集合，对审批记录进行更新，并根据审批结果（通过或拒绝）执行相应的业务逻辑。
     * 如果审批结果为拒绝，则仅更新审批记录状态。
     * 如果审批结果为通过，则还需要更新审批流程中下一个审批人的状态，并发送通知。
     *
     * @param approveRecordsDto 审批记录的DTO（数据传输对象），包含审批结果及相关信息。
     */
    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void doApprove(ApproveRecords approveRecordsDto) {
        // 将审批记录ID字符串转换为ID数组
        String[] approveRecordsIds = approveRecordsDto.getApproveRecordsIds().split(",");
        // 根据ID数组查询对应的审批记录
        List<ApproveRecords> approveRecordsList = this.list(new QueryWrapper<ApproveRecords>()
                .lambda().in(ApproveRecords::getId, CollectionUtil.toList(approveRecordsIds)));
        List<String> batchCodes = approveRecordsList.stream().map(ApproveRecords::getBatchCode).collect(Collectors.toList());
        // 判断审批结果是通过还是拒绝
        if (ApproveStatusEnum.REJECT.getState().equals(approveRecordsDto.getIdea())) {
            // 如果审批结果为拒绝，更新审批记录状态并处理业务状态
            List<ApproveBusiness> approveBusinessList = approveBusinessService.list(new QueryWrapper<ApproveBusiness>()
                    .lambda().in(ApproveBusiness::getId, approveRecordsList.stream().map(ApproveRecords::getApproveBusinessId).collect(Collectors.toList())));
            for (ApproveRecords approveRecords : approveRecordsList) {
                // 更新审批记录信息
                approveRecords.setIdea(approveRecordsDto.getIdea());
                approveRecords.setRemark(approveRecordsDto.getRemark());
                approveRecords.setApprove(approveRecordsDto.getWebUserId().equals(approveRecords.getUserId()) ?
                        TrueOrFalseEnum.TRUE.getValue() : TrueOrFalseEnum.FALSE.getValue());
            }
            // 批量保存或更新审批记录
            this.saveOrUpdateBatch(approveRecordsList);
            // 更新业务状态
            changeBusinessStatus(approveBusinessList, approveRecordsDto.getIdea());
        } else {
            // 如果审批结果为通过，更新审批记录状态，并根据审批流程推进处理后续审批人
            List<ApproveRecords> approveRecordsSaveList = new ArrayList<>();
            for (ApproveRecords approveRecords : approveRecordsList) {
                // 更新审批记录信息
                approveRecords.setIdea(approveRecordsDto.getIdea());
                approveRecords.setRemark(approveRecordsDto.getRemark());
                approveRecords.setApprove(approveRecordsDto.getWebUserId().equals(approveRecords.getUserId()) ?
                        TrueOrFalseEnum.TRUE.getValue() : TrueOrFalseEnum.FALSE.getValue());

                // 查询当前审批人员信息
                ApprovePersonnel approvePersonnel = approvePersonnelService.getById(approveRecords.getApprovePersonnelId());
                if (Objects.nonNull(approvePersonnel)) {
                    // 查询当前审批人员后的审批步骤
                    List<ApproveStep> approveStepList = approveStepService.list(new QueryWrapper<ApproveStep>()
                            .lambda()
                            .eq(ApproveStep::getApproveId, approvePersonnel.getApproveId())
                            .orderByAsc(ApproveStep::getSeq));
                    // 计算当前审批人员所在的步骤索引
                    int approveStepIndex = approveStepList.indexOf(approveStepService.getById(approvePersonnel.getApproveStepId()));
                    if ((approveStepIndex + 1) < approveStepList.size()) {
                        // 如果还有后续审批人，处理后续审批人
                        ApproveStep approveStep = approveStepList.get(approveStepIndex + 1);
                        List<ApprovePersonnel> approvePersonnelList = approvePersonnelService.list(new QueryWrapper<ApprovePersonnel>()
                                .lambda()
                                .eq(ApprovePersonnel::getApproveId, approvePersonnel.getApproveId())
                                .eq(ApprovePersonnel::getApproveStepId, approveStep.getId())
                                .orderByAsc(ApprovePersonnel::getSeq));
                        List<Integer> webUserIds = new ArrayList<>();
                        // 处理后续审批人并发送通知
                        ApproveBusiness approveBusiness = approveBusinessService.getById(approveRecords.getApproveBusinessId());
                        queryAndCreateApproveRecords(approveRecordsSaveList, approveBusiness, approvePersonnelList, webUserIds);
                        executor.execute(() -> webMessageService.sendApproveWebMessage(webUserIds));
                    } else {
                        // 如果没有后续审批人，更新业务状态
                        List<ApproveBusiness> approveBusinessList = approveBusinessService.list(new QueryWrapper<ApproveBusiness>()
                                .lambda().eq(ApproveBusiness::getId, approveRecords.getApproveBusinessId()));
                        changeBusinessStatus(approveBusinessList, approveRecordsDto.getIdea());
                    }
                }
            }
            // 批量更新已处理的审批记录
            this.updateBatchById(approveRecordsList);
            // 批量保存新的审批记录
            this.saveBatch(approveRecordsSaveList);
        }
        // 更新相同批次码的审批记录状态
        this.update(new ApproveRecords().setIdea(approveRecordsDto.getIdea()),
                new QueryWrapper<ApproveRecords>()
                        .lambda().in(ApproveRecords::getBatchCode, batchCodes));
    }

    /**
     * 根据审批人员列表和业务ID，生成审批记录。
     * 如果审批人员随机同意为否，则为该审批人员生成一条审批记录。
     * 如果审批人员随机同意为是，则为该审批人员所在组织的所有用户生成审批记录。
     *
     * @param approveRecordsList 审批记录列表，用于添加新生成的审批记录。
     * @param approveBusiness  审批业务信息
     * @param approvePersonnelList 审批人员列表，用于遍历并生成审批记录。
     * @param webUserIds 用户ID列表，用于添加参与审批的用户ID。
     */
    @Override
    public void queryAndCreateApproveRecords(List<ApproveRecords> approveRecordsList, ApproveBusiness approveBusiness,
                                             List<ApprovePersonnel> approvePersonnelList, List<Integer> webUserIds) {
        // 生成批次代码，用于标识同一批次的审批记录
        String batchCode = idGenerator.getSequence();

        // 遍历审批人员列表
        for (ApprovePersonnel ap : approvePersonnelList) {
            // 判断审批人员的随机同意状态
            if (ap.getRandomAgree().equals(TrueOrFalseEnum.FALSE.getValue())) {
                // 如果随机同意为否，为该审批人员生成一条审批记录
                ApproveRecords ard = new ApproveRecords();
                // 设置审批记录的相关信息
                ard.setApproveBusinessId(approveBusiness.getId());
                ard.setApprovePersonnelId(ap.getId());
                ard.setOrgId(ap.getOrgId());
                ard.setUserId(ap.getUserId());
                ard.setRandomAgree(TrueOrFalseEnum.FALSE.getValue());
                ard.setApprove(TrueOrFalseEnum.FALSE.getValue());
                ard.setIdea(ApproveStatusEnum.WAIT.getState());
                ard.setBatchCode(batchCode);
                ard.setApproveBusinessBatchCode(approveBusiness.getBatchCode());
                // 添加审批记录到列表
                approveRecordsList.add(ard);
                // 将该审批人员的用户ID添加到用户ID列表
                webUserIds.add(ap.getUserId());
            } else {
                // 如果随机同意为是，查询该审批人员所在组织的所有用户
                List<Integer> userIds = webUserService.list(new QueryWrapper<WebUser>()
                                .lambda()
                                .eq(WebUser::getOrgId, ap.getOrgId()))
                        .stream()
                        .map(WebUser::getId)
                        .collect(Collectors.toList());

                // 遍历组织用户列表，为每个用户生成审批记录
                for (Integer userId : userIds) {
                    ApproveRecords ards = new ApproveRecords();
                    // 设置审批记录的相关信息
                    ards.setApproveBusinessId(approveBusiness.getId());
                    ards.setApprovePersonnelId(ap.getId());
                    ards.setOrgId(ap.getOrgId());
                    ards.setUserId(userId);
                    ards.setRandomAgree(TrueOrFalseEnum.TRUE.getValue());
                    ards.setApprove(TrueOrFalseEnum.FALSE.getValue());
                    ards.setIdea(ApproveStatusEnum.WAIT.getState());
                    ards.setBatchCode(batchCode);
                    ards.setApproveBusinessBatchCode(approveBusiness.getBatchCode());
                    // 添加审批记录到列表
                    approveRecordsList.add(ards);
                }
                // 将组织的所有用户ID添加到用户ID列表
                webUserIds.addAll(userIds);
            }
        }
    }

    /**
     * 根据ID获取审批流程信息。
     * 该方法通过审批记录ID，查询并组装整个审批流程的详细信息，包括每个审批步骤的审批人（或组织）、审批时间、审批意见等。
     *
     * @param id 审批记录ID，用于查询具体的审批记录。
     * @return 返回一个列表，每个元素代表一个审批步骤的信息，包括审批人（或组织）、审批时间、审批意见等。
     */
    @Override
    public List<Map<String, Object>> getApproveFlowInfo(Integer id) {
        // 初始化结果列表
        List<Map<String, Object>> resultList = new ArrayList<>();

        // 根据ID查询审批记录
        ApproveRecords approveRecords = this.getById(id);
        // 如果审批记录存在
        if (Objects.nonNull(approveRecords)) {
            // 根据审批记录中的审批人员ID查询审批人员信息
            ApprovePersonnel approvePersonnel = approvePersonnelService.getById(approveRecords.getApprovePersonnelId());
            // 如果审批人员信息存在
            if (Objects.nonNull(approvePersonnel)) {
                // 查询该审批人员所有的审批步骤，并按序列号升序排列
                List<ApproveStep> approveStepList = approveStepService.list(new QueryWrapper<ApproveStep>()
                        .lambda()
                        .eq(ApproveStep::getApproveId, approvePersonnel.getApproveId())
                        .orderByAsc(ApproveStep::getSeq));
                // 遍历每个审批步骤
                for (ApproveStep as : approveStepList) {
                    // 初始化每个审批步骤的信息映射
                    Map<String, Object> map = MapUtil.newHashMap();
                    // 初始化默认值
                    map.put("randomAgree", StringUtils.EMPTY);
                    map.put("approverName", StringUtils.EMPTY);
                    map.put("approveTime", StringUtils.EMPTY);
                    map.put("idea", StringUtils.EMPTY);
                    // 查询该审批步骤中的所有审批人员（或组织）
                    List<ApprovePersonnel> approvePersonnelList = approvePersonnelService.list(new QueryWrapper<ApprovePersonnel>()
                            .lambda()
                            .select(ApprovePersonnel::getRandomAgree, ApprovePersonnel::getUserId,
                                    ApprovePersonnel::getOrgId, ApprovePersonnel::getId)
                            .eq(ApprovePersonnel::getApproveId, approvePersonnel.getApproveId())
                                    .eq(ApprovePersonnel::getApproveStepId, as.getId())
                            .orderByAsc(ApprovePersonnel::getSeq));
                    // 遍历每个审批人员（或组织）
                    for (ApprovePersonnel ap : approvePersonnelList) {
                        // 如果是随机分配的个人审批
                        if (TrueOrFalseEnum.FALSE.getValue().equals(ap.getRandomAgree())) {
                            // 查询对应的用户信息
                            WebUser webUser = webUserService.getById(ap.getUserId());
                            // 如果用户信息存在
                            if (Objects.nonNull(webUser)) {
                                // 更新审批人名称
                                map.put("approverName", MapUtil.getStr(map, "approverName") + "[用户]" + webUser.getNickName() + "<br>");
                            }
                        } else {
                            // 如果是组织审批
                            Org org = orgService.getById(ap.getOrgId());
                            // 如果组织信息存在
                            if (Objects.nonNull(org)) {
                                // 更新审批人名称
                                map.put("approverName", MapUtil.getStr(map, "approverName") + "[组织]" + org.getTitle() + "<br>");
                            }
                        }
                    }
                    // 如果存在审批人员（或组织）
                    if (CollectionUtil.isNotEmpty(approvePersonnelList)) {
                        // 查询对应的审批记录
                        List<Map<String, Object>> approveRecordsList = this.listMaps(new QueryWrapper<ApproveRecords>()
                                .lambda()
                                .select(ApproveRecords::getUpdateTime, ApproveRecords::getIdea, ApproveRecords::getApprove)
                                .in(ApproveRecords::getApprovePersonnelId,
                                        approvePersonnelList.stream().map(ApprovePersonnel::getId).collect(Collectors.toList()))
                                .eq(ApproveRecords::getApproveBusinessId, approveRecords.getApproveBusinessId())
                                .eq(ApproveRecords::getApproveBusinessBatchCode, approveRecords.getApproveBusinessBatchCode()));
                        // 查找第一个通过的审批记录
                        Optional<Map<String, Object>> approveRecordsOptional = approveRecordsList.stream()
                                .filter(m -> TrueOrFalseEnum.TRUE.getValue().equals(MapUtil.getStr(m, "approve"))).findFirst();
                        // 如果找到通过的审批记录
                        if (approveRecordsOptional.isPresent()) {
                            // 更新审批时间和意见
                            map.put("approveTime", DateUtil.parseDate(MapUtil.getStr(approveRecordsOptional.get(), "updateTime")));
                            map.put("idea", MapUtil.getStr(approveRecordsOptional.get(), "idea"));
                        } else {
                            // 如果没有通过的审批记录，则标记为等待状态
                            map.put("idea", ApproveStatusEnum.WAIT.getState());
                        }
                    }
                    // 将当前审批步骤的信息添加到结果列表
                    resultList.add(map);
                }
            }
        }
        // 返回完整的审批流程信息列表
        return resultList;
    }



    @Transactional(rollbackFor = BusinessException.class)
    public void changeBusinessStatus(List<ApproveBusiness> approveBusinessList, String idea) {
        List<TestFunction> testFunctionList = new ArrayList<>();
        for (ApproveBusiness approveBusiness : approveBusinessList) {
            approveBusiness.setStatus(idea);
            approveBusiness.setComplete(TrueOrFalseEnum.TRUE.getValue());
            switch (ApproveBusinessTypeEnum.getApproveBusinessTypeEnum(approveBusiness.getBusinessType())) {
                case TEST:
                    TestFunction testFunction = new TestFunction();
                    testFunction.setId(approveBusiness.getBusinessId());
                    testFunction.setStatus(idea);
                    testFunctionList.add(testFunction);
                    break;
                case OTHER:
                    System.out.println("其他业务审核");
                    break;
                default:
                    break;
            }
        }
        approveBusinessService.updateBatchById(approveBusinessList);
        if (CollectionUtil.isNotEmpty(testFunctionList)) {
            testFunctionService.updateBatchById(testFunctionList);
        }
        //补充其他流程
    }
}
