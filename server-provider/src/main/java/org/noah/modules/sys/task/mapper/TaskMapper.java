package org.noah.modules.sys.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.task.entity.Task;

/**
 * <p>
 * 系统任务 Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2022-01-13
 */
public interface TaskMapper extends BaseMapper<Task> {

}
