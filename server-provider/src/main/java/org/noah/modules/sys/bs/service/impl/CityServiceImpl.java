package org.noah.modules.sys.bs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.modules.sys.bs.entity.City;
import org.noah.modules.sys.bs.mapper.CityMapper;
import org.noah.modules.sys.bs.service.CityService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 城市设置 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
@Slf4j
@DubboService
@Component
public class CityServiceImpl extends ServiceImpl<CityMapper, City> implements CityService {

    @Override
    public List<Map<String, Object>> getListByProvinceCode(String code) {
        return listMaps(new QueryWrapper<City>()
                .lambda().select(City::getCityCode, City::getCityName)
                .eq(City::getProvinceCode, code)
                .orderByAsc(City::getSort));
    }
}
