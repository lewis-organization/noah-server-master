package org.noah.modules.sys.approve.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.modules.sys.approve.entity.ApprovePersonnel;
import org.noah.modules.sys.approve.mapper.ApprovePersonnelMapper;
import org.noah.modules.sys.approve.service.ApprovePersonnelService;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
@Slf4j
@Component
@DubboService
public class ApprovePersonnelServiceImpl extends ServiceImpl<ApprovePersonnelMapper, ApprovePersonnel> implements ApprovePersonnelService {

}
