package org.noah.modules.sys.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.log.entity.Log;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2022-04-15
 */
public interface LogMapper extends BaseMapper<Log> {

}
