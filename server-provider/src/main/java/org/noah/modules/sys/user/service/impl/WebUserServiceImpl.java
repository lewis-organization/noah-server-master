package org.noah.modules.sys.user.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.RedisCacheKeyEnum;
import enumc.TrueOrFalseEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.CommonConstant;
import org.noah.config.exception.BusinessException;
import org.noah.dto.WebUserChangePasswordDto;
import org.noah.dto.WebUserDto;
import org.noah.dto.WebUserQueryDto;
import org.noah.mapstruct.webUser.WebUserStructMapper;
import org.noah.modules.sys.org.entity.Org;
import org.noah.modules.sys.org.service.OrgService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.modules.sys.user.mapper.WebUserMapper;
import org.noah.modules.sys.user.service.WebUserService;
import org.noah.utils.RedisUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2021-11-01
 */
@Slf4j
@DubboService(version = "1.0.0")
@Component
@RequiredArgsConstructor
public class WebUserServiceImpl extends ServiceImpl<WebUserMapper, WebUser> implements WebUserService {

    private final RedisUtil redisUtil;
    private final OrgService orgService;

    /**
     * 查询用户根据用户名
     * @author Liuxu
     * @date 2024/1/23 12:11
     * @param username 用户名
     * @return org.noah.modules.sys.user.entity.WebUser
     */
    @Override
    public WebUser getWebUserByUsername(String username) {
        return getOne(new QueryWrapper<WebUser>()
                .lambda().select(WebUser::getId, WebUser::getSalt, WebUser::getPassword, WebUser::getUsername,
                        WebUser::getType, WebUser::getEnable, WebUser::getOrgId,
                        WebUser::getNickName).eq(WebUser::getUsername, username));
    }

    @Override
    public void test(String id) {
        int a = Integer.parseInt(id) / 0;
    }

    @Override
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, WebUserQueryDto webUserQueryDto) {
        Page<Map<String, Object>> mapPage = new Page<>();
        IPage<Map<String, Object>> iPage = pageMaps(page, new QueryWrapper<WebUser>()
                .lambda()
                .eq(Objects.nonNull(webUserQueryDto.getOrgId()), WebUser::getOrgId, webUserQueryDto.getOrgId())
                .like(StringUtils.isNotBlank(webUserQueryDto.getUsername()), WebUser::getUsername, webUserQueryDto.getUsername())
                .like(StringUtils.isNotBlank(webUserQueryDto.getNickName()), WebUser::getNickName, webUserQueryDto.getNickName())
                .eq(StringUtils.isNotBlank(webUserQueryDto.getType()), WebUser::getType, webUserQueryDto.getType())
                .eq(StringUtils.isNotBlank(webUserQueryDto.getEnable()), WebUser::getEnable, webUserQueryDto.getEnable()));
        mapPage.setTotal(iPage.getTotal());
        mapPage.setRecords(iPage.getRecords());
        return mapPage;
    }

    @Override
    public void saveEntity(WebUserDto webUserDto) {
        if (count(new QueryWrapper<WebUser>()
                .lambda().eq(WebUser::getUsername, webUserDto.getUsername())) > 0) {
            throw new BusinessException("用户名已经存在了");
        }
        webUserDto.setEnable(TrueOrFalseEnum.TRUE.getValue());
        WebUser webUser = WebUserStructMapper.INSTANCE.toWebUser(webUserDto);
        save(webUser);
    }

    @Override
    public void updateEntity(WebUserDto webUserDto) {
        WebUser validUser = getOne(new QueryWrapper<WebUser>()
                .lambda().eq(WebUser::getUsername, webUserDto.getUsername()));
        if (null != validUser) {
            if (!webUserDto.getId().toString().equals(validUser.getId().toString())) {
                throw new BusinessException("用户名已经存在了");
            }
        }
        WebUser webUser = WebUserStructMapper.INSTANCE.toWebUser(webUserDto);
        updateById(webUser);
    }

    @Override
    public String getRandomStr() {
        String randomString;
        do {
            randomString = RandomUtil.randomString(5);
        } while (count(new QueryWrapper<WebUser>().lambda().eq(WebUser::getSalt, randomString)) > 0);
        return randomString;
    }

    @Override
    public WebUser getUserUseByUserRole(String userId) throws BusinessException {
        return getOne(new QueryWrapper<WebUser>()
                .lambda().select(WebUser::getId, WebUser::getNickName).eq(WebUser::getId, userId));
    }

    @Override
    public void changeState(String id, String state) throws BusinessException {
        WebUser webUser = new WebUser();
        if (TrueOrFalseEnum.TRUE.getValue().equals(state)) {
            webUser.setEnable(TrueOrFalseEnum.TRUE.getValue());
        } else if (TrueOrFalseEnum.FALSE.getValue().equals(state)) {
            webUser.setEnable(TrueOrFalseEnum.FALSE.getValue());
        }
        update(webUser,
                new QueryWrapper<WebUser>().lambda().eq(WebUser::getId, id));
    }

    @Override
    public void changePassword(WebUserChangePasswordDto webUserChangePasswordDto) throws BusinessException {
        WebUser webUser = new WebUser().setPassword(webUserChangePasswordDto.getPassword());
        update(webUser, new QueryWrapper<WebUser>()
                .lambda().eq(WebUser::getId, webUserChangePasswordDto.getId()));
    }

    @Override
    public void lockedAccount(String username) {
        update(new WebUser().setEnable(TrueOrFalseEnum.FALSE.getValue()),
                new QueryWrapper<WebUser>().lambda().eq(WebUser::getUsername, username));
    }

    /**
     * 根据组织湖区用户列表
     * @author Liuxu
     * @date 2024/1/23 12:12
     * @param orgId 组织id
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    @Override
    public List<Map<String, Object>> getByOrgId(String orgId) {
        return this.listMaps(new QueryWrapper<WebUser>().lambda()
                .select(WebUser::getId, WebUser::getNickName).eq(WebUser::getOrgId, orgId));
    }

    /**
     * 获取用户组织树
     * @author Liuxu
     * @date 2024/1/23 12:12
     * @param webUserDto dto
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */
    @Override
    public Map<String, Object> getOrgAndUserTree(WebUserDto webUserDto) {
        Map<String, Object> resultMap = MapUtil.newHashMap();
        List<Org> resultList;
        if (redisUtil.hasKey(RedisCacheKeyEnum.ORG_CACHE.getKey())) {
            resultList = redisUtil.lGet(RedisCacheKeyEnum.ORG_CACHE.getKey(), 0, -1)
                    .stream().map(o -> (Org) o).collect(Collectors.toList());
        } else {
            resultList = orgService.list(new QueryWrapper<Org>()
                    .lambda()
                    .eq(Org::getEnable, TrueOrFalseEnum.TRUE.getValue()));
        }
        String parentId = CommonConstant.ZERO;
        Org org;
        if (!webUserDto.isAdmin()) {
            org = resultList.stream().filter(o -> o.getId().equals(webUserDto.getOrgId())).findAny().get();
            parentId = String.valueOf(org.getId());
        }
        List<WebUser> finalUserList = this.list(new QueryWrapper<WebUser>().lambda()
                .select(WebUser::getId, WebUser::getOrgId, WebUser::getNickName)
                .eq(WebUser::getEnable, TrueOrFalseEnum.TRUE.getValue()));
        finalUserList.forEach(u -> {
            Org tmp = new Org();
            tmp.setId(-u.getId());
            tmp.setParentId(u.getOrgId());
            tmp.setTitle(u.getNickName());
            resultList.add(tmp);
        });
        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        List<Tree<String>> list = TreeUtil.build(resultList, parentId, treeNodeConfig, (o, tree) -> {
            tree.setId(String.valueOf(o.getId()));
            tree.setParentId(String.valueOf(o.getParentId()));
            tree.putExtra("name", o.getTitle());
            tree.putExtra("value", String.valueOf(o.getId()));
        });
        resultMap.put("list", list);
        return resultMap;
    }
}
