package org.noah.modules.sys.userRole.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.noah.modules.sys.userRole.entity.UserRole;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2021-12-21
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    List<String> getRolesCodeByUserId(@Param("webUserId") Integer webUserId);
}
