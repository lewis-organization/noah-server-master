package org.noah.modules.sys.test.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.ApproveBusinessTypeEnum;
import enumc.ApproveStatusEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.TestFunctionQueryDto;
import org.noah.modules.sys.approve.entity.ApproveBusiness;
import org.noah.modules.sys.approve.service.ApproveBusinessService;
import org.noah.modules.sys.test.entity.TestFunction;
import org.noah.modules.sys.test.mapper.TestFunctionMapper;
import org.noah.modules.sys.test.service.TestFunctionService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Liuxu
 * @since 2024-05-28
 */
@Slf4j
@Component
@DubboService
@RequiredArgsConstructor
public class TestFunctionServiceImpl extends ServiceImpl<TestFunctionMapper, TestFunction> implements TestFunctionService {

    private final ApproveBusinessService approveBusinessService;

    /**
     * 分页查询
     * @author liuxu
     * @date 2024/5/28 16:08
     * @param page 分页参数
     * @param testFunctionQueryDto 查询参数
     * @return Page<Map < String, Object>>
     */
    @Override
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, TestFunctionQueryDto testFunctionQueryDto) {
        Page<Map<String, Object>> resultPage = new Page<>();
        IPage<Map<String, Object>> iPage = this.pageMaps(page, new QueryWrapper<TestFunction>()
                .lambda()
                .eq(StringUtils.isNotBlank(testFunctionQueryDto.getStatus()), TestFunction::getStatus, testFunctionQueryDto.getStatus())
                .orderByDesc(TestFunction::getCreateTime));
        for (Map<String, Object> map : iPage.getRecords()) {
            map.put("statusDes", ApproveStatusEnum.getValueByKey(MapUtil.getStr(map, "status")));
        }
        resultPage.setTotal(iPage.getTotal());
        resultPage.setRecords(iPage.getRecords());
        return resultPage;
    }

    /**
     * 新增保存
     * @author liuxu
     * @date 2024/5/28 16:16
     * @param testFunction 保存参数
     */
    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void saveObj(TestFunction testFunction) {
        testFunction.setStatus(ApproveStatusEnum.NO_SUBMIT.getState());
        this.save(testFunction);
        approveBusinessService.startFlow("test", testFunction.getId(), ApproveBusinessTypeEnum.TEST.getType());
    }

    /**
     * 修改保存
     * @author liuxu
     * @date 2024/5/28 16:16
     * @param testFunction 保存参数
     */
    @Override
    public void updateObj(TestFunction testFunction) {
        this.updateById(testFunction);
    }

    /**
     * 提交审核操作。
     * 将指定ID列表中的测试功能对象设置为等待审核状态，并触发相应的审核流程。
     *
     * @param collect 测试功能对象的ID列表，这些对象将被设置为等待审核状态。
     */
    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void submitApprove(List<Integer> collect) {
        // 创建一个新的TestFunction实例，用于更新状态。
        TestFunction testFunction = new TestFunction();
        // 设置实例的状态为等待审核。
        testFunction.setStatus(ApproveStatusEnum.WAIT.getState());

        // 更新状态为等待审核的测试功能对象。如果ID集合不为空，则使用in查询条件。
        this.update(testFunction, new QueryWrapper<TestFunction>()
                .lambda()
                .in(CollectionUtil.isNotEmpty(collect), TestFunction::getId, collect));

        // 查询与待审核的测试功能对象关联的审核业务ID列表。
        // 提交审核流程。
        List<Integer> approveBusinessIds = approveBusinessService.list(new QueryWrapper<ApproveBusiness>()
                .lambda()
                .in(ApproveBusiness::getBusinessId, collect))
                .stream()
                .map(ApproveBusiness::getId)
                .collect(Collectors.toList());

        // 提交审核流程，指定审核业务的名称和相关的业务ID列表。
        approveBusinessService.submitApprove(ApproveBusinessTypeEnum.TEST.getType(), approveBusinessIds);
    }

    /**
     * 根据业务ID获取审批记录ID。
     * 此方法旨在通过已有的业务ID，查询对应的审批业务表，进一步获取审批记录的ID。
     * 如果找到了对应的审批业务ID，并且该审批业务ID对应有审批记录ID，则将审批记录ID返回。
     * 如果没有找到对应的审批业务ID，或者审批业务ID对应的审批记录ID为空，则返回一个包含默认ID值0的结果Map。
     *
     * @param id 业务ID，用于查询审批业务表。
     * @return 包含审批记录ID的Map，如果未找到则ID值为0。
     */
    @Override
    public Map<String, Object> getApproveRecordsId(Integer id) {
        // 初始化结果Map，用于存放查询结果。
        Map<String, Object> resultMap = MapUtil.newHashMap();
        // 默认情况下，将ID设置为0。
        resultMap.put("id", 0);

        // 根据业务ID和业务类型查询审批业务ID。
        Integer approveBusinessId = approveBusinessService
                .getIdByBusinessIdAndBusinessType(id, ApproveBusinessTypeEnum.TEST.getType());

        // 如果找到了审批业务ID。
        if (Objects.nonNull(approveBusinessId)) {
            // 根据审批业务ID查询审批记录ID。
            Integer approveRecordsId = approveBusinessService.getApproveRecordsIdByApproveBusinessId(approveBusinessId);
            // 如果找到了审批记录ID，则将其放入结果Map中。
            if (Objects.nonNull(approveRecordsId)) {
                resultMap.put("id", approveRecordsId);
            }
        }
        // 返回结果Map。
        return resultMap;
    }


}
