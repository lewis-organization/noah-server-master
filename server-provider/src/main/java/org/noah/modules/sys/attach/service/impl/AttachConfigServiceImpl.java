package org.noah.modules.sys.attach.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.modules.sys.attach.entity.AttachConfig;
import org.noah.modules.sys.attach.mapper.AttachConfigMapper;
import org.noah.modules.sys.attach.service.AttachConfigService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2023-03-06
 */
@Slf4j
@DubboService
@Component
public class AttachConfigServiceImpl extends ServiceImpl<AttachConfigMapper, AttachConfig> implements AttachConfigService {


    @Override
    public Map<String, Object> getModelMapById(Integer id) {
        return getMap(new QueryWrapper<AttachConfig>()
                .lambda()
                .select(AttachConfig::getCompressType,
                        AttachConfig::getSupportType,
                        AttachConfig::getPreviewType)
                .eq(AttachConfig::getId, id));
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void saveSetting(AttachConfig attachConfig) {
        saveOrUpdate(attachConfig);
    }
}
