package org.noah.modules.sys.log.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.LogSourceEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.modules.sys.log.entity.Log;
import org.noah.modules.sys.log.mapper.LogMapper;
import org.noah.modules.sys.log.service.LogService;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-04-15
 */
@Slf4j
@DubboService
@Component
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements LogService {

    @Override
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, String type) {
        Page<Map<String, Object>> mapPage = new Page<>();
        IPage<Map<String, Object>> iPage = pageMaps(page, new QueryWrapper<Log>()
                .lambda().eq(Log::getType, type).orderByDesc(Log::getCreateTime));
        for (Map<String, Object> m : iPage.getRecords()) {
            m.put("sourceName", LogSourceEnum.getValueByKey(String.valueOf(m.get("source"))));
        }
        mapPage.setTotal(iPage.getTotal());
        mapPage.setRecords(iPage.getRecords());
        return mapPage;
    }
}
