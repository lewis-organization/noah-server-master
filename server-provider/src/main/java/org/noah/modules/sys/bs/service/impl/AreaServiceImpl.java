package org.noah.modules.sys.bs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.modules.sys.bs.entity.Area;
import org.noah.modules.sys.bs.mapper.AreaMapper;
import org.noah.modules.sys.bs.service.AreaService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 地区设置 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
@Slf4j
@DubboService
@Component
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements AreaService {

    @Override
    public List<Map<String, Object>> getListByCityCode(String code) {
        return listMaps(new QueryWrapper<Area>()
                .lambda().select(Area::getAreaCode, Area::getAreaName)
                .eq(Area::getCityCode, code)
                .orderByAsc(Area::getSort));
    }
}
