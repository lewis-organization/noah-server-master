package org.noah.modules.sys.role.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.noah.config.mybatis.MybatisEncrypt;
import org.noah.modules.sys.role.entity.Role;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2021-12-21
 */
public interface RoleMapper extends BaseMapper<Role> {

    int testAddRole(@Param("roleName") MybatisEncrypt mybatisEncrypt, @Param("roleCode") String roleCode);

    List<Role> queryRoleList(@Param("roleName") MybatisEncrypt mybatisEncrypt);

    List<String> getRolesCodeByUserId(@Param("webUserId") Integer id);
}
