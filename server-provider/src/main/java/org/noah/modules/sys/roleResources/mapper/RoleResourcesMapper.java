package org.noah.modules.sys.roleResources.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.noah.modules.sys.resources.entity.Resources;
import org.noah.modules.sys.roleResources.entity.RoleResources;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2022-01-07
 */
public interface RoleResourcesMapper extends BaseMapper<RoleResources> {

    List<Resources> getResourcesCodeByUserId(@Param("webUserId") Integer webUserId);
}
