package org.noah.modules.sys.attach.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.config.id.IdGenerator;
import org.noah.dto.SaveAttachDto;
import org.noah.modules.sys.attach.entity.Attach;
import org.noah.modules.sys.attach.mapper.AttachMapper;
import org.noah.modules.sys.attach.service.AttachService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.modules.sys.user.service.WebUserService;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-01-18
 */
@Slf4j
@DubboService
@Component
@RequiredArgsConstructor
public class AttachServiceImpl extends ServiceImpl<AttachMapper, Attach> implements AttachService {

    private final IdGenerator idGenerator;
    private final WebUserService webUserService;

    @Override
    public Map<String, Object> upload(byte[] bytes, String originalFilename, long size, String path) throws BusinessException {
        Map<String, Object> resultMap = MapUtil.newHashMap();
        FileUtil.mkdir(path);
        String fileSuffix = FileUtil.getSuffix(originalFilename);
        String serverName = idGenerator.getSequence() + "." + fileSuffix;

        //创建输出文件对象
        File outFile = new File(path + "/"
                + serverName);
        //拷贝文件到输出文件对象
        FileUtil.writeBytes(bytes, outFile);

        //初始化文件对象
        Attach attach = new Attach();
        attach.setSourceName(originalFilename);
        attach.setServerName(serverName);
        attach.setSuffix(fileSuffix);
        attach.setSize(size);
        this.save(attach);

        //返回信息
        resultMap.put("file_id", attach.getId());
        resultMap.put("file_name", attach.getSourceName());
        resultMap.put("file_suffix", attach.getSuffix());

        return resultMap;
    }

    @Override
    public void saveAttach(SaveAttachDto saveAttachDto) throws BusinessException {
        webUserService.update(new WebUser().setImgIds(saveAttachDto.getImages()), new QueryWrapper<WebUser>()
                .lambda().eq(WebUser::getId, 1));
    }

    @Override
    public List<Map<String, Object>> getInfo(String ids) throws BusinessException {
        return this.listMaps(new QueryWrapper<Attach>().select("id as 'file_id'",
                "source_name as 'file_name'", "suffix as 'file_suffix'").lambda().in(Attach::getId, (Object[]) ids.split(StrUtil.COMMA)));
    }

    @Override
    public Attach getAttachById(String id) {
        return this.getOne(new QueryWrapper<Attach>()
                .lambda().select(Attach::getServerName, Attach::getSuffix)
                .eq(Attach::getId, id));
    }
}
