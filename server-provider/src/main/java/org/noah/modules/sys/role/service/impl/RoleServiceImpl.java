package org.noah.modules.sys.role.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.RoleTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.config.mybatis.MybatisEncrypt;
import org.noah.dto.RoleQueryDto;
import org.noah.modules.sys.role.entity.Role;
import org.noah.modules.sys.role.mapper.RoleMapper;
import org.noah.modules.sys.role.service.RoleService;
import org.noah.utils.AesUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2021-12-21
 */
@Slf4j
@DubboService
@Component
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Override
    public void testAddRole() {
        baseMapper.testAddRole(new MybatisEncrypt("超级管理员"), "1234");
    }

    @Override
    public List<Role> queryRoleList(String name) {
        return list(new QueryWrapper<Role>()
                .lambda().eq(Role::getRoleName, new MybatisEncrypt(name)));
    }

    /**
     * 角色分页数据
     * @author Liuxu
     * @date 2024/1/23 12:10
     * @param page 分页参数
     * @param roleQueryDto 查询dto
     * @return com.baomidou.mybatisplus.extension.plugins.pagination.Page<java.util.Map<java.lang.String,java.lang.Object>>
     */
    @Override
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, RoleQueryDto roleQueryDto) {
        Page<Map<String, Object>> mapPage = new Page<>();
        IPage<Map<String, Object>> iPage = pageMaps(page, new QueryWrapper<Role>()
                .lambda().like(StringUtils.isNotBlank(roleQueryDto.getRoleName()), Role::getRoleName, roleQueryDto.getRoleName())
                .like(StringUtils.isNotBlank(roleQueryDto.getRoleCode()), Role::getRoleCode, roleQueryDto.getRoleCode())
                .eq(StringUtils.isNotBlank(roleQueryDto.getRoleType()), Role::getRoleType, roleQueryDto.getRoleType()));
        String roleName;
        for (Map<String, Object> map : iPage.getRecords()) {
            roleName = String.valueOf(map.get("roleName"));
            if (StringUtils.isNotBlank(roleName)) {
                map.put("roleName", AesUtil.decrypt(roleName, AesUtil.getAssetsDevPwdField()));
            }
            map.put("roleType", RoleTypeEnum.getValueByKey(String.valueOf(map.get("roleType"))));
        }
        mapPage.setRecords(iPage.getRecords());
        mapPage.setTotal(iPage.getTotal());
        return mapPage;
    }

    @Override
    public void saveRole(Role role) {
        if (count(new QueryWrapper<Role>()
                .lambda().eq(Role::getRoleCode, role.getRoleCode())) > 0) {
            throw new BusinessException("角色编码已经存在了");
        }
        save(role);
    }

    @Override
    public void updateRole(Role role) {
        Role validRole = getOne(new QueryWrapper<Role>()
                .lambda().eq(Role::getRoleCode, role.getRoleCode()));
        if (null != validRole) {
            if (!role.getId().toString().equals(validRole.getId().toString())) {
                throw new BusinessException("角色编码已经存在了");
            }
        }
        updateById(role);
    }

    @Override
    public List<Map<String, Object>> getListUseByUser() throws BusinessException {
        List<Map<String, Object>> resultList = listMaps(new QueryWrapper<Role>()
                .select("id", "0 as 'parentId'", "role_name as 'roleName'"));
        String roleName;
        for (Map<String, Object> map : resultList) {
            roleName = String.valueOf(map.get("roleName"));
            map.put("roleName", AesUtil.decrypt(roleName, AesUtil.getAssetsDevPwdField()));
        }
        return resultList;
    }

    @Override
    public List<String> getRolesCodeByUserId(Integer id) {
        return this.baseMapper.getRolesCodeByUserId(id);
    }
}
