package org.noah.modules.sys.resources.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.RedisCacheKeyEnum;
import enumc.ResourcesTypeEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.CommonConstant;
import org.noah.config.exception.BusinessException;
import org.noah.dto.ResourcesQueryDto;
import org.noah.modules.sys.resources.entity.Resources;
import org.noah.modules.sys.resources.mapper.ResourcesMapper;
import org.noah.modules.sys.resources.service.ResourcesService;
import org.noah.utils.RedisUtil;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2021-11-04
 */
@Slf4j
@DubboService(version = "1.0.0")
@Component
@RequiredArgsConstructor
public class ResourcesServiceImpl extends ServiceImpl<ResourcesMapper, Resources> implements ResourcesService {

    private final RedisUtil redisUtil;
    private final ThreadPoolTaskExecutor executor;

    /**
     * 获取用户菜单
     * @author Liuxu
     * @date 2024/1/23 12:08
     * @param contextPath contextPath
     * @param userId userId
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */
    @Override
    public Map<String, Object> getUserResources(String contextPath, Integer userId) {
        Map<String, Object> resultMap = MapUtil.newHashMap();
        String redisCacheKey = RedisCacheKeyEnum.USER_MENU_CACHE.getKey() + userId;
        if (redisUtil.hasKey(redisCacheKey)) {
            resultMap.put("list", redisUtil.lGet(redisCacheKey, 0, -1));
            return resultMap;
        }
        return this.cachedUserResources(contextPath, userId);
    }

    /**
     * 查询并缓存用户菜单信息
     * @author Liuxu
     * @date 2024/1/23 12:08
     * @param contextPath contextPath
     * @param userId userId
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */
    private Map<String, Object> cachedUserResources(String contextPath, Integer userId) {
        Map<String, Object> resultMap = MapUtil.newHashMap();
        //先删除
        String redisCacheKey = RedisCacheKeyEnum.USER_MENU_CACHE.getKey() + userId;
        redisUtil.delete(redisCacheKey);
        List<Resources> resourcesList = this.baseMapper.getResourcesByUserId(userId);
        TreeNodeConfig treeNodeConfig = new TreeNodeConfig();
        treeNodeConfig.setWeightKey("seq");
        String parentId = CommonConstant.ZERO;
        List<Tree<String>> resultList = TreeUtil.build(resourcesList, parentId, treeNodeConfig, (o, tree) -> {
            if (!o.getType().equals(ResourcesTypeEnum.BUTTON.getType())) {
                tree.setId(String.valueOf(o.getId()));
                tree.setParentId(String.valueOf(o.getParentId()));
                tree.putExtra("title", o.getTitle());
                tree.putExtra("icon", o.getIcon());
                tree.putExtra("type", o.getType());
                tree.setWeight(o.getSeq());
                if (!CommonConstant.JIN.equals(o.getHref())) {
                    tree.putExtra("href", contextPath + o.getHref());
                }
            }
            //缓存权限
            redisUtil.lSet(RedisCacheKeyEnum.USER_RESOURCES_CACHE.getKey().concat(String.valueOf(userId)), o, -1L);
        });
        //异步缓存菜单信息
        executor.execute(() -> resultList.forEach(tree -> {
            if (!redisUtil.lSet(redisCacheKey, tree, -1L)) {
                log.warn("缓存用户菜单信息失败, 列表数据将从数据库获取...");
            }
        }));
        resultMap.put("list", resultList);
        return resultMap;
    }

    /**
     * v
     * @author Liuxu
     * @date 2024/1/23 12:09
     * @param resourcesQueryDto 查询dto
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    @Override
    public List<Map<String, Object>> getList(ResourcesQueryDto resourcesQueryDto) {
        //从缓存中获取
        List<Map<String, Object>> resultList;
        if (redisUtil.hasKey(RedisCacheKeyEnum.RESOURCES_CACHE.getKey())) {
            resultList = redisUtil.lGet(RedisCacheKeyEnum.RESOURCES_CACHE.getKey(), 0, -1).stream()
                    .map(o -> Convert.toMap(String.class, Object.class, o)).collect(Collectors.toList());
        } else {
            resultList = this.getResourcesFromDb();
            this.setResourcesListCache(resultList);
        }
        return resultList;
    }

    /**
     * 从数据库查询菜单信息
     * @author Liuxu
     * @date 2024/1/23 12:09
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    private List<Map<String, Object>> getResourcesFromDb() {
        return this.listMaps(new QueryWrapper<Resources>()
                .select("id as 'powerId'", "title as 'powerName'", "type as 'powerType'",
                        "code as 'powerCode'", "href as 'powerUrl'", "open_type as 'openType'",
                        "parent_id as 'parentId'", "icon", "seq as 'sort'", "create_time as 'createTime'",
                        "update_time as 'updateTime'"));
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void deleteById(Integer id) {
        this.removeById(id);
    }

    @Override
    public List<Map<String, Object>> getResourcesByType(String type) {
        return this.listMaps(new QueryWrapper<Resources>()
                .lambda().select(Resources::getId, Resources::getTitle).eq(Resources::getType, type).orderByAsc(Resources::getSeq));
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void saveEntity(Resources resources) {
        if (ResourcesTypeEnum.CATALOGUE.getType().equals(resources.getType())) {
            resources.setIcon(CommonConstant.ICON_PREFIX + resources.getIcon());
            resources.setHref(CommonConstant.JIN);
            resources.setCode(CommonConstant.JIN);
        } else {
            resources.setCode(resources.getHref().substring(1).replaceAll("/", ":"));
        }
        this.save(resources);
        //异步线程更新缓存
        executor.execute(() -> this.setResourcesListCache(null));
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void updateEntity(Resources resources) {
        if (ResourcesTypeEnum.CATALOGUE.getType().equals(resources.getType())) {
            resources.setHref(CommonConstant.JIN);
            resources.setCode(CommonConstant.JIN);
            if (!resources.getIcon().contains(CommonConstant.ICON_PREFIX)) {
                resources.setIcon(CommonConstant.ICON_PREFIX + resources.getIcon());
            }
        } else {
            resources.setCode(resources.getHref().substring(1).replaceAll("/", ":"));
        }
        this.updateById(resources);
        //异步线程更新缓存
        executor.execute(() -> this.setResourcesListCache(null));
    }

    @Override
    @Transactional(rollbackFor = BusinessException.class)
    public void batchRemove(String ids) {
        this.removeByIds(Convert.toList(Integer.class, ids));
    }

    /**
     * 设置缓存
     */
    @Override
    public void setCache() {
        //查询是否存在菜单缓存
        if (!redisUtil.hasKey(RedisCacheKeyEnum.RESOURCES_CACHE.getKey())) {
            this.setResourcesListCache(null);
        }
    }

    @Override
    public List<Map<String, Object>> getListUseByRole() {
        return this.listMaps(new QueryWrapper<Resources>()
                .lambda().select(Resources::getId, Resources::getParentId, Resources::getTitle)
                .orderByAsc(Resources::getSeq));
    }

    @Override
    public List<Resources> getAll() throws BusinessException {
        return this.list(new QueryWrapper<Resources>().lambda().select(Resources::getHref, Resources::getCode)
                .ne(Resources::getParentId, 0));
    }

    @Override
    public void setResourcesListCache(List<Map<String, Object>> map) {
        if (Objects.isNull(map)) {
            map = this.getResourcesFromDb();
        }
        //先删除
        redisUtil.delete(RedisCacheKeyEnum.RESOURCES_CACHE.getKey());
        for (Map<String, Object> m : map) {
            if (!redisUtil.lSet(RedisCacheKeyEnum.RESOURCES_CACHE.getKey(), m, -1L)) {
                log.warn("缓存菜单列表信息失败, 列表数据将从数据库获取...");
            }
        }
    }

    @Override
    public List<Resources> getResourcesByUserId(Integer id) {
        return this.baseMapper.getResourcesByUserId(id);
    }
}
