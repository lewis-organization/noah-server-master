package org.noah.modules.sys.approve.service.impl;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.ApproveTypeEnum;
import enumc.TrueOrFalseEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.dto.ApproveDto;
import org.noah.dto.ApproveQueryDto;
import org.noah.modules.sys.approve.entity.Approve;
import org.noah.modules.sys.approve.entity.ApprovePersonnel;
import org.noah.modules.sys.approve.entity.ApproveStep;
import org.noah.modules.sys.approve.mapper.ApproveMapper;
import org.noah.modules.sys.approve.service.ApprovePersonnelService;
import org.noah.modules.sys.approve.service.ApproveService;
import org.noah.modules.sys.approve.service.ApproveStepService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
@Slf4j
@Component
@DubboService
@RequiredArgsConstructor
public class ApproveServiceImpl extends ServiceImpl<ApproveMapper, Approve> implements ApproveService {

    private final ApproveStepService approveStepService;
    private final ApprovePersonnelService approvePersonnelService;

    /**
     * 分页查询
     * @author Liuxu
     * @date 2024/1/23 10:44
     * @param page 分页参数
     * @param approveQueryDto 分页查询dto
     * @return com.baomidou.mybatisplus.extension.plugins.pagination.Page<java.util.Map<java.lang.String,java.lang.Object>>
     */
    @Override
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, ApproveQueryDto approveQueryDto) {
        Page<Map<String, Object>> resultPage = new Page<>();
        IPage<Map<String, Object>> iPage = this.pageMaps(page, new QueryWrapper<Approve>()
                .lambda().like(StringUtils.isNotBlank(approveQueryDto.getTitle()), Approve::getTitle, approveQueryDto.getTitle())
                .like(StringUtils.isNotBlank(approveQueryDto.getCode()), Approve::getCode, approveQueryDto.getCode())
                .orderByDesc(Approve::getCreateTime));
        for (Map<String, Object> map : iPage.getRecords()) {
            map.put("typeDes", ApproveTypeEnum.getValueByKey(MapUtil.getStr(map, "type")));
        }
        resultPage.setRecords(iPage.getRecords());
        resultPage.setTotal(iPage.getTotal());
        return resultPage;
    }

    /**
     * 保存
     * @author Liuxu
     * @date 2024/1/23 14:57
     * @param approveDto 保存dto
     */
    @Override
    public void saveObj(ApproveDto approveDto) {
        save(approveDto);
        saveStepAndPersonnel(approveDto);
    }

    private void saveStepAndPersonnel(ApproveDto approveDto) {
        ApproveStep approveStep;
        ApprovePersonnel approvePersonnel;
        int seqLength = approveDto.getApproveIds().size(),
                idsLength;
        List<Integer> approveIds;
        AtomicInteger seq = new AtomicInteger(0);

        for (int i = 0; i < seqLength; i++) {
            approveStep = new ApproveStep();
            approveStep.setApproveId(approveDto.getId());
            approveStep.setSeq(i + 1);
            approveStepService.save(approveStep);
            approveIds = approveDto.getApproveIds().get(i);
            idsLength = approveIds.size();
            for (int j = 0; j < idsLength; j++) {
                approvePersonnel = new ApprovePersonnel();
                approvePersonnel.setApproveStepId(approveStep.getId());
                approvePersonnel.setRandomAgree(TrueOrFalseEnum.FALSE.getValue());
                if (approveIds.get(j) > 0) {
                    approvePersonnel.setOrgId(approveIds.get(j));
                    approvePersonnel.setRandomAgree(TrueOrFalseEnum.TRUE.getValue());
                } else {
                    approvePersonnel.setUserId(-approveIds.get(j));
                }
                approvePersonnel.setSeq(seq.incrementAndGet());
                approvePersonnel.setApproveId(approveDto.getId());
                approvePersonnelService.save(approvePersonnel);
            }
            approveStepService.updateById(approveStep);
        }
    }

    @Override
    public void updateObj(ApproveDto approveDto) {
        updateById(approveDto);
        removeChildrens(approveDto.getId());
        saveStepAndPersonnel(approveDto);
    }

    private void removeChildrens(Integer approveId) {
        approveStepService.remove(new QueryWrapper<ApproveStep>()
                .lambda().eq(ApproveStep::getApproveId, approveId));
        approvePersonnelService.remove(new QueryWrapper<ApprovePersonnel>()
                .lambda().eq(ApprovePersonnel::getApproveId, approveId));
    }

    @Override
    public void removeObj(Integer id) {
        this.removeById(id);
        removeChildrens(id);
    }

    @Override
    public Approve getByCode(String approveCode) {
        if (StringUtils.isBlank(approveCode)) {
            return null;
        }
        return this.getOne(new QueryWrapper<Approve>()
                .lambda().eq(Approve::getCode, approveCode));
    }
}
