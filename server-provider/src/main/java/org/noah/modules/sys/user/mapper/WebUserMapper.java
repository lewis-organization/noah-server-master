package org.noah.modules.sys.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.user.entity.WebUser;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2021-11-01
 */
public interface WebUserMapper extends BaseMapper<WebUser> {

}
