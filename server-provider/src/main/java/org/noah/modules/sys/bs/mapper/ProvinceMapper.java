package org.noah.modules.sys.bs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.bs.entity.Province;

/**
 * <p>
 * 省份设置 Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
public interface ProvinceMapper extends BaseMapper<Province> {

}
