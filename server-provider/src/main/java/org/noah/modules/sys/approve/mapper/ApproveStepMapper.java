package org.noah.modules.sys.approve.mapper;

import org.noah.modules.sys.approve.entity.ApproveStep;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Liuxu
 * @since 2024-01-24
 */
public interface ApproveStepMapper extends BaseMapper<ApproveStep> {

}
