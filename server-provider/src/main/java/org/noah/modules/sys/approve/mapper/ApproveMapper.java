package org.noah.modules.sys.approve.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.approve.entity.Approve;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
public interface ApproveMapper extends BaseMapper<Approve> {

}
