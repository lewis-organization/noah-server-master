package org.noah.modules.sys.config.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.config.entity.SystemConfig;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2022-09-30
 */
public interface SystemConfigMapper extends BaseMapper<SystemConfig> {

}
