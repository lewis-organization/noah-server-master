package org.noah.modules.sys.approve.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.noah.dto.ApproveRecordsQueryDto;
import org.noah.modules.sys.approve.entity.ApproveRecords;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Liuxu
 * @since 2024-05-31
 */
public interface ApproveRecordsMapper extends BaseMapper<ApproveRecords> {

    IPage<Map<String, Object>> getCustomPage(Page<Map<String, Object>> page, @Param("dto") ApproveRecordsQueryDto approveRecordsQueryDto);
}
