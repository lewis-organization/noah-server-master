package org.noah.modules.sys.attach.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.attach.entity.AttachConfig;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2023-03-06
 */
public interface AttachConfigMapper extends BaseMapper<AttachConfig> {

}
