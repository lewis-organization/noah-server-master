package org.noah.modules.sys.test.mapper;

import org.noah.modules.sys.test.entity.TestFunction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Liuxu
 * @since 2024-05-28
 */
public interface TestFunctionMapper extends BaseMapper<TestFunction> {

}
