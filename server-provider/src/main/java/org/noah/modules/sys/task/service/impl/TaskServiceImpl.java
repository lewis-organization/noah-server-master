package org.noah.modules.sys.task.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.TaskStateEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.config.quartz.QuartzJobManager;
import org.noah.dto.TaskQueryDto;
import org.noah.modules.sys.task.entity.Task;
import org.noah.modules.sys.task.mapper.TaskMapper;
import org.noah.modules.sys.task.service.TaskService;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * <p>
 * 系统任务 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-01-13
 */
@Slf4j
@DubboService
@Component
@RequiredArgsConstructor
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements TaskService {

    private final QuartzJobManager quartzJobManager;

    /**
     * 分页数据
     *
     * @param page
     * @param taskQueryDto
     * @return
     * @throws BusinessException
     */
    @Override
    public Page<Map<String, Object>> getPage(Page<Map<String, Object>> page, TaskQueryDto taskQueryDto) throws BusinessException {
        Page<Map<String, Object>> mapPage = new Page<>();
        IPage<Map<String, Object>> iPage = this.pageMaps(page, new QueryWrapper<Task>()
                .lambda().like(StringUtils.isNotBlank(taskQueryDto.getName()), Task::getName, taskQueryDto.getName())
                .eq(StringUtils.isNotBlank(taskQueryDto.getGroup()), Task::getGroupName, taskQueryDto.getGroup())
                .eq(StringUtils.isNotBlank(taskQueryDto.getState()), Task::getState, taskQueryDto.getState()));
        mapPage.setRecords(iPage.getRecords());
        mapPage.setTotal(iPage.getTotal());
        return mapPage;
    }

    @Override
    public void saveTask(Task task) throws BusinessException {
        //通过反射获得执行任务类
        @SuppressWarnings("rawtypes")
        Class jobClass;
        try {
            jobClass = Class.forName("org.noah.modules.task." + task.getClassForName());
        } catch (ClassNotFoundException e) {
            throw new BusinessException("类名没有找到, 请检查");
        }
        //获得任务类所实现的所有接口
        Class<?>[] interfaces = jobClass.getInterfaces();
        int flag = 0;
        for (Class<?> inte :
                interfaces) {
            if (inte.getSimpleName().equals("Job")) {
                flag = 1;
                break;
            }
        }
        //判断是否实现了Job接口
        if (0 == flag) {
            throw new BusinessException("任务类配置错误, 请检查");
        }
        //保存任务到数据库
        task.setState(TaskStateEnum.NORMAL.getState());
        try {
            quartzJobManager.addJob(jobClass, task.getName(), task.getGroupName(), task.getCron());
        } catch (Exception e) {
            log.error("系统错误", e);
            throw new BusinessException();
        }
        task.setClassName(task.getClassForName());
        this.save(task);
    }

    @Override
    public void updateTask(Task task) throws BusinessException {
        try {
            quartzJobManager.updateJob(task.getName(), task.getGroupName(), task.getCron());
        } catch (Exception e) {
            log.error("系统错误", e);
            throw new BusinessException();
        }
        task.setCron(task.getCron());
        task.setDes(task.getDes());
        task.setClassName(task.getClassForName());
        this.updateById(task);
    }

    @Override
    public void removeTask(Integer id) throws BusinessException {
        Task task = this.getById(id);
        try {
            quartzJobManager.deleteJob(task.getName(), task.getGroupName());
        } catch (Exception e) {
            log.error("系统错误", e);
            throw new BusinessException();
        }
        this.removeById(id);
    }

    @Override
    public void start() throws BusinessException {
        quartzJobManager.startAllJobs();
        //暂停状态
        this.list(new QueryWrapper<Task>().lambda().eq(Task::getState, TaskStateEnum.PAUSED.getState()))
                .forEach(t -> this.update(new Task().setState(TaskStateEnum.NORMAL.getState()),
                        new QueryWrapper<Task>().lambda().eq(Task::getId, t.getId())));
    }

    @Override
    public void shutdown() throws BusinessException {
        quartzJobManager.standbyAllJobs();
        //正常状态
        this.list(new QueryWrapper<Task>().lambda().eq(Task::getState, TaskStateEnum.NORMAL.getState()))
                .forEach(t -> this.update(new Task().setState(TaskStateEnum.PAUSED.getState()),
                        new QueryWrapper<Task>().lambda().eq(Task::getId, t.getId())));

    }

    @Override
    public void allJob() throws BusinessException {

    }

    @Override
    public void changeState(String id, String state) throws BusinessException {
        Task task = this.getById(id);
        if (null != task) {
            if (TaskStateEnum.NORMAL.getState().equals(state)) {
                try {
                    quartzJobManager.resumeJob(task.getName(), task.getGroupName());
                } catch (Exception e) {
                    log.error("", e);
                    throw new BusinessException();
                }
            } else if (TaskStateEnum.PAUSED.getState().equals(state)) {
                try {
                    quartzJobManager.pauseJob(task.getName(), task.getGroupName());
                } catch (Exception e) {
                    log.error("", e);
                    throw new BusinessException();
                }
            }
            this.update(new Task().setState(state),
                    new QueryWrapper<Task>().lambda().eq(Task::getId, id));
        }
    }
}
