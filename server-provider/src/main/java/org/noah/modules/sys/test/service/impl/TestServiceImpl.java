package org.noah.modules.sys.test.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mzlion.easyokhttp.HttpClient;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.config.id.IdGenerator;
import org.noah.config.quartz.QuartzJobManager;
import org.noah.dto.SaveAttachDto;
import org.noah.modules.sys.message.entity.WebMessage;
import org.noah.modules.sys.message.service.WebMessageService;
import org.noah.modules.sys.test.entity.Test;
import org.noah.modules.sys.test.mapper.TestMapper;
import org.noah.modules.sys.test.service.TestService;
import org.springframework.boot.autoconfigure.klock.annotation.Klock;
import org.springframework.boot.autoconfigure.klock.model.LockTimeoutStrategy;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author liuxu
 * @packageName org.noah.modules.sys.test.service.impl
 * @time 2021/10/20-10:18
 */
@DubboService
@Component
@RequiredArgsConstructor
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements TestService {

    private final ThreadPoolTaskExecutor executor;
    private final IdGenerator idGenerator;
    private final WebMessageService webMessageService;
    int a = 50;
    private final QuartzJobManager quartzJobManager;
    private final PlatformTransactionManager transactionManager;
    //private final EsWebMessageMapper esWebMessageMapper;
    @Override
    public String test() {
        //this.baseMapper.insert(new Test());
        //int a = 1 / 0;
        return "ok";
    }

    @Override
    public void addQuartzTask() {
        //quartzJobManager.addJob(TestTask1.class, "test1", "test1", "0/5 * * * * ?");
        quartzJobManager.deleteJob("test1", "test1");
        throw new BusinessException("测试异常");
//        executor.execute(() -> System.out.println("liuxu"));
    }

    @Override
    @Klock(name = "测试分布式锁", waitTime = 2, lockTimeoutStrategy = LockTimeoutStrategy.FAIL_FAST)
    public void testSync() {
        CountDownLatch rollBackLatch = new CountDownLatch(1);
        CountDownLatch mainThreadLatch = new CountDownLatch(3);
        AtomicBoolean rollbackFlag = new AtomicBoolean(false);
         //用来存每个子线程的异常，把每个线程的自定义异常向vector的首位置插入，其余异常向末位置插入，避免线程不安全，所以使用vector代替list
        Vector<Throwable> exceptionVector = new Vector<>();
        AtomicInteger atomicInteger = new AtomicInteger(3);
        for (int i = 0; i < 5; i++) {
            executor.execute(() -> testTransactional(rollBackLatch, mainThreadLatch, rollbackFlag, exceptionVector, atomicInteger));
        }
        if (!rollbackFlag.get()) {
            try {
                mainThreadLatch.await();
                rollBackLatch.countDown();
            } catch (InterruptedException e) {
                throw new BusinessException("系统错误");
            }
        }
        if (CollectionUtil.isNotEmpty(exceptionVector)) {
            throw new BusinessException(exceptionVector.get(0).getMessage());
        }
    }

    public void testTransactional(CountDownLatch rollBackLatch, CountDownLatch mainThreadLatch,
                                  AtomicBoolean rollbackFlag, Vector<Throwable> exceptionVector, AtomicInteger atomicInteger) {
        //如果其他线程已经报错 就停止线程
        if (rollbackFlag.get()) {
            return;
        }
        //设置一个事务
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        //事物隔离级别，开启新事务，这样会比较安全些。
        def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        //获得事务状态
        TransactionStatus status = transactionManager.getTransaction(def);
        try {
            //模拟报错
            if (atomicInteger.get() == 3) {
                atomicInteger.set(0);
                int a = 10 / 0;
            }
            List<WebMessage> webMessageList = new ArrayList<>();
            for (int i = 0; i < 100; i++) {
                WebMessage webMessage = new WebMessage();
                webMessage.setTitle(Thread.currentThread().getName());
                webMessageList.add(webMessage);
            }
            webMessageService.saveBatch(webMessageList);
            mainThreadLatch.countDown();
            //线程等待
            rollBackLatch.await();
            if (rollbackFlag.get()) {
                transactionManager.rollback(status);
            } else {
                transactionManager.commit(status);
            }
        } catch (Exception e) {
            //如果出错了 就放开锁 让别的线程进入提交/回滚 本线程进行回滚
            rollbackFlag.set(true);
            rollBackLatch.countDown();
            mainThreadLatch.countDown();
            transactionManager.rollback(status);
            //保存异常信息
            exceptionVector.add(0, e);
        }
    }

    @Override
    public Map<String, Object> testUpload(byte[] byteFiles, String fileName, long size) throws BusinessException {
        Map<String, Object> resultMap = MapUtil.newHashMap();
        resultMap.put("file_id", RandomUtil.randomInt(1, 100));
        resultMap.put("file_name", fileName);
        resultMap.put("file_suffix", FileUtil.getSuffix(fileName));
        resultMap.put("file_url", "http://www.baidu.com");
        return resultMap;
    }

    @Override
    public void saveAttach(SaveAttachDto saveAttachDto) throws BusinessException {
        CompletableFuture<String> testFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println(123);
            return null;
        }, executor);
    }

    @Override
    public String getNewMessage() {
        //京东万象接口
        String apiKey = "91caed7b25ab82d51627f1c7428419dc";
        return HttpClient.get("https://way.jd.com/jisuapi/get")
                .queryString("channel", "头条")
                .queryString("num", "10")
                .queryString("start", "0")
                .queryString("appkey", apiKey).execute().asString();
    }

    //@Override
    //public void esAdd() {
    //    List<EsWebMessage> list = new ArrayList<>();
    //    for (int i = 0; i < 10000; i++) {
    //        EsWebMessage esWebMessage = new EsWebMessage();
    //        esWebMessage.setId(String.valueOf(i + 1));
    //        esWebMessage.setTitle("标题".concat(esWebMessage.getId()));
    //        esWebMessage.setContent("内容".concat(esWebMessage.getId()));
    //        list.add(esWebMessage);
    //    }
    //    Integer addCount = esWebMessageMapper.insertBatch(list);
    //    System.out.println(addCount);
    //}
    //
    //@Override
    //public Object esQuery() {
    //    return esWebMessageMapper.pageQuery(EsWrappers.lambdaQuery(EsWebMessage.class)
    //            .eq(EsWebMessage::getTitle, "标题11"), 1, 100);
    //}
}
