package org.noah.modules.sys.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.message.entity.WebMessage;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2022-01-19
 */
public interface WebMessageMapper extends BaseMapper<WebMessage> {

}
