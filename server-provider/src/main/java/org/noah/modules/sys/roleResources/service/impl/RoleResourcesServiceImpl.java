package org.noah.modules.sys.roleResources.service.impl;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.config.exception.BusinessException;
import org.noah.dto.SaveRoleResourcesDto;
import org.noah.modules.sys.resources.service.ResourcesService;
import org.noah.modules.sys.roleResources.entity.RoleResources;
import org.noah.modules.sys.roleResources.mapper.RoleResourcesMapper;
import org.noah.modules.sys.roleResources.service.RoleResourcesService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-01-07
 */
@Slf4j
@DubboService
@Component
@RequiredArgsConstructor
public class RoleResourcesServiceImpl extends ServiceImpl<RoleResourcesMapper, RoleResources> implements RoleResourcesService {

    private final ResourcesService resourcesService;

    @Override
    public Map<String, Object> getRoleResourcesByRoleId(String roleId) {
        Map<String, Object> resultMap = MapUtil.newHashMap();
        List<Map<String, Object>> resourcesList = resourcesService.getListUseByRole();
        List<Integer> checkedIds = this.listMaps(new QueryWrapper<RoleResources>()
                .lambda().select(RoleResources::getResourcesId).eq(RoleResources::getRoleId, roleId))
                .stream().map(r -> Integer.valueOf(String.valueOf(r.get("resourcesId")))).collect(Collectors.toList());
        resultMap.put("list", resourcesList);
        resultMap.put("checkedIds", checkedIds);
        return resultMap;
    }

    @Override
    public void saveSetting(SaveRoleResourcesDto saveRoleResourcesDto) {
        //保存选中的数据
        RoleResources roleResources;
        for (Integer id : saveRoleResourcesDto.getResourcesIds()) {
            roleResources = new RoleResources();
            roleResources.setRoleId(saveRoleResourcesDto.getRoleId());
            roleResources.setResourcesId(id);
            this.save(roleResources);
        }
        //删除取消选中的数据
        for (Integer id : saveRoleResourcesDto.getCancelIds()) {
            if (!this.remove(new QueryWrapper<RoleResources>().lambda()
                    .eq(RoleResources::getRoleId, saveRoleResourcesDto.getRoleId())
                    .eq(RoleResources::getResourcesId, id))) {
                throw new BusinessException();
            }
        }
    }
}
