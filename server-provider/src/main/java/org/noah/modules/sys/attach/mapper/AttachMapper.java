package org.noah.modules.sys.attach.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.attach.entity.Attach;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2022-01-18
 */
public interface AttachMapper extends BaseMapper<Attach> {

}
