package org.noah.modules.sys.approve.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import enumc.TrueOrFalseEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.modules.sys.approve.entity.ApprovePersonnel;
import org.noah.modules.sys.approve.entity.ApproveStep;
import org.noah.modules.sys.approve.mapper.ApproveStepMapper;
import org.noah.modules.sys.approve.service.ApprovePersonnelService;
import org.noah.modules.sys.approve.service.ApproveStepService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Liuxu
 * @since 2024-01-24
 */
@Slf4j
@Component
@DubboService
@RequiredArgsConstructor
public class ApproveStepServiceImpl extends ServiceImpl<ApproveStepMapper, ApproveStep> implements ApproveStepService {

    private final ApprovePersonnelService approvePersonnelService;

    /**
     * 根据id获取人员
     * @author Liuxu
     * @date 2024/1/24 14:35
     * @param id id
     * @return java.util.List<java.util.List<java.lang.Integer>>
     */
    @Override
    public List<List<Integer>> getByApproveId(Integer id) {
        List<List<Integer>> resultList = new ArrayList<>();
        List<ApproveStep> approveStepList = this.list(new QueryWrapper<ApproveStep>()
                .lambda().select(ApproveStep::getId).eq(ApproveStep::getApproveId, id).orderByAsc(ApproveStep::getSeq));
        approveStepList.forEach(a -> {
            List<ApprovePersonnel> approvePersonnels = approvePersonnelService.list(new QueryWrapper<ApprovePersonnel>()
                    .lambda()
                    .select(ApprovePersonnel::getOrgId, ApprovePersonnel::getUserId,
                            ApprovePersonnel::getRandomAgree)
                    .eq(ApprovePersonnel::getApproveStepId, a.getId())
                    .orderByAsc(ApprovePersonnel::getSeq));
            resultList.add(approvePersonnels.stream().map(b -> {
                if (b.getRandomAgree().equals(TrueOrFalseEnum.TRUE.getValue())) {
                    return b.getOrgId();
                } else {
                    return -b.getUserId();
                }
            }).collect(Collectors.toList()));
        });
        return resultList;
    }
}
