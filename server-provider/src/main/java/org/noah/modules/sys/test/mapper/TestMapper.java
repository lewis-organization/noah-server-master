package org.noah.modules.sys.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.sys.test.entity.Test;

/**
 * @author liuxu
 * @packageName org.noah.modules.sys.test.mapper
 * @time 2021/10/20-10:14
 */
public interface TestMapper extends BaseMapper<Test> {
}
