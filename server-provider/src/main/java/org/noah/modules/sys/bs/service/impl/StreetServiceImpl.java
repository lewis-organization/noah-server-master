package org.noah.modules.sys.bs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.modules.sys.bs.entity.Street;
import org.noah.modules.sys.bs.mapper.StreetMapper;
import org.noah.modules.sys.bs.service.StreetService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 街道设置 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
@Slf4j
@DubboService
@Component
public class StreetServiceImpl extends ServiceImpl<StreetMapper, Street> implements StreetService {

    @Override
    public List<Map<String, Object>> getListByAreaCode(String code) {
        return listMaps(new QueryWrapper<Street>()
                .lambda().select(Street::getStreetCode, Street::getStreetName)
                .eq(Street::getAreaCode, code)
                .orderByAsc(Street::getSort));
    }
}
