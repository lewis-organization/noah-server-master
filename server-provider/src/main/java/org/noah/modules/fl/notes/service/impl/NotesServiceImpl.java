package org.noah.modules.fl.notes.service.impl;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.modules.fl.notes.entity.Notes;
import org.noah.modules.fl.notes.mapper.NotesMapper;
import org.noah.modules.fl.notes.service.NotesService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 审核历史记录 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2022-11-18
 */
@DubboService
@Component
@RequiredArgsConstructor
public class NotesServiceImpl extends ServiceImpl<NotesMapper, Notes> implements NotesService {

    @Override
    public void saveObj(String processInstanceId, String idea, String des) {
        Notes notes = new Notes();
        notes.setIdea(idea);
        notes.setDes(des);
        notes.setProcessInstanceId(processInstanceId);
        save(notes);
    }

    @Override
    public List<Map<String, Object>> getByProcessInstanceId(String processInstanceId) {
        return listMaps(new QueryWrapper<Notes>()
                .lambda()
                .select(Notes::getIdea, Notes::getDes, Notes::getCreateTime)
                .eq(Notes::getProcessInstanceId, processInstanceId)
                .orderByDesc(Notes::getCreateTime))
                .stream()
                .peek(m -> m.put("createTime",
                        DateUtil.formatChineseDate(DateUtil.parse(String.valueOf(m.get("createTime")),
                                DatePattern.UTC_SIMPLE_PATTERN), false, true)))
                .collect(Collectors.toList());
    }
}