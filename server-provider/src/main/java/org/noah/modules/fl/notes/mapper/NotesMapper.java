package org.noah.modules.fl.notes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.fl.notes.entity.Notes;

/**
 * <p>
 * 审核历史记录 Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2022-11-18
 */
public interface NotesMapper extends BaseMapper<Notes> {

}
