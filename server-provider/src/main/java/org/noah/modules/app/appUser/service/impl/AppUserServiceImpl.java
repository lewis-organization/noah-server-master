package org.noah.modules.app.appUser.service.impl;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.noah.modules.app.appUser.entity.AppUser;
import org.noah.modules.app.appUser.mapper.AppUserMapper;
import org.noah.modules.app.appUser.service.AppUserService;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author liuxu
 * @since 2023-02-28
 */
@Slf4j
@Component
@DubboService
@RequiredArgsConstructor
public class AppUserServiceImpl extends ServiceImpl<AppUserMapper, AppUser> implements AppUserService {

    @Override
    public AppUser getAppUserByUserName(String username) {
        return getOne(new QueryWrapper<AppUser>()
                .lambda()
                .eq(AppUser::getName, username));
    }

    @Override
    public AppUser getAppUserByPhone(String phone) {
        return getOne(new QueryWrapper<AppUser>()
                .lambda()
                .eq(AppUser::getPhone, phone));
    }

    @Override
    public Map<Object, Object> getUserInfo(Integer id) {
        AppUser appUser = getById(id);
        return MapUtil.builder()
                .put("id", appUser.getId())
                .put("name", appUser.getName())
                .put("nickName", appUser.getNickName())
                .put("createTime", appUser.getCreateTime()).map();
    }
}
