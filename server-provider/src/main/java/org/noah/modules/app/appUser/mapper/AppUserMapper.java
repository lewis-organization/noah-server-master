package org.noah.modules.app.appUser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.noah.modules.app.appUser.entity.AppUser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuxu
 * @since 2023-02-28
 */
public interface AppUserMapper extends BaseMapper<AppUser> {

}
