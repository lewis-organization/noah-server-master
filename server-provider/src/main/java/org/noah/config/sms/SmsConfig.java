package org.noah.config.sms;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "sms")
public class SmsConfig {

    private boolean enabled;
    private String region;
    private String accessKeyId;
    private String accessKeySecret;
    private String signName;
    private String templateCode;
}
