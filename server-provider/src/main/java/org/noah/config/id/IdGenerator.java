package org.noah.config.id;

import com.github.wujun234.uid.impl.CachedUidGenerator;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class IdGenerator {

    private final CachedUidGenerator cachedUidGenerator;

    /**
     * 获取指定前缀的序列号
     * @author Liuxu
     * @date 2024/1/23 11:57
     * @param prefix 前缀
     * @return java.lang.String
     */
    public String getSequence(String prefix) {
        if (StringUtils.isNotBlank(prefix)) {
            return prefix + getSequence();
        }
        return getSequence();
    }

    /**
     * 获取序列号
     * @author Liuxu
     * @date 2024/1/23 11:57
     * @return java.lang.String
     */
    public String getSequence() {
        return String.valueOf(cachedUidGenerator.getUID());
    }

    /**
     * 格式化传入的uid，方便查看其实际含义
     * @author Liuxu
     * @date 2024/1/23 11:58
     * @param uid uid
     * @return java.lang.String
     */
    private String parse(long uid) {
        return cachedUidGenerator.parseUID(uid);
    }
}
