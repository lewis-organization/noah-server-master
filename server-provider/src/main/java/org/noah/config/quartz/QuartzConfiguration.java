package org.noah.config.quartz;

import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import lombok.RequiredArgsConstructor;
import org.quartz.Scheduler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;
import java.io.IOException;

/**
 * quartz配置
 */
@Configuration
@RequiredArgsConstructor
public class QuartzConfiguration {
    private final DataSource dataSource;
    private final TaskJobFactory jobFactory;
    private final ThreadPoolTaskExecutor executor;

    @Bean(name = "SchedulerFactory")
    public SchedulerFactoryBean schedulerFactoryBean() {
        //创建SchedulerFactoryBean
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        factory.setSchedulerName("QUARTZ_SCHEDULER");
        DataSource ds = ((DynamicRoutingDataSource) dataSource).getDataSource("quartz");
        factory.setDataSource(ds);
        factory.setJobFactory(jobFactory);
        factory.setTaskExecutor(executor);
        return factory;
    }

    /*
     * 通过SchedulerFactoryBean获取Scheduler的实例
     */
    @Bean(name = "scheduler")
    public Scheduler scheduler() throws IOException {
        return schedulerFactoryBean().getScheduler();
    }

}
