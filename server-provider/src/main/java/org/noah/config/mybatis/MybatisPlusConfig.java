package org.noah.config.mybatis;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis-plus 核心配置
 *
 * @author liuxu
 */
@Configuration
@MapperScan(value = "org.noah.modules.*.*.mapper")
public class MybatisPlusConfig {

    /**
     * 插件配置
     * @author Liuxu
     * @date 2024/1/23 12:03
     * @return MybatisPlusInterceptor
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        //添加分页
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        //添加乐观锁
        mybatisPlusInterceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
        //SQL 性能规范
//        mybatisPlusInterceptor.addInnerInterceptor(new IllegalSQLInnerInterceptor());
        //防止全表更新与删除
        mybatisPlusInterceptor.addInnerInterceptor(new BlockAttackInnerInterceptor());
        return mybatisPlusInterceptor;
    }

    /**
     * 注入sql注入器
     * @author Liuxu
     * @date 2024/1/23 12:03
     * @return ISqlInjector
     */
    @Bean
    public ISqlInjector sqlInjector() {
        return new DefaultSqlInjector();
    }

    /**
     * 公共字段自动填充
     * @author Liuxu
     * @date 2024/1/23 12:03
     * @return com.baomidou.mybatisplus.core.handlers.MetaObjectHandler
     */
    @Bean
    public MetaObjectHandler metaObjectHandler() {
        return new GeneralMetaObjectHandler();
    }

}
