package org.noah.config.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import enumc.TrueOrFalseEnum;
import org.apache.ibatis.reflection.MetaObject;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 公共字段填充执行类
 *
 * @author liuxu
 */
public class GeneralMetaObjectHandler implements MetaObjectHandler {

    /**
     * 新增数据时自动填充字段
     * @author Liuxu
     * @date 2024/1/23 12:01
     * @param metaObject metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Object createTime = getFieldValByName("createTime", metaObject);
        Object delete = getFieldValByName("deleteFlag", metaObject);
        Object version = getFieldValByName("version", metaObject);
        if (createTime == null) {
            setFieldValByName("createTime", LocalDateTime.now(), metaObject);
        }
        if (delete == null) {
            setFieldValByName("deleteFlag", TrueOrFalseEnum.FALSE.getValue(), metaObject);
        }
        if (Objects.isNull(version)) {
            setFieldValByName("version", 1, metaObject);
        }
    }

    /**
     * 数据修改时填充
     * @author Liuxu
     * @date 2024/1/23 12:02
     * @param metaObject metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        Object updateTime = getFieldValByName("updateTime", metaObject);
        if (updateTime == null) {
            setFieldValByName("updateTime", LocalDateTime.now(), metaObject);
        }
    }

}
