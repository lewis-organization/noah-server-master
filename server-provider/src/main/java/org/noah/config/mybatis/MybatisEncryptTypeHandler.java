package org.noah.config.mybatis;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;
import org.noah.utils.AesUtil;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * 加解密TypeHandler
 * <p>
 * 1.@MappedTypes表示该处理器处理的java类型是什么。
 * 2.@MappedJdbcTypes表示处理器处理的Jdbc类型。
 */
@MappedJdbcTypes(JdbcType.VARCHAR)
@MappedTypes(MybatisEncrypt.class)
public class MybatisEncryptTypeHandler extends BaseTypeHandler<MybatisEncrypt> {

    /**
     * 设置参数
     * @author Liuxu
     * @date 2024/1/23 12:02
     * @param preparedStatement preparedStatement
     * @param i i
     * @param mybatisEncrypt mybatisEncrypt
     * @param jdbcType jdbcType
     */
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, MybatisEncrypt mybatisEncrypt, JdbcType jdbcType) throws SQLException {
        if (mybatisEncrypt == null || mybatisEncrypt.getValue() == null) {
            preparedStatement.setString(i, null);
            return;
        }
        String encrypt = AesUtil.encrypt(mybatisEncrypt.getValue(), AesUtil.getAssetsDevPwdField());
        preparedStatement.setString(i, encrypt);
    }

    /**
     * 获取值
     * @author Liuxu
     * @date 2024/1/23 12:02
     * @param resultSet resultSet
     * @param columnName columnName
     * @return org.noah.config.mybatis.MybatisEncrypt
     */
    @Override
    public MybatisEncrypt getNullableResult(ResultSet resultSet, String columnName) throws SQLException {
        return mybatisEncrypt(resultSet.getString(columnName));
    }

    /**
     * 获取值
     * @author Liuxu
     * @date 2024/1/23 12:02
     * @param resultSet resultSet
     * @param columnIndex columnIndex
     * @return org.noah.config.mybatis.MybatisEncrypt
     */
    @Override
    public MybatisEncrypt getNullableResult(ResultSet resultSet, int columnIndex) throws SQLException {
        return mybatisEncrypt(resultSet.getString(columnIndex));
    }

    @Override
    public MybatisEncrypt getNullableResult(CallableStatement callableStatement, int columnIndex) throws SQLException {
        return mybatisEncrypt(callableStatement.getString(columnIndex));
    }

    public MybatisEncrypt mybatisEncrypt(String value) {
        if (null == value) {
            return null;
        }
        return new MybatisEncrypt(AesUtil.decrypt(value, AesUtil.getAssetsDevPwdField()));
    }
}
