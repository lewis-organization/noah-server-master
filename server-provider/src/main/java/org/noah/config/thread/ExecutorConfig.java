package org.noah.config.thread;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

@Configuration
@EnableAsync
@Slf4j
public class ExecutorConfig {

    /**
     * 线程名称前缀
     **/
    private static final String THREAD_NAME_PREFIX = "task-async-service-";

    /**
     * 核心线程数
     **/
    @Value("${threadPool.corePoolSize}")
    private int corePoolSize;
    /**
     * 最大线程数
     **/
    @Value("${threadPool.maxPoolSize}")
    private int maxPoolSize;
    /**
     * 队列长度
     **/
    @Value("${threadPool.queueCapacity}")
    private int queueCapacity;
    @Value("${threadPool.keepAliveSeconds}")
    private int keepAliveSeconds;

    @Bean(name = "asyncServiceExecutor")
    public ThreadPoolTaskExecutor serviceExecutor() {
        ThreadPoolTaskExecutor executor = new TaskThreadPoolExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setQueueCapacity(queueCapacity);
        executor.setKeepAliveSeconds(keepAliveSeconds);

        //配置线程名称前缀
        executor.setThreadNamePrefix(THREAD_NAME_PREFIX);
        //当前线程池maxPoolSize满了，采取由当前调用者所在的线程处理任务
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        //初始化线程池
        executor.initialize();
        log.info("Thread pool executor initialized");
        return executor;
    }
}
