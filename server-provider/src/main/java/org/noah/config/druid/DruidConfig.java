package org.noah.config.druid;


import cn.hutool.core.map.MapUtil;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * druid数据源配置
 */
@Configuration
public class DruidConfig {

    /**
     * 主要实现web监控的配置处理
     * @author Liuxu
     * @date 2024/1/23 11:57
     * @return ServletRegistrationBean
     */
    @Bean
    public ServletRegistrationBean druidServlet() {
        //表示进行druid监控的配置处理操作
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(
                new StatViewServlet(), "/druid/*");
        //是否可以重置数据源
        // 配置druid参数
        Map<String, String> initParams = MapUtil.newHashMap();
        // 默认访问用户密码 admin:admin
        initParams.put("loginUsername", "admin");
        initParams.put("loginPassword", "qwer1234!@#$");
        // 允许访问的ip
        initParams.put("allow", "127.0.0.1");
        // 黑名单ip，优先级比allow配置高
        initParams.put("deny", "");
        servletRegistrationBean.setInitParameters(initParams);
        return servletRegistrationBean;

    }

    /**
     * 监控
     *
     * @return
     */
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new WebStatFilter());
        //所有请求进行监控处理
        filterRegistrationBean.addUrlPatterns("/*");
        //排除
        filterRegistrationBean.addInitParameter("exclusions", "/static/*,*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        return filterRegistrationBean;
    }

}
