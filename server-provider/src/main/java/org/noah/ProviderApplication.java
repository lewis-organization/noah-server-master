package org.noah;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuxu
 * @packageName org.noah
 * @time 2021/10/19-14:30
 */
@SpringBootApplication
@EnableDubbo(scanBasePackages = "org.noah.modules.*.*.service.impl")
public class ProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProviderApplication.class, args);
    }
}
