package org.noah.modules.fl.notes.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 审核历史记录
 * </p>
 *
 * @author liuxu
 * @since 2022-11-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("fl_notes")
public class Notes extends BaseEntity<Notes> {

    private static final long serialVersionUID = 1L;

    /**
     * 任务ID
     */
    @TableField("process_instance_id")
    private String processInstanceId;

    /**
     * 审核意见
     */
    @TableField("idea")
    private String idea;

    /**
     * 描述
     */
    @TableField("des")
    private String des;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
