package org.noah.modules.sys.attach.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author liuxu
 * @since 2023-03-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_attach_config")
public class AttachConfig extends BaseEntity<AttachConfig> {

    private static final long serialVersionUID = 1L;

    /**
     * 文件大小
     */
    @TableField("file_size")
    private Long fileSize;

    /**
     * 保存路径
     */
    @TableField("save_path")
    private String savePath;

    /**
     * 支持的类型
     */
    @TableField("support_type")
    private String supportType;

    /**
     * 压缩类型
     */
    @TableField("compress_type")
    private String compressType;

    /**
     * 预览类型
     */
    @TableField("preview_type")
    private String previewType;

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
