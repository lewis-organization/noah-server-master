package org.noah.modules.sys.message.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author liuxu
 * @since 2022-01-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_web_message")
public class WebMessage extends BaseEntity<WebMessage> {

    private static final long serialVersionUID = 1L;

    /**
     * 图片
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 标题
     */
    @TableField("title")
    private String title;

    /**
     * 内容
     */
    @TableField("content")
    private String content;

    /**
     * 发送人
     */
    @TableField("from_user")
    private String fromUser;

    /**
     * 类型
     */
    @TableField("type")
    private String type;

    /**
     * 是否显示
     */
    @TableField("display")
    private String display;

    /**
     * 接受组织
     */
    @TableField("org_id")
    private Integer orgId;

    /**
     * 接收人
     */
    @TableField("receiver")
    private Integer receiver;

    /**
     * 是否已读
     */
    @TableField("read_flag")
    private String readFlag;

    /**
     * 时间线
     */
    @TableField(exist = false)
    private String time;

    /**
     * 接收用户ID
     */
    @TableField(exist = false)
    private List<Integer> userIds;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
