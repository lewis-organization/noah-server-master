package org.noah.modules.sys.userRole.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author liuxu
 * @since 2021-12-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_user_role")
public class UserRole extends BaseEntity<UserRole> {

    private static final long serialVersionUID = -4570415828198585550L;

    /**
     * 后台用户id
     */
    @TableField("web_user_id")
    private Integer webUserId;

    /**
     * 角色id
     */
    @TableField("role_id")
    private Integer roleId;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
