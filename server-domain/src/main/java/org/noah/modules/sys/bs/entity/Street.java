package org.noah.modules.sys.bs.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 街道设置
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("bs_street")
public class Street extends BaseEntity<Street> {

    private static final long serialVersionUID = 1L;

    /**
     * 街道代码
     */
    @TableField("street_code")
    private String streetCode;

    /**
     * 父级区代码
     */
    @TableField("area_code")
    private String areaCode;

    /**
     * 街道名称
     */
    @TableField("street_name")
    private String streetName;

    /**
     * 简称
     */
    @TableField("short_name")
    private String shortName;

    /**
     * 经度
     */
    @TableField("lng")
    private String lng;

    /**
     * 纬度
     */
    @TableField("lat")
    private String lat;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
