package org.noah.modules.sys.log.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author liuxu
 * @since 2022-04-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_log")
public class Log extends BaseEntity<Log> {

    private static final long serialVersionUID = 1L;

    /**
     * 模块名称
     */
    @TableField("module")
    private String module;

    /**
     * 请求方式
     */
    @TableField("method")
    private String method;

    /**
     * 请求方法名称
     */
    @TableField("method_name")
    private String methodName;

    /**
     * 请求地址
     */
    @TableField("uri")
    private String uri;

    /**
     * 请求参数名
     */
    @TableField("param_name")
    private String paramName;

    /**
     * 请求参数
     */
    @TableField("param_value")
    private String paramValue;

    /**
     * 浏览器
     */
    @TableField("browser")
    private String browser;

    /**
     * 操作地址
     */
    @TableField("ip")
    private String ip;

    /**
     * 操作系统
     */
    @TableField("os")
    private String os;

    /**
     * 操作人
     */
    @TableField("creater")
    private String creater;

    /**
     * 访问状态
     */
    @TableField("state")
    private String state;

    /**
     * 类型
     */
    @TableField("type")
    private String type;

    /**
     * 来源
     */
    @TableField("source")
    private String source;

    /**
     * 错误消息
     */
    @TableField("message")
    private String message;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
