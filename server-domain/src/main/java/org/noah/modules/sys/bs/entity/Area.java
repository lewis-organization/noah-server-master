package org.noah.modules.sys.bs.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 地区设置
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("bs_area")
public class Area extends BaseEntity<Area> {

    private static final long serialVersionUID = 1L;

    /**
     * 区代码
     */
    @TableField("area_code")
    private String areaCode;

    /**
     * 父级市代码
     */
    @TableField("city_code")
    private String cityCode;

    /**
     * 区名称
     */
    @TableField("area_name")
    private String areaName;

    /**
     * 简称
     */
    @TableField("short_name")
    private String shortName;

    /**
     * 经度
     */
    @TableField("lng")
    private String lng;

    /**
     * 纬度
     */
    @TableField("lat")
    private String lat;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
