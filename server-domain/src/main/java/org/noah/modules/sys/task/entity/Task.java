package org.noah.modules.sys.task.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 系统任务
 * </p>
 *
 * @author liuxu
 * @since 2022-01-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_task")
public class Task extends BaseEntity<Task> {

    private static final long serialVersionUID = 1L;

    /**
     * 任务名称
     */
    @TableField("name")
    private String name;

    /**
     * 任务组名称
     */
    @TableField("group_name")
    private String groupName;

    /**
     * 执行时间
     */
    @TableField("cron")
    private String cron;

    /**
     * 状态
     */
    @TableField("state")
    private String state;

    /**
     * 执行类名
     */
    @TableField("class_name")
    private String className;

    /**
     * 描述
     */
    @TableField("des")
    private String des;

    @TableField(exist = false)
    private String classForName;

    @Override
    protected Serializable pkVal() {
        return null;
    }

}
