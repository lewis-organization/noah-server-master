package org.noah.modules.sys.bs.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 省份设置
 * </p>
 *
 * @author liuxu
 * @since 2022-03-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("bs_province")
public class Province extends BaseEntity<Province> {

    private static final long serialVersionUID = 1L;

    /**
     * 省份代码
     */
    @TableField("province_code")
    private String provinceCode;

    /**
     * 省份名称
     */
    @TableField("province_name")
    private String provinceName;

    /**
     * 简称
     */
    @TableField("short_name")
    private String shortName;

    /**
     * 经度
     */
    @TableField("lng")
    private String lng;

    /**
     * 纬度
     */
    @TableField("lat")
    private String lat;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
