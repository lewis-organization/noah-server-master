package org.noah.modules.sys.approve.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Liuxu
 * @since 2024-01-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_approve_step")
public class ApproveStep extends BaseEntity<ApproveStep> {

    /**
     * 审核关联id
     */
    @TableField("approve_id")
    private Integer approveId;

    /**
     * 顺序
     */
    @TableField("seq")
    private Integer seq;


    @Override
    public Serializable pkVal() {
        return null;
    }

}
