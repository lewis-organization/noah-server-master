package org.noah.modules.sys.approve.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Liuxu
 * @since 2024-01-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_approve")
public class Approve extends BaseEntity<Approve> {

    /**
     * 标题
     */
    @TableField("title")
    private String title;

    /**
     * 编号
     */
    @TableField("code")
    private String code;

    /**
     * 类型
     */
    @TableField("type")
    private String type;

    /**
     * 备注说明
     */
    @TableField("remark")
    private String remark;


    @Override
    public Serializable pkVal() {
        return null;
    }

}
