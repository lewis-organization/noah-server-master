package org.noah.modules.sys.approve.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Liuxu
 * @since 2024-05-31
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_approve_records")
public class ApproveRecords extends BaseEntity<ApproveRecords> {

    /**
     * 业务审核ID
     */
    @TableField("approve_business_id")
    private Integer approveBusinessId;

    /**
     * 审核人员ID
     */
    @TableField("approve_personnel_id")
    private Integer approvePersonnelId;

    /**
     * 组织ID
     */
    @TableField("org_id")
    private Integer orgId;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 是否组织随机通过
     */
    @TableField("random_agree")
    private String randomAgree;

    /**
     * 是否审核
     */
    @TableField("approve")
    private String approve;

    /**
     * 审核意见（同意/拒绝）
     */
    @TableField("idea")
    private String idea;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 批次号
     */
    @TableField("batch_code")
    private String batchCode;

    /**
     * 审核业务批次号
     */
    @TableField("approve_business_batch_code")
    private String approveBusinessBatchCode;

    @TableField(exist = false)
    private String approveRecordsIds;
    @TableField(exist = false)
    private Integer webUserId;

    @Override
    public Serializable pkVal() {
        return null;
    }

}
