package org.noah.modules.sys.test.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Liuxu
 * @since 2024-05-28
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_test_function")
public class TestFunction extends BaseEntity<TestFunction> {

    /**
     * 标题
     */
    @TableField("title")
    private String title;
    /**
     * 状态
     */
    @TableField("status")
    private String status;


    @Override
    public Serializable pkVal() {
        return null;
    }

}
