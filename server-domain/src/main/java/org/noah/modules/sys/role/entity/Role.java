package org.noah.modules.sys.role.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;
import org.noah.config.mybatis.MybatisEncrypt;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author liuxu
 * @since 2021-12-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_role")
public class Role extends BaseEntity<Role> {

    private static final long serialVersionUID = 7782637948691662349L;
    /**
     * 角色名称
     */
    @TableField("role_name")
    private MybatisEncrypt roleName;

    /**
     * 角色编码
     */
    @TableField("role_code")
    private String roleCode;

    /**
     * 角色类型
     */
    @TableField("role_type")
    private String roleType;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
