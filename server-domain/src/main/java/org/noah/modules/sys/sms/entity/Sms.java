package org.noah.modules.sys.sms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuxu
 * @since 2023-02-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_sms")
public class Sms extends BaseEntity<Sms> {

    private static final long serialVersionUID = 1L;

    /**
     * 电话号码
     */
    @TableField("phone")
    private String phone;

    /**
     * 验证码
     */
    @TableField("code")
    private String code;

    /**
     * 状态
     */
    @TableField("state")
    private String state;

    /**
     * 过期时间
     */
    @TableField("expire_time")
    private LocalDateTime expireTime;

    /**
     * 失败原因
     */
    @TableField("reason")
    private String reason;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
