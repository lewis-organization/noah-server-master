package org.noah.modules.sys.approve.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_approve_business")
public class ApproveBusiness extends BaseEntity<ApproveBusiness> {

    /**
     * 关联审核id
     */
    @TableField("approve_id")
    private Integer approveId;

    /**
     * 关联业务id
     */
    @TableField("business_id")
    private Integer businessId;

    /**
     * 业务类型标识
     */
    @TableField("business_type")
    private String businessType;

    /**
     * 审核状态
     */
    @TableField("status")
    private String status;

    /**
     * 是否完成
     */
    @TableField("complete")
    private String complete;

    /**
     * 批次号
     */
    @TableField("batch_code")
    private String batchCode;

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
