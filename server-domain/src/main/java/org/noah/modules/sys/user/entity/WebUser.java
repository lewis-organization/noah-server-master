package org.noah.modules.sys.user.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 系统用户
 * </p>
 *
 * @author liuxu
 * @since 2021-11-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_web_user")
public class WebUser extends BaseEntity<WebUser> {

    private static final long serialVersionUID = -9172983301700532999L;

    /**
     * 用户名
     */
    @TableField("username")
    private String username;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 组织机构id
     */
    @TableField("org_id")
    private Integer orgId;

    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;

    /**
     * 用户类型（见字典user_type）
     */
    @TableField("type")
    private String type;

    /**
     * 盐值
     */
    @TableField("salt")
    private String salt;

    /**
     * 是否可用（1：是，0：否）
     */
    @TableField("enable")
    private String enable;

    /**
     * 图片ID
     */
    @TableField("img_ids")
    private String imgIds;

    @Override
    public Serializable pkVal() {
        return null;
    }
}
