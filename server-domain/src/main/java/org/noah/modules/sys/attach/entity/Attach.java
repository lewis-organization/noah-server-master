package org.noah.modules.sys.attach.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author liuxu
 * @since 2022-01-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_attach")
public class Attach extends BaseEntity<Attach> {

    private static final long serialVersionUID = 1L;

    /**
     * 原始文件名
     */
    @TableField("source_name")
    private String sourceName;

    /**
     * 服务端文件名
     */
    @TableField("server_name")
    private String serverName;

    /**
     * 文件格式
     */
    @TableField("suffix")
    private String suffix;

    /**
     * 文件大小
     */
    @TableField("size")
    private Long size;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
