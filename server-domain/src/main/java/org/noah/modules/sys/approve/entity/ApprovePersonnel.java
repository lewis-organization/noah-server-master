package org.noah.modules.sys.approve.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuxu
 * @since 2024-01-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_approve_personnel")
public class ApprovePersonnel extends BaseEntity<ApprovePersonnel> {

    /**
     * 关联审核id
     */
    @TableField("approve_id")
    private Integer approveId;

    /**
     * 审核步骤id
     */
    @TableField("approve_step_id")
    private Integer approveStepId;

    /**
     * 关联组织id
     */
    @TableField("org_id")
    private Integer orgId;

    /**
     * 关联用户id
     */
    @TableField("user_id")
    private Integer userId;

    /**
     * 顺序
     */
    @TableField("seq")
    private Integer seq;

    /**
     * 是否组织随机通过
     */
    @TableField("random_agree")
    private String randomAgree;

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
