package org.noah.modules.sys.config.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author liuxu
 * @since 2022-09-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_system_config")
public class SystemConfig extends BaseEntity<SystemConfig> {

    private static final long serialVersionUID = 1L;

    /**
     * 应用程序名称
     */
    @TableField("application_name")
    private String applicationName;

    /**
     * 是否显示验证码
     */
    @TableField("show_captcha")
    private String showCaptcha;

    /**
     * 登录公钥
     */
    @TableField("pub_key")
    private String pubKey;

    /**
     * 登录私钥
     */
    @TableField("pri_key")
    private String priKey;

    /**
     * 系统描述
     */
    @TableField("sys_des")
    private String sysDes;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
