package org.noah.modules.sys.test.entity;

import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * @author liuxu
 * @packageName org.noah.modules.sys.test.entity
 * @time 2021/10/20-10:15
 */
public class Test extends BaseEntity {

    private static final long serialVersionUID = 601760030531501816L;

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
