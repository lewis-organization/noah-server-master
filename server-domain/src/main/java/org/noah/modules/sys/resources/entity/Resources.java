package org.noah.modules.sys.resources.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author liuxu
 * @since 2021-11-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_resources")
public class Resources extends BaseEntity<Resources> {

    private static final long serialVersionUID = -2673854076953758286L;
    /**
     * 上级ID
     */
    @TableField("parent_id")
    private Integer parentId;

    /**
     * 菜单名称
     */
    @TableField("title")
    private String title;

    /**
     * 类型（0：目录，1：菜单，2：按钮）
     */
    @TableField("type")
    private String type;

    /**
     * 图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 地址
     */
    @TableField("href")
    private String href;

    /**
     * 权限编码
     */
    @TableField("code")
    private String code;

    /**
     * 打开方式
     */
    @TableField("open_type")
    private String openType;

    /**
     * 顺序
     */
    @TableField("seq")
    private Integer seq;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
