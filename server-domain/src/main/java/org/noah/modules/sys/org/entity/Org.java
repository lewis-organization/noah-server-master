package org.noah.modules.sys.org.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author liuxu
 * @since 2022-11-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_org")
public class Org extends BaseEntity<Org> {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    @TableField("title")
    private String title;

    /**
     * 上级ID
     */
    @TableField("parent_id")
    private Integer parentId;

    /**
     * 是否可用
     */
    @TableField("enable")
    private String enable;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
