package org.noah.modules.app.appUser.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.noah.config.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author liuxu
 * @since 2023-02-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("app_user")
public class AppUser extends BaseEntity<AppUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 手机号
     */
    @TableField("phone")
    private String phone;

    /**
     * 用户名称
     */
    @TableField("name")
    private String name;

    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;

    /**
     * 盐
     */
    @TableField("salt")
    private String salt;

    /**
     * 密码
     */
    @TableField("password")
    private String password;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
