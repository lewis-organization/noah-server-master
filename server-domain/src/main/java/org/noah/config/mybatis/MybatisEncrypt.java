package org.noah.config.mybatis;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class MybatisEncrypt implements Serializable {

    private String value;
}
