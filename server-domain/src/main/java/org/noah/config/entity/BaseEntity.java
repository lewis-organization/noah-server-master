package org.noah.config.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public abstract class BaseEntity<T> implements Serializable {

    private static final long serialVersionUID = -5213638409121126744L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(value = "update_time", fill = FieldFill.UPDATE)
    private LocalDateTime updateTime;

    @TableLogic
    @TableField(value = "delete_flag", fill = FieldFill.INSERT)
    private String deleteFlag;

    @Version
    @TableField(value = "version", fill = FieldFill.INSERT)
    private Integer version;

    protected abstract Serializable pkVal();
}
