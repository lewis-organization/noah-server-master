package org.noah.config.sms;

import org.apache.shiro.authc.AuthenticationException;

public class SmsCodeExpiredException extends AuthenticationException {

    public SmsCodeExpiredException() {
    }

    public SmsCodeExpiredException(String message) {
        super(message);
    }

    public SmsCodeExpiredException(Throwable cause) {
        super(cause);
    }

    public SmsCodeExpiredException(String message, Throwable cause) {
        super(message, cause);
    }
}
