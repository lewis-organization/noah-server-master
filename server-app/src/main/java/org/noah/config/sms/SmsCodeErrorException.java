package org.noah.config.sms;

import org.apache.shiro.authc.AuthenticationException;

public class SmsCodeErrorException extends AuthenticationException {

    public SmsCodeErrorException() {
    }

    public SmsCodeErrorException(String message) {
        super(message);
    }

    public SmsCodeErrorException(Throwable cause) {
        super(cause);
    }

    public SmsCodeErrorException(String message, Throwable cause) {
        super(message, cause);
    }
}
