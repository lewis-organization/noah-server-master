package org.noah.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Data
public class AppConfig {

    @Value("${app.token.expiredTime}")
    private Long expiredTime;

    @Value("${customConfig.swagger.open}")
    private boolean swaggerOpen;
}
