package org.noah.config.shiro;

import enumc.RedisCacheKeyEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.noah.config.CommonConstant;
import org.noah.config.sms.SmsCodeErrorException;
import org.noah.config.sms.SmsCodeExpiredException;
import org.noah.modules.app.appUser.entity.AppUser;
import org.noah.modules.app.appUser.service.AppUserService;
import org.noah.modules.sys.sms.entity.Sms;
import org.noah.modules.sys.sms.service.SmsService;

import java.util.Objects;

@Slf4j
public class ShiroAppRealm extends AuthorizingRealm {

    @DubboReference
    private SmsService smsService;
    @DubboReference
    private AppUserService appUserService;

    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        return null;
    }

    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        ShiroAppToken shiroAppToken = (ShiroAppToken) token;
        //app用户
        AppUser appUser = appUserService.getAppUserByPhone(shiroAppToken.getPhone());
        if (Objects.isNull(appUser)) {
            throw new UnknownAccountException();
        }
        SimpleAuthenticationInfo simpleAuthenticationInfo;
        // 这里验证authenticationToken和simpleAuthenticationInfo的信息
        if (StringUtils.isBlank(shiroAppToken.getCode())) {
            //验证码为空， 判断使用密码登录
            simpleAuthenticationInfo = new SimpleAuthenticationInfo(appUser,
                    appUser.getPassword(), ByteSource.Util.bytes(appUser.getSalt()), getName());
        } else {
            //使用验证码登录
            //判断验证码是否正确
            Sms sms = smsService.getByPhoneAndCode(shiroAppToken.getPhone(), shiroAppToken.getCode());
            if (Objects.isNull(sms)) {
                throw new SmsCodeErrorException();
            }
            //判断验证码是否过期
            if (smsService.isExpired(shiroAppToken.getPhone(), shiroAppToken.getCode())) {
                throw new SmsCodeExpiredException();
            }
            //通用的密码
            shiroAppToken.setPassword("123456".toCharArray());
            simpleAuthenticationInfo = new SimpleAuthenticationInfo(appUser,
                    CommonConstant.PASSWORD, ByteSource.Util.bytes(CommonConstant.SALT), getName());
        }
        return simpleAuthenticationInfo;
    }

    @Override
    protected Object getAuthorizationCacheKey(PrincipalCollection principals) {
        AppUser appUser = (AppUser) principals.getPrimaryPrincipal();
        return RedisCacheKeyEnum.APP_PERM_CACHE.getKey() + appUser.getPhone();
    }
}
