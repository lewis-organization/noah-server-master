package org.noah.config.shiro;

import cn.hutool.core.codec.Base64;
import lombok.RequiredArgsConstructor;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.noah.config.filter.SessionControlFilter;
import org.noah.config.shiro.cache.ShiroRedisCacheManager;
import org.noah.config.shiro.session.GlobalSessionManager;
import org.noah.config.shiro.session.RedisSessionDao;
import org.noah.utils.RedisUtil;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
@RequiredArgsConstructor
public class ShiroAppConfig {

    private final RedisUtil redisUtil;

    @Bean
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        // 其他过滤器配置
        Map<String, Filter> filters = new HashMap<>(1);
        filters.put("kickout", sessionControlFilter());
        shiroFilterFactoryBean.setFilters(filters);

        // url过滤器链配置
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        // 未授权界面;
        shiroFilterFactoryBean.setUnauthorizedUrl("/unauthorized");
        //系统页面
        shiroFilterFactoryBean.setLoginUrl("/unauthorized");
        //系统方法
        filterChainDefinitionMap.put("/login", "anon");
        //swagger资源不过滤
        filterChainDefinitionMap.put("/swagger-resources", "anon");
        filterChainDefinitionMap.put("/v2/**", "anon");
        filterChainDefinitionMap.put("/document.html", "anon");
        filterChainDefinitionMap.put("/assets/**", "anon");
        filterChainDefinitionMap.put("/logo.png", "anon");
        //项目性能工具不过滤
        filterChainDefinitionMap.put("/koTime/**", "anon");
        //测试方法
        filterChainDefinitionMap.put("/test/**", "anon");
        //静态资源
        filterChainDefinitionMap.put("/static/**", "anon");
        // 注意过滤器配置顺序 不能颠倒
        filterChainDefinitionMap.put("/**", "authc, kickout");
        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }

    /**
     * 登录的MD5密码加密
     * @author Liuxu
     * @date 2024/1/23 11:23
     * @return org.apache.shiro.authc.credential.HashedCredentialsMatcher
     */
    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        // 散列算法:这里使用MD5算法;
        hashedCredentialsMatcher.setHashAlgorithmName("md5");
        // 散列的次数，比如散列两次，相当于 md5(md5(""));
        hashedCredentialsMatcher.setHashIterations(2);
        hashedCredentialsMatcher.setStoredCredentialsHexEncoded(true);
        return hashedCredentialsMatcher;
    }

    /**
     * 登录授权认证
     * @author Liuxu
     * @date 2024/1/23 11:23
     * @return org.noah.config.shiro.ShiroAppRealm
     */
    @Bean
    public ShiroAppRealm shiroAppRealm() {
        ShiroAppRealm shiroAppRealm = new ShiroAppRealm();
        shiroAppRealm.setCredentialsMatcher(hashedCredentialsMatcher());
        return shiroAppRealm;
    }

    /**
     * shiro 安全管理器
     * @author Liuxu
     * @date 2024/1/23 11:23
     * @return org.apache.shiro.mgt.SecurityManager
     */
    @Bean
    public SecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(shiroAppRealm());
        // 自定义session管理 使用redis
        securityManager.setSessionManager(sessionManager());
        // 自定义缓存实现 使用redis
        securityManager.setCacheManager(shiroRedisCacheManager());
        //记住我cookie
        securityManager.setRememberMeManager(cookieRememberMeManager());
        return securityManager;
    }

    /**
     * cookie配置
     * @author Liuxu
     * @date 2024/1/23 11:23
     * @return org.apache.shiro.web.servlet.SimpleCookie
     */
    @Bean
    public SimpleCookie simpleCookie() {
        // 这个参数是cookie的名称，对应前端的checkbox的name = rememberMe
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
        simpleCookie.setHttpOnly(true);
        simpleCookie.setPath("/");
        // <!-- 记住我cookie生效时间7天 ,单位秒;-->
        simpleCookie.setMaxAge(604800);
        return simpleCookie;
    }

    /**
     * 记住我cookie配置
     * @author Liuxu
     * @date 2024/1/23 11:23
     * @return org.apache.shiro.web.mgt.CookieRememberMeManager
     */
    @Bean
    public CookieRememberMeManager cookieRememberMeManager() {
        CookieRememberMeManager cookieRememberMeManager = new CookieRememberMeManager();
        cookieRememberMeManager.setCipherKey(Base64.decode("2AvVhdsgUs0FSA3SDFAdag=="));
        cookieRememberMeManager.setCookie(simpleCookie());
        return cookieRememberMeManager;
    }

    /**
     * session管理器
     * @author Liuxu
     * @date 2024/1/23 11:23
     * @return org.apache.shiro.web.session.mgt.DefaultWebSessionManager
     */
    @Bean
    public DefaultWebSessionManager sessionManager() {
        GlobalSessionManager globalSessionManager = new GlobalSessionManager(redisSessionDao());
        // 用redis缓存session
        globalSessionManager.setSessionDAO(redisSessionDao());
        // 删除失效的session
        globalSessionManager.setDeleteInvalidSessions(true);
        // session不在url后面显示
        globalSessionManager.setSessionIdUrlRewritingEnabled(false);
        // 定时验证session可用
        globalSessionManager.setSessionValidationSchedulerEnabled(true);
        // sessionId-cookie 配置
        Cookie cookie = new SimpleCookie();
        cookie.setName("APP-SESSION");
        cookie.setHttpOnly(true);
        globalSessionManager.setSessionIdCookie(cookie);
        return globalSessionManager;
    }

    /**
     * redis sessionDao 实现
     * @author Liuxu
     * @date 2024/1/23 11:23
     * @return org.noah.config.shiro.session.RedisSessionDao
     */
    @Bean
    public RedisSessionDao redisSessionDao() {
        return new RedisSessionDao(redisUtil);
    }

    /**
     * <p>
     * shiro-redis缓存管理
     * </p>
     *
     * @author 刘旭
     */
    public ShiroRedisCacheManager shiroRedisCacheManager() {
        return new ShiroRedisCacheManager(redisUtil);
    }

    /**
     * 限制同一账号登录同时登录人数控制
     * @author Liuxu
     * @date 2024/1/23 11:24
     * @return org.noah.config.filter.SessionControlFilter
     */
    @Bean
    public SessionControlFilter sessionControlFilter() {
        SessionControlFilter sessionControlFilter = new SessionControlFilter();
        sessionControlFilter.setRedisCacheManager(shiroRedisCacheManager());
        sessionControlFilter.setSessionManager(sessionManager());
        sessionControlFilter.setKickoutAfter(false);
        sessionControlFilter.setMaxSession(1);
        return sessionControlFilter;
    }

    /**
     * 使用注解控制权限
     * @author Liuxu
     * @date 2024/1/23 11:24
     * @param securityManager 安全管理器
     * @return org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }

    @Bean
    public DefaultAdvisorAutoProxyCreator getDefaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator autoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        autoProxyCreator.setProxyTargetClass(true);
        return autoProxyCreator;
    }
}
