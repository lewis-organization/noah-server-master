package org.noah.config.shiro;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * 这个就类似UsernamePasswordToken
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ShiroAppToken extends UsernamePasswordToken {

    private String phone;

    private String type;

    private String code;
}
