package org.noah.config.shiro.session;

import enumc.RedisCacheKeyEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
import org.noah.utils.RedisUtil;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@RequiredArgsConstructor
public class RedisSessionDao extends CachingSessionDAO {
    private final RedisUtil redisUtils;

    @Override
    public void update(Session session) {
        if (session == null || session.getId() == null) {
            log.error("update session error: session or session id is null");
            return;
        }
        try {
            redisUtils.set(RedisCacheKeyEnum.APP_SESSION_CACHE.getKey() + session.getId(), session,
                    session.getTimeout() / 1000);
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
            throw new UnknownSessionException(e);
        }
    }

    @Override
    protected void doUpdate(Session session) {

    }

    @Override
    protected void doDelete(Session session) {
        if (session == null || session.getId() == null) {
            log.error("redis delete session error:session or session id is null");
            return;
        }
        try {
            redisUtils.delete(RedisCacheKeyEnum.APP_SESSION_CACHE.getKey() + session.getId());
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
        }
    }

    @Override
    protected Serializable doCreate(Session session) {
        if (session == null) {
            log.error("create session error: session is null");
            return null;
        }
        Serializable sessionId = this.generateSessionId(session);
        this.assignSessionId(session, sessionId);
        redisUtils.set(RedisCacheKeyEnum.APP_SESSION_CACHE.getKey() + sessionId, session,
                session.getTimeout() / 1000);
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        if (sessionId == null) {
            log.error("redis read session error: sessionId is null");
            return null;
        }
        Session session = null;
        try {
            session = (Session) redisUtils.get(RedisCacheKeyEnum.APP_SESSION_CACHE.getKey() + sessionId);
        } catch (Exception e) {
            // TODO: handle exception
            log.error(e.getMessage(), e);
        }
        return session;
    }

    /**
     * 获取所有活动session
     * @author Liuxu
     * @date 2024/1/23 11:24
     * @return Collection<Session>
     */
    @Override
    public Collection<Session> getActiveSessions() {
        Set<String> kes = redisUtils.keys(RedisCacheKeyEnum.APP_SESSION_CACHE.getKey() + "*");
        Set<Session> sessions = new HashSet<>();
        for (String key : kes) {
            sessions.add((Session) redisUtils.get(key));
        }
        return sessions;
    }

    /**
     * session redis 缓存前缀
     * @author Liuxu
     * @date 2024/1/23 11:24
     * @return java.lang.String
     */
    @Override
    public String getActiveSessionsCacheName() {
        return RedisCacheKeyEnum.APP_SESSION_CACHE.getKey();
    }
}
