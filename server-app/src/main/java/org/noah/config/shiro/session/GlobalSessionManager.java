package org.noah.config.shiro.session;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.InvalidSessionException;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.session.mgt.WebSessionKey;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.Serializable;

import static org.noah.config.CommonConstant.AUTHORIZATION;
import static org.noah.config.CommonConstant.REFERENCED_SESSION_ID_SOURCE;

/**
 * @author hefu
 */
@RequiredArgsConstructor
public class GlobalSessionManager extends DefaultWebSessionManager {

    private final RedisSessionDao redisSessionDAO;

    @Override
    protected Serializable getSessionId(ServletRequest request, ServletResponse response) {
        String id = WebUtils.toHttp(request).getHeader(AUTHORIZATION);
        // 如果请求头中有 token 则其值为sessionId
        if (StringUtils.isNotBlank(id)) {
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_SOURCE, REFERENCED_SESSION_ID_SOURCE);
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID, id);
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_IS_VALID, Boolean.TRUE);
            request.setAttribute(ShiroHttpServletRequest.SESSION_ID_URL_REWRITING_ENABLED, this.isSessionIdUrlRewritingEnabled());
            return id;
        } else {
            // 否则按默认规则从cookie取sessionId
            return super.getSessionId(request, response);
        }
    }

    /**
     * 对 doReadSession  和 doUpdate 次数过于频繁进行优化
     * 我们通过shiroConfiguration中的SessionManager进行追踪， 发现是DefaultWebSessionManager类，
     * 而它又是继承自DefaultSessionManager，其中retrieveSession方法中进行doReadSession,onChange方法进行doUpdate。
     * 对这两个方法进行重写，即可进行优化。在这里retrieveSession方法中我先将SessionKey强转成WebSessionKey，
     * 从中取出request。第一次readSession我们把Session写入request中，那么之后如果有重复操作的时候
     * 就可以从request中将最先写入的Session给读出来。 从而对 doReadSession的次数进行优化。 doUpdate
     * 次数过多的原因基本来源于 timeout的问题，所以可以在onChange 方法中对timeout进行一个限制 ，从而减少 doUpdate的次数
     */
    @Override
    protected Session retrieveSession(SessionKey sessionKey) throws UnknownSessionException {

        Serializable sessionId = getSessionId(sessionKey);
        ServletRequest request = null;
        if (sessionKey instanceof WebSessionKey) {
            request = ((WebSessionKey) sessionKey).getServletRequest();
        }

        if (request != null && sessionId != null) {

            Session session = (Session) request.getAttribute(sessionId.toString());
            if (session != null) {
                return session;
            }
        }

        Session session = super.retrieveSession(sessionKey);

        if (request != null && sessionId != null) {
            request.setAttribute(sessionId.toString(), session);
        }
        return session;
    }

    @Override
    protected void onChange(Session session) {
        if (session.getTimeout() > 10000 * 500) {
            return;
        }
        super.onChange(session);
    }

    @Override
    protected void validate(Session session, SessionKey key) throws InvalidSessionException {
        this.doValidate(session);
    }

    /**
     * 删除未授权的session
     * @author Liuxu
     * @date 2024/1/23 11:24
     * @param session 会话
     */
    @Override
    protected void doValidate(Session session) throws InvalidSessionException {
        Session redisSession = redisSessionDAO.doReadSession(session.getId());
        if (null != redisSession) {
            if ((null == redisSession.getAttribute("appUser") ||
                    session.getAttribute("kickout") != null)) {
                redisSessionDAO.delete(redisSession);
            }
        }
    }
}
