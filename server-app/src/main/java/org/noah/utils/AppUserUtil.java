package org.noah.utils;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.noah.modules.app.appUser.entity.AppUser;

/**
 * @author liuxu
 * @date 2021/11/1-15:52
 */
public class AppUserUtil {

    public static AppUser getAppUser() {
        Subject subject = SecurityUtils.getSubject();
        return (AppUser) subject.getPrincipal();
    }

}
