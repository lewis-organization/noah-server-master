package org.noah.modules.test.test.controller;

import cn.hutool.core.collection.ListUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.annotation.NoRepeatSubmit;
import org.noah.config.excel.TestExcelDto;
import org.noah.config.json.AjaxResult;
import org.noah.dto.LoginDto;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.test.service.TestService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(value = "")
public class TestController extends BaseController {

    @DubboReference
    private TestService testService;

    @GetMapping("/testdemo")
    @NoRepeatSubmit
    public AjaxResult testdemo() {
//        testService.testSync();
        //System.out.println(AesUtil.encrypt("123456", AesUtil.getAssetsDevPwdField()));
        return AjaxResult.success(testService.getNewMessage(), null);
    }

    @PostMapping(value = "/testValid")
    public AjaxResult testValid(@RequestBody @Valid LoginDto loginDto) {
        return AjaxResult.success();
    }

    //@PostMapping(value = "/esAdd")
    //public AjaxResult esAdd() {
    //    testService.esAdd();
    //    return AjaxResult.success();
    //}

    //@GetMapping(value = "/esQuery")
    //public AjaxResult esQuery() {
    //    return AjaxResult.success(testService.esQuery());
    //}

    @GetMapping(value = "/testExcelExport")
    public void testExcelExport(HttpServletResponse response) {
        renderExcel(response, "测试导出", TestExcelDto.class, ListUtil.empty());
    }
}
