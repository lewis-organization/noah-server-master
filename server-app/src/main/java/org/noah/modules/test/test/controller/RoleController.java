package org.noah.modules.test.test.controller;

import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.role.service.RoleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/role")
public class RoleController extends BaseController {

    @DubboReference
    private RoleService roleService;

    @GetMapping(value = "/testAddRole")
    public AjaxResult testAddRole() {
        roleService.testAddRole();
        return AjaxResult.success();
    }

    @GetMapping(value = "/queryRoleList")
    public AjaxResult queryRoleList(String name) {
        return AjaxResult.success(roleService.queryRoleList(name));
    }
}
