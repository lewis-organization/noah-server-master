package org.noah.modules.app.appUser.controller;

import io.swagger.annotations.ApiOperation;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.modules.app.appUser.service.AppUserService;
import org.noah.modules.controller.BaseController;
import org.noah.utils.AppUserUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/appUser")
public class AppUserController extends BaseController {

    @DubboReference
    private AppUserService appUserService;

    @ApiOperation(value = "获取App用户信息")
    @GetMapping(value = "/getUserInfo")
    public AjaxResult getUserInfo() {
        return AjaxResult.success(appUserService.getUserInfo(AppUserUtil.getAppUser().getId()));
    }
}
