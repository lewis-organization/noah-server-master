package org.noah.modules.sys.system.controller;

import enumc.LogSourceEnum;
import enumc.LogTypeEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.noah.config.AppConfig;
import org.noah.config.annotation.SysLog;
import org.noah.config.json.AjaxResult;
import org.noah.config.shiro.ShiroAppToken;
import org.noah.config.sms.SmsCodeErrorException;
import org.noah.config.sms.SmsCodeExpiredException;
import org.noah.dto.LoginDto;
import org.noah.modules.controller.BaseController;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Slf4j
@RestController
@RequestMapping(value = "")
@RequiredArgsConstructor
public class AppSystemController extends BaseController implements ErrorController {

    private final AppConfig appConfig;

    /**
     * 系统默认提示
     * @author Liuxu
     * @date 2024/1/23 11:26
     * @param loginDto 登录dto
     * @return org.noah.config.json.AjaxResult
     */
    @PostMapping(value = "/login")
    @SysLog(type = LogTypeEnum.LOGIN, source = LogSourceEnum.APP)
    public AjaxResult login(@RequestBody @Valid LoginDto loginDto) {
        Subject subject = SecurityUtils.getSubject();
        ShiroAppToken token = new ShiroAppToken();
        token.setUsername(loginDto.getUsername());
        token.setPassword(loginDto.getPassword().toCharArray());
        token.setCode(loginDto.getCode());
        token.setPhone(loginDto.getPhone());
        try {
            subject.login(token);
            subject.getSession().setAttribute("appUser", subject.getPrincipal());
            subject.getSession().setTimeout(appConfig.getExpiredTime());
        } catch (IncorrectCredentialsException e) {
            return AjaxResult.fail("密码错误");
        } catch (UnknownAccountException e) {
            return AjaxResult.fail("账号不存在");
        } catch (DisabledAccountException e) {
            return AjaxResult.fail("账号不可用");
        } catch (SmsCodeErrorException e) {
            return AjaxResult.fail("验证码错误");
        } catch (SmsCodeExpiredException e) {
            return AjaxResult.fail("验证码已过期");
        } catch (Exception e) {
            return AjaxResult.fail();
        }
        return AjaxResult.success(subject.getSession().getId(), null);
    }

    /**
     * 退出登录
     * @author Liuxu
     * @date 2024/1/23 11:27
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/logout")
    public AjaxResult logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return AjaxResult.success();
    }

    /**
     * 无权限返回
     * @author Liuxu
     * @date 2024/1/23 11:27
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping(value = "/unauthorized")
    public AjaxResult unauthorized() {
        return AjaxResult.fail(HttpStatus.UNAUTHORIZED.value(), "Unauthorized");
    }

    /**
     * 默认错误页自定义返回
     * @author Liuxu
     * @date 2024/1/23 11:27
     * @return org.noah.config.json.AjaxResult
     */
    @GetMapping("/error")
    public AjaxResult error(){
        return AjaxResult.fail("错误的访问");
    }

}
