package org.noah.modules.sys.attach.controller;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.noah.config.json.AjaxResult;
import org.noah.config.upload.MultipartConfig;
import org.noah.modules.controller.BaseController;
import org.noah.modules.sys.attach.entity.Attach;
import org.noah.modules.sys.attach.entity.AttachConfig;
import org.noah.modules.sys.attach.service.AttachConfigService;
import org.noah.modules.sys.attach.service.AttachService;
import org.noah.utils.ImageUtil;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping("/appAttach")
@RequiredArgsConstructor
public class AppAttachController extends BaseController {

    @DubboReference
    private AttachService attachService;
    @DubboReference
    private AttachConfigService attachConfigService;
    private final MultipartConfig multipartConfig;

    /**
     * 文件上传
     * @author Liuxu
     * @date 2024/1/23 11:25
     * @param file 文件
     * @param route 路径
     * @return AjaxResult
     */
    @ApiOperation(value = "图片上传")
    @PostMapping(value = "/upload/{route}")
    public AjaxResult upload(@RequestPart(value = "file") MultipartFile file, @PathVariable String route) {
        AttachConfig attachConfig = attachConfigService.getOne(null);
        if (Objects.isNull(attachConfig)) {
            return AjaxResult.fail("上传配置不明确");
        }
        multipartConfig.setSize(attachConfig.getFileSize());
        multipartConfig.setPath(attachConfig.getSavePath());
        multipartConfig.setType(".*(" + attachConfig.getSupportType().replaceAll(StrUtil.COMMA, "|") + ").*");
        multipartConfig.setCompressTypes(".*(" + attachConfig.getCompressType().replaceAll(StrUtil.COMMA, "|") + ").*");
        if (!multipartConfig.validateSuffix(file)) {
            return AjaxResult.fail("上传文件格式不支持");
        }
        if (multipartConfig.validateSize(file)) {
            return AjaxResult.fail("上传文件大小超出系统限制[" + multipartConfig.getSize() / 1024 + "]M");
        }
        Map<String, Object> resultMap;
        try {
            InputStream inputStream = file.getInputStream();
            //指定类型的文件才进行压缩
            if (multipartConfig.compressSuffix(file)) {
                inputStream = ImageUtil.imgProcess(file.getInputStream(), file.getOriginalFilename());
            }
            resultMap = attachService.upload(IoUtil.readBytes(inputStream), file.getOriginalFilename(), file.getSize(),
                    multipartConfig.getPath() + "/" + route);
            resultMap.put("file_url", request.getContextPath() + "/appAttach/" + route + "/" + resultMap.get("file_id"));
        } catch (Exception ex) {
            return AjaxResult.fail("上传错误");
        }
        return AjaxResult.success(resultMap);
    }

    /**
     * 图片显示
     * @author Liuxu
     * @date 2024/1/23 11:26
     * @param id id
     * @param route 路径
     * @param response 响应
     */
    @ApiOperation(value = "显示图片")
    @GetMapping(value = "/{route}/{id}")
    public void show(@PathVariable String id, @PathVariable String route, HttpServletResponse response) {
        if (StringUtils.isBlank(id)) {
            return;
        }
        Attach attach = attachService.getAttachById(id);
        if (null == attach) {
            return;
        }
        AttachConfig attachConfig = attachConfigService.getOne(null);
        multipartConfig.setPath(attachConfig.getSavePath());
        multipartConfig.setCompressTypes(".*(" + attachConfig.getPreviewType().replaceAll(StrUtil.COMMA, "|") + ").*");
        renderImage(response, multipartConfig, attach, route);
    }
}
