/*
 Navicat Premium Data Transfer

 Source Server         : 虚拟机
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : 192.168.200.3:3306
 Source Schema         : noah-server-flowable

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 23/11/2022 15:03:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for act_app_appdef
-- ----------------------------
DROP TABLE IF EXISTS `act_app_appdef`;
CREATE TABLE `act_app_appdef`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `REV_` int(11) NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `VERSION_` int(11) NOT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_APP_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE,
  INDEX `ACT_IDX_APP_DEF_DPLY`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_APP_DEF_DPLY` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_app_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_app_appdef
-- ----------------------------

-- ----------------------------
-- Table structure for act_app_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_app_databasechangelog`;
CREATE TABLE `act_app_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_app_databasechangelog
-- ----------------------------
INSERT INTO `act_app_databasechangelog` VALUES ('1', 'flowable', 'org/flowable/app/db/liquibase/flowable-app-db-changelog.xml', '2022-11-18 14:40:17', 1, 'EXECUTED', '8:496fc778bdf2ab13f2e1926d0e63e0a2', 'createTable tableName=ACT_APP_DEPLOYMENT; createTable tableName=ACT_APP_DEPLOYMENT_RESOURCE; addForeignKeyConstraint baseTableName=ACT_APP_DEPLOYMENT_RESOURCE, constraintName=ACT_FK_APP_RSRC_DPL, referencedTableName=ACT_APP_DEPLOYMENT; createIndex...', '', NULL, '4.5.0', NULL, NULL, '8753617096');
INSERT INTO `act_app_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/app/db/liquibase/flowable-app-db-changelog.xml', '2022-11-18 14:40:17', 2, 'EXECUTED', '8:ccea9ebfb6c1f8367ca4dd473fcbb7db', 'modifyDataType columnName=DEPLOY_TIME_, tableName=ACT_APP_DEPLOYMENT', '', NULL, '4.5.0', NULL, NULL, '8753617096');
INSERT INTO `act_app_databasechangelog` VALUES ('3', 'flowable', 'org/flowable/app/db/liquibase/flowable-app-db-changelog.xml', '2022-11-18 14:40:17', 3, 'EXECUTED', '8:f1f8aff320aade831944ebad24355f3d', 'createIndex indexName=ACT_IDX_APP_DEF_UNIQ, tableName=ACT_APP_APPDEF', '', NULL, '4.5.0', NULL, NULL, '8753617096');

-- ----------------------------
-- Table structure for act_app_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_app_databasechangeloglock`;
CREATE TABLE `act_app_databasechangeloglock`  (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_app_databasechangeloglock
-- ----------------------------
INSERT INTO `act_app_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_app_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_app_deployment`;
CREATE TABLE `act_app_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_app_deployment
-- ----------------------------

-- ----------------------------
-- Table structure for act_app_deployment_resource
-- ----------------------------
DROP TABLE IF EXISTS `act_app_deployment_resource`;
CREATE TABLE `act_app_deployment_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_APP_RSRC_DPL`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_APP_RSRC_DPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_app_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_app_deployment_resource
-- ----------------------------

-- ----------------------------
-- Table structure for act_cmmn_casedef
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_casedef`;
CREATE TABLE `act_cmmn_casedef`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `REV_` int(11) NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `VERSION_` int(11) NOT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` bit(1) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `DGRM_RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `HAS_START_FORM_KEY_` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_CASE_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE,
  INDEX `ACT_IDX_CASE_DEF_DPLY`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_CASE_DEF_DPLY` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_cmmn_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_casedef
-- ----------------------------

-- ----------------------------
-- Table structure for act_cmmn_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_databasechangelog`;
CREATE TABLE `act_cmmn_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_databasechangelog
-- ----------------------------
INSERT INTO `act_cmmn_databasechangelog` VALUES ('1', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 1, 'EXECUTED', '8:8b4b922d90b05ff27483abefc9597aa6', 'createTable tableName=ACT_CMMN_DEPLOYMENT; createTable tableName=ACT_CMMN_DEPLOYMENT_RESOURCE; addForeignKeyConstraint baseTableName=ACT_CMMN_DEPLOYMENT_RESOURCE, constraintName=ACT_FK_CMMN_RSRC_DPL, referencedTableName=ACT_CMMN_DEPLOYMENT; create...', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 2, 'EXECUTED', '8:65e39b3d385706bb261cbeffe7533cbe', 'addColumn tableName=ACT_CMMN_CASEDEF; addColumn tableName=ACT_CMMN_DEPLOYMENT_RESOURCE; addColumn tableName=ACT_CMMN_RU_CASE_INST; addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('3', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 3, 'EXECUTED', '8:c01f6e802b49436b4489040da3012359', 'addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_RU_CASE_INST; createIndex indexName=ACT_IDX_PLAN_ITEM_STAGE_INST, tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableNam...', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('4', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 4, 'EXECUTED', '8:e40d29cb79345b7fb5afd38a7f0ba8fc', 'createTable tableName=ACT_CMMN_HI_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_RU_MIL_INST; addColumn tableName=ACT_CMMN_HI_MIL_INST', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('5', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 5, 'EXECUTED', '8:70349de472f87368dcdec971a10311a0', 'modifyDataType columnName=DEPLOY_TIME_, tableName=ACT_CMMN_DEPLOYMENT; modifyDataType columnName=START_TIME_, tableName=ACT_CMMN_RU_CASE_INST; modifyDataType columnName=START_TIME_, tableName=ACT_CMMN_RU_PLAN_ITEM_INST; modifyDataType columnName=T...', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('6', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 6, 'EXECUTED', '8:10e82e26a7fee94c32a92099c059c18c', 'createIndex indexName=ACT_IDX_CASE_DEF_UNIQ, tableName=ACT_CMMN_CASEDEF', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('7', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 7, 'EXECUTED', '8:530bc81a1e30618ccf4a2da1f7c6c043', 'renameColumn newColumnName=CREATE_TIME_, oldColumnName=START_TIME_, tableName=ACT_CMMN_RU_PLAN_ITEM_INST; renameColumn newColumnName=CREATE_TIME_, oldColumnName=CREATED_TIME_, tableName=ACT_CMMN_HI_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_RU_P...', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('8', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 8, 'EXECUTED', '8:e8c2eb1ce28bc301efe07e0e29757781', 'addColumn tableName=ACT_CMMN_HI_PLAN_ITEM_INST', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('9', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 9, 'EXECUTED', '8:4cb4782b9bdec5ced2a64c525aa7b3a0', 'addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_HI_PLAN_ITEM_INST', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('10', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 10, 'EXECUTED', '8:341c16be247f5d17badc9809da8691f9', 'addColumn tableName=ACT_CMMN_RU_CASE_INST; addColumn tableName=ACT_CMMN_RU_CASE_INST; createIndex indexName=ACT_IDX_CASE_INST_REF_ID_, tableName=ACT_CMMN_RU_CASE_INST; addColumn tableName=ACT_CMMN_HI_CASE_INST; addColumn tableName=ACT_CMMN_HI_CASE...', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('11', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 11, 'EXECUTED', '8:d7c4da9276bcfffbfb0ebfb25e3f7b05', 'addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_HI_PLAN_ITEM_INST', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('12', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:16', 12, 'EXECUTED', '8:adf4ecc45f2aa9a44a5626b02e1d6f98', 'addColumn tableName=ACT_CMMN_RU_CASE_INST', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('13', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:17', 13, 'EXECUTED', '8:7550626f964ab5518464709408333ec1', 'addColumn tableName=ACT_CMMN_RU_PLAN_ITEM_INST; addColumn tableName=ACT_CMMN_HI_PLAN_ITEM_INST', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('14', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:17', 14, 'EXECUTED', '8:086b40b3a05596dcc8a8d7479922d494', 'addColumn tableName=ACT_CMMN_RU_CASE_INST; addColumn tableName=ACT_CMMN_HI_CASE_INST', '', NULL, '4.5.0', NULL, NULL, '8753616357');
INSERT INTO `act_cmmn_databasechangelog` VALUES ('16', 'flowable', 'org/flowable/cmmn/db/liquibase/flowable-cmmn-db-changelog.xml', '2022-11-18 14:40:17', 15, 'EXECUTED', '8:a697a222ddd99dd15b36516a252f1c63', 'addColumn tableName=ACT_CMMN_RU_CASE_INST; addColumn tableName=ACT_CMMN_HI_CASE_INST', '', NULL, '4.5.0', NULL, NULL, '8753616357');

-- ----------------------------
-- Table structure for act_cmmn_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_databasechangeloglock`;
CREATE TABLE `act_cmmn_databasechangeloglock`  (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_databasechangeloglock
-- ----------------------------
INSERT INTO `act_cmmn_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_cmmn_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_deployment`;
CREATE TABLE `act_cmmn_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_deployment
-- ----------------------------

-- ----------------------------
-- Table structure for act_cmmn_deployment_resource
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_deployment_resource`;
CREATE TABLE `act_cmmn_deployment_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  `GENERATED_` bit(1) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_CMMN_RSRC_DPL`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_CMMN_RSRC_DPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_cmmn_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_deployment_resource
-- ----------------------------

-- ----------------------------
-- Table structure for act_cmmn_hi_case_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_hi_case_inst`;
CREATE TABLE `act_cmmn_hi_case_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `REV_` int(11) NOT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `PARENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `STATE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NULL DEFAULT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LAST_REACTIVATION_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_REACTIVATION_USER_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `BUSINESS_STATUS_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_hi_case_inst
-- ----------------------------

-- ----------------------------
-- Table structure for act_cmmn_hi_mil_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_hi_mil_inst`;
CREATE TABLE `act_cmmn_hi_mil_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `REV_` int(11) NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `TIME_STAMP_` datetime(3) NULL DEFAULT NULL,
  `CASE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_hi_mil_inst
-- ----------------------------

-- ----------------------------
-- Table structure for act_cmmn_hi_plan_item_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_hi_plan_item_inst`;
CREATE TABLE `act_cmmn_hi_plan_item_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `REV_` int(11) NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `STATE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CASE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `STAGE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `IS_STAGE_` bit(1) NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ITEM_DEFINITION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ITEM_DEFINITION_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_AVAILABLE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_ENABLED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_DISABLED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_STARTED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_SUSPENDED_TIME_` datetime(3) NULL DEFAULT NULL,
  `COMPLETED_TIME_` datetime(3) NULL DEFAULT NULL,
  `OCCURRED_TIME_` datetime(3) NULL DEFAULT NULL,
  `TERMINATED_TIME_` datetime(3) NULL DEFAULT NULL,
  `EXIT_TIME_` datetime(3) NULL DEFAULT NULL,
  `ENDED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `ENTRY_CRITERION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `EXIT_CRITERION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `SHOW_IN_OVERVIEW_` bit(1) NULL DEFAULT NULL,
  `EXTRA_VALUE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DERIVED_CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LAST_UNAVAILABLE_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_hi_plan_item_inst
-- ----------------------------

-- ----------------------------
-- Table structure for act_cmmn_ru_case_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_ru_case_inst`;
CREATE TABLE `act_cmmn_ru_case_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `REV_` int(11) NOT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `PARENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `STATE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `LOCK_TIME_` datetime(3) NULL DEFAULT NULL,
  `IS_COMPLETEABLE_` bit(1) NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LAST_REACTIVATION_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_REACTIVATION_USER_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `BUSINESS_STATUS_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_CASE_INST_CASE_DEF`(`CASE_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_CASE_INST_PARENT`(`PARENT_ID_`) USING BTREE,
  INDEX `ACT_IDX_CASE_INST_REF_ID_`(`REFERENCE_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_CASE_INST_CASE_DEF` FOREIGN KEY (`CASE_DEF_ID_`) REFERENCES `act_cmmn_casedef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_ru_case_inst
-- ----------------------------

-- ----------------------------
-- Table structure for act_cmmn_ru_mil_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_ru_mil_inst`;
CREATE TABLE `act_cmmn_ru_mil_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `TIME_STAMP_` datetime(3) NULL DEFAULT NULL,
  `CASE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_MIL_CASE_DEF`(`CASE_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_MIL_CASE_INST`(`CASE_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MIL_CASE_DEF` FOREIGN KEY (`CASE_DEF_ID_`) REFERENCES `act_cmmn_casedef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MIL_CASE_INST` FOREIGN KEY (`CASE_INST_ID_`) REFERENCES `act_cmmn_ru_case_inst` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_ru_mil_inst
-- ----------------------------

-- ----------------------------
-- Table structure for act_cmmn_ru_plan_item_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_ru_plan_item_inst`;
CREATE TABLE `act_cmmn_ru_plan_item_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `REV_` int(11) NOT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CASE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `STAGE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `IS_STAGE_` bit(1) NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `STATE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `ITEM_DEFINITION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ITEM_DEFINITION_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `IS_COMPLETEABLE_` bit(1) NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` bit(1) NULL DEFAULT NULL,
  `VAR_COUNT_` int(11) NULL DEFAULT NULL,
  `SENTRY_PART_INST_COUNT_` int(11) NULL DEFAULT NULL,
  `LAST_AVAILABLE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_ENABLED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_DISABLED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_STARTED_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_SUSPENDED_TIME_` datetime(3) NULL DEFAULT NULL,
  `COMPLETED_TIME_` datetime(3) NULL DEFAULT NULL,
  `OCCURRED_TIME_` datetime(3) NULL DEFAULT NULL,
  `TERMINATED_TIME_` datetime(3) NULL DEFAULT NULL,
  `EXIT_TIME_` datetime(3) NULL DEFAULT NULL,
  `ENDED_TIME_` datetime(3) NULL DEFAULT NULL,
  `ENTRY_CRITERION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `EXIT_CRITERION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `EXTRA_VALUE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DERIVED_CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LAST_UNAVAILABLE_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_PLAN_ITEM_CASE_DEF`(`CASE_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_PLAN_ITEM_CASE_INST`(`CASE_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_PLAN_ITEM_STAGE_INST`(`STAGE_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_PLAN_ITEM_CASE_DEF` FOREIGN KEY (`CASE_DEF_ID_`) REFERENCES `act_cmmn_casedef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_PLAN_ITEM_CASE_INST` FOREIGN KEY (`CASE_INST_ID_`) REFERENCES `act_cmmn_ru_case_inst` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_ru_plan_item_inst
-- ----------------------------

-- ----------------------------
-- Table structure for act_cmmn_ru_sentry_part_inst
-- ----------------------------
DROP TABLE IF EXISTS `act_cmmn_ru_sentry_part_inst`;
CREATE TABLE `act_cmmn_ru_sentry_part_inst`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `REV_` int(11) NOT NULL,
  `CASE_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CASE_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `PLAN_ITEM_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ON_PART_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `IF_PART_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TIME_STAMP_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_SENTRY_CASE_DEF`(`CASE_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_SENTRY_CASE_INST`(`CASE_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_SENTRY_PLAN_ITEM`(`PLAN_ITEM_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_SENTRY_CASE_DEF` FOREIGN KEY (`CASE_DEF_ID_`) REFERENCES `act_cmmn_casedef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SENTRY_CASE_INST` FOREIGN KEY (`CASE_INST_ID_`) REFERENCES `act_cmmn_ru_case_inst` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SENTRY_PLAN_ITEM` FOREIGN KEY (`PLAN_ITEM_INST_ID_`) REFERENCES `act_cmmn_ru_plan_item_inst` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_cmmn_ru_sentry_part_inst
-- ----------------------------

-- ----------------------------
-- Table structure for act_co_content_item
-- ----------------------------
DROP TABLE IF EXISTS `act_co_content_item`;
CREATE TABLE `act_co_content_item`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `MIME_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TASK_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CONTENT_STORE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CONTENT_STORE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `FIELD_` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CONTENT_AVAILABLE_` bit(1) NULL DEFAULT b'0',
  `CREATED_` timestamp(6) NULL DEFAULT NULL,
  `CREATED_BY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LAST_MODIFIED_` timestamp(6) NULL DEFAULT NULL,
  `LAST_MODIFIED_BY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CONTENT_SIZE_` bigint(20) NULL DEFAULT 0,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `idx_contitem_taskid`(`TASK_ID_`) USING BTREE,
  INDEX `idx_contitem_procid`(`PROC_INST_ID_`) USING BTREE,
  INDEX `idx_contitem_scope`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_co_content_item
-- ----------------------------

-- ----------------------------
-- Table structure for act_co_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_co_databasechangelog`;
CREATE TABLE `act_co_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_co_databasechangelog
-- ----------------------------
INSERT INTO `act_co_databasechangelog` VALUES ('1', 'activiti', 'org/flowable/content/db/liquibase/flowable-content-db-changelog.xml', '2022-11-18 14:40:16', 1, 'EXECUTED', '8:7644d7165cfe799200a2abdd3419e8b6', 'createTable tableName=ACT_CO_CONTENT_ITEM; createIndex indexName=idx_contitem_taskid, tableName=ACT_CO_CONTENT_ITEM; createIndex indexName=idx_contitem_procid, tableName=ACT_CO_CONTENT_ITEM', '', NULL, '4.5.0', NULL, NULL, '8753616038');
INSERT INTO `act_co_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/content/db/liquibase/flowable-content-db-changelog.xml', '2022-11-18 14:40:16', 2, 'EXECUTED', '8:fe7b11ac7dbbf9c43006b23bbab60bab', 'addColumn tableName=ACT_CO_CONTENT_ITEM; createIndex indexName=idx_contitem_scope, tableName=ACT_CO_CONTENT_ITEM', '', NULL, '4.5.0', NULL, NULL, '8753616038');

-- ----------------------------
-- Table structure for act_co_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_co_databasechangeloglock`;
CREATE TABLE `act_co_databasechangeloglock`  (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_co_databasechangeloglock
-- ----------------------------
INSERT INTO `act_co_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_dmn_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_databasechangelog`;
CREATE TABLE `act_dmn_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_dmn_databasechangelog
-- ----------------------------
INSERT INTO `act_dmn_databasechangelog` VALUES ('1', 'activiti', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-11-18 14:40:15', 1, 'EXECUTED', '8:c8701f1c71018b55029f450b2e9a10a1', 'createTable tableName=ACT_DMN_DEPLOYMENT; createTable tableName=ACT_DMN_DEPLOYMENT_RESOURCE; createTable tableName=ACT_DMN_DECISION_TABLE', '', NULL, '4.5.0', NULL, NULL, '8753615489');
INSERT INTO `act_dmn_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-11-18 14:40:15', 2, 'EXECUTED', '8:47f94b27feb7df8a30d4e338c7bd5fb8', 'createTable tableName=ACT_DMN_HI_DECISION_EXECUTION', '', NULL, '4.5.0', NULL, NULL, '8753615489');
INSERT INTO `act_dmn_databasechangelog` VALUES ('3', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-11-18 14:40:15', 3, 'EXECUTED', '8:ac17eae89fbdccb6e08daf3c7797b579', 'addColumn tableName=ACT_DMN_HI_DECISION_EXECUTION', '', NULL, '4.5.0', NULL, NULL, '8753615489');
INSERT INTO `act_dmn_databasechangelog` VALUES ('4', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-11-18 14:40:15', 4, 'EXECUTED', '8:f73aabc4529e7292c2942073d1cff6f9', 'dropColumn columnName=PARENT_DEPLOYMENT_ID_, tableName=ACT_DMN_DECISION_TABLE', '', NULL, '4.5.0', NULL, NULL, '8753615489');
INSERT INTO `act_dmn_databasechangelog` VALUES ('5', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-11-18 14:40:15', 5, 'EXECUTED', '8:3e03528582dd4eeb4eb41f9b9539140d', 'modifyDataType columnName=DEPLOY_TIME_, tableName=ACT_DMN_DEPLOYMENT; modifyDataType columnName=START_TIME_, tableName=ACT_DMN_HI_DECISION_EXECUTION; modifyDataType columnName=END_TIME_, tableName=ACT_DMN_HI_DECISION_EXECUTION', '', NULL, '4.5.0', NULL, NULL, '8753615489');
INSERT INTO `act_dmn_databasechangelog` VALUES ('6', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-11-18 14:40:15', 6, 'EXECUTED', '8:646c6a061e0b6e8a62e69844ff96abb0', 'createIndex indexName=ACT_IDX_DEC_TBL_UNIQ, tableName=ACT_DMN_DECISION_TABLE', '', NULL, '4.5.0', NULL, NULL, '8753615489');
INSERT INTO `act_dmn_databasechangelog` VALUES ('7', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-11-18 14:40:15', 7, 'EXECUTED', '8:215a499ff7ae77685b55355245b8b708', 'dropIndex indexName=ACT_IDX_DEC_TBL_UNIQ, tableName=ACT_DMN_DECISION_TABLE; renameTable newTableName=ACT_DMN_DECISION, oldTableName=ACT_DMN_DECISION_TABLE; createIndex indexName=ACT_IDX_DMN_DEC_UNIQ, tableName=ACT_DMN_DECISION', '', NULL, '4.5.0', NULL, NULL, '8753615489');
INSERT INTO `act_dmn_databasechangelog` VALUES ('8', 'flowable', 'org/flowable/dmn/db/liquibase/flowable-dmn-db-changelog.xml', '2022-11-18 14:40:15', 8, 'EXECUTED', '8:5355bee389318afed91a11702f2df032', 'addColumn tableName=ACT_DMN_DECISION', '', NULL, '4.5.0', NULL, NULL, '8753615489');

-- ----------------------------
-- Table structure for act_dmn_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_databasechangeloglock`;
CREATE TABLE `act_dmn_databasechangeloglock`  (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_dmn_databasechangeloglock
-- ----------------------------
INSERT INTO `act_dmn_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_dmn_decision
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_decision`;
CREATE TABLE `act_dmn_decision`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `VERSION_` int(11) NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DECISION_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_DMN_DEC_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_dmn_decision
-- ----------------------------

-- ----------------------------
-- Table structure for act_dmn_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_deployment`;
CREATE TABLE `act_dmn_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_dmn_deployment
-- ----------------------------

-- ----------------------------
-- Table structure for act_dmn_deployment_resource
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_deployment_resource`;
CREATE TABLE `act_dmn_deployment_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_dmn_deployment_resource
-- ----------------------------

-- ----------------------------
-- Table structure for act_dmn_hi_decision_execution
-- ----------------------------
DROP TABLE IF EXISTS `act_dmn_hi_decision_execution`;
CREATE TABLE `act_dmn_hi_decision_execution`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `DECISION_DEFINITION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NULL DEFAULT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `INSTANCE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `ACTIVITY_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `FAILED_` bit(1) NULL DEFAULT b'0',
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `EXECUTION_JSON_` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_dmn_hi_decision_execution
-- ----------------------------

-- ----------------------------
-- Table structure for act_evt_log
-- ----------------------------
DROP TABLE IF EXISTS `act_evt_log`;
CREATE TABLE `act_evt_log`  (
  `LOG_NR_` bigint(20) NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DATA_` longblob NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_PROCESSED_` tinyint(4) NULL DEFAULT 0,
  PRIMARY KEY (`LOG_NR_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_evt_log
-- ----------------------------

-- ----------------------------
-- Table structure for act_fo_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_databasechangelog`;
CREATE TABLE `act_fo_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_fo_databasechangelog
-- ----------------------------
INSERT INTO `act_fo_databasechangelog` VALUES ('1', 'activiti', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-11-18 14:40:15', 1, 'EXECUTED', '8:033ebf9380889aed7c453927ecc3250d', 'createTable tableName=ACT_FO_FORM_DEPLOYMENT; createTable tableName=ACT_FO_FORM_RESOURCE; createTable tableName=ACT_FO_FORM_DEFINITION; createTable tableName=ACT_FO_FORM_INSTANCE', '', NULL, '4.5.0', NULL, NULL, '8753615719');
INSERT INTO `act_fo_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-11-18 14:40:15', 2, 'EXECUTED', '8:986365ceb40445ce3b27a8e6b40f159b', 'addColumn tableName=ACT_FO_FORM_INSTANCE', '', NULL, '4.5.0', NULL, NULL, '8753615719');
INSERT INTO `act_fo_databasechangelog` VALUES ('3', 'flowable', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-11-18 14:40:15', 3, 'EXECUTED', '8:abf482518ceb09830ef674e52c06bf15', 'dropColumn columnName=PARENT_DEPLOYMENT_ID_, tableName=ACT_FO_FORM_DEFINITION', '', NULL, '4.5.0', NULL, NULL, '8753615719');
INSERT INTO `act_fo_databasechangelog` VALUES ('4', 'flowable', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-11-18 14:40:15', 4, 'EXECUTED', '8:2087829f22a4b2298dbf530681c74854', 'modifyDataType columnName=DEPLOY_TIME_, tableName=ACT_FO_FORM_DEPLOYMENT; modifyDataType columnName=SUBMITTED_DATE_, tableName=ACT_FO_FORM_INSTANCE', '', NULL, '4.5.0', NULL, NULL, '8753615719');
INSERT INTO `act_fo_databasechangelog` VALUES ('5', 'flowable', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-11-18 14:40:15', 5, 'EXECUTED', '8:b4be732b89e5ca028bdd520c6ad4d446', 'createIndex indexName=ACT_IDX_FORM_DEF_UNIQ, tableName=ACT_FO_FORM_DEFINITION', '', NULL, '4.5.0', NULL, NULL, '8753615719');
INSERT INTO `act_fo_databasechangelog` VALUES ('6', 'flowable', 'org/flowable/form/db/liquibase/flowable-form-db-changelog.xml', '2022-11-18 14:40:15', 6, 'EXECUTED', '8:384bbd364a649b67c3ca1bcb72fe537f', 'createIndex indexName=ACT_IDX_FORM_TASK, tableName=ACT_FO_FORM_INSTANCE; createIndex indexName=ACT_IDX_FORM_PROC, tableName=ACT_FO_FORM_INSTANCE; createIndex indexName=ACT_IDX_FORM_SCOPE, tableName=ACT_FO_FORM_INSTANCE', '', NULL, '4.5.0', NULL, NULL, '8753615719');

-- ----------------------------
-- Table structure for act_fo_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_databasechangeloglock`;
CREATE TABLE `act_fo_databasechangeloglock`  (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_fo_databasechangeloglock
-- ----------------------------
INSERT INTO `act_fo_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for act_fo_form_definition
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_form_definition`;
CREATE TABLE `act_fo_form_definition`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `VERSION_` int(11) NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_FORM_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_fo_form_definition
-- ----------------------------

-- ----------------------------
-- Table structure for act_fo_form_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_form_deployment`;
CREATE TABLE `act_fo_form_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_fo_form_deployment
-- ----------------------------

-- ----------------------------
-- Table structure for act_fo_form_instance
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_form_instance`;
CREATE TABLE `act_fo_form_instance`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `FORM_DEFINITION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `TASK_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `SUBMITTED_DATE_` datetime(3) NULL DEFAULT NULL,
  `SUBMITTED_BY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `FORM_VALUES_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_FORM_TASK`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_IDX_FORM_PROC`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_FORM_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_fo_form_instance
-- ----------------------------

-- ----------------------------
-- Table structure for act_fo_form_resource
-- ----------------------------
DROP TABLE IF EXISTS `act_fo_form_resource`;
CREATE TABLE `act_fo_form_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_fo_form_resource
-- ----------------------------

-- ----------------------------
-- Table structure for act_ge_bytearray
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_bytearray`;
CREATE TABLE `act_ge_bytearray`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTES_` longblob NULL,
  `GENERATED_` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_BYTEARR_DEPL`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ge_bytearray
-- ----------------------------
INSERT INTO `act_ge_bytearray` VALUES ('5fd2571a-6af7-11ed-827c-000c29f10ec6', 1, '/usr/local/project/provider/resources/processes/leaveApproval.bpmn20.xml', '5fd25719-6af7-11ed-827c-000c29f10ec6', 0x3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554462D38223F3E0A3C646566696E6974696F6E7320786D6C6E733D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F4D4F44454C2220786D6C6E733A7873693D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612D696E7374616E6365220A20202020202020202020202020786D6C6E733A666C6F7761626C653D22687474703A2F2F666C6F7761626C652E6F72672F62706D6E2220786D6C6E733A62706D6E64693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F4449220A20202020202020202020202020786D6C6E733A6F6D6764633D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44432220786D6C6E733A6F6D6764693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F4449220A20202020202020202020202020747970654C616E67756167653D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D61222065787072657373696F6E4C616E67756167653D22687474703A2F2F7777772E77332E6F72672F313939392F5850617468220A202020202020202020202020207461726765744E616D6573706163653D22687474703A2F2F7777772E666C6F7761626C652E6F72672F70726F6365737364656622206578706F727465723D22466C6F7761626C65204F70656E20536F75726365204D6F64656C6572220A202020202020202020202020206578706F7274657256657273696F6E3D22362E372E32223E0A20203C70726F636573732069643D226C65617665417070726F76616C22206E616D653D22E8AFB7E58187E5AEA1E689B92220697345786563757461626C653D2274727565223E0A202020203C73746172744576656E742069643D2273746172744576656E743122206E616D653D22E5BC80E5A78B2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E3C2F73746172744576656E743E0A202020203C757365725461736B2069643D227369642D43444246414130332D424234372D344246362D413144352D32363830313244313346453222206E616D653D22E5ADA6E7949F2220666C6F7761626C653A61737369676E65653D22247B73747564656E747D2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B3132334071712E636F6D5D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE588985D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE697AD5D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C757365725461736B2069643D227369642D45323132383530362D453734432D343246432D393634382D43394133354537413030393222206E616D653D22E88081E5B8882220666C6F7761626C653A61737369676E65653D22247B746561636865727D2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B313233344071712E636F6D5D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE78E8B5D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE6B5A95D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C757365725461736B2069643D227369642D42394235304130302D353845332D343532452D424444322D33323441464632343131343222206E616D653D22E7AEA1E79086E591982220666C6F7761626C653A61737369676E65653D22247B61646D696E7D2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B746573742D61646D696E406578616D706C652D646F6D61696E2E746C645D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B546573745D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B41646D696E6973747261746F725D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C6578636C7573697665476174657761792069643D227369642D30383134303345442D433946422D343830372D414137462D464132313241413245393939223E3C2F6578636C7573697665476174657761793E0A202020203C656E644576656E742069643D227369642D36394244303537372D323732462D344642342D393644352D31454433463946433945424522206E616D653D22E7BB93E69D9F223E3C2F656E644576656E743E0A202020203C6578636C7573697665476174657761792069643D227369642D38353045333145342D363641422D343045352D423637332D454543443743393931324345223E3C2F6578636C7573697665476174657761793E0A202020203C73657175656E6365466C6F772069643D227369642D42384641363233462D464145322D344544382D413346392D4443324432433533343646322220736F757263655265663D2273746172744576656E743122207461726765745265663D227369642D43444246414130332D424234372D344246362D413144352D323638303132443133464532223E3C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D41464143304341322D453935312D343039452D414138422D35373739354634343541383622206E616D653D22E68B92E7BB9D2220736F757263655265663D227369642D38353045333145342D363641422D343045352D423637332D45454344374339393132434522207461726765745265663D227369642D43444246414130332D424234372D344246362D413144352D323638303132443133464532223E0A2020202020203C646F63756D656E746174696F6E3EE88081E5B888E68B92E7BB9DE4BA86E8AFB7E58187E5AEA1E689B93C2F646F63756D656E746174696F6E3E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B636F6D6D616E64203D3D2027726566757365277D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D31393835354130432D363646312D343639352D384645452D46373741313146424535313322206E616D653D22E5908CE6848F2220736F757263655265663D227369642D30383134303345442D433946422D343830372D414137462D46413231324141324539393922207461726765745265663D227369642D36394244303537372D323732462D344642342D393644352D314544334639464339454245223E0A2020202020203C646F63756D656E746174696F6E3EE7AEA1E79086E59198E5AEA1E689B9E9809AE8BF873C2F646F63756D656E746174696F6E3E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B636F6D6D616E64203D3D20276167726565277D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D37423343313034352D443133382D344532452D414636352D39433344353639333037384422206E616D653D22E68B92E7BB9D2220736F757263655265663D227369642D30383134303345442D433946422D343830372D414137462D46413231324141324539393922207461726765745265663D227369642D43444246414130332D424234372D344246362D413144352D323638303132443133464532223E0A2020202020203C646F63756D656E746174696F6E3EE7AEA1E79086E59198E5AEA1E689B9E68B92E7BB9D3C2F646F63756D656E746174696F6E3E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B636F6D6D616E64203D3D2027726566757365277D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D31393345444430322D413436362D343442382D414330412D44333438394444434337393522206E616D653D22E8AFB7E581872220736F757263655265663D227369642D43444246414130332D424234372D344246362D413144352D32363830313244313346453222207461726765745265663D227369642D45323132383530362D453734432D343246432D393634382D433941333545374130303932223E0A2020202020203C646F63756D656E746174696F6E3EE8AFB7E581873C2F646F63756D656E746174696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D46424134344536332D424443412D343344392D383341362D32343742313137383535463422206E616D653D22E5AEA1E689B92220736F757263655265663D227369642D42394235304130302D353845332D343532452D424444322D33323441464632343131343222207461726765745265663D227369642D30383134303345442D433946422D343830372D414137462D464132313241413245393939223E0A2020202020203C646F63756D656E746174696F6E3EE5AEA1E689B93C2F646F63756D656E746174696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D33333233333546422D303731462D343535432D413943362D42323446373244443844303022206E616D653D22E5AEA1E689B92220736F757263655265663D227369642D45323132383530362D453734432D343246432D393634382D43394133354537413030393222207461726765745265663D227369642D38353045333145342D363641422D343045352D423637332D454543443743393931324345223E0A2020202020203C646F63756D656E746174696F6E3EE5AEA1E689B93C2F646F63756D656E746174696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D32343342423931422D333034462D343444392D424342382D44313832453232443430353422206E616D653D22E5908CE6848F2220736F757263655265663D227369642D38353045333145342D363641422D343045352D423637332D45454344374339393132434522207461726765745265663D227369642D42394235304130302D353845332D343532452D424444322D333234414646323431313432223E0A2020202020203C646F63756D656E746174696F6E3EE88081E5B888E5AEA1E689B9E9809AE8BF873C2F646F63756D656E746174696F6E3E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B636F6D6D616E64203D3D20276167726565277D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A20203C2F70726F636573733E0A20203C62706D6E64693A42504D4E4469616772616D2069643D2242504D4E4469616772616D5F6C65617665417070726F76616C223E0A202020203C62706D6E64693A42504D4E506C616E652062706D6E456C656D656E743D226C65617665417070726F76616C222069643D2242504D4E506C616E655F6C65617665417070726F76616C223E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D2273746172744576656E7431222069643D2242504D4E53686170655F73746172744576656E7431223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2233302E30222077696474683D2233302E302220783D223130302E302220793D223136332E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D43444246414130332D424234372D344246362D413144352D323638303132443133464532222069643D2242504D4E53686170655F7369642D43444246414130332D424234372D344246362D413144352D323638303132443133464532223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223137352E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D45323132383530362D453734432D343246432D393634382D433941333545374130303932222069643D2242504D4E53686170655F7369642D45323132383530362D453734432D343246432D393634382D433941333545374130303932223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223332302E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D42394235304130302D353845332D343532452D424444322D333234414646323431313432222069643D2242504D4E53686170655F7369642D42394235304130302D353845332D343532452D424444322D333234414646323431313432223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223631302E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D30383134303345442D433946422D343830372D414137462D464132313241413245393939222069643D2242504D4E53686170655F7369642D30383134303345442D433946422D343830372D414137462D464132313241413245393939223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2234302E30222077696474683D2234302E302220783D223735352E302220793D223135382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D36394244303537372D323732462D344642342D393644352D314544334639464339454245222069643D2242504D4E53686170655F7369642D36394244303537372D323732462D344642342D393644352D314544334639464339454245223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2232382E30222077696474683D2232382E302220783D223834302E302220793D223136342E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D38353045333145342D363641422D343045352D423637332D454543443743393931324345222069643D2242504D4E53686170655F7369642D38353045333145342D363641422D343045352D423637332D454543443743393931324345223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2234302E30222077696474683D2234302E302220783D223532352E302220793D223135382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D31393345444430322D413436362D343442382D414330412D443334383944444343373935222069643D2242504D4E456467655F7369642D31393345444430322D413436362D343442382D414330412D4433343839444443433739352220666C6F7761626C653A736F75726365446F636B6572583D2235302E302220666C6F7761626C653A736F75726365446F636B6572593D2234302E302220666C6F7761626C653A746172676574446F636B6572583D2235302E302220666C6F7761626C653A746172676574446F636B6572593D2234302E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223237342E393439393939393939393930372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223331392E393939393939393939393830372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D41464143304341322D453935312D343039452D414138422D353737393546343435413836222069643D2242504D4E456467655F7369642D41464143304341322D453935312D343039452D414138422D3537373935463434354138362220666C6F7761626C653A736F75726365446F636B6572583D2232302E352220666C6F7761626C653A736F75726365446F636B6572593D2232302E352220666C6F7761626C653A746172676574446F636B6572583D2235302E302220666C6F7761626C653A746172676574446F636B6572593D2237392E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223534352E352220793D223139372E34333738363831313737393737223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223534352E352220793D223235362E35223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223232352E302220793D223235362E35223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223232352E302220793D223231372E3935303030303030303030303032223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D32343342423931422D333034462D343444392D424342382D443138324532324434303534222069643D2242504D4E456467655F7369642D32343342423931422D333034462D343444392D424342382D4431383245323244343035342220666C6F7761626C653A736F75726365446F636B6572583D2232302E352220666C6F7761626C653A736F75726365446F636B6572593D2232302E352220666C6F7761626C653A746172676574446F636B6572583D2235302E302220666C6F7761626C653A746172676574446F636B6572593D2234302E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223536342E353234373337303732373432382220793D223137382E3431363636363636363636363633223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223630392E393939393939393939393935332220793D223137382E3231383132323237303734323333223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D33333233333546422D303731462D343535432D413943362D423234463732444438443030222069643D2242504D4E456467655F7369642D33333233333546422D303731462D343535432D413943362D4232344637324444384430302220666C6F7761626C653A736F75726365446F636B6572583D2235302E302220666C6F7761626C653A736F75726365446F636B6572593D2234302E302220666C6F7761626C653A746172676574446F636B6572583D22322E352220666C6F7761626C653A746172676574446F636B6572593D2232302E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223431392E39343939393939393939393133352220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223532352E302220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D46424134344536332D424443412D343344392D383341362D323437423131373835354634222069643D2242504D4E456467655F7369642D46424134344536332D424443412D343344392D383341362D3234374231313738353546342220666C6F7761626C653A736F75726365446F636B6572583D2235302E302220666C6F7761626C653A736F75726365446F636B6572593D2234302E302220666C6F7761626C653A746172676574446F636B6572583D2232302E352220666C6F7761626C653A746172676574446F636B6572593D2232302E35223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223730392E393439393939393939393937372220793D223137382E3231363233333736363233333736223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223735352E343133303433343738323535342220793D223137382E3431333034333437383236303835223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D42384641363233462D464145322D344544382D413346392D444332443243353334364632222069643D2242504D4E456467655F7369642D42384641363233462D464145322D344544382D413346392D4443324432433533343646322220666C6F7761626C653A736F75726365446F636B6572583D2231352E302220666C6F7761626C653A736F75726365446F636B6572593D2231352E302220666C6F7761626C653A746172676574446F636B6572583D2235302E302220666C6F7761626C653A746172676574446F636B6572593D2234302E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223132392E393439393938343839393537362220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223137342E393939393939393939393931372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D37423343313034352D443133382D344532452D414636352D394333443536393330373844222069643D2242504D4E456467655F7369642D37423343313034352D443133382D344532452D414636352D3943334435363933303738442220666C6F7761626C653A736F75726365446F636B6572583D2232302E352220666C6F7761626C653A736F75726365446F636B6572593D2232302E352220666C6F7761626C653A746172676574446F636B6572583D2235302E302220666C6F7761626C653A746172676574446F636B6572593D22312E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223737352E352220793D223135382E35223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223737352E352220793D223130302E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223232352E302220793D223130302E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223232352E302220793D223133382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D31393835354130432D363646312D343639352D384645452D463737413131464245353133222069643D2242504D4E456467655F7369642D31393835354130432D363646312D343639352D384645452D4637374131314642453531332220666C6F7761626C653A736F75726365446F636B6572583D2232302E352220666C6F7761626C653A736F75726365446F636B6572593D2232302E352220666C6F7761626C653A746172676574446F636B6572583D2231342E302220666C6F7761626C653A746172676574446F636B6572593D2231342E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223739342E353539313836393339383230372220793D223137382E33373832303531323832303531223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223834302E303030323735353532343833382220793D223137382E3038383835313838343236343037223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A202020203C2F62706D6E64693A42504D4E506C616E653E0A20203C2F62706D6E64693A42504D4E4469616772616D3E0A3C2F646566696E6974696F6E733E, 0);
INSERT INTO `act_ge_bytearray` VALUES ('600e279b-6af7-11ed-827c-000c29f10ec6', 1, '/usr/local/project/provider/resources/processes/leaveApproval.leaveApproval.png', '5fd25719-6af7-11ed-827c-000c29f10ec6', 0x89504E470D0A1A0A0000000D494844520000036E0000010A08060000006AAB06020000136C4944415478DAEDDDDD6F5C757A0060EF2A12BDDC3F808B48AD54FE805C70D10B5FEC852F223542A0893D26B14C64034D023820A1023202220C08596257AA9052ADB24AE54A0805EAA604EC04272110350D021714916C43F870129CA65EF062C7D8E4F4FC2CCFF6D4F1D833E3F1CC3933CF23BD32EBB19368F63DEFC79C33675A5A000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000058D5F6EDDB232184104288B484E90CA0C8E2E6590000CC25000A240080B90440810400CC25000A240080B90440810400CC250028900080B9044081040030970028900080B9044081040030970028900000E6120005120030970028900000E6120005120030970028900A2400602E0150200100CC25000A2400602E0150200100CC25000A2400602E31970028900080B9044081040030970028900080B9044081040030970028900000E6120005120030970028900000E61200051200309700A0400200E61280B415C3E15010D788B39E2900C0E20650BF62B8258EF9D516B75C2EB7D533050058DC00EA5B10DF596571FBA8B5B57593670900B0B801D4B7206E29B6B8B5B7B76FF30C01001637807414C595DEEB76D6D93600C0E206909EA278DB7BDDBCB70D00B0B801A44CBCA81D76B60D00B0B801A47B71BB3BF1DEB636CF08006071034867715C7CAF9BB36D0080C50D20BDC5718B02090058DC80861545D1A64B972EBD71E6CC999F8F1D3B168D8C8C64324281CCEABF7D7474343A79F2E47FC7D129874433E610A843EA90C50D600DA1D19D3A752A9A9C9C8CE6E6E6449DE2FAF5EBD1891327BE8F1BDFBD7248345B0E813AA40E59DC00D6105E9DD4E852D3F0E6464646CECB21D16C3904EA903A6471035843B8A444A3494FC4CD6E5E0E8966CB215087D4218B1BC01AC275E99A4CAA9A5D248744B3E510A843EA90C50DA04ACDEE4F7FBC1A5D3AFBBBE8F3D1E71623FC77F89E06A5D9C9213904EA9050872C6E400A9ADDF4D444F4D9BBCF449F1E79E2FF45F85E784C93B2B8C9213904EA9050872C6E409D9BDD379FFFCB6D8DAE10DF7E3EAC4959DCE4901C027548A8431637A0DECDEEFCFB2F156D76E1314DCAE22687E410A843421DB2B801756E769F8D3C5BB4D985C734298B9B1C9243A00E0975C8E20668769A9D1C1206265087D4218B1BA0D9AD16E1CE5BC59A5D784C93B2B8C9213904EA9050872C6E409D9BDDC5D3BF2DDAECC2639A94C54D0EC9215087843A647103EADCEC6E4C8C479FBDD77FFBA525F1F7C2639A94C54D0EC9215087843A647103EADCEC427CF91FBFBFADD985EF69509A9D1C9243A00E0975C8E206A4A1D9DDBC195DFCF01F6EBFB424FE5E784C93B2B8C9213904EA9050872C6E401D9BDDF4D44474E1F46F8ABE2F203C167E46A3B2B8C9213904EA9050872C6E40AD9BDDCD9BD1D58BC7A3FF3CFAF7451B5D21C2CF849FF58AA5C54D0EC9215087843A6471036AD4ECD67A65D22B969A9D1C9243A00E0975C8E206D4B9D995F2CAE46AAF586A5A1637392487A8A95FE965EA903A6471039AB0D955DAE80AA16959DCE4901CA266DAE28896BEEA65EA903A9492E37269716B53A2800D6D7642B3934372884CF8751C53710C2E7DDDAA970975C87109687642B393437288F40D8785A130BCB27FBD59874475481D725C029A9DD0ECE4901C22EDC3E15ADFD7CB843AE4B804343B6171934372881A6A5B63082C3CDEA6970975C871090D218AA2BF181F1FEF3D78F0E089818181EFF7EEDD3BDFD5D515853793767676DE7AE8A187E69E7AEAA94B2FBDF4D2A138FE5AB3139A9D1C9243A464385C6BF86BBA57F8D52175C871994DF97CFECE5C2EB73B9EBFDF88E3621CD361168F63368E2FE3C70EC75F1F0F3FE7D96AD285EDCD37DFFCFDF3CF3F3FFFE8A38F46AFBFFE7A74E6CC99686262229A9D9D8D82F0757272323A77EE5C142F76D1238F3C72EBB1C71EBBDCD3D3B34BB3139A9D1C9243D4C1AFCB7CC5BEDC9FD7CB843AE4B8AC99F6F6F66D718CC431B7B4A89512EFC73FDFA91D34897841EB7EE185176E3EF1C4138B4BD9C2C24254AA4F3EF9247AF2C927177A7B7BCFDF77DF7D7FA5D909CD4E0EC9216A3C1C6EADE0F7A69B61485487D421C76536C4CBD7E678F93A5AC6B2B6529C6AA4599C65E2BD6BD3DB6FBF7DFCC1071F8C868787CB5AD8963B7AF468D4DDDD3DD32867DF343BCD4E0EC9211A7238ACD6EFAB43421D725C56452E97DBDED1D1F13FC9252C9FCF472FBEF86274FCF8F1E8EBAFBF8E7EF8E187C579FBC71F7F5CBC1AEEF4E9D3D1E0E06074FFFDF72F5FDEA69D7D6BD0A56D6C6CECDFF7ECD9135DB87021AA86CB972F47F11238B36BD7AEBFD3EC84C54D0EC92136C85A373CA8F59FA30E0975C87159E9D2B6355EB416920BDBA1438716DF9A548AA9A9A9C59F0FBF975CE0E23F73570B8D239C690B4B5BA98951AAF0E7F5F4F4647EDBD7EC343B39248748F57058ADCBA91AFA157E75481D725CA67A69DB9E5CDAFAFAFA16CFAE5522FC5E78CBD3B2E5CD99B74610DED3162E8FACD699B695CEBCEDD8B1632A4E98BB343B61711372882A0F7385E1F01771EC5CFA5A8EE5BFD7B037465087D421C7653A85F7B4252F8FECEFEFFFF3E590950A9751EEDFBF7FF9659377691D1916EE1EF9DC73CFCD85F7B46DA423478ECCE4F3F98F343B61711372880D78053E0C77FF18DADAD2D75287C462BFD79043A23AA40E392ED3295EDA469367DAD6BBB42597B76567DE4E691F1976F8F0E17F0AFF87FEF4D34FD1467BF8E187AF65F534AD66A7D9C92139446A87C396A557E6A3449432242687C342EC483CDE70EFAD5187D421C765FA845BFE27DFD356E9E591AB5D36997CCF9B4B26337CB62D7C4E5BB8E57F2D7CFAE9A773593DEBA6D9697672480E910AC586B69586BDD586C4957EFEC00A3FDF50EFAD5187D421C7652A17B791C252156E2CB2118686869C75CBBAF1F1F1DE704392F5DCF6BF5CDDDDDDD7C275BC693A58E2F81BCD4EB393437288741D33AB0C876D650C7D2B0D89A50E87CB87C4B6AC3FA7EA903AE4B84CD7739ACFE7EF2CDC90249C15BB71E3C686CCDFE16E93CB3E2A60B36E9431070F1E3C71E0C081A8960607073F8993E5F1B43C0789D3C623B95CEE6ECD4EB393437288741C33150E696B0D89E50E87C9BFFF7A5A5FE15787D421C765368FCBF8FBBB0B3FF7F2CB2F6FF40C9E5CDC1ED78D32666060E0FB3367CED47471FBF0C30F2FC7C9329CB6832A11EFACF4EA8866A7D9C92139446D8F991586C35287B36243E22F2B1C0E979F59D89AD5E7541D52871C97E97A4EE3EFBD51787C6C6C6C4367F0F021DD897FCBB06E94317BF7EE9DBF7AF56A4D17B76BD7AE4DC6C9F24D8A0FAA3FBF3A923CB8343BCD4E0EC9216A7BCC54381CAE36247EB18EE170BDFF1E754834442F735C56F7398DFFF7C5C2F7AB7D5392E526262692FF8E6F74A38CE9EAEA8A6667676BBAB885BF2F4E96D9B41F54C95747C2E96DCD4EB393437288DA1E335578257DA521713DC3E1F221B14D1D12CDD6CB1C97D57D4EE39829FCEF6A7D04C06A1F0D90F8BB6775A30C36CF7A28219153179A5DBA420E8966CCA1668A7BEEB9A730C80DAEB3D5FD728557F4BF58FAFE7A0C66F5B975FCAB438ECB7446AD67709B50C6ECDCB9F356ADCFB8CDCCCC5CCBC819B7B3B95C6E6B6B6BEB2697977895520EC9216A7FCCB4AC7DB7BAF5BCB25FCE870167F9957D75481D725CA6F8398DFF7BDA19374AB267CF9EB95ABFC7EDCA952B5FA4FC3D6ECB0B544B569B5D91A2B8E6639A9D1C9243A4E198490C63D32D1BF35E9A4A86C4ACBC9726B375A8D2BA93C59A94C55EE6B8ACEE73EA3D6E94EC99679EB958EBBB4ABEFBEEBBFF96D2BB4A7E143EB9BE4881CAECD05DEC7BAB3D66E896437288341C33EB7825BDD8ADC557BA7B5D39436216EE5E97F93A5469DDC9624DCA622F735C56F7397557494AF6CA2BAF1CA8F5E7B8F5F7F71F49D9E7B8ADF68A92A15BB3934372883A1D3315BEA2BED6E74195FA61C0CB6D6949F7E745354C1DB2B8392E9BE9B8F4396E94ECE9A79FFECBDDBB77FFBCB0B050B39B4A767474FC57163FADDDD0ADD9C9213944DDADF5CA7AA91FE25BEE9058B82C6C5BD69F408B9B3AE4B84C977C3E7F673C17CF87652AFEEFE8C68D1B1B32804F4D4D459D9D9DB7128BDB662D2583FAFAFAFE70EEDCB95A5D2679284E94539A9DA1DBE22687E410152AF60A7FA9C361B94362E1EFDBD6084F9EC54D1D725CA64FF85CB7C24275E8D0A10D99C18786869267DB4E692519F5C0030FDCB76FDFBEF91A9C759BEAE8E8F8384ECE4ECDCED06D719343728875BEC2BFFCF2A89D2DE57F1ED44A43E28E32CE24E865163775C871598DC56D5B61A90A67DDAA7D9392F0E7C533F8ADC40780776A2319D6DBDB3B3E3C3CBCA15B5B783F5DD8F0CBBC765AB333745BDCE4901CA2D89098BC314272D82BE7437C8BFD5EEA6E2D6E71B3B8392E1BF3B80CE219F9FDC262D5D7D757B58F06081F01B06FDFBE85E4D9B6ACCEE2FC5FB26CEEEEEE9EB970E1C2862C6DE3E3E36FC77FC7D578C3BF4BB333745BDCE4901CA24A965F9EF58BA557E6CBBD95F8F2DF5BEFE754A94316B766AE438ECB0A67F138AE1716ACFEFEFE752F6F6169DBBF7F7FF27D6DD3599EC549E8ECECCCF7F6F6FE38393959D5A5EDDB6FBFFDA0A3A3E3ABAC9F9635746B7672480E918921B11A670C527B973A8B9BC5CD71D9B872B9DCF6C28D4A0A67DE2ABD6C32FCDEB2336D2E916C34F97CBEBBA7A767BA5A67DEC299B6B0B4C589F8A46657FBB8E38E3B568CB51E3374CB213944930E89A9FC10DF66AB4395D69D2CD6A406AF438ECB0AC40B565772790BEF790B372C0977852CF5EE91E14624C9F7B4856884599C22DB7E6767E7F76FBDF5D69FD671C392A9575F7DF577E1F2C890809A9DD0ECE4901C6283ADF732AA86BE0C4B1D52871C97D99AC5E3984A2E5E61810B9FF3163EA43B9C4D2B5C46192E879C989858FC70EDD75E7B2DDAB973E7CFC9DF0B974736CA2C4EF16D7F73BCA91FEBEDEDBDF6F1C71FCF94F3396DEFBDF7DE50BCF89D0D6F7E6CA4EB68353BCD4E0EC92132332496FBCA7CC3DEF0401D52871C97D99DC5931F1350619CF29EB6E64A9A7BC302D7D5D535393030F0F9071F7CF055BCD55F9D9D9D5DDCD2666666AE5DB972E58BD1D1D17F7DF6D967DFC9E7F397429284CDBED1EE58A3D9697672480E9109E55E56D5549761A943EA90E3327BB3F8D202375FCEC2D688B338250A9FEA1E27CDA37112BC11C79771CC2E2546F8FA4D1CC3713CDEC89FC2AED9697672480ED17043E29638A65B1AE4437CD52175C87169166FE4591C343BCD4E0EC9211A53DBD2F0D7B6CAE3D79B6D385487D421C725A0D909CD4E0EC921B2F20A7FD35E86A50EA9438E4B40B3139A9D1C92436461486CEAE1501D52871C97806627343B39248748FB9038D8ECC3A13AA40E392E01CD4E687672480E91666DE18DFC2D4D7A6B7175481D4AEB711947D31F978066A7D9C921616022616971D3CB1CFFEA50BAFCCA530068769A9D1C1206262C6EEA903A04A0D909CD4E0EC9212C6EEA90508700343B61711372088B9B3AA40E01A0D9697672480E6171D3CB843A04A0D909CD4E0EC9212C6EEA90508700343BCD4E0E090313163775481D02B0B869769A9D1C924358DCF432A10E016876C2E226E410163775481D0240B3D3ECE490303059DCF432A10E01687642B3934372088B9B3A24D42100CD4E58DC8481098B9B3AA40E01A0D9697672480E6171D3CB843A04A0D9098B9B904358DCD42175481D02D0EC343B39240C4C58DCD421750840B3139A9D1C924358DCD421A10E01A4C9B163C73499F4C46CDCECE6E59068B61CC2E2A60EA94300ACE1E4C99357272727359A14C4C4C4C43FC7CDEEBC1C12CD964358DCD421750880351C3F7EFC6FC7C6C6FEF8DD77DFCD6838F57B753234BAD1D1D1AFE2B8570E8966CB212C6EEA903A044009E202BB756464E46CB8B4215C979EB5181A1A8AC2E092C57FFB5284E7FD7C961B5DD673A80122F33984C54D1D52870068FCA165380C2EADADAD9B3C1B80C50D00206572B9DCDDF1D0321F0697F6F6F636CF086071030048DFE276380C2D4B71D6330258DC0000D235AC6C299C6D2B44BCC86DF5CC0016370080F40C2BC3C9A5AD70D6CD7BDD008B1B00403A0695DBCEB615A2BDBD7D9B6708B0B80100D47F507967A5A56D293EF20C0116370080FA0E2945CFB679AF1B6071030048C79032BCDAD2E60E9380C50D00C0E002A0FE0100185C00D43F00C0E002A0FE0100185C00D43F0000830BA0FEA97F0080C10540FD030030B800A87F0080C10540FD030030B800A87F0000061700F50F0030B800A87F0000061700F50F0030B800A87F0000061700F50F00C0E002A0FE0100061700F50F00C0E002A0FE0100185C00D43F00C0E002A0FE0100185C00D43F00C0E002A0FE0100185C00D43F0000830B80FA0700185C00D43F0000830B80FA0700185C00D43F0000830B80FA070060700150FF0080861D5C8410A259431700000000000000000000000000000000000000000000000000000000000000800CFA5F85465B6102B605AA0000000049454E44AE426082, 1);
INSERT INTO `act_ge_bytearray` VALUES ('debad27f-670b-11ed-b9c8-7486e21fa90d', 1, 'D:\\Program Files\\idea\\noah-server-master\\server-provider\\target\\classes\\processes\\leaveApproval.bpmn20.xml', 'debaab6e-670b-11ed-b9c8-7486e21fa90d', 0x3C3F786D6C2076657273696F6E3D22312E302220656E636F64696E673D225554462D38223F3E0A3C646566696E6974696F6E7320786D6C6E733D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F4D4F44454C2220786D6C6E733A7873693D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D612D696E7374616E6365220A20202020202020202020202020786D6C6E733A666C6F7761626C653D22687474703A2F2F666C6F7761626C652E6F72672F62706D6E2220786D6C6E733A62706D6E64693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F42504D4E2F32303130303532342F4449220A20202020202020202020202020786D6C6E733A6F6D6764633D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F44432220786D6C6E733A6F6D6764693D22687474703A2F2F7777772E6F6D672E6F72672F737065632F44442F32303130303532342F4449220A20202020202020202020202020747970654C616E67756167653D22687474703A2F2F7777772E77332E6F72672F323030312F584D4C536368656D61222065787072657373696F6E4C616E67756167653D22687474703A2F2F7777772E77332E6F72672F313939392F5850617468220A202020202020202020202020207461726765744E616D6573706163653D22687474703A2F2F7777772E666C6F7761626C652E6F72672F70726F6365737364656622206578706F727465723D22466C6F7761626C65204F70656E20536F75726365204D6F64656C6572220A202020202020202020202020206578706F7274657256657273696F6E3D22362E372E32223E0A20203C70726F636573732069643D226C65617665417070726F76616C22206E616D653D22E8AFB7E58187E5AEA1E689B92220697345786563757461626C653D2274727565223E0A202020203C73746172744576656E742069643D2273746172744576656E743122206E616D653D22E5BC80E5A78B2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E3C2F73746172744576656E743E0A202020203C757365725461736B2069643D227369642D43444246414130332D424234372D344246362D413144352D32363830313244313346453222206E616D653D22E5ADA6E7949F2220666C6F7761626C653A61737369676E65653D22247B73747564656E747D2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B3132334071712E636F6D5D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE588985D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE697AD5D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C757365725461736B2069643D227369642D45323132383530362D453734432D343246432D393634382D43394133354537413030393222206E616D653D22E88081E5B8882220666C6F7761626C653A61737369676E65653D22247B746561636865727D2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B313233344071712E636F6D5D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE78E8B5D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415BE6B5A95D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C757365725461736B2069643D227369642D42394235304130302D353845332D343532452D424444322D33323441464632343131343222206E616D653D22E7AEA1E79086E591982220666C6F7761626C653A61737369676E65653D22247B61646D696E7D2220666C6F7761626C653A666F726D4669656C6456616C69646174696F6E3D2274727565223E0A2020202020203C657874656E73696F6E456C656D656E74733E0A20202020202020203C6D6F64656C65723A61637469766974692D69646D2D61737369676E656520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B747275655D5D3E3C2F6D6F64656C65723A61637469766974692D69646D2D61737369676E65653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D656D61696C20786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B746573742D61646D696E406578616D706C652D646F6D61696E2E746C645D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D656D61696C3E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B546573745D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D66697273746E616D653E0A20202020202020203C6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D6520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B41646D696E6973747261746F725D5D3E3C2F6D6F64656C65723A61737369676E65652D696E666F2D6C6173746E616D653E0A20202020202020203C6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C65746520786D6C6E733A6D6F64656C65723D22687474703A2F2F666C6F7761626C652E6F72672F6D6F64656C6572223E3C215B43444154415B66616C73655D5D3E3C2F6D6F64656C65723A696E69746961746F722D63616E2D636F6D706C6574653E0A2020202020203C2F657874656E73696F6E456C656D656E74733E0A202020203C2F757365725461736B3E0A202020203C6578636C7573697665476174657761792069643D227369642D30383134303345442D433946422D343830372D414137462D464132313241413245393939223E3C2F6578636C7573697665476174657761793E0A202020203C656E644576656E742069643D227369642D36394244303537372D323732462D344642342D393644352D31454433463946433945424522206E616D653D22E7BB93E69D9F223E3C2F656E644576656E743E0A202020203C6578636C7573697665476174657761792069643D227369642D38353045333145342D363641422D343045352D423637332D454543443743393931324345223E3C2F6578636C7573697665476174657761793E0A202020203C73657175656E6365466C6F772069643D227369642D42384641363233462D464145322D344544382D413346392D4443324432433533343646322220736F757263655265663D2273746172744576656E743122207461726765745265663D227369642D43444246414130332D424234372D344246362D413144352D323638303132443133464532223E3C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D41464143304341322D453935312D343039452D414138422D35373739354634343541383622206E616D653D22E68B92E7BB9D2220736F757263655265663D227369642D38353045333145342D363641422D343045352D423637332D45454344374339393132434522207461726765745265663D227369642D43444246414130332D424234372D344246362D413144352D323638303132443133464532223E0A2020202020203C646F63756D656E746174696F6E3EE88081E5B888E68B92E7BB9DE4BA86E8AFB7E58187E5AEA1E689B93C2F646F63756D656E746174696F6E3E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B636F6D6D616E64203D3D2027726566757365277D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D31393835354130432D363646312D343639352D384645452D46373741313146424535313322206E616D653D22E5908CE6848F2220736F757263655265663D227369642D30383134303345442D433946422D343830372D414137462D46413231324141324539393922207461726765745265663D227369642D36394244303537372D323732462D344642342D393644352D314544334639464339454245223E0A2020202020203C646F63756D656E746174696F6E3EE7AEA1E79086E59198E5AEA1E689B9E9809AE8BF873C2F646F63756D656E746174696F6E3E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B636F6D6D616E64203D3D20276167726565277D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D37423343313034352D443133382D344532452D414636352D39433344353639333037384422206E616D653D22E68B92E7BB9D2220736F757263655265663D227369642D30383134303345442D433946422D343830372D414137462D46413231324141324539393922207461726765745265663D227369642D43444246414130332D424234372D344246362D413144352D323638303132443133464532223E0A2020202020203C646F63756D656E746174696F6E3EE7AEA1E79086E59198E5AEA1E689B9E68B92E7BB9D3C2F646F63756D656E746174696F6E3E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B636F6D6D616E64203D3D2027726566757365277D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D31393345444430322D413436362D343442382D414330412D44333438394444434337393522206E616D653D22E8AFB7E581872220736F757263655265663D227369642D43444246414130332D424234372D344246362D413144352D32363830313244313346453222207461726765745265663D227369642D45323132383530362D453734432D343246432D393634382D433941333545374130303932223E0A2020202020203C646F63756D656E746174696F6E3EE8AFB7E581873C2F646F63756D656E746174696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D46424134344536332D424443412D343344392D383341362D32343742313137383535463422206E616D653D22E5AEA1E689B92220736F757263655265663D227369642D42394235304130302D353845332D343532452D424444322D33323441464632343131343222207461726765745265663D227369642D30383134303345442D433946422D343830372D414137462D464132313241413245393939223E0A2020202020203C646F63756D656E746174696F6E3EE5AEA1E689B93C2F646F63756D656E746174696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D33333233333546422D303731462D343535432D413943362D42323446373244443844303022206E616D653D22E5AEA1E689B92220736F757263655265663D227369642D45323132383530362D453734432D343246432D393634382D43394133354537413030393222207461726765745265663D227369642D38353045333145342D363641422D343045352D423637332D454543443743393931324345223E0A2020202020203C646F63756D656E746174696F6E3EE5AEA1E689B93C2F646F63756D656E746174696F6E3E0A202020203C2F73657175656E6365466C6F773E0A202020203C73657175656E6365466C6F772069643D227369642D32343342423931422D333034462D343444392D424342382D44313832453232443430353422206E616D653D22E5908CE6848F2220736F757263655265663D227369642D38353045333145342D363641422D343045352D423637332D45454344374339393132434522207461726765745265663D227369642D42394235304130302D353845332D343532452D424444322D333234414646323431313432223E0A2020202020203C646F63756D656E746174696F6E3EE88081E5B888E5AEA1E689B9E9809AE8BF873C2F646F63756D656E746174696F6E3E0A2020202020203C636F6E646974696F6E45787072657373696F6E207873693A747970653D2274466F726D616C45787072657373696F6E223E3C215B43444154415B247B636F6D6D616E64203D3D20276167726565277D5D5D3E3C2F636F6E646974696F6E45787072657373696F6E3E0A202020203C2F73657175656E6365466C6F773E0A20203C2F70726F636573733E0A20203C62706D6E64693A42504D4E4469616772616D2069643D2242504D4E4469616772616D5F6C65617665417070726F76616C223E0A202020203C62706D6E64693A42504D4E506C616E652062706D6E456C656D656E743D226C65617665417070726F76616C222069643D2242504D4E506C616E655F6C65617665417070726F76616C223E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D2273746172744576656E7431222069643D2242504D4E53686170655F73746172744576656E7431223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2233302E30222077696474683D2233302E302220783D223130302E302220793D223136332E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D43444246414130332D424234372D344246362D413144352D323638303132443133464532222069643D2242504D4E53686170655F7369642D43444246414130332D424234372D344246362D413144352D323638303132443133464532223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223137352E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D45323132383530362D453734432D343246432D393634382D433941333545374130303932222069643D2242504D4E53686170655F7369642D45323132383530362D453734432D343246432D393634382D433941333545374130303932223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223332302E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D42394235304130302D353845332D343532452D424444322D333234414646323431313432222069643D2242504D4E53686170655F7369642D42394235304130302D353845332D343532452D424444322D333234414646323431313432223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2238302E30222077696474683D223130302E302220783D223631302E302220793D223133382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D30383134303345442D433946422D343830372D414137462D464132313241413245393939222069643D2242504D4E53686170655F7369642D30383134303345442D433946422D343830372D414137462D464132313241413245393939223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2234302E30222077696474683D2234302E302220783D223735352E302220793D223135382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D36394244303537372D323732462D344642342D393644352D314544334639464339454245222069643D2242504D4E53686170655F7369642D36394244303537372D323732462D344642342D393644352D314544334639464339454245223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2232382E30222077696474683D2232382E302220783D223834302E302220793D223136342E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E53686170652062706D6E456C656D656E743D227369642D38353045333145342D363641422D343045352D423637332D454543443743393931324345222069643D2242504D4E53686170655F7369642D38353045333145342D363641422D343045352D423637332D454543443743393931324345223E0A20202020202020203C6F6D6764633A426F756E6473206865696768743D2234302E30222077696474683D2234302E302220783D223532352E302220793D223135382E30223E3C2F6F6D6764633A426F756E64733E0A2020202020203C2F62706D6E64693A42504D4E53686170653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D31393345444430322D413436362D343442382D414330412D443334383944444343373935222069643D2242504D4E456467655F7369642D31393345444430322D413436362D343442382D414330412D4433343839444443433739352220666C6F7761626C653A736F75726365446F636B6572583D2235302E302220666C6F7761626C653A736F75726365446F636B6572593D2234302E302220666C6F7761626C653A746172676574446F636B6572583D2235302E302220666C6F7761626C653A746172676574446F636B6572593D2234302E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223237342E393439393939393939393930372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223331392E393939393939393939393830372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D41464143304341322D453935312D343039452D414138422D353737393546343435413836222069643D2242504D4E456467655F7369642D41464143304341322D453935312D343039452D414138422D3537373935463434354138362220666C6F7761626C653A736F75726365446F636B6572583D2232302E352220666C6F7761626C653A736F75726365446F636B6572593D2232302E352220666C6F7761626C653A746172676574446F636B6572583D2235302E302220666C6F7761626C653A746172676574446F636B6572593D2237392E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223534352E352220793D223139372E34333738363831313737393737223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223534352E352220793D223235362E35223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223232352E302220793D223235362E35223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223232352E302220793D223231372E3935303030303030303030303032223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D32343342423931422D333034462D343444392D424342382D443138324532324434303534222069643D2242504D4E456467655F7369642D32343342423931422D333034462D343444392D424342382D4431383245323244343035342220666C6F7761626C653A736F75726365446F636B6572583D2232302E352220666C6F7761626C653A736F75726365446F636B6572593D2232302E352220666C6F7761626C653A746172676574446F636B6572583D2235302E302220666C6F7761626C653A746172676574446F636B6572593D2234302E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223536342E353234373337303732373432382220793D223137382E3431363636363636363636363633223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223630392E393939393939393939393935332220793D223137382E3231383132323237303734323333223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D33333233333546422D303731462D343535432D413943362D423234463732444438443030222069643D2242504D4E456467655F7369642D33333233333546422D303731462D343535432D413943362D4232344637324444384430302220666C6F7761626C653A736F75726365446F636B6572583D2235302E302220666C6F7761626C653A736F75726365446F636B6572593D2234302E302220666C6F7761626C653A746172676574446F636B6572583D22322E352220666C6F7761626C653A746172676574446F636B6572593D2232302E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223431392E39343939393939393939393133352220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223532352E302220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D46424134344536332D424443412D343344392D383341362D323437423131373835354634222069643D2242504D4E456467655F7369642D46424134344536332D424443412D343344392D383341362D3234374231313738353546342220666C6F7761626C653A736F75726365446F636B6572583D2235302E302220666C6F7761626C653A736F75726365446F636B6572593D2234302E302220666C6F7761626C653A746172676574446F636B6572583D2232302E352220666C6F7761626C653A746172676574446F636B6572593D2232302E35223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223730392E393439393939393939393937372220793D223137382E3231363233333736363233333736223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223735352E343133303433343738323535342220793D223137382E3431333034333437383236303835223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D42384641363233462D464145322D344544382D413346392D444332443243353334364632222069643D2242504D4E456467655F7369642D42384641363233462D464145322D344544382D413346392D4443324432433533343646322220666C6F7761626C653A736F75726365446F636B6572583D2231352E302220666C6F7761626C653A736F75726365446F636B6572593D2231352E302220666C6F7761626C653A746172676574446F636B6572583D2235302E302220666C6F7761626C653A746172676574446F636B6572593D2234302E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223132392E393439393938343839393537362220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223137342E393939393939393939393931372220793D223137382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D37423343313034352D443133382D344532452D414636352D394333443536393330373844222069643D2242504D4E456467655F7369642D37423343313034352D443133382D344532452D414636352D3943334435363933303738442220666C6F7761626C653A736F75726365446F636B6572583D2232302E352220666C6F7761626C653A736F75726365446F636B6572593D2232302E352220666C6F7761626C653A746172676574446F636B6572583D2235302E302220666C6F7761626C653A746172676574446F636B6572593D22312E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223737352E352220793D223135382E35223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223737352E352220793D223130302E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223232352E302220793D223130302E30223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223232352E302220793D223133382E30223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A2020202020203C62706D6E64693A42504D4E456467652062706D6E456C656D656E743D227369642D31393835354130432D363646312D343639352D384645452D463737413131464245353133222069643D2242504D4E456467655F7369642D31393835354130432D363646312D343639352D384645452D4637374131314642453531332220666C6F7761626C653A736F75726365446F636B6572583D2232302E352220666C6F7761626C653A736F75726365446F636B6572593D2232302E352220666C6F7761626C653A746172676574446F636B6572583D2231342E302220666C6F7761626C653A746172676574446F636B6572593D2231342E30223E0A20202020202020203C6F6D6764693A776179706F696E7420783D223739342E353539313836393339383230372220793D223137382E33373832303531323832303531223E3C2F6F6D6764693A776179706F696E743E0A20202020202020203C6F6D6764693A776179706F696E7420783D223834302E303030323735353532343833382220793D223137382E3038383835313838343236343037223E3C2F6F6D6764693A776179706F696E743E0A2020202020203C2F62706D6E64693A42504D4E456467653E0A202020203C2F62706D6E64693A42504D4E506C616E653E0A20203C2F62706D6E64693A42504D4E4469616772616D3E0A3C2F646566696E6974696F6E733E, 0);
INSERT INTO `act_ge_bytearray` VALUES ('df132bb0-670b-11ed-b9c8-7486e21fa90d', 1, 'D:\\Program Files\\idea\\noah-server-master\\server-provider\\target\\classes\\processes\\leaveApproval.leaveApproval.png', 'debaab6e-670b-11ed-b9c8-7486e21fa90d', 0x89504E470D0A1A0A0000000D494844520000036E0000010A08060000006AAB0602000017C94944415478DAEDDD6D8C5D759900F05131AE1F889268FCA046C81297103F194C34AEA6312696A404A292A13363DB0C4D0759C05A4A820B4D11241484D4C8261B0D8B4058BBA621B53BDB2D74DAD252A00D85A6CC8248BB20D00EADD3EDD63232A5B672F63C3773664F6FEFBCCFDC39E7DEDF2FF96766EEBDD396CB739E97F3765B5A000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000018D555575D955896655996651565E9CE004618DCBC0B0080BE0440820400D0970048900080BE0440820400D0970048900080BE0400091200D0970048900000FA1200091200D0970048900000FA1200091200405F0220410200FA1200091200405F0220410200FA12000952820400F4250012240080BE0440820400F4250012240080BE0440820400F425FA1200091200D0970048900000FA1200091200D0970048900000FA1200091200405F0220410200FA1200091200405F0220410200FA1200244800405F0250B464D81D09718CB5DB3B050018DC00662F195E92AE53A30D6EADADADF3BC530080C10D607613E2C65106B79D73E6CC39C7BB040018DC006637215E32D2E0367FFEFC2BBC430080C10DA01849B1D6B56EBB1D6D03000C6E00C5498A675DEBE6DA3600C0E0065030E9A0B6CED13600C0E00650ECC1EDCBB96BDBE67A470000831B40319363E55A3747DB0000831B407193E3251224006070031A569224E7BCFEFAEB6B77EDDAF5D72D5BB6243D3D3DA55C9120CBFA6FDFBC7973F2D4534FFD4FBA3AC490D58C3104F2903C647003184314BA1D3B7624FDFDFDC9C99327AD595A478E1C49B66FDF7E3C2D7CDF154356B3C510C843F290C10D600CB17752A12B4CC13BD9D3D3F38A18B29A2D86401E92870C6E006388534A149AE2ACB4D89D124356B3C510C843F290C10D600C715EBA2253A862978821ABD96208E42179C8E006304DC5EECF7F3A94BCBEFB57C9CB9B6FAFACF83E1E53A0143B312486401EB2E421831B5080623770AC2F79E98915C98B1B6E3A63C563F19C22657013436208E4214B1E32B801B35CEC0EBCFCEF6715BA6C1D7CB95B9132B889213104F290250F19DC80D92E76AF3C79F788C52E9E53A40C6E62480C813C64C943063760968BDD4B3DB78D58ECE23945CAE02686C410C843963C647003143BC54E0C591A269087E421831BA0D88DB6E2CE5B2315BB784E9132B889213104F290250F19DC80592E76FB9FF9A7118B5D3CA74819DCC490180279C892870C6EC02C17BBA37DBDC94B9B569E7D6A49FA583CA74819DCC490180279C892870C6EC02C17BB587F78FE91B38A5D3CA640297662480C813C64C9430637A008C5EEBDF792FDCFFEF3D9A796A48FC5738A94C14D0C892190872C79C8E006CC62B11B38D697EC7BE6FE11AF0B88E7E2350A95C14D0C892190872C79C8E006D4BBD8BDF75E7268FFD6E4BF1EFFC7110B5DB6E235F15A7B2C0D6E62480C813C64C9430637A04EC56EAC3D93F6582A7662480C813C64C943063760968BDD78F64C8EB6C752D132B8892131445D7D5C2D9387E421831BD084C56EB2852E5B8A96C14D0C8921EA666EBA92A1AF6A993C240F1564BB1C1ADCE64A51C08C163B4BB11343628852F866BA8EA56BF5D0D7796A99250FD92E01C5CE52ECC49018A278CD61D614C69EFD23CDDA24CA43F290ED1250EC2CC54E0C89218ADE1C8EF5B85A66C943B64B40B1B30C6E62480C514773C76802B3E7E7AA65963C64BB84869024C9DFF4F6F6763DFCF0C3DB57AD5A75FC861B6E38B568D1A2242E26EDE8E878FFFBDFFFFEC95B6EB9E5F5BBEFBEFBD174FD9D6267297662480C5190E670ACE6AFE9F6F0CB43F290EDB29CDADBDB3FD3DADA7A5DDA7FAF4DD7FE740D442F9EAE13E9FA43FADCBAF4EBF2789D77AB4907B6C71E7BEC913BEEB8E3D4D2A54B935FFCE217C9AE5DBB92BEBEBEE4C489134988AFFDFDFDC90B2FBC90A4835DF2831FFCE0FD1FFEF0876F2C59B264B16267297662480C310BBE39C13DF6137DBD5A66C943B6CBBA993F7FFE15E9EA49D7C9A1416D3CEBC9F4F51DCA41934807B4CE9FFCE427EFDD74D34D95A1ECF4E9D3C978EDDDBB37B9F9E69B4F777575BD72E595575EA8D8598A9D181243D4B9399C3789DF1B688626511E92876C97E5900E5FE7A7C3D7E31318D66AAD1D8DD48B53259DBBCE59BF7EFDD66BAEB926E9EEEE9ED0C056EDF1C71F4F3A3B3B071BE5E89B62A7D889213144433687D3F5FBF290250FD92EA7456B6BEB556D6D6DFF9B1FC2DADBDB93BBEEBA2BD9BA756BF2D65B6F25EFBCF34EA5DF7EF7DD772B67C33DF3CC33C9EAD5AB93EF7DEF7BD5C3DB80A36F0D3AB46DDBB6EDB9EBAFBF3ED9B76F5F321DDE78E38D241D0207172F5EFC0F8A9D6570134362881932D60D0FEAFDE7C843963C64BB9CECD0362F1DB44EE707B6471F7DB47269D2781C3B76ACF2FAF8BDFC0097FE998B5B681C71A42D86B6F106C678C59FB764C992D24FFB8A9D622786C410856E0EA7EB74AA86DEC32F0FC943B6CB420F6D57E587B665CB96558EAE4D46FC5E5CF25435BC39F2D608E29AB6383D72BA8EB4D53AF2B660C1826369C05CA4D85906374B0C31CDCD5CD61C7E205D0B87BE4E44F5EF35EC8D11E42179C876594C714D5BFEF4C8952B570E9F0E3959711AE59D77DE597DDAE4454A4789C5DD236FBFFDF693714DDB4CDAB061C3607B7BFB4EC5CE32B859628819D8031FCDDDBF44591BFA3ADE2671A4DF6BC826511E92876C97C5940E6D9BF347DAA63AB4E587B7AA236F3B948F125BB76EDDBFC6FFD0BFFCE52FC94CBBF6DA6B0F97F530AD62A7D889213144619BC396A13DF3496E8DA749CC3787D95A907BBEE1AEAD9187E421DB65F1C42DFFF3D7B44DF6F4C8D14E9BCC5FF3E694C9121F6D8BCF698B5BFED7C38B2FBE78B2AC47DD143BC54E0C89210A61A4A6AD56B3375A9358EBF50FD4787D435D5B230FC943B6CB420E6E3DD95015371699096BD6AC71D4ADEC7A7B7BBBE2862453B9EDFF447576761E8EF3788BB4B1A4EBEF153BC54E0C89218AB5CD8CD21CCE9D40D357AB491C6F7358DD24CE2DFB7B2A0FC943B6CB62BDA7EDEDED9FC96E481247C58E1E3D3A23FD77DC6DB2EAA302CE578D4AE6E1871FDEFEC0030F24F5B47AF5EABD69B02C2FCA7B903B6CDCD3DADAFA65C54EB113436288626C33936CD2C66A1227DA1CE6FFFE2345DDC32F0FC943B6CB726E97E9E3D765AFBBE79E7B66BA07CF0F6ECB55A39259B56AD5F15DBB76D575707BF6D967DF4883A5BB681B556E6DACB57744B153ECC49018A2BEDB4C8DE670BCCDD9484DE20727D91C561F599857D6F7541E92876C97C57A4FD3C7D666CF6FDBB66D467BF0F890EEDCBFA55B352A991B6EB8E1D4A14387EA3AB81D3E7CB83F0D960305DEA886F78EE4372EC54EB113436288FA6E33936C0E476B125F9D427338D57F8F3C6435442DB35D4EEF7B9AFEBC3F7B7CBA6F4A52ADAFAF2FFFEF38A01A95CCA2458B9213274ED475708BBF2F0D961345DFA8F27B47E2F0B662A7D8892131447DB79969D8935EAB499C4A7358DD24CE9587AC66AB65B6CBE97D4FD33598FD3C5D1F0130DA4703E4FEEE13AA51098BE76C184720176E2976C55A62C86AC6186AA6F5ED6F7F3B6BE4564FB1D47DB0C61EFD57871E9F8AD5657D6F6DFFF290EDB298ABDE3DB849A864162E5CF87EBD8FB80D0E0E1E2EC911B7DDADADADF3E6CC99734E594E2F8973977FFFFBDF578E6A46128C9FE3F1F87EF1E2C567BD3EF6BCECDFBFBFF2FD6F7EF39BE4E28B2FB697B2C96328BF226E7EF9CB5F0EFFFCEB5FFF7AC4D7C57FDB65975D5659EBD7AF1F5EF1DC8E1D3BC41093DA665AC6BE5BDD54F6EC4FE4C380CBBC67BFB079286E8EB661C386B3F2C9BA75EBA6753B8F5CB460C182CACAF2D2934F3E290FD92E0BF19EA6DF0F38E2C6B85C7FFDF527EB7D8DDBDB6FBFFD6AC1AF71AB4E502D656ABA23F16DDAB429F9C4273E917CFDEB5FAFDCFE351EDBB265CB59AF7DECB1C72ACFA5FF4F92B56BD726175E78A1C1ADC96328E2A4B3B333D9B871632536A2B18ABB5CAD5CB9F28C9D01B1A2018AD889C7E3FBD86130138D97C1AD2906B79ADB4CAE191B6899996B6926D32496E55A9AC2E7A1F87FF0F9CF7FBE923F3EFDE94F27CF3DF75CE5B1EF7CE73BC30356349A870F1F4EBEF18D6F541E8BCFB8FACA57BE52F96FCC06B2F8FAC8238F241FFDE8476BFE1D51DF6672386CB65A66BB9CDEF7D4356E8CDB8A152BF6D7FBAE924F3CF1C47F16F4AE923BE393EB474850856FBAB76FDF5E295EE79E7B6EA5207DEB5BDFAA1C21899FAFBDF6DAE12278E0C081338AD705175C5079EE473FFA5165D8BBE5965B865FFBB39FFD4CD3DD4431142B06FD6894B2F8D8BB776F72F0E0C1CAF70F3EF8E059CD4F6F6F6FE5EB9E3D7B2A476CB3D8696B6BAB7CFFA10F7D480C31E96D660A7BD247BAB578ADBBD74DA4492CC3DDEB4A918762D07AE9A5972A7925CEFC88236071E647965BE2F9EA3344E2F1EC085D9693E26B0C6F31D07DF6B39FAD39B87DF5AB5F3DEB885B7E27943C64BB9CCDF7D45D2519B79FFEF4A70FD4FB73DC56AE5CB9A1609FE336DA1EA5D234DD91E81E7AE8A1119FDFB97367E535D184C7CF31C0C5CF5138E36747DC9A3B868E1C3992A4FFFECACA3737911FE2EB97BEF4A5E1C7E2685CADC12DBE460396DF9B5D6B6FB71862BCDBCC24F7A88FF57950E3FD30E06A97B414FBF3A24A9587E28C8FC8295FFBDAD72A0358EC3CFCE4273F5979ECA28B2EAAEC78CCD7AC2C9FC4F3D9F751BFE26BBC363BFA9FDF39196710C4637116417E708BBF27FE0E79C8765984F7D4E7B8316EB7DE7AEBDF5E77DD757F3D7DFA74DD6E2AD9D6D6F65A193FADBD0C835B5CDF76E9A5975656BE487DEC631F4B9E7FFEF9B35E9FDFE368701343D991B6FC301771B27CF9F211632E1BDCE2C85C1683063766D0587BD6C7FB21BE136D12B3D3C2AE28FB1B58843C14A73E668357EC70ACBE06ED739FFB5CF2BBDFFD6E7867D0F1E3C7CFD89974FFFDF70FE796D18EB865CFC5F7AFBDF65AE5CF1CED144A79C876391BDADBDB3F93F6C5A762984ABF4F8E1E3D3A230D789C51D3D1D1F17E6E703B5F4929A165CB96FDF70B2FBC50AFD3241F4D0365876237332BAE31CAAE338AAFB1E7310A59F58D4962CFE49B6FBE59299A975F7EF919A74AC6F7D9519628AE06B7E68AA16CDD7BEFBD675C1BD952E3D4A2FCE0967D35B8510723EDE11F6F7338D12631FBFBAE688437AF28792886B12C47C411FD6C68CBAEBD8E412BFFFA818181E16BB8B3DFCB8EB2C511B7783EBBD9563E47C5C71E65A766C64E4C839BEDB288E273DDB2812A7642CC84356BD6E48FB6ED504A4AEAEAABAFBEF2C61B6F3C5587A36EC7DADADAF6A4C1D9A1D84DEF8ABD337123925859F1FBF18F7F3C7CB4247ECEAE05888BBFF34758B241CF1137835B76946DC58A15C3A7286547D2E25AC8FC294B59E31383DD473EF291E1A6CAE0461DF7F0579F1EB5B065E29F0755AB495C308123096AD9349C2D92CF115193F275AAFAB5D9A99231C06547E1A2D18DEBBAB3A36BB58EB8C5F7CB962D33B8D92E8B3AB85D910D5571D46DBA6F52127F5EDA83BF9FFB00F00E65A4C4BABABA7ABBBBBB67746A8BEBE962C29FE0B9D38ADD385734CEF99FE33AA40F7FF8C3E3FE7D835B73C7500C6571D17F1C75CDEE44FA852F7CE18C5B75C7756CB51AA9D8DB1D7BCFE3FBECF4CAECEE709FFAD4A7C41033D924E66F8C906FF626F221BE23FD5EE16E2DDEA8835B76C42DDBF118B52B761A55BFF6BCF3CE4BBEF8C52F566EC6953F6237DA4795C411BCD8B919DF479F13D7B7C51927230D87F290ED72B6A43DF293D960153B19A6EBA3016227FD8D37DE783A7FB4ADACBD38FF1F2CE77776760EEEDBB76F4686B6DEDEDEF5E9DF71289DF02F52ECEA73CA6434DC7124A4D6F3710A65EC9DCCF650D6BAAB641C3119AD202A768D1D43D95EECEABBC0E57F8E412E62A6D6EFFA3800EAA4FAF4AC0F0CED999FE8ADC4AB7F6FAA9F53250F4D61706BA93A353B4E9B6C19FAE880383D323E1A20FB7880ECA85AF67D0C6AF93F3F86B496DC5D92A3FE75747414E6CE920D9A876C9793ECC5D375241BB0E26378A63ABCC5D076E79D77E6AF6B1B28732F4E4E9AC8DABBBABADEEDEFEF9FD6A1EDE0C1834FB7B5B5BD59F6C3B2656ABA63AF624BD56921B586BB327D18B4C1ADFECD54AC6BAEB9A6B2873ABEB60CDD9D2DBBB940ECFD1EEB742631C42C3489D371C4A0B077A96BC4C12D6AD66837CFAA3EAB64ACFA15CFC769DC712649DCE8243B3A57FD772C5DBA541EB25D164A6B6BEB55D98D4AB2236F933D6D327EAFEA489B53241B4D7B7B7BE792254B06A6EBC85B1C698BA12D0DC49B153BCBE02686C410056F120BF921BEF2903C64BB6CECED322F1DB016E587B7B8E62DCE8089CB18C67BF7C8B81149FE9AB6588DD08B33C2B4DFD1D171FCB7BFFDED9FA770C39263F7DD77DFAFE2F4C80840C5CE52ECC4901862864DF534AA863E0D4B1E92876C97E5EAC5D3752C3F78C500179FF3161FD21D47D3B2D328E374C8BEBEBECA51EA9FFFFCE7C9C2850BFF9AFFBD383DB2517A71469EF6CF4F27F52D5D5D5D87F7ECD9333891CF69DBB469D39A74F0DB1D173F36D279B48A9D622786C410A5691227BA67BE616F78200FC943B6CBF2F6E2F98F0998E4DAE19AB6E60A9AEFC600B768D1A2FE55AB56BDFCF4D34FBF994EF587E2FCF130383878F8EDB7DF7E75F3E6CDFF71DB6DB76D6C6F6F7F3D822426FB46BB638D62A7D889213144294CF4B4AAA63A0D4B1E92876C97E5EBC58706B8531319D81AB117679CE253DDD3A0599A06C1DA74FD215D27860223BE1E485777BA9637F2A7B02B768A9D181243345C937849BA065A1AE4437CE52179C876A9176FE45E1C143BC54E0C89211AD3DCA1E66FEE28CF1F69B6E6501E92876C97806267297662480C51963DFC4D7B1A963C240FD92E01C5CE52ECC49018A20C4D62533787F2903C64BB04143B4BB113436288A23789AB9BBD399487E421DB25A0D8598A9D18124314D9DCB890BFA5496F2D2E0FC94345DD2ED3D5F4DB25A0D8297662C8D230913334B8A965B67F79A8583EEE2D00143BC54E0C591A260C6EF2903C04A0D8598A9D18124318DCE4214B1E0250EC2C839B258630B8C943F210008A9D622786C4100637B5CC928700143B4BB113436208839B3C64C943008A9D6227862C0D13063779481E0230B829768A9D18124318DCD4324B1E0250EC2C839B258630B8C943F210008A9D6227862C0D93C14D2DB3E42100C5CE52ECC49018C2E0260F59F21080626719DC2C0D13063779481E0240B153ECC49018C2E0A69659F21080626719DC2C3184C14D1E9287E42100C54EB11343968609839B3C240F01287696622786C410063779C89287008A64CB962D8A4C71D689B4D89D124356B3C510063779481E02600C4F3DF5D4A1FEFE7E85A600ABAFAFEFDFD262F78A18B29A2D8630B8C943F2100063D8BA75EBE5DBB66DFBD31FFFF8C7410567F6F64E46A1DBBC79F39BE9FAAE18B29A2D8630B8C943F21000E39026D8793D3D3DBBE3D486382FBD6C6BCD9A3549342E65FCB70FAD78DF5F2973A12B7B0C35C02A7D0C61709387E421001ABF69E98EC665CE9C39E7783700831B0040C1B4B6B67E396D5A4E45E3327FFEFCB9DE11C0E0060050BCC16D5D342D436BB7770430B8010014AB59B9243BDA96AD74909BE79D010C6E0000C56956BAF3435B76D4CDB56E80C10D00A0188DCA5947DBB2357FFEFC2BBC4380C10D0060F61B958DB586B6A1B5D33B0418DC000066B74919F1689B6BDD00831B0040319A94EED186367798040C6E00001A1700F90F0040E30220FF01001A1700F90F0040E30220FF0100685C00F94FFE0300342E00F21F0080C60540FE0300342E00F21F0080C60540FE0300D0B800C87F0080C60540FE0300D0B800C87F0080C60540FE0300D0B800C87F00001A1700F90F00D0B800C87F00001A1700F90F0040E30220FF01001A1700F90F0040E30220FF01001A1700F90F0040E30220FF0100685C00E43F0040E30220FF0100685C00E43F0040E30220FF0100685C00E43F00008D0B80FC0700346CE3625996D5AC4B15000000000000000000000000000000000000000000000000000000000000008012FA3FEBAD960F661C1DE30000000049454E44AE426082, 1);

-- ----------------------------
-- Table structure for act_ge_property
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_property`;
CREATE TABLE `act_ge_property`  (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ge_property
-- ----------------------------
INSERT INTO `act_ge_property` VALUES ('batch.schema.version', '6.7.2.0', 1);
INSERT INTO `act_ge_property` VALUES ('cfg.execution-related-entities-count', 'true', 1);
INSERT INTO `act_ge_property` VALUES ('cfg.task-related-entities-count', 'true', 1);
INSERT INTO `act_ge_property` VALUES ('common.schema.version', '6.7.2.0', 1);
INSERT INTO `act_ge_property` VALUES ('entitylink.schema.version', '6.7.2.0', 1);
INSERT INTO `act_ge_property` VALUES ('eventsubscription.schema.version', '6.7.2.0', 1);
INSERT INTO `act_ge_property` VALUES ('identitylink.schema.version', '6.7.2.0', 1);
INSERT INTO `act_ge_property` VALUES ('job.schema.version', '6.7.2.0', 1);
INSERT INTO `act_ge_property` VALUES ('next.dbid', '1', 1);
INSERT INTO `act_ge_property` VALUES ('schema.history', 'create(6.7.2.0)', 1);
INSERT INTO `act_ge_property` VALUES ('schema.version', '6.7.2.0', 1);
INSERT INTO `act_ge_property` VALUES ('task.schema.version', '6.7.2.0', 1);
INSERT INTO `act_ge_property` VALUES ('variable.schema.version', '6.7.2.0', 1);

-- ----------------------------
-- Table structure for act_hi_actinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_actinst`;
CREATE TABLE `act_hi_actinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `TRANSACTION_ORDER_` int(11) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_START`(`START_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_PROCINST`(`PROC_INST_ID_`, `ACT_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ACT_INST_EXEC`(`EXECUTION_ID_`, `ACT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_actinst
-- ----------------------------
INSERT INTO `act_hi_actinst` VALUES ('0d589199-670c-11ed-b9c8-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-11-18 14:41:37.953', '2022-11-18 14:41:37.960', 1, 7, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('0d59f12a-670c-11ed-b9c8-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-B8FA623F-FAE2-4ED8-A3F9-DC2D2C5346F2', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-11-18 14:41:37.962', '2022-11-18 14:41:37.962', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('0d59f12b-670c-11ed-b9c8-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', '0d5d739c-670c-11ed-b9c8-7486e21fa90d', NULL, '学生', 'userTask', '测试用户', '2022-11-18 14:41:37.962', '2022-11-18 14:41:53.084', 3, 15122, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('165dae71-670c-11ed-b9c8-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-193EDD02-A466-44B8-AC0A-D3489DDCC795', NULL, NULL, '请假', 'sequenceFlow', NULL, '2022-11-18 14:41:53.086', '2022-11-18 14:41:53.086', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('165dae72-670c-11ed-b9c8-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-E2128506-E74C-42FC-9648-C9A35E7A0092', '165dd583-670c-11ed-b9c8-7486e21fa90d', NULL, '老师', 'userTask', '刘旭', '2022-11-18 14:41:53.086', '2022-11-18 14:43:42.048', 2, 108962, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('379c6824-6713-11ed-a5a9-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-193EDD02-A466-44B8-AC0A-D3489DDCC795', NULL, NULL, '请假', 'sequenceFlow', NULL, '2022-11-18 15:32:55.339', '2022-11-18 15:32:55.339', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('379c6825-6713-11ed-a5a9-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-E2128506-E74C-42FC-9648-C9A35E7A0092', '379cb646-6713-11ed-a5a9-7486e21fa90d', NULL, '老师', 'userTask', '刘旭', '2022-11-18 15:32:55.339', '2022-11-18 15:33:23.850', 2, 28511, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('489afe2b-6713-11ed-a5a9-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-332335FB-071F-455C-A9C6-B24F72DD8D00', NULL, NULL, '审批', 'sequenceFlow', NULL, '2022-11-18 15:33:23.851', '2022-11-18 15:33:23.851', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('489afe2c-6713-11ed-a5a9-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-850E31E4-66AB-40E5-B673-EECD7C9912CE', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2022-11-18 15:33:23.851', '2022-11-18 15:33:23.869', 2, 18, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('489dbd4d-6713-11ed-a5a9-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-243BB91B-304F-44D9-BCB8-D182E22D4054', NULL, NULL, '同意', 'sequenceFlow', NULL, '2022-11-18 15:33:23.869', '2022-11-18 15:33:23.869', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('489e0b6e-6713-11ed-a5a9-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-B9B50A00-58E3-452E-BDD2-324AFF241142', '489e0b6f-6713-11ed-a5a9-7486e21fa90d', NULL, '管理员', 'userTask', '超级管理员', '2022-11-18 15:33:23.871', '2022-11-18 15:33:50.595', 4, 26724, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('575044b8-670c-11ed-b9c8-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-332335FB-071F-455C-A9C6-B24F72DD8D00', NULL, NULL, '审批', 'sequenceFlow', NULL, '2022-11-18 14:43:42.050', '2022-11-18 14:43:42.050', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('57506bc9-670c-11ed-b9c8-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-850E31E4-66AB-40E5-B673-EECD7C9912CE', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2022-11-18 14:43:42.051', '2022-11-18 14:43:42.099', 2, 48, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('5757beca-670c-11ed-b9c8-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-243BB91B-304F-44D9-BCB8-D182E22D4054', NULL, NULL, '同意', 'sequenceFlow', NULL, '2022-11-18 14:43:42.099', '2022-11-18 14:43:42.099', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('57580ceb-670c-11ed-b9c8-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-B9B50A00-58E3-452E-BDD2-324AFF241142', '57580cec-670c-11ed-b9c8-7486e21fa90d', NULL, '管理员', 'userTask', '超级管理员', '2022-11-18 14:43:42.101', '2022-11-18 14:45:21.665', 4, 99564, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('588bcdb2-6713-11ed-a5a9-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-FBA44E63-BDCA-43D9-83A6-247B117855F4', NULL, NULL, '审批', 'sequenceFlow', NULL, '2022-11-18 15:33:50.595', '2022-11-18 15:33:50.595', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('588bf4c3-6713-11ed-a5a9-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-081403ED-C9FB-4807-AA7F-FA212AA2E999', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2022-11-18 15:33:50.596', '2022-11-18 15:33:50.596', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('588bf4c4-6713-11ed-a5a9-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-7B3C1045-D138-4E2E-AF65-9C3D5693078D', NULL, NULL, '拒绝', 'sequenceFlow', NULL, '2022-11-18 15:33:50.596', '2022-11-18 15:33:50.596', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('588c1bd5-6713-11ed-a5a9-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', '588c1bd6-6713-11ed-a5a9-7486e21fa90d', NULL, '学生', 'userTask', '测试用户', '2022-11-18 15:33:50.597', '2022-11-23 13:48:13.579', 4, 425662982, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('6b767adc-6af2-11ed-8aee-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-193EDD02-A466-44B8-AC0A-D3489DDCC795', NULL, NULL, '请假', 'sequenceFlow', NULL, '2022-11-23 13:48:13.591', '2022-11-23 13:48:13.591', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('6b76f00d-6af2-11ed-8aee-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-E2128506-E74C-42FC-9648-C9A35E7A0092', '6b7d7fbe-6af2-11ed-8aee-7486e21fa90d', NULL, '老师', 'userTask', '刘旭', '2022-11-23 13:48:13.594', '2022-11-23 13:49:58.091', 2, 104497, 'delete', '');
INSERT INTO `act_hi_actinst` VALUES ('7c8f8178-6af3-11ed-b7e1-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-11-23 13:55:51.774', '2022-11-23 13:55:51.779', 1, 5, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('7c90b9f9-6af3-11ed-b7e1-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', 'sid-B8FA623F-FAE2-4ED8-A3F9-DC2D2C5346F2', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-11-23 13:55:51.781', '2022-11-23 13:55:51.781', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('7c90b9fa-6af3-11ed-b7e1-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', '7c94b19b-6af3-11ed-b7e1-7486e21fa90d', NULL, '学生', 'userTask', '测试用户', '2022-11-23 13:55:51.781', '2022-11-23 13:56:12.121', 3, 20340, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('88b08450-6af3-11ed-b7e1-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', 'sid-193EDD02-A466-44B8-AC0A-D3489DDCC795', NULL, NULL, '请假', 'sequenceFlow', NULL, '2022-11-23 13:56:12.122', '2022-11-23 13:56:12.122', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('88b0ab61-6af3-11ed-b7e1-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', 'sid-E2128506-E74C-42FC-9648-C9A35E7A0092', '88b0ab62-6af3-11ed-b7e1-7486e21fa90d', NULL, '老师', 'userTask', '刘旭', '2022-11-23 13:56:12.123', '2022-11-23 13:56:52.200', 2, 40077, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('92b076bf-670c-11ed-b9c8-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-FBA44E63-BDCA-43D9-83A6-247B117855F4', NULL, NULL, '审批', 'sequenceFlow', NULL, '2022-11-18 14:45:21.666', '2022-11-18 14:45:21.666', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('92b09dd0-670c-11ed-b9c8-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-081403ED-C9FB-4807-AA7F-FA212AA2E999', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2022-11-18 14:45:21.667', '2022-11-18 14:45:21.667', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('92b09dd1-670c-11ed-b9c8-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-19855A0C-66F1-4695-8FEE-F77A11FBE513', NULL, NULL, '同意', 'sequenceFlow', NULL, '2022-11-18 14:45:21.667', '2022-11-18 14:45:21.667', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('92b0c4e2-670c-11ed-b9c8-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', 'sid-69BD0577-272F-4FB4-96D5-1ED3F9FC9EBE', NULL, NULL, '结束', 'endEvent', NULL, '2022-11-18 14:45:21.668', '2022-11-18 14:45:21.672', 4, 4, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('9e0ad167-6710-11ed-a797-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-11-18 15:14:18.700', '2022-11-18 15:14:18.708', 1, 8, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('9e0ca628-6710-11ed-a797-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-B8FA623F-FAE2-4ED8-A3F9-DC2D2C5346F2', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-11-18 15:14:18.712', '2022-11-18 15:14:18.712', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('9e0ca629-6710-11ed-a797-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', '9e14956a-6710-11ed-a797-7486e21fa90d', NULL, '学生', 'userTask', '测试用户', '2022-11-18 15:14:18.712', '2022-11-18 15:14:56.409', 3, 37697, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('a0941647-6af3-11ed-b7e1-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', 'sid-332335FB-071F-455C-A9C6-B24F72DD8D00', NULL, NULL, '审批', 'sequenceFlow', NULL, '2022-11-23 13:56:52.201', '2022-11-23 13:56:52.201', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('a0941648-6af3-11ed-b7e1-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', 'sid-850E31E4-66AB-40E5-B673-EECD7C9912CE', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2022-11-23 13:56:52.201', '2022-11-23 13:56:52.235', 2, 34, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('a0994669-6af3-11ed-b7e1-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', 'sid-243BB91B-304F-44D9-BCB8-D182E22D4054', NULL, NULL, '同意', 'sequenceFlow', NULL, '2022-11-23 13:56:52.235', '2022-11-23 13:56:52.235', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('a0996d7a-6af3-11ed-b7e1-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', 'sid-B9B50A00-58E3-452E-BDD2-324AFF241142', 'a0996d7b-6af3-11ed-b7e1-7486e21fa90d', NULL, '管理员', 'userTask', '超级管理员', '2022-11-23 13:56:52.236', '2022-11-23 13:57:12.061', 4, 19825, 'delete', '');
INSERT INTO `act_hi_actinst` VALUES ('b4850f5f-6710-11ed-a797-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-193EDD02-A466-44B8-AC0A-D3489DDCC795', NULL, NULL, '请假', 'sequenceFlow', NULL, '2022-11-18 15:14:56.411', '2022-11-18 15:14:56.411', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('b4853670-6710-11ed-a797-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-E2128506-E74C-42FC-9648-C9A35E7A0092', 'b4853671-6710-11ed-a797-7486e21fa90d', NULL, '老师', 'userTask', '刘旭', '2022-11-18 15:14:56.412', '2022-11-18 15:15:38.302', 2, 41890, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('cd7d6cb6-6710-11ed-a797-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-332335FB-071F-455C-A9C6-B24F72DD8D00', NULL, NULL, '审批', 'sequenceFlow', NULL, '2022-11-18 15:15:38.304', '2022-11-18 15:15:38.304', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('cd7d6cb7-6710-11ed-a797-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-850E31E4-66AB-40E5-B673-EECD7C9912CE', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2022-11-18 15:15:38.304', '2022-11-18 15:15:38.339', 2, 35, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('cd82c3e8-6710-11ed-a797-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-243BB91B-304F-44D9-BCB8-D182E22D4054', NULL, NULL, '同意', 'sequenceFlow', NULL, '2022-11-18 15:15:38.339', '2022-11-18 15:15:38.339', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('cd831209-6710-11ed-a797-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-B9B50A00-58E3-452E-BDD2-324AFF241142', 'cd83120a-6710-11ed-a797-7486e21fa90d', NULL, '管理员', 'userTask', '超级管理员', '2022-11-18 15:15:38.341', '2022-11-18 15:16:02.325', 4, 23984, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('dbcf0b2d-6710-11ed-a797-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-FBA44E63-BDCA-43D9-83A6-247B117855F4', NULL, NULL, '审批', 'sequenceFlow', NULL, '2022-11-18 15:16:02.327', '2022-11-18 15:16:02.327', 1, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('dbcf594e-6710-11ed-a797-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-081403ED-C9FB-4807-AA7F-FA212AA2E999', NULL, NULL, NULL, 'exclusiveGateway', NULL, '2022-11-18 15:16:02.329', '2022-11-18 15:16:02.329', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('dbcf594f-6710-11ed-a797-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-7B3C1045-D138-4E2E-AF65-9C3D5693078D', NULL, NULL, '拒绝', 'sequenceFlow', NULL, '2022-11-18 15:16:02.329', '2022-11-18 15:16:02.329', 3, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('dbcf8060-6710-11ed-a797-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', 'dbcf8061-6710-11ed-a797-7486e21fa90d', NULL, '学生', 'userTask', '测试用户', '2022-11-18 15:16:02.330', '2022-11-18 15:26:38.786', 4, 636456, 'delete', '');
INSERT INTO `act_hi_actinst` VALUES ('ef04932c-6712-11ed-a5a9-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'startEvent1', NULL, NULL, '开始', 'startEvent', NULL, '2022-11-18 15:30:53.548', '2022-11-18 15:30:53.574', 1, 26, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('ef09752d-6712-11ed-a5a9-7486e21fa90d', 1, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-B8FA623F-FAE2-4ED8-A3F9-DC2D2C5346F2', NULL, NULL, NULL, 'sequenceFlow', NULL, '2022-11-18 15:30:53.580', '2022-11-18 15:30:53.580', 2, 0, NULL, '');
INSERT INTO `act_hi_actinst` VALUES ('ef09752e-6712-11ed-a5a9-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', 'ef0ecc5f-6712-11ed-a5a9-7486e21fa90d', NULL, '学生', 'userTask', '测试用户', '2022-11-18 15:30:53.580', '2022-11-18 15:32:55.338', 3, 121758, NULL, '');

-- ----------------------------
-- Table structure for act_hi_attachment
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_attachment`;
CREATE TABLE `act_hi_attachment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `URL_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CONTENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_comment
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_comment`;
CREATE TABLE `act_hi_comment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACTION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `MESSAGE_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `FULL_MSG_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_comment
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_detail
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_detail`;
CREATE TABLE `act_hi_detail`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_PROC_INST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_ACT_INST`(`ACT_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_TIME`(`TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_NAME`(`NAME_`) USING BTREE,
  INDEX `ACT_IDX_HI_DETAIL_TASK_ID`(`TASK_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_detail
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_entitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_entitylink`;
CREATE TABLE `act_hi_entitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LINK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HIERARCHY_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_ENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_ENT_LNK_REF_SCOPE`(`REF_SCOPE_ID_`, `REF_SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_ENT_LNK_ROOT_SCOPE`(`ROOT_SCOPE_ID_`, `ROOT_SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_ENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_entitylink
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_identitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_identitylink`;
CREATE TABLE `act_hi_identitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_TASK`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_IDENT_LNK_PROCINST`(`PROC_INST_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_identitylink
-- ----------------------------
INSERT INTO `act_hi_identitylink` VALUES ('0d5e5dfd-670c-11ed-b9c8-7486e21fa90d', NULL, 'assignee', '测试用户', '0d5d739c-670c-11ed-b9c8-7486e21fa90d', '2022-11-18 14:41:37.991', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('0d5e850e-670c-11ed-b9c8-7486e21fa90d', NULL, 'participant', '测试用户', NULL, '2022-11-18 14:41:37.992', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('165dd584-670c-11ed-b9c8-7486e21fa90d', NULL, 'assignee', '刘旭', '165dd583-670c-11ed-b9c8-7486e21fa90d', '2022-11-18 14:41:53.087', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('165dfc95-670c-11ed-b9c8-7486e21fa90d', NULL, 'participant', '刘旭', NULL, '2022-11-18 14:41:53.088', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('379cb647-6713-11ed-a5a9-7486e21fa90d', NULL, 'assignee', '刘旭', '379cb646-6713-11ed-a5a9-7486e21fa90d', '2022-11-18 15:32:55.341', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('379d0468-6713-11ed-a5a9-7486e21fa90d', NULL, 'participant', '刘旭', NULL, '2022-11-18 15:32:55.343', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('489e0b70-6713-11ed-a5a9-7486e21fa90d', NULL, 'assignee', '超级管理员', '489e0b6f-6713-11ed-a5a9-7486e21fa90d', '2022-11-18 15:33:23.871', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('489e3281-6713-11ed-a5a9-7486e21fa90d', NULL, 'participant', '超级管理员', NULL, '2022-11-18 15:33:23.872', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('575833fd-670c-11ed-b9c8-7486e21fa90d', NULL, 'assignee', '超级管理员', '57580cec-670c-11ed-b9c8-7486e21fa90d', '2022-11-18 14:43:42.102', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('57585b0e-670c-11ed-b9c8-7486e21fa90d', NULL, 'participant', '超级管理员', NULL, '2022-11-18 14:43:42.103', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('588c1bd7-6713-11ed-a5a9-7486e21fa90d', NULL, 'assignee', '测试用户', '588c1bd6-6713-11ed-a5a9-7486e21fa90d', '2022-11-18 15:33:50.597', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('6b81775f-6af2-11ed-8aee-7486e21fa90d', NULL, 'assignee', '刘旭', '6b7d7fbe-6af2-11ed-8aee-7486e21fa90d', '2022-11-23 13:48:13.663', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('7c9574ec-6af3-11ed-b7e1-7486e21fa90d', NULL, 'assignee', '测试用户', '7c94b19b-6af3-11ed-b7e1-7486e21fa90d', '2022-11-23 13:55:51.812', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('7c959bfd-6af3-11ed-b7e1-7486e21fa90d', NULL, 'participant', '测试用户', NULL, '2022-11-23 13:55:51.813', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('88b0ab63-6af3-11ed-b7e1-7486e21fa90d', NULL, 'assignee', '刘旭', '88b0ab62-6af3-11ed-b7e1-7486e21fa90d', '2022-11-23 13:56:12.123', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('88b0f984-6af3-11ed-b7e1-7486e21fa90d', NULL, 'participant', '刘旭', NULL, '2022-11-23 13:56:12.125', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('9e15a6db-6710-11ed-a797-7486e21fa90d', NULL, 'assignee', '测试用户', '9e14956a-6710-11ed-a797-7486e21fa90d', '2022-11-18 15:14:18.771', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('9e15cdec-6710-11ed-a797-7486e21fa90d', NULL, 'participant', '测试用户', NULL, '2022-11-18 15:14:18.772', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('a099948c-6af3-11ed-b7e1-7486e21fa90d', NULL, 'assignee', '超级管理员', 'a0996d7b-6af3-11ed-b7e1-7486e21fa90d', '2022-11-23 13:56:52.237', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('a099bb9d-6af3-11ed-b7e1-7486e21fa90d', NULL, 'participant', '超级管理员', NULL, '2022-11-23 13:56:52.238', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('b4853672-6710-11ed-a797-7486e21fa90d', NULL, 'assignee', '刘旭', 'b4853671-6710-11ed-a797-7486e21fa90d', '2022-11-18 15:14:56.412', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('b4855d83-6710-11ed-a797-7486e21fa90d', NULL, 'participant', '刘旭', NULL, '2022-11-18 15:14:56.413', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('cd83391b-6710-11ed-a797-7486e21fa90d', NULL, 'assignee', '超级管理员', 'cd83120a-6710-11ed-a797-7486e21fa90d', '2022-11-18 15:15:38.342', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('cd83873c-6710-11ed-a797-7486e21fa90d', NULL, 'participant', '超级管理员', NULL, '2022-11-18 15:15:38.344', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('dbcf8062-6710-11ed-a797-7486e21fa90d', NULL, 'assignee', '测试用户', 'dbcf8061-6710-11ed-a797-7486e21fa90d', '2022-11-18 15:16:02.330', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('ef0f8fb0-6712-11ed-a5a9-7486e21fa90d', NULL, 'assignee', '测试用户', 'ef0ecc5f-6712-11ed-a5a9-7486e21fa90d', '2022-11-18 15:30:53.620', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_identitylink` VALUES ('ef0fddd1-6712-11ed-a5a9-7486e21fa90d', NULL, 'participant', '测试用户', NULL, '2022-11-18 15:30:53.622', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for act_hi_procinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_procinst`;
CREATE TABLE `act_hi_procinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `END_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BUSINESS_STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `PROC_INST_ID_`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PRO_INST_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_HI_PRO_I_BUSKEY`(`BUSINESS_KEY_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_procinst
-- ----------------------------
INSERT INTO `act_hi_procinst` VALUES ('0d581c62-670c-11ed-b9c8-7486e21fa90d', 2, '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '2022-11-18 14:41:37.949', '2022-11-18 14:45:21.699', 223750, NULL, 'startEvent1', 'sid-69BD0577-272F-4FB4-96D5-1ED3F9FC9EBE', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_procinst` VALUES ('7c8e9711-6af3-11ed-b7e1-7486e21fa90d', 2, '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '2022-11-23 13:55:51.767', '2022-11-23 13:57:12.083', 80316, NULL, 'startEvent1', NULL, NULL, 'delete', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_procinst` VALUES ('9e094ac0-6710-11ed-a797-7486e21fa90d', 2, '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '2022-11-18 15:14:18.690', '2022-11-18 15:26:38.811', 740121, NULL, 'startEvent1', NULL, NULL, 'delete', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `act_hi_procinst` VALUES ('ef035aa5-6712-11ed-a5a9-7486e21fa90d', 2, 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', '2022-11-18 15:30:53.540', '2022-11-23 13:49:58.130', 425944590, NULL, 'startEvent1', NULL, NULL, 'delete', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for act_hi_taskinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_taskinst`;
CREATE TABLE `act_hi_taskinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `CLAIM_TIME_` datetime(3) NULL DEFAULT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PRIORITY_` int(11) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) NULL DEFAULT NULL,
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_TASK_INST_PROCINST`(`PROC_INST_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_taskinst
-- ----------------------------
INSERT INTO `act_hi_taskinst` VALUES ('0d5d739c-670c-11ed-b9c8-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '学生', NULL, NULL, NULL, '测试用户', '2022-11-18 14:41:37.962', NULL, '2022-11-18 14:41:53.078', 15116, NULL, 50, NULL, NULL, NULL, '', '2022-11-18 14:41:53.078');
INSERT INTO `act_hi_taskinst` VALUES ('165dd583-670c-11ed-b9c8-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-E2128506-E74C-42FC-9648-C9A35E7A0092', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '老师', NULL, NULL, NULL, '刘旭', '2022-11-18 14:41:53.086', NULL, '2022-11-18 14:43:42.043', 108957, NULL, 50, NULL, NULL, NULL, '', '2022-11-18 14:43:42.043');
INSERT INTO `act_hi_taskinst` VALUES ('379cb646-6713-11ed-a5a9-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-E2128506-E74C-42FC-9648-C9A35E7A0092', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '老师', NULL, NULL, NULL, '刘旭', '2022-11-18 15:32:55.339', NULL, '2022-11-18 15:33:23.847', 28508, NULL, 50, NULL, NULL, NULL, '', '2022-11-18 15:33:23.847');
INSERT INTO `act_hi_taskinst` VALUES ('489e0b6f-6713-11ed-a5a9-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-B9B50A00-58E3-452E-BDD2-324AFF241142', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '管理员', NULL, NULL, NULL, '超级管理员', '2022-11-18 15:33:23.871', NULL, '2022-11-18 15:33:50.592', 26721, NULL, 50, NULL, NULL, NULL, '', '2022-11-18 15:33:50.592');
INSERT INTO `act_hi_taskinst` VALUES ('57580cec-670c-11ed-b9c8-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-B9B50A00-58E3-452E-BDD2-324AFF241142', '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d589198-670c-11ed-b9c8-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '管理员', NULL, NULL, NULL, '超级管理员', '2022-11-18 14:43:42.101', NULL, '2022-11-18 14:45:21.662', 99561, NULL, 50, NULL, NULL, NULL, '', '2022-11-18 14:45:21.662');
INSERT INTO `act_hi_taskinst` VALUES ('588c1bd6-6713-11ed-a5a9-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '学生', NULL, NULL, NULL, '测试用户', '2022-11-18 15:33:50.597', NULL, '2022-11-23 13:48:13.556', 425662959, NULL, 50, NULL, NULL, NULL, '', '2022-11-23 13:48:13.556');
INSERT INTO `act_hi_taskinst` VALUES ('6b7d7fbe-6af2-11ed-8aee-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-E2128506-E74C-42FC-9648-C9A35E7A0092', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '老师', NULL, NULL, NULL, '刘旭', '2022-11-23 13:48:13.594', NULL, '2022-11-23 13:49:58.088', 104494, 'delete', 50, NULL, NULL, NULL, '', '2022-11-23 13:49:58.088');
INSERT INTO `act_hi_taskinst` VALUES ('7c94b19b-6af3-11ed-b7e1-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '学生', NULL, NULL, NULL, '测试用户', '2022-11-23 13:55:51.781', NULL, '2022-11-23 13:56:12.116', 20335, NULL, 50, NULL, NULL, NULL, '', '2022-11-23 13:56:12.116');
INSERT INTO `act_hi_taskinst` VALUES ('88b0ab62-6af3-11ed-b7e1-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-E2128506-E74C-42FC-9648-C9A35E7A0092', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '老师', NULL, NULL, NULL, '刘旭', '2022-11-23 13:56:12.123', NULL, '2022-11-23 13:56:52.198', 40075, NULL, 50, NULL, NULL, NULL, '', '2022-11-23 13:56:52.198');
INSERT INTO `act_hi_taskinst` VALUES ('9e14956a-6710-11ed-a797-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '学生', NULL, NULL, NULL, '测试用户', '2022-11-18 15:14:18.712', NULL, '2022-11-18 15:14:56.403', 37691, NULL, 50, NULL, NULL, NULL, '', '2022-11-18 15:14:56.403');
INSERT INTO `act_hi_taskinst` VALUES ('a0996d7b-6af3-11ed-b7e1-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-B9B50A00-58E3-452E-BDD2-324AFF241142', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8f8177-6af3-11ed-b7e1-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '管理员', NULL, NULL, NULL, '超级管理员', '2022-11-23 13:56:52.236', NULL, '2022-11-23 13:57:12.058', 19822, 'delete', 50, NULL, NULL, NULL, '', '2022-11-23 13:57:12.058');
INSERT INTO `act_hi_taskinst` VALUES ('b4853671-6710-11ed-a797-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-E2128506-E74C-42FC-9648-C9A35E7A0092', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '老师', NULL, NULL, NULL, '刘旭', '2022-11-18 15:14:56.412', NULL, '2022-11-18 15:15:38.300', 41888, NULL, 50, NULL, NULL, NULL, '', '2022-11-18 15:15:38.300');
INSERT INTO `act_hi_taskinst` VALUES ('cd83120a-6710-11ed-a797-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-B9B50A00-58E3-452E-BDD2-324AFF241142', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '管理员', NULL, NULL, NULL, '超级管理员', '2022-11-18 15:15:38.341', NULL, '2022-11-18 15:16:02.318', 23977, NULL, 50, NULL, NULL, NULL, '', '2022-11-18 15:16:02.318');
INSERT INTO `act_hi_taskinst` VALUES ('dbcf8061-6710-11ed-a797-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e0ad166-6710-11ed-a797-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '学生', NULL, NULL, NULL, '测试用户', '2022-11-18 15:16:02.330', NULL, '2022-11-18 15:26:38.780', 636450, 'delete', 50, NULL, NULL, NULL, '', '2022-11-18 15:26:38.780');
INSERT INTO `act_hi_taskinst` VALUES ('ef0ecc5f-6712-11ed-a5a9-7486e21fa90d', 2, 'leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', NULL, 'sid-CDBFAA03-BB47-4BF6-A1D5-268012D13FE2', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef04932b-6712-11ed-a5a9-7486e21fa90d', NULL, NULL, NULL, NULL, NULL, '学生', NULL, NULL, NULL, '测试用户', '2022-11-18 15:30:53.580', NULL, '2022-11-18 15:32:55.334', 121754, NULL, 50, NULL, NULL, NULL, '', '2022-11-18 15:32:55.334');

-- ----------------------------
-- Table structure for act_hi_tsk_log
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_tsk_log`;
CREATE TABLE `act_hi_tsk_log`  (
  `ID_` bigint(20) NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DATA_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_tsk_log
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_varinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_varinst`;
CREATE TABLE `act_hi_varinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_NAME_TYPE`(`NAME_`, `VAR_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_VAR_SCOPE_ID_TYPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_VAR_SUB_ID_TYPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_PROC_INST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_TASK_ID`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_IDX_HI_PROCVAR_EXE`(`EXECUTION_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_hi_varinst
-- ----------------------------
INSERT INTO `act_hi_varinst` VALUES ('0d581c63-670c-11ed-b9c8-7486e21fa90d', 0, '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, 'leaveDays', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '10', NULL, '2022-11-18 14:41:37.952', '2022-11-18 14:41:37.952');
INSERT INTO `act_hi_varinst` VALUES ('0d586a84-670c-11ed-b9c8-7486e21fa90d', 0, '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, 'student', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '测试用户', NULL, '2022-11-18 14:41:37.952', '2022-11-18 14:41:37.952');
INSERT INTO `act_hi_varinst` VALUES ('0d586a85-670c-11ed-b9c8-7486e21fa90d', 0, '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, 'leaveReason', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '我想请10天假', NULL, '2022-11-18 14:41:37.952', '2022-11-18 14:41:37.952');
INSERT INTO `act_hi_varinst` VALUES ('0d586a86-670c-11ed-b9c8-7486e21fa90d', 0, '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, 'leavePerson', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '测试用户', NULL, '2022-11-18 14:41:37.952', '2022-11-18 14:41:37.952');
INSERT INTO `act_hi_varinst` VALUES ('0d586a87-670c-11ed-b9c8-7486e21fa90d', 3, '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, 'status', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '3', NULL, '2022-11-18 14:41:37.952', '2022-11-18 14:45:21.662');
INSERT INTO `act_hi_varinst` VALUES ('165c4edf-670c-11ed-b9c8-7486e21fa90d', 0, '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, 'teacher', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '刘旭', NULL, '2022-11-18 14:41:53.077', '2022-11-18 14:41:53.077');
INSERT INTO `act_hi_varinst` VALUES ('165c4ee0-670c-11ed-b9c8-7486e21fa90d', 2, '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, 'refusedReason', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2022-11-18 14:41:53.077', '2022-11-18 14:45:21.660');
INSERT INTO `act_hi_varinst` VALUES ('379b2fa2-6713-11ed-a5a9-7486e21fa90d', 1, 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, 'teacher', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '刘旭', NULL, '2022-11-18 15:32:55.332', '2022-11-23 13:48:13.537');
INSERT INTO `act_hi_varinst` VALUES ('379b56b3-6713-11ed-a5a9-7486e21fa90d', 3, 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, 'refusedReason', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2022-11-18 15:32:55.332', '2022-11-23 13:48:13.543');
INSERT INTO `act_hi_varinst` VALUES ('489a3ad9-6713-11ed-a5a9-7486e21fa90d', 0, 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, 'admin', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '超级管理员', NULL, '2022-11-18 15:33:23.846', '2022-11-18 15:33:23.846');
INSERT INTO `act_hi_varinst` VALUES ('489a3ada-6713-11ed-a5a9-7486e21fa90d', 1, 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, 'command', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'refuse', NULL, '2022-11-18 15:33:23.846', '2022-11-18 15:33:50.590');
INSERT INTO `act_hi_varinst` VALUES ('574ebe16-670c-11ed-b9c8-7486e21fa90d', 1, '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, 'admin', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '超级管理员', NULL, '2022-11-18 14:43:42.040', '2022-11-18 14:45:21.659');
INSERT INTO `act_hi_varinst` VALUES ('574f0c37-670c-11ed-b9c8-7486e21fa90d', 1, '0d581c62-670c-11ed-b9c8-7486e21fa90d', '0d581c62-670c-11ed-b9c8-7486e21fa90d', NULL, 'command', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'agree', NULL, '2022-11-18 14:43:42.042', '2022-11-18 14:45:21.661');
INSERT INTO `act_hi_varinst` VALUES ('7c8ee532-6af3-11ed-b7e1-7486e21fa90d', 0, '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, 'leaveDays', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '50', NULL, '2022-11-23 13:55:51.772', '2022-11-23 13:55:51.772');
INSERT INTO `act_hi_varinst` VALUES ('7c8f5a63-6af3-11ed-b7e1-7486e21fa90d', 0, '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, 'student', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '测试用户', NULL, '2022-11-23 13:55:51.772', '2022-11-23 13:55:51.772');
INSERT INTO `act_hi_varinst` VALUES ('7c8f5a64-6af3-11ed-b7e1-7486e21fa90d', 0, '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, 'leaveReason', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '我不想干了', NULL, '2022-11-23 13:55:51.772', '2022-11-23 13:55:51.772');
INSERT INTO `act_hi_varinst` VALUES ('7c8f5a65-6af3-11ed-b7e1-7486e21fa90d', 0, '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, 'leavePerson', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '测试用户', NULL, '2022-11-23 13:55:51.772', '2022-11-23 13:55:51.772');
INSERT INTO `act_hi_varinst` VALUES ('7c8f5a66-6af3-11ed-b7e1-7486e21fa90d', 2, '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, 'status', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, '2022-11-23 13:55:51.772', '2022-11-23 13:56:52.198');
INSERT INTO `act_hi_varinst` VALUES ('88aed69e-6af3-11ed-b7e1-7486e21fa90d', 0, '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, 'teacher', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '刘旭', NULL, '2022-11-23 13:56:12.111', '2022-11-23 13:56:12.111');
INSERT INTO `act_hi_varinst` VALUES ('88aefdaf-6af3-11ed-b7e1-7486e21fa90d', 1, '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, 'refusedReason', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '2022-11-23 13:56:12.112', '2022-11-23 13:56:52.197');
INSERT INTO `act_hi_varinst` VALUES ('9e09e701-6710-11ed-a797-7486e21fa90d', 0, '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, 'leaveDays', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '20', NULL, '2022-11-18 15:14:18.697', '2022-11-18 15:14:18.697');
INSERT INTO `act_hi_varinst` VALUES ('9e0a8342-6710-11ed-a797-7486e21fa90d', 0, '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, 'student', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '测试用户', NULL, '2022-11-18 15:14:18.698', '2022-11-18 15:14:18.698');
INSERT INTO `act_hi_varinst` VALUES ('9e0a8343-6710-11ed-a797-7486e21fa90d', 0, '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, 'leaveReason', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '我想请20天假', NULL, '2022-11-18 15:14:18.698', '2022-11-18 15:14:18.698');
INSERT INTO `act_hi_varinst` VALUES ('9e0a8344-6710-11ed-a797-7486e21fa90d', 0, '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, 'leavePerson', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '测试用户', NULL, '2022-11-18 15:14:18.698', '2022-11-18 15:14:18.698');
INSERT INTO `act_hi_varinst` VALUES ('9e0a8345-6710-11ed-a797-7486e21fa90d', 3, '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, 'status', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '3', NULL, '2022-11-18 15:14:18.698', '2022-11-18 15:16:02.318');
INSERT INTO `act_hi_varinst` VALUES ('a0937a05-6af3-11ed-b7e1-7486e21fa90d', 0, '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, 'admin', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '超级管理员', NULL, '2022-11-23 13:56:52.197', '2022-11-23 13:56:52.197');
INSERT INTO `act_hi_varinst` VALUES ('a093a116-6af3-11ed-b7e1-7486e21fa90d', 0, '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', '7c8e9711-6af3-11ed-b7e1-7486e21fa90d', NULL, 'command', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'agree', NULL, '2022-11-23 13:56:52.198', '2022-11-23 13:56:52.198');
INSERT INTO `act_hi_varinst` VALUES ('b48388bd-6710-11ed-a797-7486e21fa90d', 0, '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, 'teacher', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '刘旭', NULL, '2022-11-18 15:14:56.401', '2022-11-18 15:14:56.401');
INSERT INTO `act_hi_varinst` VALUES ('b48388be-6710-11ed-a797-7486e21fa90d', 2, '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, 'refusedReason', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '不能请假', NULL, '2022-11-18 15:14:56.401', '2022-11-18 15:16:02.316');
INSERT INTO `act_hi_varinst` VALUES ('cd7ca964-6710-11ed-a797-7486e21fa90d', 0, '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, 'admin', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '超级管理员', NULL, '2022-11-18 15:15:38.299', '2022-11-18 15:15:38.299');
INSERT INTO `act_hi_varinst` VALUES ('cd7ca965-6710-11ed-a797-7486e21fa90d', 1, '9e094ac0-6710-11ed-a797-7486e21fa90d', '9e094ac0-6710-11ed-a797-7486e21fa90d', NULL, 'command', 'string', NULL, NULL, NULL, NULL, NULL, NULL, 'refuse', NULL, '2022-11-18 15:15:38.299', '2022-11-18 15:16:02.317');
INSERT INTO `act_hi_varinst` VALUES ('ef0381b6-6712-11ed-a5a9-7486e21fa90d', 0, 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, 'leaveDays', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '30', NULL, '2022-11-18 15:30:53.546', '2022-11-18 15:30:53.546');
INSERT INTO `act_hi_varinst` VALUES ('ef044507-6712-11ed-a5a9-7486e21fa90d', 0, 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, 'student', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '测试用户', NULL, '2022-11-18 15:30:53.546', '2022-11-18 15:30:53.546');
INSERT INTO `act_hi_varinst` VALUES ('ef044508-6712-11ed-a5a9-7486e21fa90d', 0, 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, 'leaveReason', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '我想请假一个月', NULL, '2022-11-18 15:30:53.546', '2022-11-18 15:30:53.546');
INSERT INTO `act_hi_varinst` VALUES ('ef044509-6712-11ed-a5a9-7486e21fa90d', 0, 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, 'leavePerson', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '测试用户', NULL, '2022-11-18 15:30:53.546', '2022-11-18 15:30:53.546');
INSERT INTO `act_hi_varinst` VALUES ('ef04450a-6712-11ed-a5a9-7486e21fa90d', 4, 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', 'ef035aa5-6712-11ed-a5a9-7486e21fa90d', NULL, 'status', 'string', NULL, NULL, NULL, NULL, NULL, NULL, '2', NULL, '2022-11-18 15:30:53.546', '2022-11-23 13:48:13.544');

-- ----------------------------
-- Table structure for act_id_bytearray
-- ----------------------------
DROP TABLE IF EXISTS `act_id_bytearray`;
CREATE TABLE `act_id_bytearray`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_bytearray
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_group
-- ----------------------------
DROP TABLE IF EXISTS `act_id_group`;
CREATE TABLE `act_id_group`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_group
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_info
-- ----------------------------
DROP TABLE IF EXISTS `act_id_info`;
CREATE TABLE `act_id_info`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PASSWORD_` longblob NULL,
  `PARENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_info
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_membership
-- ----------------------------
DROP TABLE IF EXISTS `act_id_membership`;
CREATE TABLE `act_id_membership`  (
  `USER_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`USER_ID_`, `GROUP_ID_`) USING BTREE,
  INDEX `ACT_FK_MEMB_GROUP`(`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `act_id_group` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `act_id_user` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_membership
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_priv
-- ----------------------------
DROP TABLE IF EXISTS `act_id_priv`;
CREATE TABLE `act_id_priv`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_PRIV_NAME`(`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_priv
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_priv_mapping
-- ----------------------------
DROP TABLE IF EXISTS `act_id_priv_mapping`;
CREATE TABLE `act_id_priv_mapping`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PRIV_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_PRIV_MAPPING`(`PRIV_ID_`) USING BTREE,
  INDEX `ACT_IDX_PRIV_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_PRIV_GROUP`(`GROUP_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_PRIV_MAPPING` FOREIGN KEY (`PRIV_ID_`) REFERENCES `act_id_priv` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_priv_mapping
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_property
-- ----------------------------
DROP TABLE IF EXISTS `act_id_property`;
CREATE TABLE `act_id_property`  (
  `NAME_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`NAME_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_property
-- ----------------------------
INSERT INTO `act_id_property` VALUES ('schema.version', '6.7.2.0', 1);

-- ----------------------------
-- Table structure for act_id_token
-- ----------------------------
DROP TABLE IF EXISTS `act_id_token`;
CREATE TABLE `act_id_token`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TOKEN_VALUE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TOKEN_DATE_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `IP_ADDRESS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_AGENT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TOKEN_DATA_` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_token
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_user
-- ----------------------------
DROP TABLE IF EXISTS `act_id_user`;
CREATE TABLE `act_id_user`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `FIRST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LAST_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DISPLAY_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EMAIL_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PWD_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PICTURE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_id_user
-- ----------------------------

-- ----------------------------
-- Table structure for act_procdef_info
-- ----------------------------
DROP TABLE IF EXISTS `act_procdef_info`;
CREATE TABLE `act_procdef_info`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `INFO_JSON_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_INFO_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_IDX_INFO_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_FK_INFO_JSON_BA`(`INFO_JSON_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_INFO_JSON_BA` FOREIGN KEY (`INFO_JSON_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_INFO_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_procdef_info
-- ----------------------------

-- ----------------------------
-- Table structure for act_re_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_re_deployment`;
CREATE TABLE `act_re_deployment`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ENGINE_VERSION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_re_deployment
-- ----------------------------
INSERT INTO `act_re_deployment` VALUES ('5fd25719-6af7-11ed-827c-000c29f10ec6', 'SpringBootAutoDeployment', NULL, NULL, '', '2022-11-23 14:23:41.532', NULL, NULL, '5fd25719-6af7-11ed-827c-000c29f10ec6', NULL);
INSERT INTO `act_re_deployment` VALUES ('debaab6e-670b-11ed-b9c8-7486e21fa90d', 'SpringBootAutoDeployment', NULL, NULL, '', '2022-11-18 14:40:19.739', NULL, NULL, 'debaab6e-670b-11ed-b9c8-7486e21fa90d', NULL);

-- ----------------------------
-- Table structure for act_re_model
-- ----------------------------
DROP TABLE IF EXISTS `act_re_model`;
CREATE TABLE `act_re_model`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LAST_UPDATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `VERSION_` int(11) NULL DEFAULT NULL,
  `META_INFO_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_SOURCE`(`EDITOR_SOURCE_VALUE_ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_SOURCE_EXTRA`(`EDITOR_SOURCE_EXTRA_VALUE_ID_`) USING BTREE,
  INDEX `ACT_FK_MODEL_DEPLOYMENT`(`DEPLOYMENT_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_MODEL_DEPLOYMENT` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MODEL_SOURCE` FOREIGN KEY (`EDITOR_SOURCE_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_MODEL_SOURCE_EXTRA` FOREIGN KEY (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_re_model
-- ----------------------------

-- ----------------------------
-- Table structure for act_re_procdef
-- ----------------------------
DROP TABLE IF EXISTS `act_re_procdef`;
CREATE TABLE `act_re_procdef`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `VERSION_` int(11) NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint(4) NULL DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint(4) NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `ENGINE_VERSION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_FROM_ROOT_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DERIVED_VERSION_` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_UNIQ_PROCDEF`(`KEY_`, `VERSION_`, `DERIVED_VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_re_procdef
-- ----------------------------
INSERT INTO `act_re_procdef` VALUES ('leaveApproval:1:df13a0e1-670b-11ed-b9c8-7486e21fa90d', 1, 'http://www.flowable.org/processdef', '请假审批', 'leaveApproval', 1, 'debaab6e-670b-11ed-b9c8-7486e21fa90d', 'D:\\Program Files\\idea\\noah-server-master\\server-provider\\target\\classes\\processes\\leaveApproval.bpmn20.xml', 'D:\\Program Files\\idea\\noah-server-master\\server-provider\\target\\classes\\processes\\leaveApproval.leaveApproval.png', NULL, 0, 1, 1, '', NULL, NULL, NULL, 0);
INSERT INTO `act_re_procdef` VALUES ('leaveApproval:2:600f390c-6af7-11ed-827c-000c29f10ec6', 1, 'http://www.flowable.org/processdef', '请假审批', 'leaveApproval', 2, '5fd25719-6af7-11ed-827c-000c29f10ec6', '/usr/local/project/provider/resources/processes/leaveApproval.bpmn20.xml', '/usr/local/project/provider/resources/processes/leaveApproval.leaveApproval.png', NULL, 0, 1, 1, '', NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for act_ru_actinst
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_actinst`;
CREATE TABLE `act_ru_actinst`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT 1,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) NULL DEFAULT NULL,
  `DURATION_` bigint(20) NULL DEFAULT NULL,
  `TRANSACTION_ORDER_` int(11) NULL DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_START`(`START_TIME_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_END`(`END_TIME_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_PROC`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_PROC_ACT`(`PROC_INST_ID_`, `ACT_ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_EXEC`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_EXEC_ACT`(`EXECUTION_ID_`, `ACT_ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_ACTI_TASK`(`TASK_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_actinst
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_deadletter_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_deadletter_job`;
CREATE TABLE `act_ru_deadletter_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_DEADLETTER_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_DEADLETTER_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_DEADLETTER_JOB_CORRELATION_ID`(`CORRELATION_ID_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_DJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_DEADLETTER_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_deadletter_job
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_entitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_entitylink`;
CREATE TABLE `act_ru_entitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `LINK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REF_SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HIERARCHY_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_ENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_ENT_LNK_REF_SCOPE`(`REF_SCOPE_ID_`, `REF_SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_ENT_LNK_ROOT_SCOPE`(`ROOT_SCOPE_ID_`, `ROOT_SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_ENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`, `LINK_TYPE_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_entitylink
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_event_subscr
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_event_subscr`;
CREATE TABLE `act_ru_event_subscr`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `EVENT_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EVENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACTIVITY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CONFIGURATION_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATED_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EVENT_SUBSCR_CONFIG_`(`CONFIGURATION_`) USING BTREE,
  INDEX `ACT_FK_EVENT_EXEC`(`EXECUTION_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EVENT_EXEC` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_event_subscr
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_execution
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_execution`;
CREATE TABLE `act_ru_execution`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ROOT_PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `IS_ACTIVE_` tinyint(4) NULL DEFAULT NULL,
  `IS_CONCURRENT_` tinyint(4) NULL DEFAULT NULL,
  `IS_SCOPE_` tinyint(4) NULL DEFAULT NULL,
  `IS_EVENT_SCOPE_` tinyint(4) NULL DEFAULT NULL,
  `IS_MI_ROOT_` tinyint(4) NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `CACHED_ENT_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_ACT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `START_TIME_` datetime(3) NULL DEFAULT NULL,
  `START_USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint(4) NULL DEFAULT NULL,
  `EVT_SUBSCR_COUNT_` int(11) NULL DEFAULT NULL,
  `TASK_COUNT_` int(11) NULL DEFAULT NULL,
  `JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `TIMER_JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `SUSP_JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `DEADLETTER_JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `EXTERNAL_WORKER_JOB_COUNT_` int(11) NULL DEFAULT NULL,
  `VAR_COUNT_` int(11) NULL DEFAULT NULL,
  `ID_LINK_COUNT_` int(11) NULL DEFAULT NULL,
  `CALLBACK_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CALLBACK_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `REFERENCE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BUSINESS_STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EXEC_BUSKEY`(`BUSINESS_KEY_`) USING BTREE,
  INDEX `ACT_IDC_EXEC_ROOT`(`ROOT_PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_IDX_EXEC_REF_ID_`(`REFERENCE_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_PARENT`(`PARENT_ID_`) USING BTREE,
  INDEX `ACT_FK_EXE_SUPER`(`SUPER_EXEC_`) USING BTREE,
  INDEX `ACT_FK_EXE_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_execution
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_external_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_external_job`;
CREATE TABLE `act_ru_external_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_EXTERNAL_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_EXTERNAL_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_EXTERNAL_JOB_CORRELATION_ID`(`CORRELATION_ID_`) USING BTREE,
  INDEX `ACT_IDX_EJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_EJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_EJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  CONSTRAINT `ACT_FK_EXTERNAL_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_EXTERNAL_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_external_job
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_history_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_history_job`;
CREATE TABLE `act_ru_history_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ADV_HANDLER_CFG_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_history_job
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_identitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_identitylink`;
CREATE TABLE `act_ru_identitylink`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `GROUP_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `USER_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_USER`(`USER_ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_GROUP`(`GROUP_ID_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_IDENT_LNK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_ATHRZ_PROCEDEF`(`PROC_DEF_ID_`) USING BTREE,
  INDEX `ACT_FK_TSKASS_TASK`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_FK_IDL_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_ATHRZ_PROCEDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_IDL_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `act_ru_task` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_identitylink
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_job`;
CREATE TABLE `act_ru_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_CORRELATION_ID`(`CORRELATION_ID_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_JOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_job
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_suspended_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_suspended_job`;
CREATE TABLE `act_ru_suspended_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_SUSPENDED_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_SUSPENDED_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_SUSPENDED_JOB_CORRELATION_ID`(`CORRELATION_ID_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_SJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_SUSPENDED_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_suspended_job
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_task
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_task`;
CREATE TABLE `act_ru_task`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROPAGATED_STAGE_INST_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ASSIGNEE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DELEGATION_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PRIORITY_` int(11) NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  `FORM_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CLAIM_TIME_` datetime(3) NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint(4) NULL DEFAULT NULL,
  `VAR_COUNT_` int(11) NULL DEFAULT NULL,
  `ID_LINK_COUNT_` int(11) NULL DEFAULT NULL,
  `SUB_TASK_COUNT_` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_TASK_CREATE`(`CREATE_TIME_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TASK_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_TASK_EXE`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_TASK_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  INDEX `ACT_FK_TASK_PROCDEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_task
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_timer_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_timer_job`;
CREATE TABLE `act_ru_timer_job`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) NULL DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `ELEMENT_NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_DEFINITION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CORRELATION_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RETRIES_` int(11) NULL DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CUSTOM_VALUES_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_EXCEPTION_STACK_ID`(`EXCEPTION_STACK_ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_CUSTOM_VALUES_ID`(`CUSTOM_VALUES_ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_CORRELATION_ID`(`CORRELATION_ID_`) USING BTREE,
  INDEX `ACT_IDX_TIMER_JOB_DUEDATE`(`DUEDATE_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SCOPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SUB_SCOPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_TJOB_SCOPE_DEF`(`SCOPE_DEFINITION_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_EXECUTION`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_PROCESS_INSTANCE`(`PROCESS_INSTANCE_ID_`) USING BTREE,
  INDEX `ACT_FK_TIMER_JOB_PROC_DEF`(`PROC_DEF_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_TIMER_JOB_CUSTOM_VALUES` FOREIGN KEY (`CUSTOM_VALUES_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_TIMER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_timer_job
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_variable
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_variable`;
CREATE TABLE `act_ru_variable`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TASK_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `DOUBLE_` double NULL DEFAULT NULL,
  `LONG_` bigint(20) NULL DEFAULT NULL,
  `TEXT_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TEXT2_` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `ACT_IDX_RU_VAR_SCOPE_ID_TYPE`(`SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_IDX_RU_VAR_SUB_ID_TYPE`(`SUB_SCOPE_ID_`, `SCOPE_TYPE_`) USING BTREE,
  INDEX `ACT_FK_VAR_BYTEARRAY`(`BYTEARRAY_ID_`) USING BTREE,
  INDEX `ACT_IDX_VARIABLE_TASK_ID`(`TASK_ID_`) USING BTREE,
  INDEX `ACT_FK_VAR_EXE`(`EXECUTION_ID_`) USING BTREE,
  INDEX `ACT_FK_VAR_PROCINST`(`PROC_INST_ID_`) USING BTREE,
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `act_ge_bytearray` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of act_ru_variable
-- ----------------------------

-- ----------------------------
-- Table structure for flw_channel_definition
-- ----------------------------
DROP TABLE IF EXISTS `flw_channel_definition`;
CREATE TABLE `flw_channel_definition`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `VERSION_` int(11) NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TYPE_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `IMPLEMENTATION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_CHANNEL_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_channel_definition
-- ----------------------------

-- ----------------------------
-- Table structure for flw_ev_databasechangelog
-- ----------------------------
DROP TABLE IF EXISTS `flw_ev_databasechangelog`;
CREATE TABLE `flw_ev_databasechangelog`  (
  `ID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `AUTHOR` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `FILENAME` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `DATEEXECUTED` datetime(0) NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `MD5SUM` varchar(35) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `COMMENTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TAG` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LIQUIBASE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CONTEXTS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `LABELS` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_ev_databasechangelog
-- ----------------------------
INSERT INTO `flw_ev_databasechangelog` VALUES ('1', 'flowable', 'org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml', '2022-11-18 14:40:15', 1, 'EXECUTED', '8:1b0c48c9cf7945be799d868a2626d687', 'createTable tableName=FLW_EVENT_DEPLOYMENT; createTable tableName=FLW_EVENT_RESOURCE; createTable tableName=FLW_EVENT_DEFINITION; createIndex indexName=ACT_IDX_EVENT_DEF_UNIQ, tableName=FLW_EVENT_DEFINITION; createTable tableName=FLW_CHANNEL_DEFIN...', '', NULL, '4.5.0', NULL, NULL, '8753615134');
INSERT INTO `flw_ev_databasechangelog` VALUES ('2', 'flowable', 'org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml', '2022-11-18 14:40:15', 2, 'EXECUTED', '8:0ea825feb8e470558f0b5754352b9cda', 'addColumn tableName=FLW_CHANNEL_DEFINITION; addColumn tableName=FLW_CHANNEL_DEFINITION', '', NULL, '4.5.0', NULL, NULL, '8753615134');
INSERT INTO `flw_ev_databasechangelog` VALUES ('3', 'flowable', 'org/flowable/eventregistry/db/liquibase/flowable-eventregistry-db-changelog.xml', '2022-11-18 14:40:15', 3, 'EXECUTED', '8:3c2bb293350b5cbe6504331980c9dcee', 'customChange', '', NULL, '4.5.0', NULL, NULL, '8753615134');

-- ----------------------------
-- Table structure for flw_ev_databasechangeloglock
-- ----------------------------
DROP TABLE IF EXISTS `flw_ev_databasechangeloglock`;
CREATE TABLE `flw_ev_databasechangeloglock`  (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime(0) NULL DEFAULT NULL,
  `LOCKEDBY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_ev_databasechangeloglock
-- ----------------------------
INSERT INTO `flw_ev_databasechangeloglock` VALUES (1, b'0', NULL, NULL);

-- ----------------------------
-- Table structure for flw_event_definition
-- ----------------------------
DROP TABLE IF EXISTS `flw_event_definition`;
CREATE TABLE `flw_event_definition`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `VERSION_` int(11) NULL DEFAULT NULL,
  `KEY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DESCRIPTION_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE,
  UNIQUE INDEX `ACT_IDX_EVENT_DEF_UNIQ`(`KEY_`, `VERSION_`, `TENANT_ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_event_definition
-- ----------------------------

-- ----------------------------
-- Table structure for flw_event_deployment
-- ----------------------------
DROP TABLE IF EXISTS `flw_event_deployment`;
CREATE TABLE `flw_event_deployment`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `CATEGORY_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOY_TIME_` datetime(3) NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `PARENT_DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_event_deployment
-- ----------------------------

-- ----------------------------
-- Table structure for flw_event_resource
-- ----------------------------
DROP TABLE IF EXISTS `flw_event_resource`;
CREATE TABLE `flw_event_resource`  (
  `ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `RESOURCE_BYTES_` longblob NULL,
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_event_resource
-- ----------------------------

-- ----------------------------
-- Table structure for flw_ru_batch
-- ----------------------------
DROP TABLE IF EXISTS `flw_ru_batch`;
CREATE TABLE `flw_ru_batch`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SEARCH_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SEARCH_KEY2_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NOT NULL,
  `COMPLETE_TIME_` datetime(3) NULL DEFAULT NULL,
  `STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `BATCH_DOC_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_ru_batch
-- ----------------------------

-- ----------------------------
-- Table structure for flw_ru_batch_part
-- ----------------------------
DROP TABLE IF EXISTS `flw_ru_batch_part`;
CREATE TABLE `flw_ru_batch_part`  (
  `ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `REV_` int(11) NULL DEFAULT NULL,
  `BATCH_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SUB_SCOPE_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SCOPE_TYPE_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SEARCH_KEY_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `SEARCH_KEY2_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `CREATE_TIME_` datetime(3) NOT NULL,
  `COMPLETE_TIME_` datetime(3) NULL DEFAULT NULL,
  `STATUS_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `RESULT_DOC_ID_` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `TENANT_ID_` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '',
  PRIMARY KEY (`ID_`) USING BTREE,
  INDEX `FLW_IDX_BATCH_PART`(`BATCH_ID_`) USING BTREE,
  CONSTRAINT `FLW_FK_BATCH_PART_PARENT` FOREIGN KEY (`BATCH_ID_`) REFERENCES `flw_ru_batch` (`ID_`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of flw_ru_batch_part
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
