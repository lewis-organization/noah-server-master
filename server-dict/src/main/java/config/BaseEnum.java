package config;

public interface BaseEnum {

    Integer getCode();

    String getMsg();
}
