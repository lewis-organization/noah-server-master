package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum TaskStateEnum {

    NORMAL("NORMAL", "正常"),
    PAUSED("PAUSED", "暂停");

    private String state;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(TaskStateEnum.values())
                .map(e -> new Kv(e.getState(), e.getDes())).collect(Collectors.toList());
    }

    public static String getValueByKey(String key) {
        return Arrays.stream(TaskStateEnum.values()).filter(r -> r.getState().equals(key)).findFirst().get().getDes();
    }
}
