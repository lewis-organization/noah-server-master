package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum TrueOrFalseEnum {

    TRUE("1", "是"),
    FALSE("0", "否");

    private String value;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(TrueOrFalseEnum.values())
                .map(e -> new Kv(e.getValue(), e.getDes())).collect(Collectors.toList());
    }

    public static String getValueByKey(String key) {
        return Arrays.stream(TrueOrFalseEnum.values()).filter(r -> r.getValue().equals(key)).findFirst().get().getDes();
    }
}
