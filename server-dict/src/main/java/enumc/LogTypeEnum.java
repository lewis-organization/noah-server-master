package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum LogTypeEnum {

    LOGIN("1", "登录日志"),
    COMMON("2", "操作日志");

    private String type;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(LogTypeEnum.values())
                .map(e -> new Kv(e.getType(), e.getDes())).collect(Collectors.toList());
    }

    public static String getValueByKey(String key) {
        return Arrays.stream(LogTypeEnum.values()).filter(r -> r.getType().equals(key)).findFirst().get().getDes();
    }
}
