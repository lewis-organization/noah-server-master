package enumc;

import config.BaseEnum;
import lombok.Getter;

@Getter
public enum AjaxReturnCodeEnum implements BaseEnum {

    AJAX_SUCCESS(200, "请求成功"),
    AJAX_FAIL(500, "系统发生错误");

    private Integer code;

    private String msg;

    AjaxReturnCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
