package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum ApproveStatusEnum {

    NO_SUBMIT("1", "未提交"),
    WAIT("2", "待审核"),
    AGREE("3", "审核通过"),
    REJECT("4", "审核拒绝");

    private String state;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(ApproveStatusEnum.values())
                .map(e -> new Kv(e.getState(), e.getDes())).collect(Collectors.toList());
    }

    public static String getValueByKey(String key) {
        return Arrays.stream(ApproveStatusEnum.values()).filter(r -> r.getState().equals(key)).findFirst().get().getDes();
    }
}
