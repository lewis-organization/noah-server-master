package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum ApproveBusinessTypeEnum {

    TEST("1", "测试业务审核"),
    OTHER("2", "其他业务审核");

    private String type;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(ApproveBusinessTypeEnum.values())
                .map(e -> new Kv(e.getType(), e.getDes())).collect(Collectors.toList());
    }

    public static String getValueByKey(String key) {
        return Arrays.stream(ApproveBusinessTypeEnum.values()).filter(r -> r.getType().equals(key)).findFirst().get().getDes();
    }

    public static ApproveBusinessTypeEnum getApproveBusinessTypeEnum(String key) {
        return Arrays.stream(ApproveBusinessTypeEnum.values()).filter(r -> r.getType().equals(key)).findFirst().get();
    }
}
