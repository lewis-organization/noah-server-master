package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum RoleTypeEnum {

    SYSTEM("1", "系统角色"),
    COMMON("2", "普通角色");

    private String type;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(RoleTypeEnum.values())
                .map(e -> new Kv(e.getType(), e.getDes())).collect(Collectors.toList());
    }

    public static String getValueByKey(String key) {
        return Arrays.stream(RoleTypeEnum.values()).filter(r -> r.getType().equals(key)).findFirst().get().getDes();
    }
}
