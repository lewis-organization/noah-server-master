package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum ResourcesTypeEnum {

    CATALOGUE("0", "目录"),
    MENU("1", "菜单"),
    BUTTON("2", "按钮");

    private String type;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(ResourcesTypeEnum.values())
                .map(e -> new Kv(e.getType(), e.getDes())).collect(Collectors.toList());
    }
}
