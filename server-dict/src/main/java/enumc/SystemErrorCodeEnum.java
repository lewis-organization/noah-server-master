package enumc;

import config.BaseEnum;
import lombok.Getter;

@Getter
public enum SystemErrorCodeEnum implements BaseEnum {

    TOKEN_ERROR_CODE(501, "Token验证失败"),
    NO_TOKEN_CODE(502, "请求Token不存在"),
    KICKOUT_CODE(503, "您已经在其他地方登录, 请重新登陆"),
    UNAUTHORIZED(401, "无权访问"),
    OFEN(201, "操作太频繁了"),
    NOTFOUNT(404, "找不到请求");

    private Integer code;

    private String msg;

    SystemErrorCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
