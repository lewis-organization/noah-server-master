package enumc;

import lombok.Getter;

@Getter
public enum RedisCacheKeyEnum {

    /**
     * 权限缓存对象前缀
     **/
    WEB_PERM_CACHE("web-perm-cache:", "权限缓存对象前缀"),

    APP_PERM_CACHE("app-perm-cache:", "权限缓存对象前缀"),
    /**
     * web-session缓存对象前缀
     **/
    WEB_SESSION_CACHE("web-session:", "web-session缓存对象前缀"),
    /**
     * app-session缓存对象前缀
     */
    APP_SESSION_CACHE("app-session:", "app-session缓存对象前缀"),
    /**
     * web互斥登录缓存
     **/
    WEB_KICKOUT_CACHE("web-kickout:", "web互斥登录缓存"),
    /**
     * app互斥登录缓存
     */
    APP_KICKOUT_CACHE("app-kickout:", "app互斥登录缓存"),
    /**
     * 重复提交限制缓存
     **/
    SUBMIT_CACHE("submit-session:", "重复提交限制缓存"),
    /**
     * 菜单列表缓存
     **/
    RESOURCES_CACHE("resources-cache:", "菜单列表缓存"),
    /**
     * 用户菜单缓存
     **/
    USER_MENU_CACHE("user-menu-cache:", "用户菜单缓存"),
    /**
     * 用户权限缓存
     */
    USER_RESOURCES_CACHE("user-resources-cache:", "用户菜单缓存"),
    /**
     * 请求白名单缓存
     */
    REFERER_WHITE_LIST("referer_white_list:", "请求白名单缓存"),
    /**
     * 系统配置缓存
     */
    SYSTEM_CONFIG("system_config:", "系统配置缓存"),
    /**
     * 组织机构缓存
     */
    ORG_CACHE("org_cache:", "组织机构缓存"),
    /**
     * 用户角色缓存
     */
    USER_ROLES_CACHE("user_roles_cache:", "用户角色缓存"),
    /**
     * 登录失败加锁缓存
     */
    LOGIN_FAIL_LOCKED("login_fail_locked:", "登录失败加锁缓存"),
    /**
     * 审核用户ID缓存
     */
    APPROVE_WEB_USER_ID("approve_web_user_id", "审核用户ID缓存");

    private String key;

    private String des;

    RedisCacheKeyEnum(String key, String des) {
        this.key = key;
        this.des = des;
    }
}
