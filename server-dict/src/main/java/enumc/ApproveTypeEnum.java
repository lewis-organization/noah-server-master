package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum ApproveTypeEnum {

    SYS("1", "系统类型"),
    COMMON("2", "常规类型");

    private String type;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(ApproveTypeEnum.values())
                .map(e -> new Kv(e.getType(), e.getDes())).collect(Collectors.toList());
    }

    public static String getValueByKey(String key) {
        return Arrays.stream(ApproveTypeEnum.values()).filter(r -> r.getType().equals(key)).findFirst().get().getDes();
    }
}
