package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum ProcessTaskStateEnum {

    NOT_SUBMIT("1", "未提交"),
    WAIT_APPROVE("2", "待审核"),
    AGREE("3", "审核通过"),
    REFUSED("4", "审核拒绝");

    private String state;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(ProcessTaskStateEnum.values())
                .map(e -> new Kv(e.getState(), e.getDes())).collect(Collectors.toList());
    }

    public static String getValueByKey(String key) {
        return Arrays.stream(ProcessTaskStateEnum.values()).filter(r -> r.getState().equals(key)).findFirst().get().getDes();
    }
}
