package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum UserRoleEnum {

    ADMIN("admin", "系统用户");

    private String type;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(UserRoleEnum.values())
                .map(e -> new Kv(e.getType(), e.getDes())).collect(Collectors.toList());
    }

    public static String getValueByKey(String key) {
        return Arrays.stream(UserRoleEnum.values()).filter(r -> r.getType().equals(key)).findFirst().get().getDes();
    }
}
