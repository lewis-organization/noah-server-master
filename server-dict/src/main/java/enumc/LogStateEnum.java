package enumc;

import config.Kv;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum LogStateEnum {

    SUCCESS("1", "请求成功"),
    FAIL("2", "请求失败");

    private String type;

    private String des;

    public static List<Kv> toKv() {
        return Arrays.stream(LogStateEnum.values())
                .map(e -> new Kv(e.getType(), e.getDes())).collect(Collectors.toList());
    }

    public static String getValueByKey(String key) {
        return Arrays.stream(LogStateEnum.values()).filter(r -> r.getType().equals(key)).findFirst().get().getDes();
    }
}
