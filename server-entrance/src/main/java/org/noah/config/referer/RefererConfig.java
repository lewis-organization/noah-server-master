package org.noah.config.referer;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author liuxu
 * @date 2022/4/22 14:26
 **/
@Component
@Data
@ConfigurationProperties(prefix = "referer")
public class RefererConfig {

    private boolean enable;

    //白名单域名
    List<String> refererDomain = new ArrayList<>();

    //忽略的域名
    List<String> excludePathPatterns = new ArrayList<>();
}
