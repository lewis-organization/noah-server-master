package org.noah.config.upload;

import cn.hutool.core.io.FileUtil;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.unit.DataSize;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.MultipartConfigElement;
import java.util.regex.Pattern;

@Data
@Configuration
public class MultipartConfig {

    @Value("${spring.servlet.multipart.location}")
    private String location;
    @Value("${spring.servlet.multipart.max-file-size}")
    private Long maxFileSize;
    @Value("${spring.servlet.multipart.max-request-size}")
    private Long maxRequestSize;
    private String type;
    private String path;
    private Long size;
    private String compressTypes;

    /**
     * 文件上传配置
     * @author Liuxu
     * @date 2024/1/23 11:53
     * @return javax.servlet.MultipartConfigElement
     */
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(DataSize.ofMegabytes(maxFileSize));
        factory.setMaxRequestSize(DataSize.ofMegabytes(maxRequestSize));
        if (StringUtils.isNotBlank(location)) {
            FileUtil.mkdir(location);
        } else {
            throw new RuntimeException("文件上传临时目录未配置");
        }
        factory.setLocation(location);
        return factory.createMultipartConfig();
    }

    public boolean validateSuffix(MultipartFile file) {
        return Pattern.matches(type, FileUtil.getSuffix(file.getOriginalFilename()).toLowerCase());
    }

    public boolean validateSize(MultipartFile file) {
        return file.getSize() / 1024 > size;
    }

    public boolean compressSuffix(MultipartFile file) {
        return Pattern.matches(compressTypes, FileUtil.getSuffix(file.getOriginalFilename()).toLowerCase());
    }

    public boolean compressSuffix(String fileName) {
        return Pattern.matches(compressTypes, FileUtil.getSuffix(fileName).toLowerCase());
    }
}
