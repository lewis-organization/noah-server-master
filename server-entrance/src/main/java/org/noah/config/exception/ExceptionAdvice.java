package org.noah.config.exception;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.noah.config.json.AjaxResult;
import org.noah.utils.RequestUtil;
import org.springframework.boot.autoconfigure.klock.handler.KlockTimeoutException;
import org.springframework.boot.context.properties.bind.BindException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

@ControllerAdvice
@ResponseBody
@Slf4j
public class ExceptionAdvice {

    /**
     * 400 - Bad Request
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler({HttpMessageNotReadableException.class,
            MissingServletRequestParameterException.class,
            BindException.class,
            org.springframework.validation.BindException.class,
            ServletRequestBindingException.class,
            MethodArgumentNotValidException.class})
    public AjaxResult handleHttpMessageNotReadableException(Exception e) {
        log.error("参数错误", e);
        if (e instanceof org.springframework.validation.BindException) {
            //参数验证错误
            org.springframework.validation.BindException bindException = (org.springframework.validation.BindException) e;
            String errorMsg = bindException.getBindingResult().getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.joining(","));
            return AjaxResult.fail(400, errorMsg);
        }
        return AjaxResult.fail(400, "参数错误");
    }

    /**
     * 401 没有权限
     * @author Liuxu
     * @date 2024/1/23 11:49
     * @param e e
     * @param request request
     * @return java.lang.Object
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(AuthorizationException.class)
    public Object authorizationException(AuthorizationException e, HttpServletRequest request) {
        log.error("没有权限", e);
        if (RequestUtil.isAjaxRequest(request)) {
            return AjaxResult.fail(401, "没有权限");
        } else {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("error/403");
            return modelAndView;
        }
    }

    /**
     * 404
     * @author Liuxu
     * @date 2024/1/23 11:49
     * @param e e
     * @param request request
     * @return java.lang.Object
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(NoHandlerFoundException.class)
    public Object pageNotFoundException(NoHandlerFoundException e, HttpServletRequest request) {
        log.error("请求地址[{}]不存在, 请检查url", e.getRequestURL());
        if (RequestUtil.isAjaxRequest(request)) {
            return AjaxResult.fail(404, "请求地址[" + e.getRequestURL() + "]不存在, 请检查url");
        } else {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("error/404");
            return modelAndView;
        }
    }

    /**
     * 405 - Method Not Allowed
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public AjaxResult handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        log.error("请求方法不允许", e);
        return AjaxResult.fail(405, "不支持[" + e.getMethod() + "]当前请求方法!");
    }

    /**
     * 500
     * @author Liuxu
     * @date 2024/1/23 11:50
     * @param e e
     * @param request request
     * @return java.lang.Object
     */
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(Exception.class)
    public Object handleException(Exception e, HttpServletRequest request) {
        log.error("系统错误", e);
        if (RequestUtil.isAjaxRequest(request)) {
            if (e instanceof BusinessException) {
                BusinessException businessException = (BusinessException) e;
                return AjaxResult.fail(businessException.getMessage());
            } else if (e instanceof AuthenticationException) {
                //参数验证错误
                AuthenticationException authenticationException = (AuthenticationException) e;
                String errorMsg = authenticationException.getCause().getMessage();
                return AjaxResult.fail(errorMsg);
            } else if (e instanceof KlockTimeoutException) {
                return AjaxResult.fail("系统繁忙, 请稍后再试");
            }
            return AjaxResult.fail();
        } else {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("error/500");
            return modelAndView;
        }
    }
}
