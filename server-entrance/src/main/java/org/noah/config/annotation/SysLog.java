package org.noah.config.annotation;

import enumc.LogSourceEnum;
import enumc.LogTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 系统日志注解
 *
 * @author liuxu
 */
@Target(ElementType.METHOD) // 作用到方法上
@Retention(RetentionPolicy.RUNTIME) // 运行时有效
public @interface SysLog {

    LogTypeEnum type();

    LogSourceEnum source() default LogSourceEnum.WEB;
}
