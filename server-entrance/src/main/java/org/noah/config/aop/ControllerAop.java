package org.noah.config.aop;

import cn.hutool.core.util.ArrayUtil;
import com.alibaba.fastjson2.JSON;
import enumc.AjaxReturnCodeEnum;
import enumc.LogSourceEnum;
import enumc.LogStateEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.noah.config.annotation.SysLog;
import org.noah.config.exception.BusinessException;
import org.noah.config.json.AjaxResult;
import org.noah.modules.sys.log.entity.Log;
import org.noah.modules.sys.log.service.LogService;
import org.noah.modules.sys.user.entity.WebUser;
import org.noah.utils.RequestUtil;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.StringJoiner;

@Order(value = 2)
@Slf4j
@Aspect
@Component
public class ControllerAop {

    @DubboReference
    private LogService logService;
    private final HttpServletRequest request;

    public ControllerAop(HttpServletRequest request) {
        this.request = request;
    }

    /**
     * 指定注解
     */
    @Pointcut(value = "@annotation(sysLog)")
    public void requestResponseLog(SysLog sysLog) {

    }

    /**
     * 前置通知
     * 方法执行前调用
     *
     * @param joinPoint joinPoint
     */
    @Before(value = "requestResponseLog(sysLog)", argNames = "joinPoint,sysLog")
    public void doBefore(JoinPoint joinPoint, SysLog sysLog) {
        //获取方法目标的参数信息
        Object[] objs = joinPoint.getArgs();
        Signature signature = joinPoint.getSignature();
        //代理得哪一个方法
        String methodName = signature.getName();

        //AOP代理类名称
        String className = signature.getDeclaringTypeName();
        MethodSignature methodSignature = (MethodSignature) signature;
        String[] strings = methodSignature.getParameterNames();

        //接收到请求，记录请求内容
        //记录日志
        Log sLog = new Log();
        sLog.setSource(sysLog.source().getType());
        Map<String, String> clientInfo = RequestUtil.getClientInfo(request);
        sLog.setBrowser(clientInfo.get("browserName"));
        sLog.setIp(RequestUtil.getIpAddress(request));
        sLog.setMethod(request.getMethod().toUpperCase());
        sLog.setMethodName(methodName);
        sLog.setType(sysLog.type().getType());
        sLog.setModule(sysLog.type().getDes());
        sLog.setOs(clientInfo.get("osName") + " " + clientInfo.get("osVersion"));
        sLog.setParamName(ArrayUtil.toString(strings));
        sLog.setParamValue(ArrayUtil.toString(objs));
        sLog.setUri(request.getRequestURI());
        sLog.setState(LogStateEnum.SUCCESS.getType());
        request.setAttribute("sLog", sLog);
        //文件记录
        StringJoiner stringJoiner = new StringJoiner("\n");
        stringJoiner.add("请求信息:");
        stringJoiner.add("os = " + clientInfo.get("osName") + " " + clientInfo.get("osVersion"));
        stringJoiner.add("browser = " + clientInfo.get("browserName") + " " + clientInfo.get("browserVersion"));
        stringJoiner.add("uri = " + request.getRequestURI());
        stringJoiner.add("paramName = " + ArrayUtil.toString(strings));
        stringJoiner.add("paramValue = " + ArrayUtil.toString(objs));
        stringJoiner.add("clientIp = " + RequestUtil.getIpAddress(request));
        stringJoiner.add("method = " + request.getMethod());
        stringJoiner.add("methodName = " + methodName);
        stringJoiner.add("methodClassName = " + className);
        log.info(stringJoiner.toString());
        request.setAttribute("STARTTIME2", System.currentTimeMillis());
    }

    /**
     * 处理完请求返回内容处理
     *
     * @param ret ret
     */
    @AfterReturning(returning = "ret", pointcut = "requestResponseLog(sysLog)", argNames = "ret,sysLog")
    public void doAfterReturning(Object ret, SysLog sysLog) {
        StringJoiner stringJoiner = new StringJoiner(StringUtils.EMPTY);
        stringJoiner.add("请求响应:");
        stringJoiner.add(JSON.toJSONString(ret));
        log.info(stringJoiner.toString());
        //系统日志对象
        Log sLog = (Log) request.getAttribute("sLog");
        AjaxResult ajaxResult = JSON.parseObject(JSON.toJSONString(ret), AjaxResult.class);
        if (AjaxReturnCodeEnum.AJAX_FAIL.getCode().equals(ajaxResult.getCode())) {
            sLog.setState(LogStateEnum.FAIL.getType());
            sLog.setMessage(ajaxResult.getMsg());
        }
        saveSLog(sLog);
    }

    /**
     * 抛出异常处理
     *
     * @param e e
     * @author liuxu
     * @date 2022/4/20 15:07
     **/
    @AfterThrowing(pointcut = "requestResponseLog(sysLog)", throwing = "e", argNames = "sysLog,e")
    public void exceptionHandler(SysLog sysLog, BusinessException e) {
        //系统日志对象
        Log sLog = (Log) request.getAttribute("sLog");
        sLog.setState(LogStateEnum.FAIL.getType());
        sLog.setMessage(e.getMessage());
        saveSLog(sLog);
    }

    /**
     * 后置最终通知，抛出异常或者正常退出都会执行
     */
    @After(value = "requestResponseLog(sysLog)", argNames = "sysLog")
    public void after(SysLog sysLog) {
        //当前时间
        long currentTime = System.currentTimeMillis();
        long requestTime = Long.parseLong(request.getAttribute("STARTTIME2").toString());
        log.info("方法请求耗时: {}ms", currentTime - requestTime);
    }

    private void saveSLog(Log sLog) {
        if (LogSourceEnum.WEB.getType().equals(sLog.getSource())) {
            //操作用户
            Subject subject = SecurityUtils.getSubject();
            WebUser webUser = (WebUser) subject.getPrincipal();
            if (null != webUser) {
                sLog.setCreater(webUser.getUsername());
            }
        } else if (LogSourceEnum.APP.getType().equals(sLog.getSource())) {

        }
        try {
            logService.save(sLog);
        } catch (Exception ex) {
            //ignore exception
        }
    }
}
