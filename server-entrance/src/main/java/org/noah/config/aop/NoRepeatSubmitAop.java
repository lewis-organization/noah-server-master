package org.noah.config.aop;

import enumc.AjaxReturnCodeEnum;
import enumc.RedisCacheKeyEnum;
import enumc.SystemErrorCodeEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.noah.config.annotation.NoRepeatSubmit;
import org.noah.config.json.AjaxResult;
import org.noah.utils.RedisUtil;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.Objects;

@Order(value = 1)
@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class NoRepeatSubmitAop {

    private final RedisUtil redisUtil;

    /**
     * controller 方法环绕监听
     * @author Liuxu
     * @date 2024/1/23 11:48
     * @param pjp pjp
     * @param nrs 注解
     * @return java.lang.Object
     */
    @Around(value = "execution(* org.noah.modules.*.*.controller.*Controller.*(..)) && @annotation(nrs)")
    public Object arround(ProceedingJoinPoint pjp, NoRepeatSubmit nrs) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
            String sessionId = Objects.requireNonNull(RequestContextHolder.getRequestAttributes()).getSessionId();
            String key = RedisCacheKeyEnum.SUBMIT_CACHE.getKey() + sessionId;
            if (null == redisUtil.get(key)) {
                Object o = pjp.proceed();
                redisUtil.set(key, 0, 2);
                return o;
            } else {
                ajaxResult.setCode(SystemErrorCodeEnum.OFEN.getCode());
                ajaxResult.setMsg(SystemErrorCodeEnum.OFEN.getMsg());
                return ajaxResult;
            }
        } catch (Throwable e) {
            log.error(e.getMessage());
            ajaxResult.setCode(AjaxReturnCodeEnum.AJAX_FAIL.getCode());
            ajaxResult.setMsg(e.getMessage());
            return ajaxResult;
        }
    }
}
