package org.noah.config.web;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.CharsetUtil;
import com.alibaba.fastjson2.JSONReader;
import com.alibaba.fastjson2.JSONWriter;
import com.alibaba.fastjson2.filter.PropertyFilter;
import com.alibaba.fastjson2.support.config.FastJsonConfig;
import com.alibaba.fastjson2.support.spring.http.converter.FastJsonHttpMessageConverter;
import lombok.RequiredArgsConstructor;
import org.noah.config.interceptor.RefererInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 刘旭
 * @date 2018/11/29-9:48
 **/
@Configuration
@RequiredArgsConstructor
public class WebConfig implements WebMvcConfigurer {

    private final RefererInterceptor refererInterceptor;
    private String staticPath;

    @Value("${customConfig.staticPath}")
    public void setStaticPath(String staticPath) {
        this.staticPath = staticPath;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations(staticPath);
        registry.addResourceHandler("/document.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/assets/**").addResourceLocations("classpath:/META-INF/resources/assets/");
        registry.addResourceHandler("/logo.png").addResourceLocations("classpath:/META-INF/resources/");
    }

    /**
     * 配置拦截器拦截请求
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(refererInterceptor).addPathPatterns("/**")
                .excludePathPatterns(refererInterceptor.getRefererConfig().getExcludePathPatterns());
    }

    /**
     * 使用fastJson2转换器
     * @author Liuxu
     * @date 2024/5/9 15:07
     * @param converters 转换器
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(0, this.fastJsonHttpMessageConverter());
    }

    @Bean
    public FastJsonConfig fastJsonConfig() {
        //添加fastJson的配置信息
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setReaderFeatures(JSONReader.Feature.FieldBased, JSONReader.Feature.SupportArrayToBean);
        fastJsonConfig.setWriterFeatures(JSONWriter.Feature.WriteMapNullValue, JSONWriter.Feature.PrettyFormat,
                JSONWriter.Feature.WriteNullStringAsEmpty, JSONWriter.Feature.WriteNullListAsEmpty);
        //全局时间配置
        fastJsonConfig.setDateFormat(DatePattern.NORM_DATETIME_PATTERN);
        fastJsonConfig.setCharset(CharsetUtil.CHARSET_UTF_8);
        PropertyFilter propertyFilter = (source, name, value) -> !"deleteFlag".equals(name);
        fastJsonConfig.setWriterFilters(propertyFilter);
        return fastJsonConfig;
    }

    /**
     * fastJson2 HTTP 转换器
     * @author Liuxu
     * @date 2024/1/23 11:53
     * @return com.alibaba.fastjson2.support.spring.http.converter.FastJsonHttpMessageConverter
     */
    private FastJsonHttpMessageConverter fastJsonHttpMessageConverter() {
        //需要定义一个convert转换消息的对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        //处理中文乱码问题
        List<MediaType> fastMediaTypes = new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON);
        fastConverter.setSupportedMediaTypes(fastMediaTypes);
        fastConverter.setFastJsonConfig(fastJsonConfig());
        return fastConverter;
    }
}
