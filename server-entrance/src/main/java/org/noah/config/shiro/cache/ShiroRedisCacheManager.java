package org.noah.config.shiro.cache;

import lombok.RequiredArgsConstructor;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.noah.utils.RedisUtil;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@RequiredArgsConstructor
public class ShiroRedisCacheManager implements CacheManager {

    private final RedisUtil redisUtil;
    //使用线程安全的map
    @SuppressWarnings("rawtypes")
    private final ConcurrentMap<String, Cache> caches = new ConcurrentHashMap<>();

    /**
     *
     * @author Liuxu
     * @date 2024/1/23 11:51
     * @param name key
     * @return org.apache.shiro.cache.Cache<K,V>
     */
    @Override
    public <K, V> Cache<K, V> getCache(String name) throws CacheException {
        return caches.computeIfAbsent(name, k -> new ShiroCache<>(redisUtil.getRedisTemplate()));
    }
}
