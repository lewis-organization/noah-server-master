package org.noah.config.interceptor;

import com.alibaba.fastjson2.JSON;
import enumc.RedisCacheKeyEnum;
import enumc.SystemErrorCodeEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.noah.config.json.AjaxResult;
import org.noah.config.referer.RefererConfig;
import org.noah.utils.RedisUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.util.List;
import java.util.function.Predicate;

/**
 * 接口控制拦截器
 *
 * @author liuxu
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class RefererInterceptor implements HandlerInterceptor {

    @Getter
    private final RefererConfig refererConfig;
    private final RedisUtil redisUtil;

    /**
     * 请求过滤
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (refererConfig.isEnable()) {
            String referer = request.getHeader("referer");
            String host = request.getServerName();
            if (StringUtils.isBlank(referer)) {
                // 状态置为404
                this.out(response, AjaxResult.fail(SystemErrorCodeEnum.NOTFOUNT.getCode(), SystemErrorCodeEnum.NOTFOUNT.getMsg()));
                return false;
            }
            java.net.URL url;
            try {
                url = new java.net.URL(referer);
            } catch (MalformedURLException e) {
                // URL解析异常，也置为404
                this.out(response, AjaxResult.fail(SystemErrorCodeEnum.NOTFOUNT.getCode(), SystemErrorCodeEnum.NOTFOUNT.getMsg()));
                return false;
            }
            // 首先判断请求域名和referer域名是否相同
            if (!host.equals(url.getHost())) {
                // 如果不等，判断是否在白名单中
                List<Object> refererDomainList = redisUtil.lGet(RedisCacheKeyEnum.REFERER_WHITE_LIST.getKey(), 0, -1);
                if (refererDomainList.stream()
                        .anyMatch(Predicate.isEqual(url.getHost()))) {
                    return true;
                }
                this.out(response, AjaxResult.fail(SystemErrorCodeEnum.NOTFOUNT.getCode(), SystemErrorCodeEnum.NOTFOUNT.getMsg()));
                return false;
            }
        }
        return true;
    }

    private void out(HttpServletResponse response, Object info) {
        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json;charset=utf-8");
            PrintWriter out = response.getWriter();
            out.println(JSON.toJSONString(info));
            out.flush();
            out.close();
        } catch (Exception e) {
            System.err.println("RefererInterceptor.class 输出JSON异常，可以忽略。");
        }
    }

}
