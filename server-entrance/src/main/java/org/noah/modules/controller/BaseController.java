package org.noah.modules.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.net.URLEncodeUtil;
import com.alibaba.excel.EasyExcel;
import lombok.extern.slf4j.Slf4j;
import org.noah.config.excel.AutoHeadColumnWidthStyleStrategy;
import org.noah.config.excel.ExcelObjectDto;
import org.noah.config.excel.HeadWriteHandle;
import org.noah.config.exception.BusinessException;
import org.noah.config.upload.MultipartConfig;
import org.noah.modules.sys.attach.entity.Attach;
import org.springframework.beans.factory.annotation.Autowired;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
public class BaseController {

    @Autowired
    protected HttpServletRequest request;

    protected void renderExcel(HttpServletResponse response, String fileName,
                               Class<? extends ExcelObjectDto> clas,
                               List<? extends ExcelObjectDto> resultList) {
        String encodeFileName = URLEncodeUtil.encode(fileName, StandardCharsets.UTF_8);
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        //设置响应头
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + encodeFileName + ".xlsx");
        response.setHeader("Mime", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        try {
            EasyExcel.write(response.getOutputStream(), clas)
                    .registerWriteHandler(new HeadWriteHandle())
                    .registerWriteHandler(new AutoHeadColumnWidthStyleStrategy())
                    .sheet(fileName)
                    .doWrite(resultList);
        } catch (IOException e) {
            throw new BusinessException();
        }
    }

    protected void renderImage(HttpServletResponse response, MultipartConfig multipartConfig, Attach attach, String route) {
        BufferedImage bi;
        ServletOutputStream out = null;

        response.reset();
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");

        File file;
        String suffix;

        if (multipartConfig.compressSuffix(attach.getServerName())) {
            //图片地址
            file = FileUtil.file(multipartConfig.getPath() + "/" + route + "/" + attach.getServerName());
            suffix = attach.getSuffix();
            response.setContentType("image/" + suffix);
        } else {
            ClassPathResource classPathResource = new ClassPathResource("/static/null.png");
            file = classPathResource.getFile();
            suffix = "png";
            response.setContentType("image/png");
        }
        try {
            out = response.getOutputStream();
            bi = ImageIO.read(file);
            ImageIO.write(bi, suffix, out);
        } catch (IOException e) {
            ClassPathResource classPathResource = new ClassPathResource("/static/fail.png");
            file = classPathResource.getFile();
            suffix = "png";
            response.setContentType("image/png");
            try {
                out = response.getOutputStream();
                bi = ImageIO.read(file);
                ImageIO.write(bi, suffix, out);
            } catch (IOException ex) {
                log.error("图片加载失败, {0}", ex);
            }
        } finally {
            IoUtil.flush(out);
            IoUtil.close(out);
        }
    }
}
