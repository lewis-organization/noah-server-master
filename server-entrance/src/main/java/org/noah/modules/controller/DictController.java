package org.noah.modules.controller;

import enumc.*;
import org.noah.config.json.AjaxResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统字典前端控制器
 */
@RestController
@RequestMapping(value = "/dict")
public class DictController {

    @GetMapping(value = "/getResourcesType")
    public AjaxResult getResourcesType() {
        return AjaxResult.success(ResourcesTypeEnum.toKv());
    }

    @GetMapping(value = "/getRoleType")
    public AjaxResult getRoleType() {
        return AjaxResult.success(RoleTypeEnum.toKv());
    }

    @GetMapping(value = "/getUserType")
    public AjaxResult getUserType() {
        return AjaxResult.success(UserTypeEnum.toKv());
    }

    @GetMapping(value = "/getTrueFalseType")
    public AjaxResult getTrueFalseType() {
        return AjaxResult.success(TrueOrFalseEnum.toKv());
    }

    @GetMapping(value = "/getTaskState")
    public AjaxResult getTaskState() {
        return AjaxResult.success(TaskStateEnum.toKv());
    }

    @GetMapping(value = "/getWebMessageType")
    public AjaxResult getWebMessageType() {
        return AjaxResult.success(WebMessageTypeEnum.toKv());
    }

    @GetMapping(value = "/getApproveStatusType")
    public AjaxResult getApproveStatusType() {
        return AjaxResult.success(ApproveStatusEnum.toKv());
    }

    @GetMapping(value = "/getApproveType")
    public AjaxResult getApproveType() {
        return AjaxResult.success(ApproveTypeEnum.toKv());
    }

    @GetMapping(value = "/getApproveBusinessType")
    public AjaxResult getApproveBusinessType() {
        return AjaxResult.success(ApproveBusinessTypeEnum.toKv());
    }
}