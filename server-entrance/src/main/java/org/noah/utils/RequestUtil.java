package org.noah.utils;

import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author liuxu
 */
public class RequestUtil {

    public static boolean isAjaxRequest(HttpServletRequest request) {
        return !StringUtils.isBlank(request.getHeader("x-requested-with"))
                && request.getHeader("x-requested-with").equals("XMLHttpRequest");
    }

    public static String getIpAddress(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.isEmpty() || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (ip.equals("0:0:0:0:0:0:0:1")) {
            ip = "127.0.0.1";
        }
        return ip;
    }

    /**
     * 获取浏览器信息
     * @author Liuxu
     * @date 2024/1/23 11:55
     * @param request request
     * @return java.util.Map<java.lang.String,java.lang.String>
     */
    public static Map<String, String> getClientInfo(HttpServletRequest request) {
        Map<String, String> resultMap = new HashMap<>();
        UserAgentStringParser parser = UADetectorServiceFactory.getResourceModuleParser();
        String ua = request.getHeader("User-Agent");
        ReadableUserAgent agent = parser.parse(ua);
        resultMap.put("browserName", agent.getName());
        resultMap.put("browserVersion", agent.getVersionNumber().toVersionString());
        resultMap.put("osName", agent.getOperatingSystem().getName());
        resultMap.put("osVersion", agent.getOperatingSystem().getVersionNumber().toVersionString());
        return resultMap;
    }

}
