package org.noah.runner;

import enumc.RedisCacheKeyEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.noah.config.referer.RefererConfig;
import org.noah.utils.RedisUtil;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 项目启动后执行的逻辑
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class ApplicationRunnerImpl implements ApplicationRunner {

    private final RedisUtil redisUtil;
    private final RefererConfig refererConfig;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //缓存白名单信息
        if (refererConfig.isEnable()) {
            if (redisUtil.hasKey(RedisCacheKeyEnum.REFERER_WHITE_LIST.getKey())) {
                redisUtil.delete(RedisCacheKeyEnum.REFERER_WHITE_LIST.getKey());
            }
            refererConfig.getRefererDomain()
                    .forEach(s -> {
                        if (!redisUtil.lSet(RedisCacheKeyEnum.REFERER_WHITE_LIST.getKey(), s, -1L)) {
                            log.warn("缓存引用配置地址失败...");
                        }
                    });
        }
    }
}
